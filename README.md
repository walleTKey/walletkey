# walleTKey

⚠⚡Do not use until you understand the security mechanisms. (See warning-section below)⚡⚠

__status__ in development, partially functional, initial focus on over-all feasibility (as opposed to wallet-functions)

Note: spin-off project ['nostrkey'][nostrkey] enables early experimentation: p256k1-conversion, Schnorr-signatures, session-handling and secure communication, use of Nostrkey/walleTKey under practical circumstances, interaction with (graphical) user-interface, user-/usage-quirks.

## Introduction

An application and command-line interface for a TKey-based wallet.

The _walleTKey_ is a proof-of-concept in that it does not provide all the benefits of the true _hardware wallets_ that are available. Limitations of _walleTKey_ are defined by the _Tillitis TKey_-device in terms of capacity and capabilities, as well as its inherent vulnerabilities. This proof-of-concept builds upon the possibilities of minimizing/eliminating the risk of corrupted/malicious firmware and therefore ability to preserve trusted operation (within limits) on an untrustworthy system.

Utilities, tests and performance evaluations for TKey are shared between the `tkey.{c,h}` C implementation and the [`rusTkey`](<https://crates.io/crates/rustkey> "rusTkey — rust library for tillitis TKey application development") [repository](<https://codeberg.org/rustkey/rustkey/> "rusTkey (\"rusty key\") -- a rust library for tillitis TKey application development").

## Features

See [main design document][design-main-document] for details about the design, including the [threat-model].

> ☐ to-do, ⋯ in-progress, ⚡(external) issues, ☑ done, ☒ cancelled (✔✘)

__Functionality__:

_walleTKey_:
- firmware:
    - `FIXME ...`
- `walletctl`: (for PoC client)
    - `FIXME mostly undeveloped, operations tested with test-client`
- `verifyfw`: (test firmware essentials)
    - ☑ Import/export wallet from hardcoded, embedded test-keypair.
    - ☑ Generate/export wallet in firmware.
    - ☑ Import/export generated wallet.
    - ☐ Flag for verifying identity during testing
    - ☐ Include application-info, firmware-checksum, identity-hash in report.
- Bitcoin-logic:
    - ☑ Schnorr-signatures (built on top of p256-m)
    - ⋯ Transaction-handling
    - ⋯ Bitcoin wallet-handling (`FIXME t.b.d., such as xpub, hierarchical wallets, etc.`)

_dependencies_:
  - ☑ NIST SEC p256k1 curve
  - ☑ SHA-256
  - ☑ RIPEMD-160

__Security__:

- Identity key-pair in firmware.
  - ☑ Identity key-pair for signing AKE challenges
  - ☐ Include context in uses of identity key-pair
- Simple, asymmetric AKE:
  - ☑ Protocol designed, documented.
  - ☑ Proof with passive attacker
  - ⚡Proof with active attacker
  - ☑ Implemented in firmware/`walletctl`/`verifyfw`
- Secure session establishment:
  - ☑ Simple, asymmetric AKE.
  - ☑ Identity verification.
  - ☑ Confidential communication.
  - ☑ Session control.
- _Sealed wallet_ format.
  - ⚡Proof for storage format.
  - ☑ Seal/load wallet in firmware.
  - ☑ Export sealed wallet data.
- Managing sensitive cryptographic material:
  - ⋯ clean up after use.
  - ⋯ compute on-the-fly. (Do not keep in memory)
- Security-mitigations:
  - ☑ Static allocation only.
  - ☑ Native/embedded in TKey.
  - ☑ CPU execution monitor.
  - ☑ Compiler hardening options. (fortifications, stack-protector, etc.)
  - ⋯ Quantum-computing risk mitigation (where possible):
    - ☐ Store hash of identity with _sealed wallet_.
    - `FIXME ...`

__Developmental__:

- ☑ `p256m.c`, conversion to p256k1 curve.
- ☑ `schnorr.h`, additions to `p256m.c`: implementation of Schnorr-signatures.
- ☑ `session.{c,h}`: implementation of simple, asymmetric AKE and secure session management (with dependency on _Monocypher_).
- ☑ `tkey.{c,h}`: utilities for TKey development libraries.

## Warning: security-mechanisms

The security-mechanisms that make it impossible to change/corrupt firmware without losing access to the wallet, also make it impossible to fix buggy firmware for said wallet. Until you are certain that a firmware works correctly on _at least_ a basic level, it is possible to lock yourself out of the sealed wallet created by buggy firmware.

See the [design][design-main-document] for details regarding functionality and security.

## Notes

- `base64 -w 0 < binary-psbt | xargs bitcoin-cli decodepsbt`
- `(echo -n 'signtx "'; base64 -w 0 < psbt.output; echo -n '"') | ./wallet-signer --stdin`

## References

- [Dark Skippy][dark-skippy-attack], an attack that relies on a malicious signer. Attacks like this can be prevented with a community of people that verify walleTKey code-base and provided binaries.


[design-main-document]: <design/walletkey.md> "Main design document (walletkey.md)"
[threat-model]: <design/threatmodel.md> "Threat-model for walleTKey"
[nostrkey]: <https://codeberg.org/walleTKey/nostrkey> "nostrkey — An open-source TKey program that operates as hardware-token for nostr-profiles, both embedded and sealed."
[dark-skippy-attack]: <https://darkskippy.com/> ""

