module codeberg.org/walletkey/walletkey

go 1.22

require (
	github.com/btcsuite/btcd v0.24.0
	github.com/btcsuite/btcd/btcutil/psbt v1.1.9
	github.com/cobratbq/goutils v0.0.0-20250103044321-89d9e6b0a9f0
	github.com/mit-dci/lit v0.0.0-20221102210550-8c3d3b49f2ce
	github.com/tillitis/tkeyclient v1.0.0
	golang.org/x/crypto v0.24.0
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.1.3 // indirect
	github.com/btcsuite/btcd/btcutil v1.1.5 // indirect
	github.com/btcsuite/btcd/chaincfg/chainhash v1.1.0 // indirect
	github.com/btcsuite/btclog v0.0.0-20170628155309-84c8d2346e9f // indirect
	github.com/ccoveille/go-safecast v1.1.0 // indirect
	github.com/creack/goselect v0.1.2 // indirect
	github.com/decred/dcrd/crypto/blake256 v1.0.0 // indirect
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.0.1 // indirect
	go.bug.st/serial v1.6.2 // indirect
	golang.org/x/sys v0.21.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace (
	github.com/cobratbq/goutils => ../goutils
	github.com/tillitis/tkeyclient => ./refs/tkeyclient
)
