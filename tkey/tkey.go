package tkey

import (
	"io"

	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	strconv_ "github.com/cobratbq/goutils/std/strconv"
	"github.com/tillitis/tkeyclient"
	"golang.org/x/crypto/blake2s"
)

const TKEY_FIRMWARE_NAME0 string = "tk1 "
const TKEY_FIRMWARE_NAME1 string = "mkdf"

var ErrNoDevicesFound = errors.NewStringError("no devices found")
var ErrUnknownDevice = errors.NewStringError("unknown firmware identifier")
var ErrLoadingFailed = errors.NewStringError("failed to load program-binary")
var ErrNotInFirmware = errors.NewStringError("device is not in firmware")

// ConnectTKey connects to a TKey device, loads an application/firmware and injects a user-supplied secret
// as part of the loading process.
// `appPath` is the path to a TKey "firmware" binary
// `secret` is an arbitrary-length user-supplied secret value
//
// This returns the tkey-instance that is connected to a TKey. If TKey is in firmware-mode, specified program-
// binary is loaded; if not in firmware-mode, the connection is returned as-is including `ErrNotInFirmware` to
// indicate an uncertain, but possibly correct, state. In that case, it is up to the caller to decide whether
// to.
//
// In case a valid tkey-instance is returned, caller is responsible for closing the connection, even if an
// error is also returned.
func ConnectTKey(appbinary []byte, secret []byte) (*tkeyclient.TillitisKey, error) {
	var err error
	var ports []tkeyclient.SerialPort
	if ports, err = tkeyclient.GetSerialPorts(); err != nil {
		return nil, errors.Context(err, "failed to acquire available ports")
	}
	if len(ports) == 0 {
		return nil, ErrNoDevicesFound
	}
	log.Debugln(ports)
	// TODO silence logging once everything works reliably and predictably
	//tkeyclient.SilenceLogging()
	tkey := tkeyclient.New()
	if err = tkey.Connect(ports[0].DevPath); err != nil {
		return nil, errors.Context(err, "failed to connect to device")
	}
	var nv *tkeyclient.NameVersion
	if nv, err = tkey.GetNameVersion(); err != nil {
		return tkey, ErrNotInFirmware
	}
	log.Tracef("Firmware name+version: %v%v:%v\n", string(nv.Name0), string(nv.Name1), nv.Version)
	if nv.Name0 != TKEY_FIRMWARE_NAME0 || nv.Name1 != TKEY_FIRMWARE_NAME1 {
		io_.CloseLogged(tkey, "Failed to gracefully close connection with TKey")
		log.Traceln("Expected TKey device with identifiers 'tk1 ' and 'mkdf'.")
		return nil, errors.Context(ErrUnknownDevice, "firmware-ID: ["+nv.Name0+"],["+nv.Name1+"]: "+strconv_.FormatUintDecimal(nv.Version))
	}
	log.DebugReport(nv.Version == 5, "unknown or untested TKey version: %v", nv.Version)
	log.Traceln("Loading wallet firmware…")
	if err = tkey.LoadApp(appbinary, secret); err != nil {
		io_.CloseLogged(tkey, "Failed to gracefully close connection with TKey")
		return nil, errors.Aggregate(ErrLoadingFailed, "firmware-binary", err)
	}
	return tkey, nil
}

// ConnectWalleTKey connects to a TKey, loads the specified program-binary if needed, then checks the app-
// info of the loaded program to verify that the expected WalleTKey identifier (`tkbcwllt`) is found.
// Only in case of successful connection, loading and verification will tkey-instance be returned as a result.
//
// Note that verifying the "app-info" only helps to determine a possible API to assume in communication, as
// any program-binary can return these values. Identity-checking and authentication must happen separately,
// after the connection is established.
func ConnectTKeyProgram(appbinary []byte, secret []byte) (*tkeyclient.TillitisKey, AppInfo, error) {
	var err error
	var tkey *tkeyclient.TillitisKey
	tkey, err = ConnectTKey(appbinary, secret)
	if err != nil && !errors.Is(err, ErrNotInFirmware) {
		if tkey != nil {
			io_.CloseLogged(tkey, "Failed to gracefully close connection with TKey")
		}
		return nil, AppInfo{}, err
	}
	if errors.Is(err, ErrNotInFirmware) {
		log.Traceln("Device is not in firmware-mode.")
		// FIXME try resetting secure session and re-establishing connection with expected identity.
	}
	log.Traceln("TKey connected.")
	if err = tkey.Write([]byte{GenerateHeader(0, CmdAppInfo.Endpoint(), CmdAppInfo.CmdLen()), CmdAppInfo.Code()}); err != nil {
		io_.CloseLogged(tkey, "Failed to gracefully close the connection.")
		return nil, AppInfo{}, errors.Aggregate(ErrLoadingFailed, "failed to send walleTKey app-info query", err)
	}
	rawdata, hdr, err := tkey.ReadFrame(RspAppInfo, 0)
	log.Traceln("Query app-info, header:", hdr, "raw data:", rawdata)
	if err != nil || hdr.ResponseNotOK {
		io_.CloseLogged(tkey, "Failed to gracefully close the connection.")
		return nil, AppInfo{}, errors.Aggregate(ErrLoadingFailed, "failed to query walleTKey app-info", err)
	}
	info := ParseAppInfo(rawdata[2 : hdr.CmdLen.Bytelen()+1])
	log.Traceln("App-info:", string(info.Name[:]), info.Version, "identity:", info.Identity[:], "firmware-checksum:", info.Checksum)
	return tkey, info, nil
}

func blake2s256(key []byte, message []byte) [32]byte {
	hash := builtin.Expect(blake2s.New256(key))
	hash.Write(message)
	return [32]byte(hash.Sum(nil))
}

func Blake2s256(key []byte, message []byte) [32]byte {
	return blake2s256(key, message)
}

func blake2s128(key []byte, message []byte) [16]byte {
	hash := builtin.Expect(blake2s.New128(key))
	hash.Write(message)
	return [16]byte(hash.Sum(nil))
}

func WriteCommand(tkey *tkeyclient.TillitisKey, id uint8, cmd tkeyclient.Cmd, data []byte) error {
	var payload []byte
	switch cmd.CmdLen() {
	case tkeyclient.CmdLen1:
		return tkey.Write([]byte{GenerateHeader(id, tkeyclient.DestApp, tkeyclient.CmdLen1), cmd.Code()})
	case tkeyclient.CmdLen4:
		payload = make([]byte, 5)
	case tkeyclient.CmdLen32:
		payload = make([]byte, 33)
	case tkeyclient.CmdLen128:
		payload = make([]byte, 129)
	default:
		panic("BUG: this should not happen.")
	}
	payload[0] = GenerateHeader(id, tkeyclient.DestApp, cmd.CmdLen())
	payload[1] = cmd.Code()
	copy(payload[2:], data)
	return tkey.Write(payload)
}

// GenerateHeader generates the single-byte frame header as specified by the TKey protocol.
// `status` is an unused field for requests. It is only used to indicate (un)successful responses.
func GenerateHeader(id uint8, endpoint tkeyclient.Endpoint, cmdlen tkeyclient.CmdLen) byte {
	var b byte
	b |= byte(id&0b0000_0011) << 5
	b |= byte(endpoint&0b0000_0011) << 3
	b |= byte(cmdlen & 0b0000_0011)
	return b
}

func ToLength(data []byte) (uint, error) {
	if len(data) < 2 {
		return 0, io.ErrShortBuffer
	}
	return uint(data[0]) | uint(data[1])<<8, nil
}

type AppCmd struct {
	name string
	code byte
	len  tkeyclient.CmdLen
}

func NewCommand(name string, code byte, len tkeyclient.CmdLen) AppCmd {
	return AppCmd{name: name, code: code, len: len}
}

// TODO implement functions on `*AppCmd` instead?
func (a AppCmd) Endpoint() tkeyclient.Endpoint { return tkeyclient.DestApp }
func (a AppCmd) CmdLen() tkeyclient.CmdLen     { return a.len }
func (a AppCmd) Code() byte                    { return a.code }
func (a AppCmd) String() string                { return a.name }
