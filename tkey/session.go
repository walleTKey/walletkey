package tkey

import (
	"crypto/ed25519"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/crypto/rand"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/math"
	"github.com/cobratbq/goutils/std/strconv"
	"github.com/tillitis/tkeyclient"
	"golang.org/x/crypto/chacha20"
	"golang.org/x/crypto/curve25519"
)

var CmdSecureSession AppCmd = NewCommand("SecureSession", 0x02, tkeyclient.CmdLen128)
var RspSecureSession AppCmd = NewCommand("RspSecureSession", 0x02, tkeyclient.CmdLen128)
var RspReinitSession AppCmd = NewCommand("RspReinitSession", 0x02, tkeyclient.CmdLen1)

type Identity [32]byte

const FLAG_CONTINUATION = 0x80
const MASK_COMMAND_BITS = 0x7f

// Session is the secure session.
type Session struct {
	device      *tkeyclient.TillitisKey
	identity    Identity
	key         [32]byte
	clientnonce [12]byte
	devicenonce [12]byte
}

func NewSession(tkey *tkeyclient.TillitisKey) Session {
	return Session{
		device:      tkey,
		identity:    Identity{},
		key:         [32]byte{},
		clientnonce: [12]byte{},
		devicenonce: [12]byte{},
	}
}

func (s *Session) IsSecure() bool {
	return s.key != [32]byte{}
}

func (s *Session) FinishSecure() error {
	var err error
	var zerokey = [32]byte{}
	if err = s.Send(0, CmdSecureSession, zerokey[:]); err != nil {
		return err
	}
	var data []byte
	if data, err = s.Receive(0, RspReinitSession); err != nil {
		return err
	}
	assert.Equal(1, len(data))
	assert.Equal(RspReinitSession.Code(), data[0])
	clear(s.key[:])
	clear(s.clientnonce[:])
	clear(s.devicenonce[:])
	return nil
}

var ErrVerificationFailed = errors.NewStringError("identity failed verification.")

func (s *Session) Secure(identity Identity) error {
	var dheSecret [32]byte
	rand.MustReadBytes(dheSecret[:])
	var dhePublic [32]byte
	curve25519.ScalarBaseMult(&dhePublic, &dheSecret)
	log.DebugReport(s.key == [32]byte{}, "session is already secured.")
	if err := WriteCommand(s.device, 0, CmdSecureSession, dhePublic[:]); err != nil {
		return errors.Context(err, "writing command to initiate secure session")
	}
	data, hdr, err := s.device.ReadFrame(RspSecureSession, 0)
	log.Traceln("Secure session, header:", hdr, "raw data:", data)
	if err != nil {
		return errors.Context(err, "failed to establish secure session")
	}
	theirs := data[2:34]
	sharedSecret, err := curve25519.X25519(dheSecret[:], theirs)
	if err != nil {
		return errors.Context(err, "failed to generate DHE shared secret")
	}
	s.key = blake2s256(sharedSecret, theirs)
	clear(s.devicenonce[:])
	clear(s.clientnonce[:])
	decrypted := s.crypt(data[34:110], ACTION_RECEIVE)
	if !ed25519.Verify(identity[:], dhePublic[:], decrypted[:64]) {
		// FIXME proper clearing after recent changes
		clear(s.key[:])
		log.Warnln("Proof-of-identity failed verification: either data got corrupted or device/firmware does not have the expected identity.")
		return ErrVerificationFailed
	}
	copy(s.clientnonce[:], decrypted[64:76])
	copy(s.devicenonce[:], s.clientnonce[:])
	s.devicenonce[len(s.devicenonce)-1] ^= 0x80
	// FIXME proper clearing after recent changes
	log.Traceln("Identity-verification succeeded.")
	return nil
}

func (s *Session) Close() error {
	clear(s.identity[:])
	clear(s.key[:])
	clear(s.clientnonce[:])
	clear(s.devicenonce[:])
	return s.device.Close()
}

type actionType uint8

const (
	ACTION_SEND actionType = iota
	ACTION_RECEIVE
)

func (s *Session) crypt(content []byte, action actionType) []byte {
	assert.NonZeroSlice(s.key[:])
	var nonce []byte
	switch action {
	case ACTION_SEND:
		nonce = s.clientnonce[:]
	case ACTION_RECEIVE:
		nonce = s.devicenonce[:]
	default:
		panic("BUG: invalid action-type specified")
	}
	log.Traceln("Crypt, action:", action, "nonce:", nonce)
	cipher := builtin.Expect(chacha20.NewUnauthenticatedCipher(s.key[:], nonce))
	result := make([]byte, len(content))
	cipher.XORKeyStream(result, content)
	nextNonce(nonce)
	return result
}

// Send sends a command with arbitrary-length data
//
// Note: this code is adapted to use the highest bit of the command-byte to indicate continuation of a
// request. This allows sending large payloads, by sending multiple frames with every subsequent frame (all
// but the first) having the continuation-bit set. In case of errors, the firmware can drop all frames with a
// continuation-bit and continue with the first normal command as subsequent request.
func (s *Session) SendFrame(id uint8, cmd tkeyclient.Cmd, data []byte, continuation bool) error {
	// TODO excessive buffering ... here, in s.write, in other layers ...
	var buffer [128]byte
	assert.Equal(0, cmd.Code()&FLAG_CONTINUATION)
	assert.AtMost(cmd.CmdLen().Bytelen()-1, len(data))
	buffer[0] = cmd.Code()
	if continuation {
		buffer[0] |= FLAG_CONTINUATION
	}
	copy(buffer[1:], data)
	payload := buffer[:cmd.CmdLen().Bytelen()]
	if s.IsSecure() {
		payload = s.crypt(payload, ACTION_SEND)
	}
	return s.write(id, cmd.CmdLen(), payload)
}

// Send sends a command with data of length at most one frame.
func (s *Session) Send(id uint8, cmd tkeyclient.Cmd, data []byte) error {
	return s.SendFrame(id, cmd, data, false)
}

// SendLarge sends a command with arbitrary-length data that is automatically split into separate frames, with
// subsequent frames having "continuation"-flag set.
func (s *Session) SendLarge(id uint8, cmd tkeyclient.Cmd, data []byte) error {
	resume := 0
	for resume < len(data) {
		partLength := math.Min(cmd.CmdLen().Bytelen()-1, len(data[resume:]))
		log.Traceln("Sending part, from", resume, "to", resume+partLength)
		if err := s.SendFrame(id, cmd, data[resume:resume+partLength], resume > 0); err != nil {
			return errors.Context(err, "Failure while sending large payload")
		}
		resume += partLength
	}
	return nil
}

// CountMany counts how many frames are sent when using SendMany.
func (s *Session) CountMany(data []byte) uint {
	return uint(len(data)/127 + math.Sign(len(data)%127))
}

// SendMany sends multiple messages consecutively. If an error occurs, the error is returned together with
// the number of frames sent up to the error. If no error occurred, the number of sent frames is returned.
func (s *Session) SendMany(id uint8, cmd tkeyclient.Cmd, data []byte) (uint, error) {
	var buffer [128]byte
	var count uint
	var sent int
	for sent < len(data) {
		clear(buffer[:])
		chunk := math.Min(len(data[sent:]), 127)
		buffer[0] = cmd.Code()
		copy(buffer[1:], data[sent:sent+chunk])
		payload := s.crypt(buffer[:], ACTION_SEND)
		if err := s.write(id, cmd.CmdLen(), payload); err != nil {
			return count, errors.Context(err, "failed to send frame "+strconv.FormatUint(uint64(count), 10))
		}
		sent += chunk
		count += 1
	}
	return count, nil
}

// write writes a command to the tkey. `data` must contain the command-byte at position 0 already.
func (s *Session) write(id uint8, cmdlen tkeyclient.CmdLen, data []byte) error {
	assert.Require(len(data) <= 128, "Data with size > 128 bytes are not supported.")
	var buffer [129]byte
	var request []byte
	switch cmdlen {
	case tkeyclient.CmdLen1:
		assert.Require(len(data) <= 1, "Provided data is too large for command-length.")
		request = buffer[:2]
	case tkeyclient.CmdLen4:
		assert.Require(len(data) <= 4, "Provided data is too large for command-length.")
		request = buffer[:5]
	case tkeyclient.CmdLen32:
		assert.Require(len(data) <= 32, "Provided data is too large for command-length.")
		request = buffer[:33]
	case tkeyclient.CmdLen128:
		assert.Require(len(data) <= 128, "Provided data is too large for command-length.")
		request = buffer[:]
	default:
		panic("BUG: this should not happen.")
	}
	// request is length 1 + payload. Length corresponds to any of CmdLen, possibly with unused bytes.
	request[0] = GenerateHeader(id, tkeyclient.DestApp, cmdlen)
	copy(request[1:], data)
	return s.device.Write(request)
}

var ErrUnexpectedCode = errors.NewStringError("unexpected command code")

// Receive receives a data frame from TKey.
// Result is `<cmdbyte> || <payload-of-cmdlen-category`
func (s *Session) Receive(id uint8, rsp tkeyclient.Cmd) ([]byte, error) {
	raw, hdr, err := s.device.ReadFrame(rsp, int(id))
	// Note: we need to make an exception: if raw-data is provided, it means we failed on the command-byte,
	// which makes sense because it is encrypted.
	if err != nil && raw == nil {
		return nil, errors.Context(err, "failed to read frame from TKey")
	}
	assert.Any(len(raw[1:]), 1, 4, 32, 128)
	data := raw[1:]
	log.Traceln("Received length:", len(data))
	// TODO are we still okay with unencrypted reply-bad responses?
	if hdr.ResponseNotOK {
		return data, tkeyclient.ErrResponseStatusNotOK
	}
	if s.key != [32]byte{} {
		log.Traceln("Secure session; decrypting data…")
		data = s.crypt(data, ACTION_RECEIVE)
	}
	if data[0] != rsp.Code() {
		return data, errors.Context(ErrUnexpectedCode, "response-code: "+strconv.FormatUintDecimal(data[0]))
	}
	return data, nil
}

func nextNonce(nonce []byte) {
	for i := 0; i < len(nonce); i++ {
		nonce[i]++
		if nonce[i] != 0 {
			break
		}
	}
}
