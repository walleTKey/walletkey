package tkey

import (
	"encoding/binary"
	"encoding/hex"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/tillitis/tkeyclient"
)

var CmdAppInfo AppCmd = NewCommand("AppInfo", 0x01, tkeyclient.CmdLen1)
var RspAppInfo AppCmd = NewCommand("RspAppInfo", 0x01, tkeyclient.CmdLen128)

type AppInfo struct {
	Name     [8]byte
	Version  uint32
	Checksum [32]byte
	Identity Identity
}

func ParseAppInfo(data []byte) AppInfo {
	// FIXME we should check length of data[] and process information accordingly, this allows parsing of the original smaller payload effortlessly and assume more extensive payload if space allows for it.
	assert.Require(len(data) >= 76, "Data buffer too small to contain AppInfo response.")
	var info AppInfo
	info.Name = [8]byte(data[:8])
	info.Version = binary.LittleEndian.Uint32(data[8:12])
	info.Identity = Identity(data[12:44])
	info.Checksum = [32]byte(data[44:76])
	return info
}

func (s *Session) QueryAppInfo() (AppInfo, error) {
	var err error
	if err = s.Send(0, CmdAppInfo, nil); err != nil {
		return AppInfo{}, errors.Context(err, "failed to send AppInfo command")
	}
	var data []byte
	data, err = s.Receive(0, RspAppInfo)
	if err != nil {
		return AppInfo{}, errors.Context(err, "failed to receive AppInfo response")
	}
	info := ParseAppInfo(data[1:])
	log.Debugln("Checksum:", hex.EncodeToString(info.Checksum[:]))
	log.Debugln("Identity:", hex.EncodeToString(info.Identity[:]))
	return info, nil
}
