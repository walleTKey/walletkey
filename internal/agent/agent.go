package agent

import (
	"net"
	"os"
	"path"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/prefixed-compact"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/errors"
)

// TODO needs proper error handling (uses assert/builtin.Expect)
func Listen() (*net.UnixListener, error) {
	runtimePath := os.Getenv("XDG_RUNTIME_DIR")
	if runtimePath == "" {
		return nil, errors.Context(errors.ErrInternalState, "Environment variable XDG_RUNTIME_DIR not set.")
	}
	sockaddr := builtin.Expect(net.ResolveUnixAddr("unix", path.Join(runtimePath, "walletkey.sock")))
	sock, err := net.ListenUnix("unix", sockaddr)
	if err != nil {
		return nil, errors.Context(err, "Failed to open agent socket.")
	}
	sock.SetUnlinkOnClose(true)
	return sock, nil
}

type Agent struct {
	conn *net.UnixConn
}

func Connect() (Agent, error) {
	runtimePath := os.Getenv("XDG_RUNTIME_DIR")
	if runtimePath == "" {
		return Agent{}, errors.Context(errors.ErrInternalState, "Environment variable XDG_RUNTIME_DIR not set.")
	}
	sockaddr := builtin.Expect(net.ResolveUnixAddr("unix", path.Join(runtimePath, "walletkey.sock")))
	sock, err := net.DialUnix("unix", nil, sockaddr)
	if err != nil {
		return Agent{}, errors.Context(err, "Failure to connect to walletkey agent socket.")
	}
	return Agent{conn: sock}, nil
}

func (a *Agent) Close() error {
	return a.conn.Close()
}

func (a *Agent) Query(query string, input prefixed.Value) (prefixed.Value, error) {
	assert.Required(input, "Input is required, even if an empty value.")
	request := prefixed.KeyValue{K: query, V: input}
	// FIXME need to check written length?
	if _, err := request.WriteTo(a.conn); err != nil {
		return nil, errors.Context(err, "Failed to send request to agent:")
	}
	response, err := prefixed.ReadValue(a.conn)
	if err != nil {
		return nil, errors.Context(err, "Failed to receive response from agent:")
	}
	return response, nil
}

func (a *Agent) Identity() ([32]byte, error) {
	response, err := a.Query("identity", prefixed.Bytes{})
	if err != nil {
		return [32]byte{}, errors.Context(err, "Failed to query device identity:")
	}
	return [32]byte(response.(prefixed.Bytes)), nil
}

func (a *Agent) Wallets() (map[btc.FingerprintType]string, error) {
	response, err := a.Query("wallets", prefixed.Bytes{})
	if err != nil {
		return nil, errors.Context(err, "Failed to query known wallets:")
	}
	return maps.Transform(response.(prefixed.MapValue),
		func(key string, path prefixed.Value) (btc.FingerprintType, string) {
			var fp btc.FingerprintType
			assert.Equal(4, len(key))
			copy(fp[:], []byte(key))
			return fp, string(path.(prefixed.Bytes))
		}), nil
}

func (a *Agent) Wallet(fpr btc.FingerprintType) (string, error) {
	response, err := a.Query("wallet", prefixed.Bytes(fpr[:]))
	if err != nil {
		return "", errors.Context(err, "Failed to query wallet by fingerprint:")
	}
	return string(response.(prefixed.Bytes)), nil
}
