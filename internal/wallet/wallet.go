package wallet

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"math"
	"os"
	"slices"
	"strings"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/btc/descriptor"
	"codeberg.org/walletkey/walletkey/internal/btc/txn"
	"codeberg.org/walletkey/walletkey/internal/btc/wallet/hierarchical"
	"codeberg.org/walletkey/walletkey/internal/curve"
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/btcsuite/btcd/btcutil/psbt"
	"github.com/btcsuite/btcd/wire"
	"github.com/cobratbq/goutils/assert"
	be "github.com/cobratbq/goutils/codec/bytes/bigendian"
	le "github.com/cobratbq/goutils/codec/bytes/littleendian"
	"github.com/cobratbq/goutils/codec/string/base58"
	"github.com/cobratbq/goutils/std/builtin"
	slices_ "github.com/cobratbq/goutils/std/builtin/slices"
	asn1_ "github.com/cobratbq/goutils/std/encoding/asn1"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
	strconv_ "github.com/cobratbq/goutils/std/strconv"
	"github.com/mit-dci/lit/crypto/koblitz"
	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/blake2s"
)

func KDF(id tkey.Identity) [HARDENED_ID_LENGTH]byte {
	// FIXME choose/determine salt, time, memory, threads
	var salt = [32]byte{}
	const time = 1
	const memory = 64 * 1024
	const threads = 4
	return [64]byte(argon2.IDKey(id[:], salt[:], time, memory, threads, HARDENED_ID_LENGTH))
}

var ErrUnknownProgram = errors.NewStringError("unknown program identifier")

func WALLETKEY_APPID_NAME() [8]byte {
	return [...]byte{'t', 'k', 'b', 'c', 'w', 'l', 'l', 't'}
}

const WALLETKEY_APPID_VERSION = 1

// FIXME verify for all needed methods if (secure) session is established.
type TKeyWallet struct {
	testing     bool
	info        tkey.AppInfo
	Session     tkey.Session
	Fingerprint btc.FingerprintType
}

// TODO maybe leave out `info` return value, and copy from TKeyWallet.info field
func ConnectWalleTKey(appbinary []byte, secret []byte) (TKeyWallet, tkey.AppInfo, error) {
	log.Traceln("Connecting to walleTKey…")
	device, info, err := tkey.ConnectTKeyProgram(appbinary, secret)
	if err != nil {
		return TKeyWallet{}, tkey.AppInfo{}, errors.Context(err, "failed to connect to device")
	}
	if info.Name != WALLETKEY_APPID_NAME() {
		io_.CloseLogged(device, "Failed to gracefully close the connection.")
		return TKeyWallet{}, tkey.AppInfo{}, errors.Context(ErrUnknownProgram, "program identifier: ["+string(info.Name[:8])+"]: "+strconv_.FormatUintDecimal(info.Version))
	}
	log.Infoln("WalleTKey connected.")
	return TKeyWallet{info: info, Session: tkey.NewSession(device)}, info, nil
}

func (device *TKeyWallet) Close() error {
	log.Traceln("Closing wallet session…")
	if device.Session.IsSecure() {
		log.WarnOnError(device.Session.FinishSecure(), "Failed to gracefully finish secure session. Subsequent connections may fail to establish a secure session.")
		log.Traceln("Secure session finished.")
	}
	return device.Session.Close()
}

func (device *TKeyWallet) SetTesting(enabled bool) {
	device.testing = enabled
}

const sanityCheckMaxPaymentTypes = 10

type PaymentType uint8

const (
	// P2PK is obsolete and obsolete and should not be used.
	P2PK  PaymentType = 0
	P2PKH PaymentType = 1
	P2SH  PaymentType = 2
)

// TODO do proper error-handling, instead of panicking
func (device *TKeyWallet) WalletInfo() (hierarchical.XpubType, []PaymentType) {
	log.Traceln("Querying wallet info…")
	var err = device.Session.Send(0, CmdWalletInfo, []byte{})
	assert.Success(err, "Failed to query wallet-info")
	var data []byte
	data, err = device.Session.Receive(0, RspWalletInfo)
	assert.Success(err, "Failed to receive wallet-info")
	var info = hierarchical.XpubType{
		Prefix: hierarchical.DeterminePrefix(!device.testing),
		Depth:  0,
		Index:  0,
		Parent: [4]byte{},
		Chain:  [32]byte(bytes.Clone(data[34:66])),
		Pubkey: builtin.Expect(curve.ParsePoint(data[1:34])),
	}
	numPaymentTypes := assert.AtMost(sanityCheckMaxPaymentTypes, data[66])
	paymentTypes := make([]PaymentType, 0, numPaymentTypes)
	for i := uint8(0); i < numPaymentTypes; i++ {
		paymentTypes = builtin.Expect(slices_.Extend(paymentTypes, PaymentType(data[67+i])))
	}
	log.Traceln("WalletInfo, public-key:", hex.EncodeToString(info.Pubkey.SerializeCompressed()), ", fingerprint:", btc.DeriveFingerprint(info.Pubkey.SerializeCompressed()), ", payment-types:", paymentTypes)
	return info, paymentTypes
}

// TODO do proper error-handling, instead of panicking
func (device *TKeyWallet) GenerateWallet() SealedWallet {
	log.Traceln("Generating wallet…")
	hardened := KDF(device.info.Identity)
	var err = device.Session.Send(0, CmdGenerateSecret, hardened[:])
	assert.Success(err, "Failed to request generating new wallet")
	var data []byte
	data, err = device.Session.Receive(0, RspGenerateSecret)
	assert.Success(err, "Failed to receive sealed wallet")
	device.Fingerprint = btc.FingerprintType(bytes.Clone(data[1:5]))
	log.Traceln("Fingerprint of generated wallet:", hex.EncodeToString(device.Fingerprint[:]))
	var sealedwallet SealedWallet
	copy(sealedwallet[:], data[5:5+SEALED_WALLET_LENGTH])
	return sealedwallet
}

func (device *TKeyWallet) RestoreWallet(secret [32]byte) SealedWallet {
	log.Traceln("Restoring wallet…")
	hardened := KDF(device.info.Identity)
	var err = device.Session.Send(0, CmdRestoreSecret, slices.Concat(secret[:], hardened[:]))
	assert.Success(err, "Failed to request restoring a wallet secret")
	var data []byte
	data, err = device.Session.Receive(0, RspRestoreSecret)
	assert.Success(err, "Failed to receive sealed wallet")
	device.Fingerprint = btc.FingerprintType(bytes.Clone(data[1:5]))
	log.Traceln("Fingerprint of restored wallet:", hex.EncodeToString(device.Fingerprint[:]))
	var sealedwallet SealedWallet
	copy(sealedwallet[:], data[5:5+SEALED_WALLET_LENGTH])
	return sealedwallet
}

func (device *TKeyWallet) LoadWallet(wallet *SealedWallet) error {
	log.Traceln("Loading wallet…")
	hardened := KDF(device.info.Identity)
	if err := device.Session.SendLarge(0, CmdLoadWallet, slices.Concat(wallet[:], hardened[:])); err != nil {
		return errors.Context(err, "Failed to send request to load wallet")
	}
	if data, err := device.Session.Receive(0, RspLoadWallet); err == nil {
		// TODO don't actually need to clone, or do we?
		device.Fingerprint = btc.FingerprintType(bytes.Clone(data[1:5]))
	} else {
		return errors.Context(err, "Failed to load wallet")
	}
	return nil
}

// TODO do proper error-handling, instead of panicking
func (device *TKeyWallet) DeriveAddress(path []uint32) (btc.FingerprintType, hierarchical.XpubType) {
	log.Traceln("Deriving address…")
	var data = make([]byte, 1+len(path)*4)
	data[0] = uint8(len(path))
	var index uint32 = 0
	for i := range path {
		binary.LittleEndian.PutUint32(data[1+i*4:1+(i+1)*4], path[i])
	}
	var err = device.Session.Send(0, CmdDeriveAddress, data)
	assert.Success(err, "Failed to request wallet address derivation")
	data, err = device.Session.Receive(0, RspDeriveAddress)
	assert.Success(err, "Failed to derive address")
	var masterFp = btc.FingerprintType(bytes.Clone(data[1:5]))
	var pubkey *koblitz.PublicKey
	pubkey, err = curve.ParsePoint(data[5:38])
	assert.Success(err, "Failed to parse public key")
	var chain [32]byte
	copy(chain[:], data[38:70])
	assert.Equal(slices_.AllEqual(0, data[70:103]), len(path) == 0)
	var parent *koblitz.PublicKey
	if len(path) > 0 {
		parent, err = curve.ParsePoint(data[70:103])
		assert.Success(err, "Failed to parse parent public key.")
	}
	return masterFp, hierarchical.XpubType{
		Prefix: hierarchical.DeterminePrefix(!device.testing),
		Depth:  uint8(len(path)),
		Parent: btc.DeriveFingerprint(parent.SerializeCompressed()),
		Index:  index,
		Chain:  chain,
		Pubkey: pubkey,
	}
}

type DescriptorSummary struct {
	Receive  []string
	Internal []string
}

// FIXME Risks? Consider also having descriptors response signed s.t. malicious behavior cannot falsify descriptors without ability to authenticate.
func (device *TKeyWallet) Descriptors(account uint32) DescriptorSummary {
	_, paytypes := device.WalletInfo()
	log.Traceln("Payment types:", paytypes)
	var accountPath = hierarchical.GenerateAccountPath(!device.testing, account)
	var masterFp, address = device.DeriveAddress(accountPath)
	var xpub = address.Encode()
	// FIXME determine descriptors from (to be added) information in wallet-info (types: (P2PK), P2PKH, P2SH, etc.)
	var summary DescriptorSummary
	for _, pt := range paytypes {
		switch pt {
		case P2PK:
			panic("BUG: illegal payment-type. Obsolete and security-risk.")
		case P2PKH:
			summary.Receive = append(summary.Receive, "pkh(["+descriptor.EncodePath(&masterFp, accountPath)+"]"+
				string(base58.ChecksumEncode(xpub[:]))+"/"+strconv.FormatUintDecimal(hierarchical.CHAIN_PUBLIC)+"/*)")
			summary.Internal = append(summary.Internal, "pkh(["+descriptor.EncodePath(&masterFp, accountPath)+"]"+
				string(base58.ChecksumEncode(xpub[:]))+"/"+strconv.FormatUintDecimal(hierarchical.CHAIN_INTERNAL)+"/*)")
		case P2SH:
			summary.Receive = append(summary.Receive, "sh(["+descriptor.EncodePath(&masterFp, accountPath)+"]"+
				string(base58.ChecksumEncode(xpub[:]))+"/"+strconv.FormatUintDecimal(hierarchical.CHAIN_PUBLIC)+"/*)")
			summary.Internal = append(summary.Internal, "sh(["+descriptor.EncodePath(&masterFp, accountPath)+"]"+
				string(base58.ChecksumEncode(xpub[:]))+"/"+strconv.FormatUintDecimal(hierarchical.CHAIN_INTERNAL)+"/*)")
		}
	}
	return summary
}

func loadWalletFile(device *TKeyWallet, path string) error {
	if path == "" {
		return errors.ErrIllegal
	}
	if content, err := os.ReadFile(path); err != nil {
		return errors.Context(err, "Failed to read sealed-wallet file: "+path)
	} else if len(content) != SEALED_WALLET_LENGTH {
		return errors.Context(errors.ErrIllegal, "Wallet-file contains invalid sealed-wallet.")
	} else {
		wallet := SealedWallet(content)
		return device.LoadWallet(&wallet)
	}
}

func reprBip32Derivation(deriv *psbt.Bip32Derivation) string {
	fprbytes := le.FromUint32(deriv.MasterKeyFingerprint)
	fpr := hex.EncodeToString(fprbytes[:])
	pubkey := hex.EncodeToString(deriv.PubKey)
	path := slices_.Transform(deriv.Bip32Path, func(v uint32) string {
		encoded := be.FromUint32(v)
		return hex.EncodeToString(encoded[:])
	})
	return "BIP32-derivation, pubkey: " + pubkey + ", master: " + fpr + ", path: " + strings.Join(path, "/")
}

// maxDerivationPathLength indicates the max support path length for child key derivations.
// (See `DERIVATION_PATH_COUNT_MAX` in `firmware/main.c`.)
// TODO ensure that limits are in sync.
const maxDerivationPathLength = 20

func (device *TKeyWallet) SignPSBT(wallets map[btc.FingerprintType]string, packet *psbt.Packet) error {
	assert.Success(psbt.InputsReadyToSign(packet), "Packet must be ready to sign.")
	var buffer bytes.Buffer
	if err := packet.UnsignedTx.BtcEncode(&buffer, 1, wire.BaseEncoding); err != nil {
		return errors.Context(err, "Failed to serialize (incomplete) transaction for transfer to device")
	}
	var txencoded = bytes.Clone(buffer.Bytes())
	var checksum = blake2s.Sum256(txencoded)
	if len(txencoded) > math.MaxUint16 {
		// TODO take into account firmware max capacity?
		// Note: remember that the firmware has a transaction size limit that is around 10kB.
		return errors.Context(errors.ErrFailure, "Transaction is too large to process.")
	}
	buffer.Reset()
	builtin.Expect(buffer.Write(checksum[:]))
	enc16 := le.FromUint16(uint16(len(txencoded)))
	builtin.Expect(buffer.Write(enc16[:]))
	builtin.Expect(buffer.Write(txencoded))
	if err := device.Session.SendLarge(0, CmdLoadTransaction, buffer.Bytes()); err != nil {
		return errors.Context(err, "Failed to transmit transaction request")
	}
	if _, err := device.Session.Receive(0, RspLoadTransaction); err != nil {
		return errors.Context(err, "Failed to load transaction on device")
	}

	// TODO consider if we can gather all signing-inputs first and only require one touch-confirmation to acknowledge all necessary signing activity?
	var updater = builtin.Expect(psbt.NewUpdater(packet))
	for i := range packet.Inputs {
		log.Traceln("Index", i, ", partial sigs present:", len(packet.Inputs[i].PartialSigs))
		if len(packet.Inputs[i].PartialSigs) > 0 {
			log.Traceln("Index", i, ", (partial) signature already present. Skipping.")
			continue
		}
		if len(packet.Inputs[i].Bip32Derivation) != 1 {
			log.Traceln("Index", i, ", expected exactly 1 BIP-32 derivation. Not sure how to handle this.", packet.Inputs[i].Bip32Derivation)
			continue
		}
		// TODO we do not properly determine if *any* wallet is loaded. Fingerprint will be all-zero if no wallet is loaded yet.
		if device.Fingerprint.Uint32() != packet.Inputs[i].Bip32Derivation[0].MasterKeyFingerprint {
			// Incorrect wallet loaded. Load wallet if available, otherwise skip and continue with next.
			nextfpr := le.FromUint32(packet.Inputs[i].Bip32Derivation[0].MasterKeyFingerprint)
			if path, ok := wallets[nextfpr]; !ok {
				log.Traceln("Agent is not aware of a wallet-file for current input's master-fingerprint", hex.EncodeToString(nextfpr[:]), ". Skipping.")
				continue
			} else if err := loadWalletFile(device, path); err != nil || packet.Inputs[i].Bip32Derivation[0].MasterKeyFingerprint != device.Fingerprint.Uint32() {
				log.Warnln("Wallet failed to load or has unexpected fingerprint. Skipping.", err)
				continue
			}
		}
		log.Traceln("Input", i, ",", reprBip32Derivation(packet.Inputs[i].Bip32Derivation[0]))
		var path = packet.Inputs[i].Bip32Derivation[0].Bip32Path
		if len(path) > maxDerivationPathLength {
			return errors.Context(errors.ErrFailure, "Requested derivation path is longer than supported")
		}
		var pathbytes = make([]byte, 0, len(path)*4)
		for p := range path {
			enc32 := le.FromUint32(path[p])
			pathbytes = append(pathbytes, enc32[:]...)
		}

		// TODO hard-coded hashtype choice
		hashtype := txn.SIGHASH_ALL
		signrequest := []byte{uint8(i), uint8(len(path))}
		signrequest = append(signrequest, pathbytes...)
		signrequest = append(signrequest, hashtype)
		if err := device.Session.Send(0, CmdSignTransaction, signrequest); err != nil {
			return errors.Context(err, "Failed to transmit transaction signing request")
		}
		var txsig []byte
		if resp, err := device.Session.Receive(0, RspSignTransaction); err != nil {
			return errors.Context(err, "Failed to receive signature from device")
		} else {
			txsig = append(asn1_.ConvertP1363(resp[1:65]), hashtype)
		}

		// FIXME debugging code: checking signature using own implementation
		var err error
		var tx txn.Transaction
		if tx, _, err = txn.ParseTransaction(txencoded); err != nil {
			return errors.Context(err, "Failed to parse encoded transaction")
		}
		script := packet.Inputs[i].NonWitnessUtxo.TxOut[packet.UnsignedTx.TxIn[i].PreviousOutPoint.Index].PkScript
		if err := tx.CheckSignature(uint(i), script, txsig, packet.Inputs[i].Bip32Derivation[0].PubKey); err != nil {
			return errors.Context(err, "Failed to verify signature internally")
		}

		if signret, err := updater.Sign(i, txsig, packet.Inputs[i].Bip32Derivation[0].PubKey, nil, nil); err != nil {
			return errors.Context(err, "Failed to sign input "+strconv.FormatIntDecimal(i))
		} else if signret < psbt.SignSuccesful {
			return errors.Context(errors.ErrFailure, "Updater: signature injection failure")
		} else if log.Tracing() {
			log.Traceln("Input", i, "signing result:", signret)
		}

		success, err := psbt.MaybeFinalize(packet, i)
		log.Traceln("Input", i, "finalized:", success, "(", err, ")")
	}
	// Note: don't return error as we do not know how many other signers are present that we cannot satisfy
	// with our wallets. So, instead, report some information on the status and let the user figure it out.
	err := psbt.MaybeFinalizeAll(packet)
	log.Traceln("Finalizing all inputs. (", err, ")")
	log.Traceln("Finished signing inputs. Completion-status:", packet.IsComplete())
	return nil
}
