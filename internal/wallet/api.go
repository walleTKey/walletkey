package wallet

import (
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/tillitis/tkeyclient"
)

var CmdGenerateSecret tkey.AppCmd = tkey.NewCommand("GenerateSecret", 0x03, tkeyclient.CmdLen128)
var RspGenerateSecret tkey.AppCmd = tkey.NewCommand("RspGenerateSecret", 0x03, tkeyclient.CmdLen128)
var CmdRestoreSecret tkey.AppCmd = tkey.NewCommand("RestoreSecret", 0x04, tkeyclient.CmdLen128)
var RspRestoreSecret tkey.AppCmd = tkey.NewCommand("RspRestoreSecret", 0x04, tkeyclient.CmdLen128)
var CmdLoadWallet tkey.AppCmd = tkey.NewCommand("LoadWallet", 0x05, tkeyclient.CmdLen128)
var RspLoadWallet tkey.AppCmd = tkey.NewCommand("RspLoadWallet", 0x05, tkeyclient.CmdLen32)
var CmdExportWalletSecret tkey.AppCmd = tkey.NewCommand("ExportWalletSecret", 0x06, tkeyclient.CmdLen1)
var RspExportWalletSecret tkey.AppCmd = tkey.NewCommand("RspExportWalletSecret", 0x06, tkeyclient.CmdLen128)
var CmdWalletInfo tkey.AppCmd = tkey.NewCommand("WalletInfo", 0x07, tkeyclient.CmdLen1)
var RspWalletInfo tkey.AppCmd = tkey.NewCommand("RspWalletInfo", 0x07, tkeyclient.CmdLen128)
var CmdUnloadWallet tkey.AppCmd = tkey.NewCommand("UnloadWallet", 0x08, tkeyclient.CmdLen1)
var RspUnloadWallet tkey.AppCmd = tkey.NewCommand("RspUnloadWallet", 0x08, tkeyclient.CmdLen1)
var CmdLoadTransaction tkey.AppCmd = tkey.NewCommand("LoadTransaction", 0x09, tkeyclient.CmdLen128)
var RspLoadTransaction tkey.AppCmd = tkey.NewCommand("RspLoadTransaction", 0x09, tkeyclient.CmdLen1)
var CmdSignTransaction tkey.AppCmd = tkey.NewCommand("SignTransaction", 0x0A, tkeyclient.CmdLen128)
var RspSignTransaction tkey.AppCmd = tkey.NewCommand("RspSignTransaction", 0x0A, tkeyclient.CmdLen128)
var CmdDeriveAddress tkey.AppCmd = tkey.NewCommand("DeriveAddress", 0x0B, tkeyclient.CmdLen128)
var RspDeriveAddress tkey.AppCmd = tkey.NewCommand("RspDeriveAddress", 0x0B, tkeyclient.CmdLen128)

const SEALED_WALLET_LENGTH = 24 + 16 + 32
const HARDENED_ID_LENGTH = 64

type SealedWallet [SEALED_WALLET_LENGTH]byte
