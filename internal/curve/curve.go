package curve

import (
	"crypto/ecdsa"
	"math/big"

	"github.com/cobratbq/goutils/std/errors"
	"github.com/mit-dci/lit/crypto/koblitz"
)

func SerializePointInto(dest []byte, p *koblitz.PublicKey) {
	if p.Y.Bit(0) == 0 {
		dest[0] = 0x02
	} else {
		dest[0] = 0x03
	}
	p.X.FillBytes(dest[1:])
}

func SerializePoint(p *koblitz.PublicKey) [33]byte {
	var encoded [33]byte
	SerializePointInto(encoded[:], p)
	return encoded
}

func ParsePoint(b []byte) (*koblitz.PublicKey, error) {
	return koblitz.ParsePubKey(b, koblitz.S256())
}

func SerializeScalarInto(dest []byte, s *koblitz.PrivateKey) {
	s.D.FillBytes(dest[:])
}

func SerializeScalar(s *koblitz.PrivateKey) [32]byte {
	var encoded [32]byte
	SerializeScalarInto(encoded[:], s)
	return encoded
}

func ParseScalar(b []byte) (*koblitz.PrivateKey, *koblitz.PublicKey) {
	return koblitz.PrivKeyFromBytes(koblitz.S256(), b)
}

func AddScalar(s1, s2 *koblitz.PrivateKey) (*koblitz.PrivateKey, error) {
	var curve = koblitz.S256()
	if s1.D.Cmp(curve.N) >= 0 || s2.D.Cmp(curve.N) >= 0 {
		return nil, errors.Context(errors.ErrIllegal, "bad scalar input")
	}
	var s3 big.Int
	s3.Add(s1.D, s2.D)
	s3.Mod(&s3, curve.N)
	if s3.Cmp(&big.Int{}) == 0 {
		return nil, errors.Context(errors.ErrFailure, "resulting scalar is zero")
	}
	var privkey, _ = koblitz.PrivKeyFromBytes(curve, s3.Bytes())
	return privkey, nil
}

func AddPoint(p1, p2 *koblitz.PublicKey) (*koblitz.PublicKey, error) {
	var curve = koblitz.S256()
	var x, y = curve.Add(p1.X, p1.Y, p2.X, p2.Y)
	if !curve.IsOnCurve(x, y) {
		return nil, errors.Context(errors.ErrFailure, "resulting point is not on the curve")
	}
	return (*koblitz.PublicKey)(&ecdsa.PublicKey{Curve: curve, X: x, Y: y}), nil
}
