// Bitcoin-script. A reduced version of the script is used in transactions, with some predefined variations
// in wide-spread use as part of standardized transaction formats.
//
// P2PK: PubKeyScript: `TODO`. SigScript: `TODO`.
//
// P2PKH: PubKeyScript: `<OP_DUP> <OP_HASH160> <OP_PUSHBYTES_20> <20-byte public-key-hash> <OP_EQUALVERIFY>
// <OP_CHECKSIG>`. // SigScript: `<OP_PUSHBYTES_72> <72-byte signature> <OP_PUSHBYTES_33>
// <33-byte compressed-public-key>`.
package script
