package script

import (
	"crypto/subtle"
	"slices"

	rmd "golang.org/x/crypto/ripemd160"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/btc/txn"
	stk "github.com/cobratbq/goutils/std/builtin/stack"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
)

type opcode uint8

const (
	// OP_0 opcode = 0x00
	OP_PUSHBYTES_01 opcode = 0x01
	// ...
	OP_PUSHBYTES_20 opcode = 0x14
	// ...
	OP_PUSHBYTES_75 opcode = 0x4b
	OP_PUSHDATA1    opcode = 0x4c
	OP_PUSHDATA2    opcode = 0x4d
	OP_PUSHDATA4    opcode = 0x4e
	// OP_1NEGATE          = 79
	// ...
	// OP_1 opcode = 0x51
	// OP_2  = 0x52
	// OP_3  = 0x53
	// OP_4  = 0x54
	// OP_5  = 0x55
	// OP_6  = 0x56
	// OP_7  = 0x57
	// OP_8  = 0x58
	// OP_9  = 0x59
	// OP_10 = 0x5A
	// OP_11 = 0x5B
	// OP_12 = 0x5C
	// OP_13 = 0x5D
	// OP_14 = 0x5E
	// OP_15 = 0x5F
	// OP_16 = 0x60
	// ...
	// OP_NOP              = 97
	// OP_IF               = 99
	// OP_NOTIF            = 100
	// OP_ELSE             = 103
	// OP_ENDIF            = 104
	OP_VERIFY opcode = 0x69
	// OP_RETURN           = 106
	// ...
	OP_DUP opcode = 0x76
	// ...
	OP_EQUAL       opcode = 0x87
	OP_EQUALVERIFY opcode = 0x88
	// ...
	// OP_RIPEMD160 = 0xa6
	// OP_SHA1 = 0xa7
	// OP_SHA256        opcode = 0xa8
	OP_HASH160 opcode = 0xa9
	// OP_HASH256       opcode = 0xaa
	OP_CODESEPARATOR opcode = 0xab
	// Reference: <https://en.bitcoin.it/wiki/OP_CHECKSIG>
	OP_CHECKSIG opcode = 0xac
	// OP_CHECKSIGVERIFY opcode = 0xad
)

// TODO not needed?
//func IsOpcodePushbytesLength(length int) bool {
//	return length >= int(OP_PUSHBYTES_01) && length <= int(OP_PUSHBYTES_75)
//}

// TODO not needed?
//func EncodeP2PKHSignatureScript(signature, pubkey []byte) []byte {
//	assert.Require(IsOpcodePushbytesLength(len(signature)), "Expected length of signature to be small enough for PUSHBYTES-opcodes.")
//	assert.Require(IsOpcodePushbytesLength(len(pubkey)), "Expected length of pubkey to be small enough for PUSHBYTES-opcodes.")
//	var scriptsig = make([]byte, 0, 1+len(signature)+1+len(pubkey))
//	scriptsig = append(scriptsig, byte(len(signature)))
//	scriptsig = append(scriptsig, signature...)
//	scriptsig = append(scriptsig, byte(len(pubkey)))
//	scriptsig = append(scriptsig, pubkey...)
//	return scriptsig
//}

func IsP2PKHRedeemScript(subscript []byte) bool {
	return len(subscript) == 25 && subscript[0] == byte(OP_DUP) && subscript[1] == byte(OP_HASH160) && subscript[2] == byte(OP_PUSHBYTES_20) && subscript[23] == byte(OP_EQUALVERIFY) && subscript[24] == byte(OP_CHECKSIG)
}

// TODO not needed?
//func EncodeP2PKHRedeemScript(pubkey *koblitz.PublicKey) []byte {
//	var buffer = make([]byte, 25)
//	buffer[0] = byte(OP_DUP)
//	buffer[1] = byte(OP_HASH160)
//	buffer[2] = byte(OP_PUSHBYTES_20)
//	var hash = btc.Hash160(pubkey.SerializeCompressed())
//	copy(buffer[3:23], hash[:])
//	buffer[23] = byte(OP_EQUALVERIFY)
//	buffer[24] = byte(OP_CHECKSIG)
//	return buffer
//}

func ripemd160(data []byte) [20]byte {
	hash := rmd.New()
	hash.Write(data)
	return [20]byte(hash.Sum(nil))
}

func isTrue(data []byte) bool {
	return len(data) == 1 && data[0] == 1
}

func encodeBoolValue(value bool) []byte {
	if value {
		return []byte{1}
	}
	return []byte{}
}

// FIXME needs testing
func filterCodeSeparatorOpcodes(script []byte) []byte {
	// REMARK: only useful if filtering produces close to full script, otherwise allocation is overkill
	filtered := make([]byte, 0, len(script))
	for i := 0; i < len(script); i++ {
		if opcode(script[i]) == OP_CODESEPARATOR {
			// skip over OP_CODESEPARATOR
			continue
		}
		filtered = append(filtered, script[i])
		if script[i] >= 0x01 && script[i] <= 0x4b {
			filtered = append(filtered, script[i+1:i+1+int(script[i])]...)
			i += int(script[i])
			continue
		}
		switch opcode(script[i]) {
		case OP_DUP, OP_HASH160, OP_EQUAL, OP_VERIFY, OP_EQUALVERIFY, OP_CHECKSIG:
			// opcodes that do not have additional payload (other than the opcode itself)
			continue
		case OP_CODESEPARATOR:
			panic("BUG: should have been filtered")
		default:
			panic("scan: unsupported opcode " + strconv.FormatUint(script[i], 16))
		}
	}
	return filtered
}

func opcodeCheckSignature(tx *txn.Transaction, idx uint, subscript, signature, pubkey []byte) error {
	log.Traceln("Signature:", len(signature), signature)
	// 2.  A new subScript is created from the scriptCode (the scriptCode is the actually executed
	// script - either the scriptPubKey for non-segwit, non-P2SH scripts, or the redeemscript in
	// non-segwit P2SH scripts). The script from the immediately after the most recently parsed
	// OP_CODESEPARATOR to the end of the script is the subScript. If there is no OP_CODESEPARATOR the
	// entire script becomes the subScript.
	// 3.  Any occurrences of sig are deleted from subScript, if present. (it is not standard to have
	// a signature in an input script of a transaction)
	// TODO not dropping occurrences of `sig`, because non-standard.
	// 4.  Any remaining OP_CODESEPARATORS are removed from subScript.
	log.Traceln("Subscript before:", subscript)
	subscript = filterCodeSeparatorOpcodes(subscript)
	log.Traceln("Subscript after:", subscript)
	return tx.CheckSignature(idx, subscript, signature, pubkey)
}

// FIXME need to calculate number of operations (per block)
// FIXME operations are counted whether or not they are executed
// FIXME OP_CHECKSIG/OP_CHECKSIGVERIFY each count as 1, OP_CHECKMULTISIG/OP_CHECKMULTISIGVERIFY preceded by OP_1--OP_16 count as <OP_n> operations, any other OP_CHECKMULTISIG/OP_CHECKMULTISIGVERIFY count as 20

func execute(stack [][]byte, tx *txn.Transaction, idx uint, script []byte) ([][]byte, error) {
	var err error
	var value []byte
	var idxSeparator = -1
	for i := 0; i < len(script); i++ {
		if script[i] >= 1 && script[i] <= 0x4b {
			// Load anywhere between 1 and 75 bytes of data, corresponding to opcode.
			log.Traceln("OP_" + strconv.FormatUintDecimal(script[i]) + ": loading data")
			if stack, err = stk.Push(stack, script[i+1:i+1+int(script[i])]); err != nil {
				return stack, errors.Context(err, "loading "+strconv.FormatUintDecimal(script[i])+"bytes of data")
			}
			i += int(script[i])
			continue
		}
		switch opcode(script[i]) {
		case OP_DUP:
			log.Traceln("OP_DUP (0x76)")
			if value, err = stk.Peek(stack); err != nil {
				return stack, errors.Context(err, "failed to peek value for duplication")
			}
			// TODO strictly speaking, duplication isn't necessary, but we can't protect against writes
			dupl := slices.Clone(value)
			if stack, err = stk.Push(stack, dupl); err != nil {
				return stack, errors.Context(err, "failed to push duplicated value")
			}
		case OP_HASH160:
			log.Traceln("OP_HASH160 (0xa9)")
			if stack, value, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop OP_HASH160 input")
			}
			var temp = btc.Hash160(value)
			if stack, err = stk.Push(stack, temp[:]); err != nil {
				return stack, errors.Context(err, "failed to push OP_HASH160 checksum")
			}
		case OP_EQUAL:
			log.Traceln("OP_EQUAL (0x87)")
			if stack, value, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop first value for OP_EQUAL check")
			}
			var temp []byte
			if stack, temp, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop second value for OP_EQUAL check")
			}
			equal := subtle.ConstantTimeCompare(value, temp) == 1
			log.Traceln("OP_EQUAL verification:", equal)
			if stack, err = stk.Push(stack, encodeBoolValue(equal)); err != nil {
				return stack, errors.Context(err, "failed to push OP_EQUAL result")
			}
		case OP_VERIFY:
			log.Traceln("OP_VERIFY (0x69)")
			if stack, value, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop verification value off stack")
			}
			if !isTrue(value) {
				log.Traceln("OP_VERIFY: verification failed")
				return stack, errors.Context(errors.ErrFailure, "previous check failed verification")
			}
			log.Traceln("OP_VERIFY: verification succeeded")
		case OP_EQUALVERIFY:
			log.Traceln("OP_EQUALVERIFY (0x88)")
			if stack, value, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop first value for OP_EQUALVERIFY check")
			}
			var temp1 []byte
			if stack, temp1, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to pop second value for OP_EQUALVERIFY checK")
			}
			if subtle.ConstantTimeCompare(value, temp1) != 1 {
				log.Traceln("OP_EQUALVERIFY: verification failed")
				return stack, errors.Context(errors.ErrFailure, "equality of values failed verification")
			}
			log.Traceln("OP_EQUALVERIFY: verification succeeded")
		case OP_CODESEPARATOR:
			log.Traceln("OP_CODESEPARATOR (0xab) at index", i)
			idxSeparator = i
		case OP_CHECKSIG:
			log.Traceln("OP_CHECKSIG (0xac)")
			log.Traceln("Stack:", len(stack), stack)
			// 1.  the public key and the signature are popped from the stack, in that order. Signature
			// format is [<DER signature> <1 byte hash-type>]. Hashtype value is last byte of the sig.
			var pubkey []byte
			if stack, pubkey, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to retrieve public key from stack")
			}
			var signature []byte
			if stack, signature, err = stk.Pop(stack); err != nil {
				return stack, errors.Context(err, "failed to retrieve signature from stack")
			}
			err = opcodeCheckSignature(tx, idx, script[idxSeparator+1:], signature, pubkey)
			result := encodeBoolValue(err == nil)
			log.Traceln("OP_CHECKSIG: transaction validation:", err == nil, "(Error:", err, ")")
			if stack, err = stk.Push(stack, result); err != nil {
				return stack, errors.Context(err, "failed to push OP_CHECKSIG result")
			}
		default:
			// return stack, errors.Context(errors.ErrUnsupported, "Opcode: 0x"+strconv.FormatUint(script[i], 16))
			// FIXME temporary panic to signal missing opcodes
			panic("Unsupported opcode " + strconv.FormatUint(script[i], 16))
		}
	}
	return stack, nil
}

const MAX_STACK_SIZE = 10

// ExecuteScript executes a script.
//
// Verification: first evaluate scriptSig (present in the current transaction), then with data remaining on
// the stack, evaluate the referenced pubkeyScript (from the indicated previous transaction used for input).
//
// - `sigscript` is the current transaction's input (signature) script.
// - `pubkeyscript` is the previous transaction's output (pubkey) script.
//
// `pubkeyscript` is acquired for the previous transaction whose output-entry is the source of the current
// transaction's input. `sigscript` provides the (signature-)data to access and finalize the payment.
//
// In case of P2SH, sigscript will contain the redeem instructions and pubkeyscript a checksum such that only
// the intended public key is allowed to define the redeem instructions/conditions.
// TODO requires extensive testing with all kinds of scripts, hash-types, ANYONECANPAY-flag, etc.
// FIXME consider extracting subset required to check script for consistency without executing.
func ExecuteScript(tx *txn.Transaction, idx uint, sigscript, pubkeyscript []byte) error {
	var store [MAX_STACK_SIZE][]byte
	stack := store[:0]
	var err error
	log.Traceln("Sigscript:", sigscript)
	if stack, err = execute(stack, tx, idx, sigscript); err != nil {
		return errors.Context(err, "Errors while executing sigscript")
	}
	log.Traceln("Stack after sigscript:", stack)
	log.Traceln("Pubkeyscript:", pubkeyscript)
	if stack, err = execute(stack, tx, idx, pubkeyscript); err != nil {
		return errors.Context(err, "Errors while executing pubkeyscript")
	}
	log.Traceln("Stack after pubkeyscript:", stack)
	var conclusion []byte
	if stack, conclusion, err = stk.Pop(stack); err != nil || len(stack) > 0 {
		return errors.Context(err, "Failed to acquire end conclusion of script execution")
	}
	if !isTrue(conclusion) {
		return errors.Context(errors.ErrFailure, "Script execution did not conclude successfully")
	}
	return nil
}

// TODO __520-byte limitation on serialized script size__: As a consequence of the requirement for backwards compatibility the serialized script is itself subject to the same rules as any other PUSHDATA operation, including the rule that no data greater than 520 bytes may be pushed to the stack. Thus it is not possible to spend a P2SH output if the redemption script it refers to is >520 bytes in length. For instance while the OP_CHECKMULTISIG opcode can itself accept up to 20 pubkeys, with 33-byte compressed pubkeys it is only possible to spend a P2SH output requiring a maximum of 15 pubkeys to redeem: 3 bytes + 15 pubkeys * 34 bytes/pubkey = 513 bytes.
