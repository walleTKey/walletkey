package btc

import (
	"bytes"
	"crypto/sha256"

	"github.com/cobratbq/goutils/codec/string/base58"
	rmd "golang.org/x/crypto/ripemd160"
)

type Encoding uint8

const (
	ENCODING_UNKNOWN Encoding = iota
	ENCODING_BASE58
	ENCODING_BECH32
)

func IdentifyEncoding(address []byte) Encoding {
	switch {
	case address[0] == '1', address[0] == 'm', address[0] == 'n':
		return ENCODING_BASE58
	case address[0] == '3', address[0] == '2':
		return ENCODING_BASE58
	case bytes.Equal(address[:4], []byte("xpub")), bytes.Equal(address[:4], []byte("tpub")):
		return ENCODING_BASE58
	case bytes.Equal(address[:4], []byte("xprv")), bytes.Equal(address[:4], []byte("tprv")):
		return ENCODING_BASE58
	case bytes.Equal(address[:3], []byte("bc1")), bytes.Equal(address[:3], []byte("tb1")):
		return ENCODING_BECH32
	case address[0] == '5', address[0] == '9':
		return ENCODING_BASE58
	case address[0] == 'K', address[0] == 'L', address[0] == 'c':
		return ENCODING_BASE58
	default:
		return ENCODING_UNKNOWN
	}
}

type AddressType uint8

const FLAG_TESTNET AddressType = 0x80

const (
	ADDRESS_UNKNOWN AddressType = iota
	ADDRESS_P2PKH
	ADDRESS_P2SH
	ADDRESS_XPUB
	ADDRESS_XPRIV
	ADDRESS_BECH32
	ADDRESS_WIF_PRIVATE_UNCOMPRESSED
	ADDRESS_WIF_PRIVATE_COMPRESSED
	ADDRESS_P2PKH_TEST                    = FLAG_TESTNET | ADDRESS_P2PKH
	ADDRESS_P2SH_TEST                     = FLAG_TESTNET | ADDRESS_P2SH
	ADDRESS_XPUB_TEST                     = FLAG_TESTNET | ADDRESS_XPUB
	ADDRESS_XPRIV_TEST                    = FLAG_TESTNET | ADDRESS_XPRIV
	ADDRESS_BECH32_TEST                   = FLAG_TESTNET | ADDRESS_BECH32
	ADDRESS_WIF_PRIVATE_UNCOMPRESSED_TEST = FLAG_TESTNET | ADDRESS_WIF_PRIVATE_UNCOMPRESSED
	ADDRESS_WIF_PRIVATE_COMPRESSED_TEST   = FLAG_TESTNET | ADDRESS_WIF_PRIVATE_COMPRESSED
)

// TODO no P2PK?
// FIXME prefix '5' indicates WIF uncompressed, but does not distinguish between standard and Electrum variants. (See <https://en.bitcoin.it/wiki/List_of_address_prefixes>.)
func IdentifyAddress(address []byte) AddressType {
	switch {
	case address[0] == '1':
		return ADDRESS_P2PKH
	case address[0] == 'm', address[0] == 'n':
		return ADDRESS_P2PKH_TEST
	case address[0] == '3':
		return ADDRESS_P2SH
	case address[0] == '2':
		return ADDRESS_P2SH_TEST
	case bytes.Equal(address[:4], []byte("xpub")):
		return ADDRESS_XPUB
	case bytes.Equal(address[:4], []byte("tpub")):
		return ADDRESS_XPUB_TEST
	case bytes.Equal(address[:4], []byte("xprv")):
		return ADDRESS_XPRIV
	case bytes.Equal(address[:4], []byte("tprv")):
		return ADDRESS_XPRIV_TEST
	case bytes.Equal(address[:3], []byte("bc1")):
		return ADDRESS_BECH32
	case bytes.Equal(address[:3], []byte("tb1")):
		return ADDRESS_BECH32_TEST
	case address[0] == '5':
		return ADDRESS_WIF_PRIVATE_UNCOMPRESSED
	case address[0] == 'K', address[0] == 'L':
		return ADDRESS_WIF_PRIVATE_COMPRESSED
	case address[0] == '9':
		return ADDRESS_WIF_PRIVATE_UNCOMPRESSED_TEST
	case address[0] == 'c':
		return ADDRESS_WIF_PRIVATE_COMPRESSED_TEST
	default:
		return ADDRESS_UNKNOWN
	}
}

func Hash160(data []byte) [20]byte {
	step := sha256.Sum256(data)
	hash := rmd.New()
	hash.Write(step[:])
	return [20]byte(hash.Sum(nil))
}

type P2PK = [65]byte

// Encode P2PK public key as encoded address.
func EncodeP2PK(mainnet bool, pubkey P2PK) []byte {
	var address [66]byte
	if mainnet {
		address[0] = 0
	} else {
		address[0] = 0x6f
	}
	copy(address[1:], pubkey[:])
	return base58.ChecksumEncode(address[:])
}

// Encodes an ECDSA compressed public key as P2PKH address.
// `prefix` is the address prefix (0x00 for mainnet, 0x6F for testnet)
func EncodeP2PKH(mainnet bool, pubkey [33]byte) []byte {
	var address [21]byte
	if mainnet {
		address[0] = 0
	} else {
		address[0] = 0x6f
	}
	digest := Hash160(pubkey[:])
	copy(address[1:], digest[:])
	return base58.ChecksumEncode(address[:])
}
