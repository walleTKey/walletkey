package btc

import (
	"testing"

	"github.com/cobratbq/goutils/codec/string/base58"
	assert "github.com/cobratbq/goutils/std/testing"
)

func TestIdentifyAddress(t *testing.T) {
	testdata := []struct {
		address []byte
		typ     AddressType
		codec   Encoding
		valid   bool
	}{
		{address: []byte("17VZNX1SN5NtKa8UQFxwQbFeFc3iqRYhem"), typ: ADDRESS_P2PKH, valid: true},
		{address: []byte("mipcBbFg9gMiCh81Kj8tqqdgoZub1ZJRfn"), typ: ADDRESS_P2PKH_TEST, valid: true},
		{address: []byte("3EktnHQD7RiAE6uzMj2ZifT9YgRrkSgzQX"), typ: ADDRESS_P2SH, valid: true},
		{address: []byte("2MzQwSSnBHWHqSAqtTVQ6v47XtaisrJa1Vc"), typ: ADDRESS_P2SH_TEST, valid: true},
		{address: []byte("xpub661MyMwAqRbcEYS8w7XLSVeEsBXy79zSzH1J8vCdxAZningWLdN3zgtU6LBpB85b3D2yc8sfvZU521AAwdZafEz7mnzBBsz4wKY5e4cp9LB"), typ: ADDRESS_XPUB, valid: true},
		{address: []byte("tpubD6NzVbkrYhZ4WLczPJWReQycCJdd6YVWXubbVUFnJ5KgU5MDQrD998ZJLNGbhd2pq7ZtDiPYTfJ7iBenLVQpYgSQqPjUsQeJXH8VQ8xA67D"), typ: ADDRESS_XPUB_TEST, valid: true},
		{address: []byte("xprv9s21ZrQH143K24Mfq5zL5MhWK9hUhhGbd45hLXo2Pq2oqzMMo63oStZzF93Y5wvzdUayhgkkFoicQZcP3y52uPPxFnfoLZB21Teqt1VvEHx"), typ: ADDRESS_XPRIV, valid: true},
		{address: []byte("tprv8ZgxMBicQKsPcsbCVeqqF1KVdH7gwDJbxbzpCxDUsoXHdb6SnTPYxdwSAKDC6KKJzv7khnNWRAJQsRA8BBQyiSfYnRt6zuu4vZQGKjeW4YF"), typ: ADDRESS_XPRIV_TEST, valid: true},
		{address: []byte("bc1qw508d6qejxtdg4y5r3zarvary0c5xw7kv8f3t4"), typ: ADDRESS_BECH32, valid: true},
		{address: []byte("tb1qw508d6qejxtdg4y5r3zarvary0c5xw7kxpjzsx"), typ: ADDRESS_BECH32_TEST, valid: true},
		{address: []byte("5Hwgr3u458GLafKBgxtssHSPqJnYoGrSzgQsPwLFhLNYskDPyyA"), typ: ADDRESS_WIF_PRIVATE_UNCOMPRESSED, valid: true},
		{address: []byte("L1aW4aubDFB7yfras2S1mN3bqg9nwySY8nkoLmJebSLD5BWv3ENZ"), typ: ADDRESS_WIF_PRIVATE_COMPRESSED, valid: true},
		{address: []byte("92Pg46rUhgTT7romnV7iGW6W1gbGdeezqdbJCzShkCsYNzyyNcc"), typ: ADDRESS_WIF_PRIVATE_UNCOMPRESSED_TEST, valid: true},
		{address: []byte("cNJFgo1driFnPcBdBX8BrJrpxchBWXwXCvNH5SoSkdcF6JXXwHMm"), typ: ADDRESS_WIF_PRIVATE_COMPRESSED_TEST, valid: true},
	}
	for _, entry := range testdata {
		assert.Equal(t, entry.typ, IdentifyAddress(entry.address))
		switch IdentifyEncoding(entry.address) {
		case ENCODING_BASE58:
			// TODO we could test for prefix of decoded content
			_, err := base58.CheckDecode(entry.address)
			assert.Equal(t, entry.valid, err == nil)
		case ENCODING_BECH32:
		// TODO support decoding BECH32.
		default:
			panic("Unsupported")
		}
	}
}
