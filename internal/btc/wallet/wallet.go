package wallet

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/btc/script"
	"codeberg.org/walletkey/walletkey/internal/btc/txn"
	"codeberg.org/walletkey/walletkey/internal/btc/wallet/hierarchical"
	"github.com/btcsuite/btcd/btcutil/psbt"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/bigendian"
	"github.com/cobratbq/goutils/codec/string/base58"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
	"github.com/mit-dci/lit/crypto/koblitz"
)

// TODO likely temporary or for testing only, given that we need to switch to walleTKey(-firmware)
type Wallet struct {
	mainnet   bool
	privkey   *koblitz.PrivateKey
	masterkey *koblitz.PrivateKey
	chaincode hierarchical.ChainCode
}

func GenerateWallet() (Wallet, error) {
	var wallet Wallet
	var err error
	curve := koblitz.S256()
	if wallet.privkey, err = koblitz.NewPrivateKey(curve); err != nil {
		return Wallet{}, errors.Aggregate(errors.ErrFailure, "failed to generate private key", err)
	}
	// FIXME what's the actual seed I should use? (this input just is accepted)
	wallet.masterkey, _, wallet.chaincode = hierarchical.GenerateMasterKey(wallet.privkey.Serialize())
	return wallet, nil
}

func LoadWallet(privkey [32]byte) Wallet {
	var w Wallet
	w.privkey, _ = koblitz.PrivKeyFromBytes(koblitz.S256(), privkey[:])
	// FIXME using `privkey` as master-key seed is rather arbitrary, probably needs to hash or something to make it unpredictable, also disconnected from classic/traditional private-key.
	w.masterkey, _, w.chaincode = hierarchical.GenerateMasterKey(privkey[:])
	return w
}

func (w *Wallet) SetMainnet(enabled bool) {
	w.mainnet = enabled
}

func (w *Wallet) ExportPrivateKey() []byte {
	return w.privkey.Serialize()
}

func (w *Wallet) PublicKey() *koblitz.PublicKey {
	return w.privkey.PubKey()
}

// Fingerprint produces the fingerprint for the public key of the master secret. This is also used to identify
// the wallet.
func (w *Wallet) Fingerprint() btc.FingerprintType {
	return btc.DeriveFingerprint(w.masterkey.PubKey().SerializeCompressed())
}

// MasterXpub produces the master XPUB for this wallet.
func (w *Wallet) MasterXPub() hierarchical.XpubType {
	var prefix [4]byte
	if w.mainnet {
		prefix = hierarchical.PREFIX_XPUB
	} else {
		prefix = hierarchical.PREFIX_TPUB
	}
	return hierarchical.XpubType{Prefix: prefix, Depth: 0, Index: 0, Parent: [4]byte{}, Chain: w.chaincode, Pubkey: w.masterkey.PubKey()}
}

type AddressPurpose uint8

const (
	PURPOSE_RECEIVE  AddressPurpose = 0
	PURPOSE_INTERNAL AddressPurpose = 1
)

func (w *Wallet) Descriptors(account uint32, purpose AddressPurpose) ([]string, error) {
	var chain uint32
	switch purpose {
	case PURPOSE_RECEIVE:
		chain = 0
	case PURPOSE_INTERNAL:
		chain = 1
	default:
		panic("BUG: illegal purpose")
	}
	var cointype uint32
	if w.mainnet {
		cointype = hierarchical.COINTYPE_BITCOIN
	} else {
		cointype = hierarchical.COINTYPE_BITCOIN_TESTNET
	}
	var descrs []string
	// var xpub = w.MasterXPub(mainnet)
	// mpubkey := w.masterkey.PubKey().SerializeCompressed()
	// fp := btc.Fingerprint(mpubkey)
	// log.Debugln("Xpub:", string(base58.ChecksumEncode(xpub[:])), "Fingerprint:", hex.EncodeToString(fp[:]))
	// descrs = append(descrs, "pkh([00000000]"+string(base58.ChecksumEncode(xpub[:]))+")")

	var _, xpub, err = hierarchical.DeriveAccount(w.masterkey, w.chaincode, cointype, account|hierarchical.HARDENED)
	if err != nil {
		return nil, err
	}
	var masterFp = w.Fingerprint()
	var encoded = xpub.Encode()
	xpubencoded := base58.ChecksumEncode(encoded[:])
	descrs = append(descrs, "pkh(["+hex.EncodeToString(masterFp[:])+"/44h/"+strconv.FormatUintDecimal(cointype&^hierarchical.HARDENED)+"h/"+strconv.FormatUintDecimal(account)+"h]"+string(xpubencoded)+"/"+strconv.FormatUintDecimal(chain)+"/*)")
	return descrs, nil
}

func (w *Wallet) DeriveAddress(path []uint32) (*koblitz.PrivateKey, hierarchical.XpubType, error) {
	return hierarchical.DerivePrivateKey(w.mainnet, w.masterkey, w.chaincode, path)
}

func (w *Wallet) DeriveAccount(cointype, account uint32) (*koblitz.PrivateKey, hierarchical.XpubType, error) {
	return hierarchical.DeriveAccount(w.masterkey, w.chaincode, cointype, account)
}

// FIXME troubleshooting/debugging
func (w *Wallet) DumpWalletAddresses(cointype, account uint32, chain uint32, from uint32, count uint32) {
	for i := from; i < from+count; i++ {
		key, _ := builtin.Expect2(hierarchical.DeriveWallet(w.masterkey, w.chaincode, cointype, account|hierarchical.HARDENED, chain, i))
		log.Debugln(i, "address:", string(btc.EncodeP2PKH(cointype == hierarchical.COINTYPE_BITCOIN, [33]byte(key.PubKey().SerializeCompressed()))), "compressed:", hex.EncodeToString(key.PubKey().SerializeCompressed()))
	}
}

const MAX_TX_VERSION int32 = 2

// TODO take multi-step approach? 1.) check-derive hardened keys and compare against expected pubkeys, 2.) signed specified keys
// FIXME needs to be cleaned up, refactored, proper case and error handling, etc.
// FIXME convert to using TKeyWallet
func (w *Wallet) SignPSBT(packet *psbt.Packet) (*psbt.Packet, error) {
	log.Traceln("Signing received PSBT")
	assert.Success(psbt.InputsReadyToSign(packet), "Cannot proceed if inputs are not ready to be signed.")
	assert.Success(packet.SanityCheck(), "Cannot proceed if PSBT packet does not pass self-check.")
	assert.Require(len(packet.Unknowns) == 0 && !packet.UnsignedTx.HasWitness() &&
		slices.All(packet.Inputs, func(i psbt.PInput) bool { return len(i.Unknowns) == 0 }) &&
		slices.All(packet.Outputs, func(o psbt.POutput) bool { return len(o.Unknowns) == 0 }) &&
		packet.UnsignedTx.Version <= MAX_TX_VERSION,
		"PSBT packet contains unknown or unsupported elements. Refusing to sign.")
	// FIXME set flag
	var tx = txn.Transaction{Version: uint32(packet.UnsignedTx.Version), Flag: 0, Locktime: packet.UnsignedTx.LockTime}
	for i := range packet.UnsignedTx.TxIn {
		tx.Inputs = append(tx.Inputs, txn.Input{
			Hash:     packet.UnsignedTx.TxIn[i].PreviousOutPoint.Hash,
			Index:    packet.UnsignedTx.TxIn[i].PreviousOutPoint.Index,
			Script:   packet.UnsignedTx.TxIn[i].SignatureScript,
			Sequence: packet.UnsignedTx.TxIn[i].Sequence,
		})
	}
	for o := range packet.UnsignedTx.TxOut {
		log.Infoln("Amount:", packet.UnsignedTx.TxOut[o].Value, "pkscript:", hex.EncodeToString(packet.UnsignedTx.TxOut[o].PkScript))
		tx.Outputs = append(tx.Outputs, txn.Output{
			Value:  uint64(packet.UnsignedTx.TxOut[o].Value),
			Script: packet.UnsignedTx.TxOut[o].PkScript,
		})
	}
	// FIXME temp, debugging/inspection code
	for i := range packet.Inputs {
		log.Traceln("Input", i, ", extra information: sighash", packet.Inputs[i].SighashType, ", unknowns:", packet.Inputs[i].Unknowns)
		for d := range packet.Inputs[i].Bip32Derivation {
			log.Traceln("    deriv:",
				hex.EncodeToString(binary.LittleEndian.AppendUint32(nil, packet.Inputs[i].Bip32Derivation[d].MasterKeyFingerprint)),
				slices.Transform(packet.Inputs[i].Bip32Derivation[d].Bip32Path, func(v uint32) string {
					enc32 := bigendian.FromUint32(v)
					return "0x" + hex.EncodeToString(enc32[:])
				}))
		}
	}
	log.Traceln("Num inputs:", len(tx.Inputs))
	var masterfp = w.Fingerprint()
	var err error
	var updater = builtin.Expect(psbt.NewUpdater(packet))
	for i := range tx.Inputs {
		// FIXME assuming, right now, that multiple derivations implies multisig
		assert.Equal(1, len(packet.Inputs[i].Bip32Derivation))
		var deriv = packet.Inputs[i].Bip32Derivation[0]
		if masterfp.Uint32() != deriv.MasterKeyFingerprint {
			// we do not own this master-key, so we cannot sign this input
			log.Traceln("Master fingerprint mismatch, ours:", hex.EncodeToString(masterfp[:]))
			continue
		}
		var childkey *koblitz.PrivateKey
		// TODO in future: defer to walleTKey for deriving (hardened) addresses
		childkey, _, err = w.DeriveAddress(deriv.Bip32Path)
		assert.Success(err, "Failed to derive address from BIP-32 derivation path")
		var childpubkeybytes = childkey.PubKey().SerializeCompressed()
		if !bytes.Equal(childpubkeybytes, deriv.PubKey) {
			panic("Failed to derive expected child-keypair.")
		}

		// FIXME not parsed so we may be missing "codeseparator" opcodes that cut off the first part(s) of the script (when signing is concerned)
		redeemscript := packet.Inputs[i].NonWitnessUtxo.TxOut[packet.UnsignedTx.TxIn[i].PreviousOutPoint.Index].PkScript
		assert.Require(script.IsP2PKHRedeemScript(redeemscript), "Expected P2PKH redeem-script")
		log.Traceln("PubKeyScript:", hex.EncodeToString(redeemscript))

		var sig []byte
		// TODO in future: defer to walleTKey for signing (determine efficient, compact custom communication format defined for single type)
		// FIXME assumes SIGHASH_ALL
		// FIXME we should determine the proper subscript here, for certain (only non-standard?) cases
		sig, err = tx.Sign(uint(i), redeemscript, txn.SIGHASH_ALL, childkey)
		assert.Success(err, "Failed to sign input")
		err = tx.CheckSignature(uint(i), redeemscript, sig, childpubkeybytes)
		assert.Success(err, "Produced signature failed verification")

		var signret psbt.SignOutcome
		signret, err = updater.Sign(i, sig, childpubkeybytes, nil, nil)
		assert.Success(err, "Failed to sign input "+strconv.FormatIntDecimal(i))
		assert.Require(signret >= psbt.SignSuccesful, "Updater: signature injection failure")
		log.Traceln("Input signing result:", signret)
	}
	return packet, nil
}
