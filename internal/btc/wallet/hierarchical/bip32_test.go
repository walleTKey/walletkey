package hierarchical

import (
	"testing"

	"github.com/cobratbq/goutils/codec/bytes/hex"
	"github.com/cobratbq/goutils/codec/string/base58"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/slices"
	assert "github.com/cobratbq/goutils/std/testing"
	"github.com/mit-dci/lit/crypto/koblitz"
)

func TestBuildingBlocks(t *testing.T) {
	var err error
	var m, _, chain = GenerateMasterKey(hex.MustDecodeString("000102030405060708090a0b0c0d0e0f"))
	var childm *koblitz.PrivateKey
	var childchain ChainCode
	childm, childchain, err = CKDPriv(m, chain, 0)
	assert.Nil(t, err)
	var childM *koblitz.PublicKey
	var childChain ChainCode
	childM, childChain = CKDPub(m.PubKey(), chain, 0)
	assert.SlicesEqual(t, childchain[:], childChain[:])
	assert.SlicesEqual(t, childm.PubKey().SerializeUncompressed(), childM.SerializeUncompressed())
	var ccm *koblitz.PrivateKey
	var ccchain ChainCode
	ccm, ccchain, err = CKDPriv(childm, childchain, 999)
	assert.Nil(t, err)
	var ccM *koblitz.PublicKey
	var ccChain ChainCode
	ccM, ccChain = CKDPub(childM, childChain, 999)
	assert.SlicesEqual(t, ccchain[:], ccChain[:])
	assert.SlicesEqual(t, ccm.PubKey().SerializeUncompressed(), ccM.SerializeUncompressed())
}

func TestDerivations(t *testing.T) {
	var err error
	testdata := []struct {
		seed   string
		path   []uint32
		extpub string
		extprv string
	}{
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{}, extpub: "xpub661MyMwAqRbcFtXgS5sYJABqqG9YLmC4Q1Rdap9gSE8NqtwybGhePY2gZ29ESFjqJoCu1Rupje8YtGqsefD265TMg7usUDFdp6W1EGMcet8", extprv: "xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi"},
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{0 | HARDENED}, extpub: "xpub68Gmy5EdvgibQVfPdqkBBCHxA5htiqg55crXYuXoQRKfDBFA1WEjWgP6LHhwBZeNK1VTsfTFUHCdrfp1bgwQ9xv5ski8PX9rL2dZXvgGDnw", extprv: "xprv9uHRZZhk6KAJC1avXpDAp4MDc3sQKNxDiPvvkX8Br5ngLNv1TxvUxt4cV1rGL5hj6KCesnDYUhd7oWgT11eZG7XnxHrnYeSvkzY7d2bhkJ7"},
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{0 | HARDENED, 1}, extpub: "xpub6ASuArnXKPbfEwhqN6e3mwBcDTgzisQN1wXN9BJcM47sSikHjJf3UFHKkNAWbWMiGj7Wf5uMash7SyYq527Hqck2AxYysAA7xmALppuCkwQ", extprv: "xprv9wTYmMFdV23N2TdNG573QoEsfRrWKQgWeibmLntzniatZvR9BmLnvSxqu53Kw1UmYPxLgboyZQaXwTCg8MSY3H2EU4pWcQDnRnrVA1xe8fs"},
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{0 | HARDENED, 1, 2 | HARDENED}, extpub: "xpub6D4BDPcP2GT577Vvch3R8wDkScZWzQzMMUm3PWbmWvVJrZwQY4VUNgqFJPMM3No2dFDFGTsxxpG5uJh7n7epu4trkrX7x7DogT5Uv6fcLW5", extprv: "xprv9z4pot5VBttmtdRTWfWQmoH1taj2axGVzFqSb8C9xaxKymcFzXBDptWmT7FwuEzG3ryjH4ktypQSAewRiNMjANTtpgP4mLTj34bhnZX7UiM"},
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{0 | HARDENED, 1, 2 | HARDENED, 2}, extpub: "xpub6FHa3pjLCk84BayeJxFW2SP4XRrFd1JYnxeLeU8EqN3vDfZmbqBqaGJAyiLjTAwm6ZLRQUMv1ZACTj37sR62cfN7fe5JnJ7dh8zL4fiyLHV", extprv: "xprvA2JDeKCSNNZky6uBCviVfJSKyQ1mDYahRjijr5idH2WwLsEd4Hsb2Tyh8RfQMuPh7f7RtyzTtdrbdqqsunu5Mm3wDvUAKRHSC34sJ7in334"},
		{seed: "000102030405060708090a0b0c0d0e0f", path: []uint32{0 | HARDENED, 1, 2 | HARDENED, 2, 1000000000}, extpub: "xpub6H1LXWLaKsWFhvm6RVpEL9P4KfRZSW7abD2ttkWP3SSQvnyA8FSVqNTEcYFgJS2UaFcxupHiYkro49S8yGasTvXEYBVPamhGW6cFJodrTHy", extprv: "xprvA41z7zogVVwxVSgdKUHDy1SKmdb533PjDz7J6N6mV6uS3ze1ai8FHa8kmHScGpWmj4WggLyQjgPie1rFSruoUihUZREPSL39UNdE3BBDu76"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{}, extpub: "xpub661MyMwAqRbcFW31YEwpkMuc5THy2PSt5bDMsktWQcFF8syAmRUapSCGu8ED9W6oDMSgv6Zz8idoc4a6mr8BDzTJY47LJhkJ8UB7WEGuduB", extprv: "xprv9s21ZrQH143K31xYSDQpPDxsXRTUcvj2iNHm5NUtrGiGG5e2DtALGdso3pGz6ssrdK4PFmM8NSpSBHNqPqm55Qn3LqFtT2emdEXVYsCzC2U"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{0}, extpub: "xpub69H7F5d8KSRgmmdJg2KhpAK8SR3DjMwAdkxj3ZuxV27CprR9LgpeyGmXUbC6wb7ERfvrnKZjXoUmmDznezpbZb7ap6r1D3tgFxHmwMkQTPH", extprv: "xprv9vHkqa6EV4sPZHYqZznhT2NPtPCjKuDKGY38FBWLvgaDx45zo9WQRUT3dKYnjwih2yJD9mkrocEZXo1ex8G81dwSM1fwqWpWkeS3v86pgKt"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{0, 2147483647 | HARDENED}, extpub: "xpub6ASAVgeehLbnwdqV6UKMHVzgqAG8Gr6riv3Fxxpj8ksbH9ebxaEyBLZ85ySDhKiLDBrQSARLq1uNRts8RuJiHjaDMBU4Zn9h8LZNnBC5y4a", extprv: "xprv9wSp6B7kry3Vj9m1zSnLvN3xH8RdsPP1Mh7fAaR7aRLcQMKTR2vidYEeEg2mUCTAwCd6vnxVrcjfy2kRgVsFawNzmjuHc2YmYRmagcEPdU9"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{0, 2147483647 | HARDENED, 1}, extpub: "xpub6DF8uhdarytz3FWdA8TvFSvvAh8dP3283MY7p2V4SeE2wyWmG5mg5EwVvmdMVCQcoNJxGoWaU9DCWh89LojfZ537wTfunKau47EL2dhHKon", extprv: "xprv9zFnWC6h2cLgpmSA46vutJzBcfJ8yaJGg8cX1e5StJh45BBciYTRXSd25UEPVuesF9yog62tGAQtHjXajPPdbRCHuWS6T8XA2ECKADdw4Ef"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{0, 2147483647 | HARDENED, 1, 2147483646 | HARDENED}, extpub: "xpub6ERApfZwUNrhLCkDtcHTcxd75RbzS1ed54G1LkBUHQVHQKqhMkhgbmJbZRkrgZw4koxb5JaHWkY4ALHY2grBGRjaDMzQLcgJvLJuZZvRcEL", extprv: "xprvA1RpRA33e1JQ7ifknakTFpgNXPmW2YvmhqLQYMmrj4xJXXWYpDPS3xz7iAxn8L39njGVyuoseXzU6rcxFLJ8HFsTjSyQbLYnMpCqE2VbFWc"},
		{seed: "fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542", path: []uint32{0, 2147483647 | HARDENED, 1, 2147483646 | HARDENED, 2}, extpub: "xpub6FnCn6nSzZAw5Tw7cgR9bi15UV96gLZhjDstkXXxvCLsUXBGXPdSnLFbdpq8p9HmGsApME5hQTZ3emM2rnY5agb9rXpVGyy3bdW6EEgAtqt", extprv: "xprvA2nrNbFZABcdryreWet9Ea4LvTJcGsqrMzxHx98MMrotbir7yrKCEXw7nadnHM8Dq38EGfSh6dqA9QWTyefMLEcBYJUuekgW4BYPJcr9E7j"},
		{seed: "4b381541583be4423346c643850da4b320e46a87ae3d2a4e6da11eba819cd4acba45d239319ac14f863b8d5ab5a0d0c64d2e8a1e7d1457df2e5a3c51c73235be", path: []uint32{}, extpub: "xpub661MyMwAqRbcEZVB4dScxMAdx6d4nFc9nvyvH3v4gJL378CSRZiYmhRoP7mBy6gSPSCYk6SzXPTf3ND1cZAceL7SfJ1Z3GC8vBgp2epUt13", extprv: "xprv9s21ZrQH143K25QhxbucbDDuQ4naNntJRi4KUfWT7xo4EKsHt2QJDu7KXp1A3u7Bi1j8ph3EGsZ9Xvz9dGuVrtHHs7pXeTzjuxBrCmmhgC6"},
		{seed: "4b381541583be4423346c643850da4b320e46a87ae3d2a4e6da11eba819cd4acba45d239319ac14f863b8d5ab5a0d0c64d2e8a1e7d1457df2e5a3c51c73235be", path: []uint32{0 | HARDENED}, extpub: "xpub68NZiKmJWnxxS6aaHmn81bvJeTESw724CRDs6HbuccFQN9Ku14VQrADWgqbhhTHBaohPX4CjNLf9fq9MYo6oDaPPLPxSb7gwQN3ih19Zm4Y", extprv: "xprv9uPDJpEQgRQfDcW7BkF7eTya6RPxXeJCqCJGHuCJ4GiRVLzkTXBAJMu2qaMWPrS7AANYqdq6vcBcBUdJCVVFceUvJFjaPdGZ2y9WACViL4L"},
		{seed: "3ddd5602285899a946114506157c7997e5444528f3003f6134712147db19b678", path: []uint32{}, extpub: "xpub661MyMwAqRbcGczjuMoRm6dXaLDEhW1u34gKenbeYqAix21mdUKJyuyu5F1rzYGVxyL6tmgBUAEPrEz92mBXjByMRiJdba9wpnN37RLLAXa", extprv: "xprv9s21ZrQH143K48vGoLGRPxgo2JNkJ3J3fqkirQC2zVdk5Dgd5w14S7fRDyHH4dWNHUgkvsvNDCkvAwcSHNAQwhwgNMgZhLtQC63zxwhQmRv"},
		{seed: "3ddd5602285899a946114506157c7997e5444528f3003f6134712147db19b678", path: []uint32{0 | HARDENED}, extpub: "xpub69AUMk3qDBi3uW1sXgjCmVjJ2G6WQoYSnNHyzkmdCHEhSZ4tBok37xfFEqHd2AddP56Tqp4o56AePAgCjYdvpW2PU2jbUPFKsav5ut6Ch1m", extprv: "xprv9vB7xEWwNp9kh1wQRfCCQMnZUEG21LpbR9NPCNN1dwhiZkjjeGRnaALmPXCX7SgjFTiCTT6bXes17boXtjq3xLpcDjzEuGLQBM5ohqkao9G"},
		{seed: "3ddd5602285899a946114506157c7997e5444528f3003f6134712147db19b678", path: []uint32{0 | HARDENED, 1 | HARDENED}, extpub: "xpub6BJA1jSqiukeaesWfxe6sNK9CCGaujFFSJLomWHprUL9DePQ4JDkM5d88n49sMGJxrhpjazuXYWdMf17C9T5XnxkopaeS7jGk1GyyVziaMt", extprv: "xprv9xJocDuwtYCMNAo3Zw76WENQeAS6WGXQ55RCy7tDJ8oALr4FWkuVoHJeHVAcAqiZLE7Je3vZJHxspZdFHfnBEjHqU5hG1Jaj32dVoS6XLT1"},
	}
	for i := range testdata {
		t.Log("Entry", i)
		prv, _, chain := GenerateMasterKey(hex.MustDecodeString(testdata[i].seed))
		var masterXpub = XpubType{
			Prefix: PREFIX_XPUB,
			Depth:  0,
			Index:  0,
			Parent: [4]byte{},
			Pubkey: prv.PubKey(),
			Chain:  chain}
		var masterXpubEncoded = masterXpub.Encode()
		// FIXME XPRV not available atm. ':-)
		//var masterXprv = Xprv(PREFIX_XPRV, 0, 0, prv, chain, nil)
		var xpub XpubType
		_, xpub, err = DerivePrivateKey(true, prv, chain, testdata[i].path)
		assert.Nil(t, err)
		var encoded = xpub.Encode()
		if len(testdata[i].path) == 0 {
			assert.SlicesEqual(t, masterXpubEncoded[:], encoded[:])
			//assert.SlicesEqual(t, masterXprv[:], xprv[:])
		}
		assert.SlicesEqual(t, builtin.Expect(base58.CheckDecode([]byte(testdata[i].extpub))), encoded[:])
		//assert.SlicesEqual(t, builtin.Expect(base58.CheckDecode([]byte(d.extprv))), xprv[:])
		var inter = slices.LastIndexFunc(testdata[i].path, func(v uint32) bool { return (v & HARDENED) > 0 }) + 1
		if len(testdata[i].path[inter:]) == 0 {
			// If all indices on the derivation path are hardened, then there is no testing of a partially
			// public derivation.
			continue
		}
		var interprv *koblitz.PrivateKey
		var interxpub XpubType
		interprv, interxpub, err = DerivePrivateKey(true, prv, chain, testdata[i].path[:inter])
		assert.Nil(t, err)
		assert.SlicesEqual(t, interprv.PubKey().SerializeCompressed(), interxpub.Pubkey.SerializeCompressed())
		xpub = DerivePublicChild(true, len(testdata[i].path[:inter]), interprv.PubKey(), interxpub.Chain, testdata[i].path[inter:])
		encoded = xpub.Encode()
		assert.SlicesEqual(t, builtin.Expect(base58.CheckDecode([]byte(testdata[i].extpub))), encoded[:])
	}
}
