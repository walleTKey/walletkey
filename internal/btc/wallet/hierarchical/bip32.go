package hierarchical

import (
	mac "crypto/hmac"
	"crypto/sha512"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/curve"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/mit-dci/lit/crypto/koblitz"
)

// FIXME convert to lowercase to prevent exporting as public api?
const XPUB_ENCODED_LENGTH int = 78

// FIXME convert to lowercase to prevent exporting as public api?
var PREFIX_XPUB [4]byte = [...]byte{0x04, 0x88, 0xb2, 0x1e}
var PREFIX_XPRV [4]byte = [...]byte{0x04, 0x88, 0xad, 0xe4}
var PREFIX_TPUB [4]byte = [...]byte{0x04, 0x35, 0x87, 0xcf}
var PREFIX_TPRV [4]byte = [...]byte{0x04, 0x35, 0x83, 0x94}

func DeterminePrefix(mainnet bool) [4]byte {
	if mainnet {
		return PREFIX_XPUB
	} else {
		return PREFIX_TPUB
	}
}

type ChainCode = [32]byte

type XpubType struct {
	Prefix [4]byte
	Depth  uint8
	Parent btc.FingerprintType
	Index  uint32
	Chain  ChainCode
	Pubkey *koblitz.PublicKey
}

func (x *XpubType) Encode() [XPUB_ENCODED_LENGTH]byte {
	var address [XPUB_ENCODED_LENGTH]byte
	// Extended public and private keys are serialized as follows:
	// 4 byte: version bytes (mainnet: 0x0488B21E public, 0x0488ADE4 private; testnet: 0x043587CF public,
	// 0x04358394 private)
	address[0], address[1], address[2], address[3] = x.Prefix[0], x.Prefix[1], x.Prefix[2], x.Prefix[3]
	// 1 byte: depth: 0x00 for master nodes, 0x01 for level-1 derived keys, ....
	address[4] = x.Depth
	// 4 bytes: the fingerprint of the parent's key (0x00000000 if master key)
	address[5], address[6], address[7], address[8] = x.Parent[0], x.Parent[1], x.Parent[2], x.Parent[3]
	// 4 bytes: child number. This is ser32(i) for i in xi = xpar/i, with xi the key being serialized.
	// (0x00000000 if master key)
	address[9], address[10], address[11], address[12] = byte(x.Index>>24), byte(x.Index>>16), byte(x.Index>>8), byte(x.Index)
	// 32 bytes: the chain code
	copy(address[13:45], x.Chain[:])
	// 33 bytes: the public key or private key data (serP(K) for public keys, 0x00 || ser256(k) for private
	// keys)
	curve.SerializePointInto(address[45:], x.Pubkey)
	return address
}

// FIXME temporarily commented out `Xprv` encoding
// func Xprv(prefix [4]byte, depth uint8, index uint32, privkey *koblitz.PrivateKey, chaincode ChainCode, parent *koblitz.PublicKey) XPUB {
// 	var address XPUB
// 	// Extended public and private keys are serialized as follows:
// 	// 4 byte: version bytes (mainnet: 0x0488B21E public, 0x0488ADE4 private; testnet: 0x043587CF public,
// 	// 0x04358394 private)
// 	address[0], address[1], address[2], address[3] = prefix[0], prefix[1], prefix[2], prefix[3]
// 	// 1 byte: depth: 0x00 for master nodes, 0x01 for level-1 derived keys, ....
// 	address[4] = depth
// 	// 4 bytes: the fingerprint of the parent's key (0x00000000 if master key)
// 	var parentFp btc.FingerprintType
// 	if parent != nil {
// 		parentFp = btc.Fingerprint(parent.SerializeCompressed())
// 	}
// 	address[5], address[6], address[7], address[8] = parentFp[0], parentFp[1], parentFp[2], parentFp[3]
// 	// 4 bytes: child number. This is ser32(i) for i in xi = xpar/i, with xi the key being serialized.
// 	// (0x00000000 if master key)
// 	address[9], address[10], address[11], address[12] = byte(index>>24), byte(index>>16), byte(index>>8), byte(index)
// 	// 32 bytes: the chain code
// 	copy(address[13:45], chaincode[:])
// 	// 33 bytes: the public key or private key data (serP(K) for public keys, 0x00 || ser256(k) for private
// 	// keys)
// 	address[45] = 0x00
// 	curve.SerializeScalarInto(address[46:78], privkey)
// 	return address
// }

func hmac(key []byte, data ...[]byte) [64]byte {
	hmac512 := mac.New(sha512.New, key)
	for i := range data {
		hmac512.Write(data[i])
	}
	return [64]byte(hmac512.Sum(nil))
}

// GenerateMasterKey generates the master-key for hierarchical deterministic wallet.
// Returns: private-key, public-key, chaincode
func GenerateMasterKey(seed []byte) (*koblitz.PrivateKey, *koblitz.PublicKey, ChainCode) {
	assert.AtLeast(16, len(seed))
	var I = hmac([]byte("Bitcoin seed"), seed)
	var privkey, pubkey = koblitz.PrivKeyFromBytes(koblitz.S256(), I[:32])
	return privkey, pubkey, ChainCode(I[32:])
}

const HARDENED uint32 = 0x80000000

func serializeUint32(v uint32) [4]byte {
	return [4]byte{byte(v >> 24), byte(v >> 16), byte(v >> 8), byte(v)}
}

func copy32(b []byte) (result [32]byte) {
	assert.Equal(32, len(b))
	copy(result[:], b)
	return
}

// TODO needs to be public/exported?
func CKDPriv(k *koblitz.PrivateKey, c ChainCode, i uint32) (*koblitz.PrivateKey, ChainCode, error) {
	var seri = serializeUint32(i)
	var I [64]byte
	if i >= HARDENED {
		serk := curve.SerializeScalar(k)
		I = hmac(c[:], []byte{0}, serk[:], seri[:])
	} else {
		serK := curve.SerializePoint(k.PubKey())
		I = hmac(c[:], serK[:], seri[:])
	}
	s, _ := curve.ParseScalar(I[:32])
	// FIXME proper error handling
	childk := builtin.Expect(curve.AddScalar(k, s))
	// FIXME parse_256(IL) >= n || k_i == 0 is bad
	return childk, copy32(I[32:]), nil
}

// TODO needs to be public/exported?
func CKDPub(K *koblitz.PublicKey, c ChainCode, i uint32) (*koblitz.PublicKey, ChainCode) {
	if i >= HARDENED {
		panic("Illegal input: cannot derive hardened public child.")
	}
	var serK, seri = curve.SerializePoint(K), serializeUint32(i)
	I := hmac(c[:], serK[:], seri[:])
	// FIXME parse_256(I_L) >= n || K_i is infinity is bad
	var _, P = curve.ParseScalar(I[:32])
	var childK = builtin.Expect(curve.AddPoint(K, P))
	return childK, copy32(I[32:])
}

func prefixes(mainnet bool) ([4]byte, [4]byte) {
	if mainnet {
		return PREFIX_XPUB, PREFIX_XPRV
	} else {
		return PREFIX_TPUB, PREFIX_TPRV
	}
}

func DerivePublicChild(mainnet bool, basedepth int, key *koblitz.PublicKey, chaincode ChainCode, path []uint32) XpubType {
	assert.Unequal(0, len(path))
	var prefix, _ = prefixes(mainnet)
	var currK, currc = key, chaincode
	var prevK *koblitz.PublicKey
	var index uint32
	for _, index = range path {
		prevK = currK
		currK, currc = CKDPub(currK, currc, index)
	}
	return XpubType{
		Prefix: prefix,
		Depth:  uint8(basedepth + len(path)),
		Index:  index,
		Parent: btc.DeriveFingerprint(prevK.SerializeCompressed()),
		Chain:  currc,
		Pubkey: currK,
	}
}

// The function CKDpub((Kpar, cpar), i) → (Ki, ci) computes a child extended public key from the parent extended public key. It is only defined for non-hardened child keys.
//
// Check whether i ≥ 231 (whether the child is a hardened key).
//
//	If so (hardened child): return failure
//	If not (normal child): let I = HMAC-SHA512(Key = cpar, Data = serP(Kpar) || ser32(i)).
//
// Split I into two 32-byte sequences, IL and IR.
// The returned child key Ki is point(parse256(IL)) + Kpar.
// The returned chain code ci is IR.
// In case parse256(IL) ≥ n or Ki is the point at infinity, the resulting key is invalid, and one should proceed with the next value for i.
func DerivePublicKey(mainnet bool, master *koblitz.PublicKey, chaincode ChainCode, path []uint32) XpubType {
	var prefix, _ = prefixes(mainnet)
	if len(path) == 0 {
		return XpubType{Prefix: prefix, Depth: 0, Index: 0, Parent: [4]byte{}, Chain: chaincode, Pubkey: master}
	}
	return DerivePublicChild(mainnet, 0, master, chaincode, path)
}

func DerivePrivateChild(mainnet bool, basedepth int, key *koblitz.PrivateKey, chaincode ChainCode, path []uint32) (*koblitz.PrivateKey, XpubType, error) {
	var currk, currc = key, chaincode
	var prevK *koblitz.PublicKey
	var index uint32
	for _, index = range path {
		prevK = currk.PubKey()
		var err error
		if currk, currc, err = CKDPriv(currk, currc, index); err != nil {
			return nil, XpubType{}, err
		}
	}
	var prefixPub, _ = prefixes(mainnet)
	var xpub = XpubType{
		Prefix: prefixPub,
		Depth:  uint8(basedepth + len(path)),
		Index:  index,
		Parent: btc.DeriveFingerprint(prevK.SerializeCompressed()),
		Chain:  currc,
		Pubkey: currk.PubKey(),
	}
	// FIXME disabled XPRV
	//var xprv = Xprv(prefixPrv, uint8(basedepth+len(path)), index, currk, currc, prevK)
	return currk, xpub, nil
}

// The function CKDpriv((kpar, cpar), i) → (ki, ci) computes a child extended private key from the parent extended private key:
//
// Check whether i ≥ 2**31 (whether the child is a hardened key).
//
//	If so (hardened child): let I = HMAC-SHA512(Key = cpar, Data = 0x00 || ser256(kpar) || ser32(i)). (Note: The 0x00 pads the private key to make it 33 bytes long.)
//	If not (normal child): let I = HMAC-SHA512(Key = cpar, Data = serP(point(kpar)) || ser32(i)).
//
// Split I into two 32-byte sequences, I_L and I_R.
// The returned child key ki is parse256(I_L) + kpar (mod n).
// The returned chain code ci is I_R.
// In case parse256(I_L) ≥ n or ki = 0, the resulting key is invalid, and one should proceed with the next value for i. (Note: this has probability lower than 1 in 2127.)
func DerivePrivateKey(mainnet bool, m *koblitz.PrivateKey, chaincode ChainCode, path []uint32) (*koblitz.PrivateKey, XpubType, error) {
	if len(path) == 0 {
		var prefixPub, _ = prefixes(mainnet)
		var xpub = XpubType{
			Prefix: prefixPub,
			Depth:  0,
			Index:  0,
			Parent: [4]byte{},
			Chain:  chaincode,
			Pubkey: m.PubKey(),
		}
		return m, xpub, nil
	}
	return DerivePrivateChild(mainnet, 0, m, chaincode, path)
}
