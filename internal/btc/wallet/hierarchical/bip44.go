package hierarchical

import (
	"github.com/cobratbq/goutils/assert"
	"github.com/mit-dci/lit/crypto/koblitz"
)

// BIP-44 purpose: Deterministic Wallet
// 44' = 44h = purpose 44 hardened = 44 + 0x80000000 = 0x8000002C
// See: <https://bips.dev/44/>
const PURPOSE_DETERMINISTIC_WALLETS uint32 = 44 | HARDENED

// BIP-44 coin-type: Bitcoin
// 0' = 0h = coin-type 0 hardened = 0 + 0x80000000 = 0x80000000
// See: <https://bips.dev/44/>
const COINTYPE_BITCOIN uint32 = 0 | HARDENED
const COINTYPE_BITCOIN_TESTNET uint32 = 1 | HARDENED

const CHAIN_PUBLIC uint32 = 0
const CHAIN_INTERNAL uint32 = 1

func GenerateAccountPath(main bool, account uint32) []uint32 {
	var cointype uint32
	if main {
		cointype = COINTYPE_BITCOIN
	} else {
		cointype = COINTYPE_BITCOIN_TESTNET
	}
	return []uint32{PURPOSE_DETERMINISTIC_WALLETS, cointype, account | HARDENED}
}

func DeriveAccount(m *koblitz.PrivateKey, chaincode ChainCode, cointype, account uint32) (*koblitz.PrivateKey, XpubType, error) {
	// TODO temporarily, at least, add assertion to ensure account is specified as hardened value.
	assert.Any(cointype, COINTYPE_BITCOIN, COINTYPE_BITCOIN_TESTNET)
	assert.Unequal(0, cointype&HARDENED)
	assert.Unequal(0, account&HARDENED)
	return DerivePrivateKey(cointype == COINTYPE_BITCOIN, m, chaincode, []uint32{PURPOSE_DETERMINISTIC_WALLETS, cointype, account})
}

func DeriveWallet(m *koblitz.PrivateKey, chaincode ChainCode, cointype, account, chain, index uint32) (*koblitz.PrivateKey, XpubType, error) {
	assert.Any(cointype, COINTYPE_BITCOIN, COINTYPE_BITCOIN_TESTNET)
	assert.Unequal(0, cointype&HARDENED)
	assert.Unequal(0, account&HARDENED)
	return DerivePrivateKey(cointype == COINTYPE_BITCOIN, m, chaincode, []uint32{PURPOSE_DETERMINISTIC_WALLETS, cointype, account, chain, index})
}
