# BTC notes

`FIXME currently updated to reflect traditional bitcoin mechanics only, i.e. not SegWit or Taproot`

## Transaction-types

- __P2PKH__ pays to a specific public-key hash, where `sigscript` provides the signature and public key, and `pubkeyscript` contains the verification instructions that expect the correct signature and public key to be present.
- __P2SH__ pays to a script-hash, i.e. inverted mechanics where `pubkeyscript` expects a certain script to be present on the stack, identified by its hash, and `sigscript` carries the script. (Useful if you want to off-load the complexities of verification to the consuming transaction. This is used to produce a small script that is scannable by QR-code, while strictly tied to the expected payment/access restrictions.)
- `FIXME to do`

## Transaction signing

`sig = <DER-encoded signature> || <1-byte hash-type>`

`hash-type` consists of:

- `SIGHASH_ALL = 1` all outputs are present for the signature
- `SIGHASH_NONE = 2` no outputs are present for the signature
- `SIGHASH_SINGLE = 3` only the corresponding output is present for the signature

with optional flag:

- `FLAG_ANYONECANPAY = 0x80` if set, indicates that only own input itself gets signed. (Allows free-form payment contributions by other people without invalidating the signature.)

Bitcoin takes the approach of transaction signing by stripping or "resetting" parts of the transaction that aren't of immediate concern for the signature. This means that whenever a transaction is signed/checked, it must be selectively hashed. Which sections are stripped/reset, is defined by the _hash-type_ corresponding to the signature. Values that are stripped/reset are consequently not "pinned down" to a particular value by the signature, allowing some parts of the changeable, e.g. in case of multiple signers.

Signature validation is a component in execution of the scripts. A full or partial script is included in the signed content. The preparation and selection of (sub)script is part of script execution.

NOTE: all serialization uses little-endian byte-order (for integer values).

1.  Execute `sigscript` (script of the current transaction's input) followed by `pubkeyscript` (script of the previous transaction's output, i.e. the address that feeds the current transaction's input). As the scripts execute, a stack is populated and between executions data will remain on the stack.
1.  `FIXME ...`
1.  `OP_CODESEPARATOR` indicates a new starting position for the (sub)script that is signed. All script-parts before this `OP_CODESEPARATOR` are not part of signed content. Until an `OP_CODESEPARATOR` is encountered, the full script is considered signed content.
1.  (non-standard) Strictly speaking, any occurrences of `sig` in the (sub)script should be removed. However, no standard script contains occurrences of `sig`, so this only applies to non-standard uses of Bitcoin-script.
1.  `OP_CODESEPARATOR` must be filtered out in the remaining subscript. Note that simply replacing specific bytes is not sufficient as the script may (also) contain arbitrary data.
1.  Separate ASN.1-encoded DER signature from hash-type.
1.  Copy transaction for modification, or selectively feed parts of transaction into hashing function.
1.  Set all transaction input scripts to empty. (_Compact-size_ varint length of `0` with no script-body bytes)
1.  Use the (sub)script, as indicated in steps above, as script for the input (index) currently checked, make sure that script-length is adjusted to match.
1.  Serialize (modified) transaction, then append 4-byte `hash-type`.
1.  Verify the signature against the serialization: `ECDSA_VerifySignature(pubkey, signature, sha256(sha256(serialized-transaction)))` (hashed transaction-serialization must be Big-Endian, i.e. the natural hash result)
1.  `FIXME ...`

`FIXME include instructions that reset sequencenr, include instructions for sighash-{none,single}, ...`

`FIXME note that hash-type is hashed as 4-byte Little-Endian value, as opposed to the original 1-byte value.`

## ASN.1 encoded DER signatures

`FIXME rough notes, need compiling`

ASN.1-encoded DER signature: `0x30` (sequence) || `0x46` (sequence-length in bytes) || `0x02` (type: INTEGER) || `0x21` (length of integer R in bytes) || `<bytes-of-integer>` || `0x02` (type: INTEGER) || `0x21` (length of integer S in bytes) || `<bytes-of-integer>` 

## Arbitrary notes

- _Fingerprint_ is first 4 bytes of `RIPEMD160(SHA256(wallet-public-key)`

## References

- `interpreter.c` and `interpreter.h` of Bitcoin Core code-base.
- <https://en.bitcoin.it/wiki/Script>
- <https://en.bitcoin.it/wiki/OP_CHECKSIG>
- <https://en.bitcoin.it/w/images/en/7/70/Bitcoin_OpCheckSig_InDetail.png>
