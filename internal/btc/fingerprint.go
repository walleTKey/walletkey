package btc

import (
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"

	rmd "golang.org/x/crypto/ripemd160"

	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/builtin"
)

type FingerprintType [4]byte

func (fp *FingerprintType) Uint32() uint32 {
	return binary.LittleEndian.Uint32(fp[:])
}

func (fp *FingerprintType) Hex() string {
	return hex.EncodeToString(fp[:])
}

// Fingerprint calculates the fingerprint of a public key.
//
// Note: for hiearchical wallets, requires fingerprinting the Compressed Public Key encoding (33 bytes).
func DeriveFingerprint(data []byte) FingerprintType {
	checksum := sha256.Sum256(data)
	hash := rmd.New()
	n := builtin.Expect(hash.Write(checksum[:]))
	assert.Equal(n, len(checksum))
	id := hash.Sum(nil)
	return FingerprintType(id[:4])
}
