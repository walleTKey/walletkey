/*
Specification

The Partially Signed Bitcoin Transaction (PSBT) format consists of key-value maps. Each map consists of a sequence of key-value records, terminated by a 0x00 byte Why is the separator here 0x00 instead of 0xff? The separator here is used to distinguish between each chunk of data. A separator of 0x00 would mean that the unserializer can read it as a key length of 0, which would never occur with actual keys. It can thus be used as a separator and allow for easier unserializer implementation..

    <psbt> := <magic> <global-map> <input-map>* <output-map>*
    <magic> := 0x70 0x73 0x62 0x74 0xFF
    <global-map> := <keypair>* 0x00
    <input-map> := <keypair>* 0x00
    <output-map> := <keypair>* 0x00
    <keypair> := <key> <value>
    <key> := <keylen> <keytype> <keydata>
    <value> := <valuelen> <valuedata>

*/
// Bitcoin PSBT support (Partially Signed Bitcoin Transactions)
package psbt

const (
	idGlobalUnsignedTX       uint = 0x00
	idGlobalXPUB             uint = 0x01
	idGlobalTXVersion        uint = 0x02
	idGlobalFallbackLocktime uint = 0x03
	idGlobalInputCount       uint = 0x04
	idGlobalOutputCount      uint = 0x05
	idGlobalTXModifiable     uint = 0x06
	idGlobalVersion          uint = 0xfb
	idGlobalProprietary      uint = 0xfc
)

const (
	idInNonWitnessUTXO           uint = 0x00
	idInWitnessUTXO              uint = 0x01
	idInPartialSig               uint = 0x02
	idInSighashType              uint = 0x03
	idInRedeemScript             uint = 0x04
	idInWitnessScript            uint = 0x05
	idInBIP32Derivation          uint = 0x06
	idInFinalSigscript           uint = 0x07
	idInFinalScriptWitness       uint = 0x08
	idInPoRCommitment            uint = 0x09
	idInRIPEMD160                uint = 0x0a
	idInSHA256                   uint = 0x0b
	idInHASH160                  uint = 0x0c
	idInHASH256                  uint = 0x0d
	idInPreviousTXID             uint = 0x0e
	idInPreviousOutIDX           uint = 0x0f
	idInSequence                 uint = 0x10
	idInRequiredTimeLocktime     uint = 0x11
	idInRequiredHeightLocktime   uint = 0x12
	idInTapKeySig                uint = 0x13
	idInTapScriptSig             uint = 0x14
	idInTapLeafScript            uint = 0x15
	idInTapBIP32Derivation       uint = 0x16
	idInTapInternalKey           uint = 0x17
	idInTapMerkleRoot            uint = 0x18
	idInMusig2ParticipantPubkeys uint = 0x1a
	idInMusig2PubNonce           uint = 0x1b
	idInMusig2PartialSig         uint = 0x1c
	idInProprietary              uint = 0xfc
)

const (
	idOutRedeemScript             uint = 0x00
	idOutWitnessScript            uint = 0x01
	idOutBIP32Derivation          uint = 0x02
	idOutAmount                   uint = 0x03
	idOutScript                   uint = 0x04
	idOutTapInternalKey           uint = 0x05
	idOutTapTree                  uint = 0x06
	idOutTapBIP32Derivation       uint = 0x07
	idOutMusig2ParticipantPubkeys uint = 0x08
	idOutProprietary              uint = 0xfc
)
