package txn

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/sha256"
	"crypto/subtle"
	"encoding/binary"
	"math"
	"slices"

	"codeberg.org/walletkey/walletkey/internal/curve"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/compactsize"
	le "github.com/cobratbq/goutils/codec/bytes/littleendian"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
	"github.com/mit-dci/lit/crypto/koblitz"
)

const MASK_SIGHASH uint8 = 0x7f
const FLAG_ANYONECANPAY uint8 = 0x80
const SIGHASH_ALL uint8 = 0x01
const SIGHASH_NONE uint8 = 0x02
const SIGHASH_SINGLE uint8 = 0x03

type Input struct {
	Hash     [32]byte
	Index    uint32
	Script   []byte
	Sequence uint32
}

// TODO check buffer availability before each read
// TODO check counts for absurdly large values, use as indicator for problems. (Err on side of safety, can always be changed for extraordinary situations.)
func readInput(data []byte) (Input, uint, error) {
	var in Input
	// <32-byte outpoint-hash><4-byte outpoint-index><1-9-byte script-length><script-signature><4-byte sequencenr>
	copy(in.Hash[:], data[:32])
	in.Index = binary.LittleEndian.Uint32(data[32:36])
	var n uint
	var length uint16
	if length, n = compactsize.DecodeUint16(data[36:]); n == 0 {
		return Input{}, 0, errors.Context(errors.ErrIllegal, "size of script")
	}
	in.Script = bytes.Clone(data[36+n : 36+n+uint(length)])
	in.Sequence = binary.LittleEndian.Uint32(data[36+n+uint(length) : 36+n+uint(length)+4])
	return in, 36 + n + uint(length) + 4, nil
}

func readInputs(data []byte) ([]Input, uint, error) {
	var count uint16
	var pos uint
	var n uint
	if count, n = compactsize.DecodeUint16(data); n == 0 {
		return nil, 0, errors.Context(errors.ErrIllegal, "number of inputs")
	}
	pos += n
	log.Traceln("Reading", count, "inputs")
	inputs := make([]Input, count)
	var err error
	for i := 0; i < int(count); i++ {
		if inputs[i], n, err = readInput(data[pos:]); err != nil {
			return nil, 0, errors.Context(err, "reading input "+strconv.FormatIntDecimal(i))
		}
		pos += n
	}
	return inputs, pos, nil
}

type Witness struct {
	Stack [][]byte
}

func readWitness(data []byte) (Witness, uint, error) {
	var witness Witness
	var pos uint
	var n uint
	// Each witness field starts with a compactSize integer to indicate the number of stack items for the corresponding txin. It is then followed by witness stack item(s) for the corresponding txin, if any.
	var count uint16
	if count, n = compactsize.DecodeUint16(data[pos:]); n == 0 {
		return Witness{}, 0, errors.Context(errors.ErrIllegal, "nr of witness stack")
	}
	pos += n
	witness.Stack = make([][]byte, count)
	var size uint16
	for i := 0; i < int(count); i++ {
		// Each witness stack item starts with a compactSize integer to indicate the number of bytes of the item.
		if size, n = compactsize.DecodeUint16(data[pos:]); n == 0 {
			return Witness{}, 0, errors.Context(errors.ErrIllegal, "size of witness stack item")
		}
		pos += n
		witness.Stack[i] = bytes.Clone(data[pos : pos+uint(size)])
		pos += uint(size)
	}
	return witness, pos, nil
}

func readWitnesses(data []byte, count int) ([]Witness, uint, error) {
	var err error
	var pos uint
	var n uint
	witnesses := make([]Witness, count)
	for i := 0; i < count; i++ {
		if witnesses[i], n, err = readWitness(data[pos:]); err != nil {
			return nil, 0, errors.Context(err, "reading witness "+strconv.FormatIntDecimal(i))
		}
		pos += n
	}
	return witnesses, pos, nil
}

type Output struct {
	Value  uint64
	Script []byte
}

func readOutput(data []byte) (Output, uint, error) {
	var out Output
	// <8-byte value-in-satoshis><1-9-byte script-length><script>
	out.Value = binary.LittleEndian.Uint64(data[:8])
	var len uint16
	var n uint
	if len, n = compactsize.DecodeUint16(data[8:]); n == 0 {
		return Output{}, 0, errors.Context(errors.ErrIllegal, "size of script")
	}
	out.Script = bytes.Clone(data[8+n : 8+n+uint(len)])
	return out, 8 + n + uint(len), nil
}

func readOutputs(data []byte) ([]Output, uint, error) {
	var pos uint
	var count uint16
	var n uint
	if count, n = compactsize.DecodeUint16(data); n == 0 {
		return nil, 0, errors.Context(errors.ErrIllegal, "number of outputs")
	}
	pos += n
	log.Traceln("Reading", count, "outputs")
	outputs := make([]Output, count)
	var err error
	for i := 0; i < int(count); i++ {
		if outputs[i], n, err = readOutput(data[pos:]); err != nil {
			return nil, 0, errors.Context(err, "reading output "+strconv.FormatIntDecimal(i))
		}
		pos += n
	}
	return outputs, pos, nil
}

// Ref: <https://en.bitcoin.it/wiki/Protocol_documentation#tx>
type Transaction struct {
	Version uint32
	Flag    uint8
	Inputs  []Input
	Outputs []Output
	// TODO (note) contain the signatures for SegWit transactions
	Witnesses []Witness
	Locktime  uint32
}

func (tx *Transaction) Hash(idx uint, subscript []byte, hashtype uint8) ([32]byte, error) {
	sighash, flagAnyoneCanPay := (hashtype & MASK_SIGHASH), (hashtype&FLAG_ANYONECANPAY) != 0
	hash := sha256.New()
	var enc32 = le.FromUint32(tx.Version)
	hash.Write(enc32[:])
	if flagAnyoneCanPay {
		hash.Write([]byte{1})
	} else {
		hash.Write(compactsize.EncodeUint(uint(len(tx.Inputs))))
	}
	for i := range tx.Inputs {
		if flagAnyoneCanPay && uint(i) != idx {
			// ANYONECANPAY: The current transaction input (with scriptPubKey modified to subScript) is set as
			// the first and only member of this vector.
			continue
		}
		hash.Write(tx.Inputs[i].Hash[:])
		enc32 = le.FromUint32(tx.Inputs[i].Index)
		hash.Write(enc32[:])
		if uint(i) == idx {
			// 8.  The script for the current transaction input in txCopy is set to subScript. (lead in by
			// its length as a var-integer encoded!)
			hash.Write(compactsize.EncodeUint(uint(len(subscript))))
			hash.Write(subscript)
		} else {
			// 7.  The scripts for all transaction inputs in txCopy are set to empty scripts. (exactly 1 byte
			// 0x00)
			hash.Write([]byte{0})
		}
		if uint(i) == idx || sighash == SIGHASH_ALL {
			enc32 = le.FromUint32(tx.Inputs[i].Sequence)
			hash.Write(enc32[:])
		} else if sighash == SIGHASH_NONE || sighash == SIGHASH_SINGLE {
			// SIGHASH_NONE: All other inputs aside from the current input in txCopy have their nSequence
			// index set to zero
			// SIGHASH_SINGLE: All other txCopy inputs aside from the current input are set to have an
			// nSequence index of zero.
			hash.Write([]byte{0, 0, 0, 0})
		} else {
			return [32]byte{}, errors.Context(errors.ErrUnsupported, "unsupported hash-type "+strconv.FormatUintDecimal(sighash))
		}
	}
	// 6.  A copy is made of the current transaction. (hereby referred to txCopy)
	// ANYONECANPAY: The txCopy input vector is resized to a length of one.
	var enc64 [8]byte
	switch sighash {
	case SIGHASH_ALL:
		hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs))))
		for o := range tx.Outputs {
			enc64 = le.FromUint64(tx.Outputs[o].Value)
			hash.Write(enc64[:])
			hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs[o].Script))))
			hash.Write(tx.Outputs[o].Script)
		}
	case SIGHASH_SINGLE:
		hash.Write(compactsize.EncodeUint(idx + 1))
		for o := uint(0); o <= idx; o++ {
			if o != idx {
				// FIXME -1 == MaxUint64, right?
				enc64 = le.FromUint64(math.MaxUint64)
				hash.Write(enc64[:])
				hash.Write([]byte{0})
				continue
			}
			enc64 = le.FromUint64(tx.Outputs[idx].Value)
			hash.Write(enc64[:])
			hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs[idx].Script))))
			hash.Write(tx.Outputs[idx].Script)
		}
	case SIGHASH_NONE:
		// SIGHASH_NONE: The output of txCopy is set to a vector of zero size.
		// no outputs to hash
		hash.Write([]byte{0})
	default:
		return [32]byte{}, errors.Context(errors.ErrIllegal, "value for hash-type")
	}
	enc32 = le.FromUint32(tx.Locktime)
	hash.Write(enc32[:])
	// 9.  An array of bytes is constructed from the serialized txCopy appended by four bytes for the
	// hash-type.
	hash.Write([]byte{hashtype, 0, 0, 0})
	// 10. This array is sha256 hashed twice, then the public key is used to check the supplied signature
	// against the hash. The secp256k1 elliptic curve is used for the verification with the given public key.
	return sha256.Sum256(hash.Sum(nil)), nil
}

// SignTransaction signs given transaction for specified idx, hash-type and subscript, using privkeybytes
// serialized private-key.
// FIXME needs testing ...
func (tx *Transaction) Sign(idx uint, subscript []byte, hashtype uint8, privkey *koblitz.PrivateKey) ([]byte, error) {
	var err error
	var checksum [32]byte
	if checksum, err = tx.Hash(idx, subscript, hashtype); err != nil {
		return nil, errors.Context(err, "failed to hash transaction for signing")
	}
	var sig *koblitz.Signature
	if sig, err = privkey.Sign(checksum[:]); err != nil {
		return nil, errors.Aggregate(errors.ErrFailure, "failed to sign transaction", err)
	}
	return append(sig.Serialize(), hashtype), nil
}

// FIXME somewhere, compare against descriptor addresses, check addresses, etc. (prly separate function for plain verification)
// ValidateTransaction validates a transaction given signature and public key. ValidateTransaction is mostly
// used from within script execution.
// - `subscript` must be cleaned up for signing/validation.
// - `signature` and `pubkey` are usually acquired during script execution.
func (tx *Transaction) CheckSignature(idx uint, subscript []byte, signature []byte, pubkey []byte) error {
	var err error
	var pk *koblitz.PublicKey
	if pk, err = curve.ParsePoint(pubkey); err != nil {
		return errors.Context(errors.ErrIllegal, "public key is bad")
	}
	// 5.  The hashtype is removed from the last byte of the sig and stored.
	var hashtype uint8
	signature, hashtype = signature[:len(signature)-1], signature[len(signature)-1]
	log.Traceln("Hash-type:", hashtype)
	var checksum [32]byte
	if checksum, err = tx.Hash(idx, subscript, hashtype); err != nil {
		return errors.Context(err, "transaction hashing for signature validation")
	}
	if !ecdsa.VerifyASN1(pk.ToECDSA(), checksum[:], signature) {
		return errors.Context(errors.ErrFailure, "transaction signature validation")
	}
	return nil
}

// TODO check correctness of transactions: <https://en.bitcoin.it/wiki/Protocol_rules#%22tx%22_messages>
// - Check syntactic correctness
// - Make sure neither in or out lists are empty
// - Size in bytes <= MAX_BLOCK_SIZE
// - Each output value, as well as the total, must be in legal money range
// - Make sure none of the inputs have hash=0, n=-1 (coinbase transactions)
// - Check that nLockTime <= INT_MAX[1], size in bytes >= 100[2], and sig opcount <= 2[3]
// - Reject "nonstandard" transactions: scriptSig doing anything other than pushing numbers on the stack, or scriptPubkey not matching the two usual forms[4]
// - Reject if we already have matching tx in the pool, or in a block in the main branch
// - For each input, if the referenced output exists in any other tx in the pool, reject this transaction.[5]
// - For each input, look in the main branch and the transaction pool to find the referenced output transaction. If the output transaction is missing for any input, this will be an orphan transaction. Add to the orphan transactions, if a matching transaction is not in there already.
// - For each input, if the referenced output transaction is coinbase (i.e. only 1 input, with hash=0, n=-1), it must have at least COINBASE_MATURITY (100) confirmations; else reject this transaction
// - For each input, if the referenced output does not exist (e.g. never existed or has already been spent), reject this transaction[6]
// - Using the referenced output transactions to get input values, check that each input value, as well as the sum, are in legal money range
// - Reject if the sum of input values < sum of output values
// - Reject if transaction fee (defined as sum of input values minus sum of output values) would be too low to get into an empty block
// - Verify the scriptPubKey accepts for each input; reject if any are bad
// - Add to transaction pool[7]
// - "Add to wallet if mine"
// - Relay transaction to peers
// - For each orphan transaction that uses this one as one of its inputs, run all these steps (including this one) recursively on that orphan

// Txid computes the transaction's ID: `SHA256(SHA256(transaction-bytes))`, note that the checksum needs to be
// reversed in order to produce the proper txid.
func (tx *Transaction) Txid() [32]byte {
	var enc32 [4]byte
	var enc64 [8]byte
	// SHA256(SHA256([nVersion][txins][txouts][nLockTime]))
	hash := sha256.New()
	enc32 = le.FromUint32(tx.Version)
	hash.Write(enc32[:])
	hash.Write(compactsize.EncodeUint(uint(len(tx.Inputs))))
	for i := range tx.Inputs {
		hash.Write(tx.Inputs[i].Hash[:])
		enc32 = le.FromUint32(tx.Inputs[i].Index)
		hash.Write(enc32[:])
		hash.Write(compactsize.EncodeUint(uint(len(tx.Inputs[i].Script))))
		hash.Write(tx.Inputs[i].Script)
		enc32 = le.FromUint32(tx.Inputs[i].Sequence)
		hash.Write(enc32[:])
	}
	hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs))))
	for i := range tx.Outputs {
		enc64 = le.FromUint64(tx.Outputs[i].Value)
		hash.Write(enc64[:])
		hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs[i].Script))))
		hash.Write(tx.Outputs[i].Script)
	}
	enc32 = le.FromUint32(tx.Locktime)
	hash.Write(enc32[:])
	checksum := sha256.Sum256(hash.Sum(nil))
	slices.Reverse(checksum[:])
	return checksum
}

// Wtxid computes the transaction's ID: `SHA256(SHA256(transaction-bytes))`, note that the checksum needs to
// be reversed in order to produce the proper txid. `Wtxid` includes the extra fields in segwit transactions.
// `Wtxid` produces a `Txid` for non-segwit transactions.
//
// Panics in case of incorrect, inconsistent transactions.
func (tx *Transaction) Wtxid() [32]byte {
	var enc32 [4]byte
	var enc64 [8]byte
	// SHA256(SHA256([nVersion][marker][flag][txins][txouts][witness][nLockTime]))
	// Format of `nVersion`, `txins`, `txouts`, and `nLockTime` are same as traditional serialization.
	// The `marker` MUST be a 1-byte zero value: `0x00`.
	// The `flag` MUST be a 1-byte non-zero value. Currently, `0x01` MUST be used.
	hash := sha256.New()
	enc32 = le.FromUint32(tx.Version)
	hash.Write(enc32[:])
	assert.Equal(tx.Flag == 0, len(tx.Witnesses) == 0)
	if tx.Flag > 0 {
		hash.Write([]byte{0, tx.Flag})
		assert.Equal(len(tx.Inputs), len(tx.Witnesses))
	}
	hash.Write(compactsize.EncodeUint(uint(len(tx.Inputs))))
	for i := range tx.Inputs {
		hash.Write(tx.Inputs[i].Hash[:])
		enc32 = le.FromUint32(tx.Inputs[i].Index)
		hash.Write(enc32[:])
		hash.Write(compactsize.EncodeUint(uint(len(tx.Inputs[i].Script))))
		hash.Write(tx.Inputs[i].Script)
		enc32 = le.FromUint32(tx.Inputs[i].Sequence)
		hash.Write(enc32[:])
	}
	hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs))))
	for o := range tx.Outputs {
		enc64 = le.FromUint64(tx.Outputs[o].Value)
		hash.Write(enc64[:])
		hash.Write(compactsize.EncodeUint(uint(len(tx.Outputs[o].Script))))
		hash.Write(tx.Outputs[o].Script)
	}
	// nr of witnesses is always same as number of inputs
	for w := range tx.Witnesses {
		hash.Write(compactsize.EncodeUint(uint(len(tx.Witnesses[w].Stack))))
		for i := range tx.Witnesses[w].Stack {
			hash.Write(compactsize.EncodeUint(uint(len(tx.Witnesses[w].Stack[i]))))
			hash.Write(tx.Witnesses[w].Stack[i])
		}
	}
	enc32 = le.FromUint32(tx.Locktime)
	hash.Write(enc32[:])
	checksum := sha256.Sum256(hash.Sum(nil))
	slices.Reverse(checksum[:])
	return checksum
}

// Verify checks the internal consistency of the transaction.
func (tx *Transaction) Verify() error {
	log.Traceln("Verifying consistency…")
	// FIXME consider checking that one of known/prescribed (P2PKH: `OP_DUP OP_HASH160 <pubKeyHash> OP_EQUALVERIFY OP_CHECKSIG`, P2SH: `OP_HASH160 <scriptHash> OP_EQUAL`, multi-sig: `OP_m <pubKey1> ... OP_n OP_CHECKMULTISIG`)
	if tx.Version > 2 {
		return errors.Context(errors.ErrIllegal, "Unsupported version: "+strconv.FormatUintDecimal(tx.Version))
	}
	if tx.Flag == 0 && len(tx.Witnesses) > 0 {
		return errors.Context(errors.ErrIllegal, "Cannot have witnesses if flag is not set, is 0.")
	}
	for i := range tx.Inputs {
		if tx.Flag == 0 && len(tx.Inputs[i].Script) == 0 {
			return errors.Context(errors.ErrIllegal, "Transaction input "+strconv.FormatIntDecimal(i)+" has empty script.")
		}
	}
	for i := range tx.Outputs {
		if tx.Flag == 0 && len(tx.Outputs[i].Script) == 0 {
			return errors.Context(errors.ErrIllegal, "Transaction output "+strconv.FormatIntDecimal(i)+" has empty script.")
		}
	}
	if len(tx.Inputs) == 0 {
		return errors.Context(errors.ErrIllegal, "Transaction cannot have 0 inputs.")
	}
	if len(tx.Outputs) == 0 {
		return errors.Context(errors.ErrIllegal, "Transaction cannot have 0 outputs.")
	}
	if tx.Flag == 1 {
		if len(tx.Inputs) != len(tx.Witnesses) {
			log.Traceln("Inputs:", len(tx.Inputs), "witnesses:", len(tx.Witnesses))
			return errors.Context(errors.ErrIllegal, "Must have same number of witnesses as inputs.")
		}
	}
	log.Traceln("Verifying consistency: OK")
	return nil
}

func (tx *Transaction) CheckInputs(prev []*Transaction) error {
	log.Traceln("Checking inputs…")
	var prevTxid [32]byte
	for i := range tx.Inputs {
		prevTxid = prev[i].Txid()
		slices.Reverse(prevTxid[:])
		if subtle.ConstantTimeCompare(tx.Inputs[i].Hash[:], prevTxid[:]) != 1 {
			return errors.Context(errors.ErrIllegal, "previous transaction "+strconv.FormatIntDecimal(i)+" does not match")
		}
	}
	log.Traceln("Checking inputs: OK")
	return nil
}

// TODO add sanity-checking of values
// TODO needs exhaustive testing and fuzzing for bad input, then also adopt lessons for C implementation
// FIXME make parsing strict, such that we can immediately detect deviations and prevent continuing parsing corrupted/malicious data
func ParseTransaction(data []byte) (Transaction, uint, error) {
	if len(data) == 0 {
		return Transaction{}, 0, errors.Context(errors.ErrUnderflow, "no transaction data")
	}
	var tx Transaction
	var pos uint
	var n uint
	tx.Version = binary.LittleEndian.Uint32(data[:4])
	pos += 4
	if data[pos] == 0 {
		tx.Flag = data[pos+1]
		pos += 2
	}
	var err error
	if tx.Inputs, n, err = readInputs(data[pos:]); err != nil {
		return Transaction{}, 0, errors.Context(err, "inputs in transaction")
	}
	pos += n
	if tx.Outputs, n, err = readOutputs(data[pos:]); err != nil {
		return Transaction{}, 0, errors.Context(err, "outputs in transaction")
	}
	pos += n
	if tx.Flag == 1 {
		if tx.Witnesses, n, err = readWitnesses(data[pos:], len(tx.Inputs)); err != nil {
			return Transaction{}, 0, errors.Context(err, "witnesses in transaction")
		}
		pos += n
	}
	tx.Locktime = binary.LittleEndian.Uint32(data[pos : pos+4])
	pos += 4
	return tx, pos, nil
}
