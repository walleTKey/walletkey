package descriptor

import (
	"bytes"
	"encoding/hex"
	"strings"

	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/btc/wallet/hierarchical"
	"codeberg.org/walletkey/walletkey/internal/curve"
	"github.com/cobratbq/goutils/assert"
	hex_ "github.com/cobratbq/goutils/codec/bytes/hex"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
	"github.com/cobratbq/goutils/std/strconv"
	"github.com/mit-dci/lit/crypto/koblitz"
)

// Descriptors
// - <https://github.com/bitcoin/bitcoin/blob/master/doc/descriptors.md>

/*
Descriptors consist of several types of expressions. The top level expression is either a SCRIPT, or SCRIPT#CHECKSUM where CHECKSUM is an 8-character alphanumeric descriptor checksum.

SCRIPT expressions:

    sh(SCRIPT) (top level only): P2SH embed the argument.
    wsh(SCRIPT) (top level or inside sh only): P2WSH embed the argument.
    pk(KEY) (anywhere): P2PK output for the given public key.
    pkh(KEY) (not inside tr): P2PKH output for the given public key (use addr if you only know the pubkey hash).
    wpkh(KEY) (top level or inside sh only): P2WPKH output for the given compressed pubkey.
    combo(KEY) (top level only): an alias for the collection of pk(KEY) and pkh(KEY). If the key is compressed, it also includes wpkh(KEY) and sh(wpkh(KEY)).
    multi(k,KEY_1,KEY_2,...,KEY_n) (not inside tr): k-of-n multisig script using OP_CHECKMULTISIG.
    sortedmulti(k,KEY_1,KEY_2,...,KEY_n) (not inside tr): k-of-n multisig script with keys sorted lexicographically in the resulting script.
    multi_a(k,KEY_1,KEY_2,...,KEY_N) (only inside tr): k-of-n multisig script using OP_CHECKSIG, OP_CHECKSIGADD, and OP_NUMEQUAL.
    sortedmulti_a(k,KEY_1,KEY_2,...,KEY_N) (only inside tr): similar to multi_a, but the (x-only) public keys in it will be sorted lexicographically.
    tr(KEY) or tr(KEY,TREE) (top level only): P2TR output with the specified key as internal key, and optionally a tree of script paths.
    addr(ADDR) (top level only): the script which ADDR expands to.
    raw(HEX) (top level only): the script whose hex encoding is HEX.
    rawtr(KEY) (top level only): P2TR output with the specified key as output key. NOTE: while it's possible to use this to construct wallets, it has several downsides, like being unable to prove no hidden script path exists. Use at your own risk.

KEY expressions:

    Optionally, key origin information, consisting of:
        An open bracket [
        Exactly 8 hex characters for the fingerprint of the key where the derivation starts (see BIP32 for details)
        Followed by zero or more /NUM or /NUM' path elements to indicate unhardened or hardened derivation steps between the fingerprint and the key or xpub/xprv root that follows
        A closing bracket ]
    Followed by the actual key, which is either:
        Hex encoded public keys (either 66 characters starting with 02 or 03 for a compressed pubkey, or 130 characters starting with 04 for an uncompressed pubkey).
            Inside wpkh and wsh, only compressed public keys are permitted.
            Inside tr and rawtr, x-only pubkeys are also permitted (64 hex characters).
        WIF encoded private keys may be specified instead of the corresponding public key, with the same meaning.
        xpub encoded extended public key or xprv encoded extended private key (as defined in BIP 32).
            Followed by zero or more /NUM unhardened and /NUM' hardened BIP32 derivation steps.
            Optionally followed by a single /* or /*' final step to denote all (direct) unhardened or hardened children.
            The usage of hardened derivation steps requires providing the private key.

(Anywhere a ' suffix is permitted to denote hardened derivation, the suffix h can be used instead.)

TREE expressions:

    any SCRIPT expression
    An open brace {, a TREE expression, a comma ,, a TREE expression, and a closing brace }

ADDR expressions are any type of supported address:

    P2PKH addresses (base58, of the form 1... for mainnet or [nm]... for testnet). Note that P2PKH addresses in descriptors cannot be used for P2PK outputs (use the pk function instead).
    P2SH addresses (base58, of the form 3... for mainnet or 2... for testnet, defined in BIP 13).
    Segwit addresses (bech32 and bech32m, of the form bc1... for mainnet or tb1... for testnet, defined in BIP 173 and BIP 350).
*/

func EncodePath(fingerprint *btc.FingerprintType, path []uint32) string {
	var encoder strings.Builder
	if fingerprint != nil {
		encoder.WriteString(hex.EncodeToString(fingerprint[:]))
		if len(path) > 0 {
			encoder.WriteByte('/')
		}
	}
	for p := range path {
		if p > 0 {
			encoder.WriteByte('/')
		}
		encoder.WriteString(strconv.FormatUintDecimal(path[p] & ^hierarchical.HARDENED))
		if path[p]&hierarchical.HARDENED > 0 {
			encoder.WriteByte('h')
		}
	}
	return encoder.String()
}

func Check(desc []byte) error {
	depth := 0
	for i := range desc {
		if desc[i] == '(' {
			depth++
		} else if desc[i] == ')' {
			depth--
		}
	}
	if depth > 0 {
		return errors.Context(errors.ErrIllegal, "parentheses are not matching")
	}
	var store [10][]byte
	var stack = store[:0]
	for i := range desc {
		if len(stack) > 0 && (bytes.HasPrefix(desc[i:], []byte("sh(")) ||
			bytes.HasPrefix(desc[i:], []byte("combo(")) ||
			bytes.HasPrefix(desc[i:], []byte("tr(")) ||
			bytes.HasPrefix(desc[i:], []byte("addr(")) ||
			bytes.HasPrefix(desc[i:], []byte("raw(")) ||
			bytes.HasPrefix(desc[i:], []byte("rawtr("))) {
			return errors.Context(errors.ErrIllegal, "top-level only element was embedded")
		}
		// if (bytes.HasPrefix(desc[i:], []byte("wsh(")) || bytes.HasPrefix(desc[i:], []byte("wpkh("))) &&
		// 	len(stack) > 0 && stack[len(stack)-1] != TR {
		// 	return errors.Context(errors.ErrIllegal, "incorrectly positioned element at "+strconv.FormatIntDecimal(i))
		// }
		// TODO wsh(...)/wpkh(...): top-level or inside sh(...)
	}
	return nil
}

func separateChecksum(desc []byte) ([]byte, []byte) {
	idx := bytes.LastIndexByte(desc, '#')
	if idx < 0 {
		return desc, nil
	}
	assert.Equal(8, len(desc[idx+1:]))
	return desc[:idx], desc[idx+1:]
}

type KeyOrigin struct {
	Root btc.FingerprintType
	Path []uint32
}

func parseKeyOrigin(script []byte) (*KeyOrigin, []byte, error) {
	if script[0] != '[' {
		return nil, script, nil
	}
	var end int
	if end = bytes.IndexByte(script, ']'); end < 0 {
		return nil, nil, errors.Context(errors.ErrIllegal, "No finish to key origin specifier.")
	}
	var origin KeyOrigin
	var fingerprint, remaining []byte
	var ok bool
	if fingerprint, remaining, ok = bytes.Cut(script[1:end], []byte{'/'}); !ok {
		return nil, nil, errors.Context(errors.ErrIllegal, "Cannot find root key fingerprint at start of key-origin.")
	}
	if len(fingerprint) != 8 || !hex_.AllHexadecimal(fingerprint) {
		return nil, nil, errors.Context(errors.ErrIllegal, "Fingerprint is not formatted as 8 hexadecimal characters.")
	}
	origin.Root = btc.FingerprintType(hex_.MustDecode(fingerprint))
	var err error
	var path = bytes.Split(remaining, []byte{'/'})
	for i := range path {
		var step uint32
		if path[i][len(path[i])-1] == 'h' || path[i][len(path[i])-1] == '\'' {
			if step, err = strconv.ParseBytesUint[uint32](path[i][:len(path[i])-1], 10); err != nil {
				return nil, nil, errors.Aggregate(errors.ErrIllegal, "Path index value (hardened) is not a valid unsigned decimal value", err)
			}
			step |= hierarchical.HARDENED
		} else if step, err = strconv.ParseBytesUint[uint32](path[i], 10); err != nil {
			return nil, nil, errors.Aggregate(errors.ErrIllegal, "Path index value is not a valid unsigned decimal value", err)
		}
		origin.Path = append(origin.Path, step)
	}
	return &origin, script[end+1:], nil
}

func parsePublicKey(encoded []byte) (*koblitz.PublicKey, error) {
	return curve.ParsePoint(encoded)
}

type Descriptor struct {
	Origin *KeyOrigin
	Key    *koblitz.PublicKey
}

func parsePKH(script []byte) (Descriptor, error) {
	log.Traceln("`parsePKH`: desc:", string(script))
	var err error
	var desc Descriptor
	var key []byte
	if desc.Origin, key, err = parseKeyOrigin(script); err != nil {
		return Descriptor{}, errors.Context(err, "Illegal key-origin")
	}
	if key, err = hex_.Decode(key); err != nil {
		return Descriptor{}, errors.Context(err, "Illegal encoding of public key")
	}
	if desc.Key, err = parsePublicKey(key); err != nil {
		return Descriptor{}, errors.Context(err, "Illegal public key")
	}
	log.Tracef("Descriptor PKH: %#v", desc)
	return desc, nil
}

func Parse(desc []byte) (Descriptor, error) {
	var script, checksum = separateChecksum(desc)
	log.Traceln("Script:", string(script), "checksum:", string(checksum))
	if bytes.HasPrefix(script, []byte("pkh(")) && bytes.HasSuffix(script, []byte(")")) {
		return parsePKH(script[4 : len(script)-1])
	}
	panic("Unsupported/unimplemented descriptor")
}
