package main

import (
	"bytes"
	"encoding/binary"
	"flag"
	"os"
	"time"

	"codeberg.org/walletkey/walletkey/internal/wallet"
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

func ZEROKEY() [32]byte {
	return [32]byte{}
}

func TESTKEY() [32]byte {
	// Use function to ensure that nothing can (accidentally) modify the test-key constant.
	return [...]byte{39, 48, 174, 141, 199, 148, 40, 112, 161, 61, 157, 252, 194, 166, 25, 21, 227, 73, 47, 9, 201, 197, 231, 38, 168, 96, 185, 232, 27, 62, 204, 138}
}

func reinitializeWallet(securedkey *tkey.Session) {
	log.Debugln("============================================")
	log.Debugln("Reinitialize wallet-key…")
	assert.Success(securedkey.FinishSecure(), "Failed to send reinitialize command.")
}

func restoreTestWallet(securedkey *tkey.Session, id tkey.Identity) []byte {
	testkey := TESTKEY()
	var err error
	log.Debugln("============================================")
	log.Debugln("RESTORING TEST WALLET FROM SEED…")
	request := make([]byte, 0, 127)
	request = append(request, testkey[:]...)
	hardened := wallet.KDF(id)
	request = append(request, hardened[:]...)
	assert.Success(securedkey.Send(0, wallet.CmdRestoreSecret, request), "Failed to restore test wallet seed.")
	var sealedWalletData []byte
	var data []byte
	data, err = securedkey.Receive(0, wallet.RspRestoreSecret)
	assert.Success(err, "Failed to receive SealedWallet response for restoring test wallet seed")
	// FIXME test fingerprint at `data[1:5]`
	sealedWalletData = data[5 : 5+wallet.SEALED_WALLET_LENGTH]
	log.Debugln("--------------------------------------------")
	log.Debugln("EXPORTING TEST WALLET SEED…")
	log.Infoln("Touch TKey to confirm action.")
	assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export test wallet secret.")
	data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	assert.Success(err, "Failed to export test wallet secret")
	assert.Equal(0, bytes.Compare(testkey[:], data[3:35]))
	return sealedWalletData
}

func loadTestWallet(securedkey *tkey.Session, sealedWalletData []byte, id tkey.Identity) {
	var err error
	log.Debugln("============================================")
	log.Debugln("LOADING TEST WALLET…")
	hardened := wallet.KDF(id)
	fullrequest := make([]byte, 0, wallet.SEALED_WALLET_LENGTH+wallet.HARDENED_ID_LENGTH)
	fullrequest = append(fullrequest, sealedWalletData...)
	fullrequest = append(fullrequest, hardened[:]...)
	assert.Success(securedkey.SendLarge(0, wallet.CmdLoadWallet, fullrequest), "Failed to request loading test wallet.")
	var data []byte
	data, err = securedkey.Receive(0, wallet.RspLoadWallet)
	// FIXME extract and verify fingerprint (data[1:5])
	assert.Success(err, "Failed to load test wallet")
	log.Debugln("--------------------------------------------")
	log.Debugln("EXPORTING TEST WALLET SEED…")
	log.Infoln("Touch TKey to confirm action.")
	assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export secret.")
	data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	assert.Success(err, "Failed to export secret")
	testkey := TESTKEY()
	assert.Equal(0, bytes.Compare(testkey[:], data[3:35]))
}

type generatedWallet struct {
	sealed []byte
	seed   []byte
}

func generateNewWallet(securedkey *tkey.Session, id tkey.Identity) generatedWallet {
	var generated generatedWallet
	var err error
	log.Debugln("============================================")
	log.Debugln("GENERATING NEW WALLET…")
	hardened := wallet.KDF(id)
	assert.Success(securedkey.Send(0, wallet.CmdGenerateSecret, hardened[:]), "Failed to generate wallet seed.")
	var data []byte
	data, err = securedkey.Receive(0, wallet.RspGenerateSecret)
	assert.Success(err, "Failed to receive SealedWallet response for generate wallet.")
	fp := binary.LittleEndian.Uint32(data[1:5])
	// FIXME test/verify fingerprint?
	fp = fp
	generated.sealed = data[5 : 5+wallet.SEALED_WALLET_LENGTH]
	log.Debugln("--------------------------------------------")
	log.Debugln("EXPORTING GENERATED WALLET SEED…")
	log.Infoln("Touch TKey to confirm action.")
	assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export secret.")
	data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	assert.Success(err, "Failed to export secret")
	generated.seed = data[3:35]
	return generated
}

func loadGeneratedWallet(securedkey *tkey.Session, generated generatedWallet, id tkey.Identity) {
	var err error
	var data []byte
	log.Debugln("============================================")
	log.Debugln("LOADING GENERATED WALLET…")
	var fullrequest = make([]byte, 0, wallet.SEALED_WALLET_LENGTH+wallet.HARDENED_ID_LENGTH)
	fullrequest = append(fullrequest, generated.sealed...)
	hardened := wallet.KDF(id)
	fullrequest = append(fullrequest, hardened[:]...)
	assert.Success(securedkey.SendLarge(0, wallet.CmdLoadWallet, fullrequest), "Failed to request loading generated wallet.")
	_, err = securedkey.Receive(0, wallet.RspLoadWallet)
	assert.Success(err, "Failed to load generated wallet")
	log.Debugln("--------------------------------------------")
	log.Debugln("EXPORTING GENERATED WALLET SEED…")
	log.Infoln("Touch TKey to confirm action.")
	assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export secret.")
	data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	assert.Success(err, "Failed to export secret")
	assert.Equal(0, bytes.Compare(generated.seed, data[3:35]))
}

func restoreGeneratedWallet(securedkey *tkey.Session, generated generatedWallet, id tkey.Identity) {
	var err error
	var data []byte
	log.Debugln("============================================")
	log.Debugln("RESTORING GENERATED WALLET…")
	hardened := wallet.KDF(id)
	var request [127]byte
	copy(request[:], generated.seed)
	copy(request[len(generated.seed):], hardened[:])
	assert.Success(securedkey.Send(0, wallet.CmdRestoreSecret, request[:]), "Failed to load generated wallet.")
	_, err = securedkey.Receive(0, wallet.RspRestoreSecret)
	// TODO test fingerprint?
	assert.Success(err, "Failed to receive sealed wallet for restored secret.")
	log.Debugln("--------------------------------------------")
	log.Debugln("EXPORTING RESTORED GENERATED WALLET SEED…")
	log.Infoln("Touch TKey to confirm action.")
	assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export secret.")
	data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	assert.Success(err, "Failed to export secret")
	assert.Equal(0, bytes.Compare(generated.seed, data[3:35]))
}

// Test essential functionality of the walletkey to ensure that a private key is accessible and exportable
// even if other functionality refuses to work. This ensures that at minimum we can migrate away from faulty
// firmware.
// TODO support intermediately disconnecting and reconnecting TKey from USB to prove functionality without rely on re-initialization.
// TODO report firmware checksum and identity of time of testing. This allows user to ensure that the wallet they create, is created on the same device, firmware, passphrase that is tested.
// TODO silence logging until `verbose` specified.
// TODO allow specifying a identity-key on the command-line for verifying the identity without working with a wallet.
// FIXME update code to include 64-byte hardened-id as part of generate/restore/load.
func main() {
	secret := flag.String("secret", "", "A user-supplied secret phrase to load with the firmware.")
	// TODO consider providing an option for a secrets-file in order to insert pure-binary content
	flag.Parse()

	if len(flag.Args()) == 0 || flag.Arg(0) == "" {
		log.Errorln("Please specify the firmware binary to load as program argument.")
		return
	}
	firmwarePath := flag.Arg(0)
	if info, err := os.Stat(firmwarePath); err != nil || info.IsDir() {
		log.Errorln("The specified firmware-binary is absent, inaccessible or not a file.")
		return
	}

	const delay = 2

	firmwarebin, err := os.ReadFile(firmwarePath)
	assert.Success(err, "Failed to read firmware-binary")
	device, appinfo, err := wallet.ConnectWalleTKey(firmwarebin, []byte(*secret))
	assert.Success(err, "Failed to establish connection with TKey")
	assert.Equal(wallet.WALLETKEY_APPID_NAME(), appinfo.Name)
	assert.Equal(wallet.WALLETKEY_APPID_VERSION, appinfo.Version)
	defer io.ClosePanicked(&device.Session, "Failed to close connection to TKey.")

	assert.Success(device.Session.Secure(appinfo.Identity), "Failed to secure connection with TKey.")
	testWallet := restoreTestWallet(&device.Session, appinfo.Identity)
	time.Sleep(delay * time.Second)
	reinitializeWallet(&device.Session)

	assert.Success(device.Session.Secure(appinfo.Identity), "Failed to secure connection with TKey.")
	generatedWallet := generateNewWallet(&device.Session, appinfo.Identity)
	time.Sleep(delay * time.Second)
	reinitializeWallet(&device.Session)

	assert.Success(device.Session.Secure(appinfo.Identity), "Failed to secure connection with TKey.")
	loadTestWallet(&device.Session, testWallet, appinfo.Identity)
	time.Sleep(delay * time.Second)
	reinitializeWallet(&device.Session)

	assert.Success(device.Session.Secure(appinfo.Identity), "Failed to secure connection with TKey.")
	loadGeneratedWallet(&device.Session, generatedWallet, appinfo.Identity)
	time.Sleep(delay * time.Second)
	reinitializeWallet(&device.Session)

	assert.Success(device.Session.Secure(appinfo.Identity), "Failed to secure connection with TKey.")
	restoreGeneratedWallet(&device.Session, generatedWallet, appinfo.Identity)
	time.Sleep(delay * time.Second)
	reinitializeWallet(&device.Session)

	log.Infoln("Finished. Generating, restoring, loading and exporting wallet secret worked as expected.")
}
