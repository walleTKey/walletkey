package main

import (
	"encoding/hex"
	"os"
	"time"

	"codeberg.org/walletkey/walletkey/internal/wallet"
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
)

// TODO consider backing up `github.com/tillitis/tkeyclient` library
// TODO we check the identity, but we can also check the firmware checksum before continuing. This too can be part of the sealed wallet.
// FIXME test-verify exports of generated, then loaded key
func main() {
	firmwarebin, err := os.ReadFile("firmware.bin")
	assert.Success(err, "Failed to read firmware-binary")
	// FIXME add identity once available, otherwise we continue to operate on TOFU
	device, info, err := wallet.ConnectWalleTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to establish connection with TKey")
	//FIXME err = securedkey.Secure()
	//assert.Success(err, "Failed to secure connection with TKey.")
	//log.Infoln("Secure session established.")
	defer io.ClosePanicked(&device.Session, "Failed to close connection to TKey.")

	var data []byte
	log.Debugln("Checksum:", hex.EncodeToString(info.Checksum[:]))
	log.Debugln("Name + version: ", info.Name, info.Version)
	//log.Debugln("============================================")
	//log.Debugln("GENERATING SECRET ...")
	//var sealedWallet []byte
	//assert.Success(securedkey.Send(0, wallet.CmdGenerateSecret, nil), "Failed to generate secret.")
	//data, err = securedkey.Receive(0, wallet.RspAppInfo)
	//name, version, checksum, identity = wallet.ParseAppInfo(data[1:])
	//log.Debugln("AppInfo:", name, version, hex.EncodeToString(checksum[:]), hex.EncodeToString(identity[:]))
	//data, err = securedkey.Receive(0, wallet.RspGenerateSecret)
	//assert.Success(err, "Failed to generate secret")
	//log.Debugln("Response:", data)
	//sealedWallet = data[1:73]
	//time.Sleep(1 * time.Second)
	//log.Debugln("============================================")
	//log.Debugln("UNLOADING WALLET ...")
	//assert.Success(securedkey.Send(0, wallet.CmdUnloadWallet, nil), "Failed to generate secret.")
	//assert.Success(builtin.Error[[]byte](securedkey.Receive(0, wallet.RspUnloadWallet)), "Failed to unload wallet.")
	//time.Sleep(1 * time.Second)
	//log.Debugln("============================================")
	//log.Debugln("LOADING WALLET ...")
	//assert.Success(securedkey.Send(0, wallet.CmdLoadWallet, sealedWallet), "Failed to initialize wallet.")
	//data, err = securedkey.Receive(0, wallet.RspLoadWallet)
	//assert.Success(err, "Failed to load secret")
	//log.Debugln("============================================")
	//log.Debugln("DUMP WALLET INFO ...")
	//assert.Success(securedkey.Send(0, wallet.CmdWalletInfo, nil), "Failed to query wallet-info.")
	//data, err = securedkey.Receive(0, wallet.RspWalletInfo)
	//log.Debugln("Wallet pubkey:", hex.EncodeToString(data[1:34]))
	//assert.Success(err, "Failed to get wallet-info")
	//log.Debugln("Response:", data)
	// FIXME temp for import issues
	_ = builtin.Add(1, 2)
	time.Sleep(0)
	//========================================================================================================
	var rawtx1 []byte
	rawtx1, err = hex.DecodeString("01000000000101b7cc9f4b3bac24e3c08f90187b1650e2194dd2f30f37d4f5527a63f3421d1a880100000000ffffffff02b3f18318000000001600143e8df7cd56cf8baa69211489267feb91fdfeba0504d526000000000016001473d44cbdf58fb9bcebe98bb41cc9e44fa410ca5d02483045022100d32e603af905516286a4dad2498c761241f6c74fe986e01c0f606d883e28a03f022002e0dc20d7bc12ada06d56e46fbddec4ddc37eefcef42cc6b9152f4f60ce7be70121024b5d5186b69f6ea2381b7c436916a0d5b850033b5fb339513f5afd67b8ca2a5600000000")
	assert.Success(err, "Failed to encode raw transaction for sending.")
	checksum := tkey.Blake2s256([]byte{}, rawtx1)
	payload := append([]byte{}, checksum[:]...)
	payload = append(payload, byte(len(rawtx1)))
	payload = append(payload, byte(len(rawtx1)>>8))
	payload = append(payload, rawtx1...)
	var count uint
	// FIXME need to change to work with SendLarge and continuation-flag
	log.Debugln("Need to send", len(rawtx1), "bytes of raw transaction body. This takes", device.Session.CountMany(payload), "frames.")
	count, err = device.Session.SendMany(0, wallet.CmdLoadTransaction, payload)
	assert.Success(err, "Failed to send multiple messages")
	log.Debugln("Sent", count, "messages to transfer raw transaction.")
	data, err = device.Session.Receive(0, wallet.RspLoadTransaction)
	assert.Success(err, "Failed to check transaction")
	log.Debugln("Received:", hex.EncodeToString(data[1:33]))
	//========================================================================================================
	//log.Debugln("============================================")
	//log.Debugln("EXPORTING WALLET SEED ...")
	//log.Infoln("Touch TKey to confirm action.")
	//assert.Success(securedkey.Send(0, wallet.CmdExportWalletSecret, nil), "Failed to export secret.")
	//data, err = securedkey.Receive(0, wallet.RspExportWalletSecret)
	//assert.Success(err, "Failed to export secret")
	//log.Debugln("Export secret:", data)
	//log.Infoln("Finished.")
}
