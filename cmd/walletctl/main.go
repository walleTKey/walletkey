package main

import (
	"bytes"
	_ "embed"
	"encoding/hex"
	"encoding/json"
	"flag"
	"io"
	"net"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"

	"codeberg.org/walletkey/walletkey/internal/agent"
	"codeberg.org/walletkey/walletkey/internal/btc"
	"codeberg.org/walletkey/walletkey/internal/btc/descriptor"
	"codeberg.org/walletkey/walletkey/internal/btc/wallet"
	tkeywallet "codeberg.org/walletkey/walletkey/internal/wallet"
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/btcsuite/btcd/btcutil/psbt"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/codec/bytes/prefixed-compact"
	"github.com/cobratbq/goutils/std/builtin"
	"github.com/cobratbq/goutils/std/builtin/maps"
	"github.com/cobratbq/goutils/std/errors"
	io_ "github.com/cobratbq/goutils/std/io"
	"github.com/cobratbq/goutils/std/log"
	os_ "github.com/cobratbq/goutils/std/os"
	"github.com/cobratbq/goutils/std/strconv"
)

//go:embed firmware.bin
var firmwarebin []byte

func cmdExportFirmware(args []string) {
	exportFlags := flag.NewFlagSet("export-firmware", flag.ExitOnError)
	output := exportFlags.String("o", "firmware.bin.export", "Output path for exported firmware-binary.")
	exportFlags.Parse(args)
	if *output == "" {
		log.Errorln("Empty output destination (`-o`) provided. Firmware cannot be exported.")
		return
	}
	err := os.WriteFile(*output, firmwarebin, 0o400)
	log.WarnOnError(err, "Failed to dump embedded firmware-binary to output location")
}

type agentContext struct {
	// TODO consider dropping identity as it would expose the public key on the system (only wallet fingerprints and paths are less of a risk)
	identity tkey.Identity
	wallets  map[btc.FingerprintType]string
}

func serve(ctx *agentContext, conn *net.UnixConn) {
	defer builtin.RecoverLogged("Panic while handling request: %+v")
	var err error
	var data *prefixed.KeyValue
	for {
		log.Traceln("Waiting for request from client…")
		data, err = prefixed.ReadKeyValue(conn, nil)
		if errors.Is(err, io.EOF) {
			return
		} else if err != nil {
			log.Warnln("Agent: failed to read request from client:", err.Error())
			continue
		}
		switch data.K {
		case "identity":
			assert.Equal(0, data.V.Len())
			log.Traceln("Sending walleTKey identity…")
			// FIXME proper error handling
			_, err = prefixed.WriteBytes(conn, ctx.identity[:])
			assert.Success(err, "Failed to write identity to client")
		case "wallets":
			assert.Equal(0, data.V.Len())
			log.Traceln("Sending known wallets…")
			walletsEncoded := maps.Transform(ctx.wallets, func(fpr btc.FingerprintType, path string) (string, prefixed.Value) {
				return string(fpr[:]), prefixed.Bytes([]byte(path))
			})
			// FIXME proper error handling
			_, err = prefixed.WriteMap(conn, walletsEncoded)
			assert.Success(err, "Failed to write wallet-listing count.")
		case "wallet":
			assert.Equal(4, data.V.Len())
			fpr := btc.FingerprintType(data.V.(prefixed.Bytes)[:4])
			if path, ok := ctx.wallets[fpr]; ok {
				_, err = prefixed.WriteBytes(conn, []byte(path))
			} else {
				_, err = prefixed.WriteBytes(conn, []byte{})
			}
			// FIXME proper error handling
			assert.Success(err, "Failed to send response for wallet query")
		default:
			log.Warnln("Received unknown/illegal request:", string(data.K))
		}
	}
}

// FIXME proper error handling
// FIXME DESIGN: what are the consequences of having an agent running in third threat-level where we cannot actually trust the client software? (the hardening means that we cannot load the wallet without knowing that we connected to the proper device, so impact of manipulating information is limited)
func cmdAgent(args []string) {
	log.Infoln("Agent running…")
	agentFlags := flag.NewFlagSet("agent", flag.ExitOnError)
	agentFlags.Parse(args)
	var err error
	device, info, err := tkeywallet.ConnectWalleTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to connect to TKey.")
	// FIXME need to verify identity or receive from user
	var context = agentContext{identity: info.Identity, wallets: make(map[btc.FingerprintType]string)}
	err = device.Session.Secure(context.identity)
	assert.Success(err, "Failed to establish secure session.")
	// Load wallets
	for _, path := range agentFlags.Args() {
		var walletbytes []byte
		walletbytes, err = os.ReadFile(path)
		if err != nil {
			log.Warnf("Failed to read wallet from '%v': %v", path, err.Error())
			continue
		}
		err = device.LoadWallet((*tkeywallet.SealedWallet)(walletbytes))
		if err != nil {
			log.Warnf("Failed to load wallet (%v): %v", path, err.Error())
			continue
		}
		path = builtin.Expect(filepath.Abs(path))
		context.wallets[device.Fingerprint] = path
		log.Infof("Registered '%v': %v", device.Fingerprint.Hex(), path)
	}
	assert.Success(device.Session.FinishSecure(), "Failed to finish secure session.")
	assert.Success(device.Session.Close(), "Failed to close connection to device.")
	// Open socket-connection for agent
	var sock *net.UnixListener
	if sock, err = agent.Listen(); err != nil {
		log.Errorln("Failed to open listening socket.", err.Error())
		return
	}
	defer io_.CloseLoggedWithIgnores(sock, "Failed to gracefully close agent-socket (for new connections)", net.ErrClosed)
	// Install signal-handler for gracefully closing agent.
	log.Traceln("Installing signal-handler…")
	signalChan := make(chan os.Signal)
	go func() {
		<-signalChan
		log.Traceln("Received SIGINT. Closing agent socket.")
		io_.CloseLoggedWithIgnores(sock, "Received SIGINT. Failed to gracefully close agent socket.", net.ErrClosed)
	}()
	signal.Notify(signalChan, os.Interrupt)
	// Start accepting client connections.
	log.Traceln("Agent is up and running…")
	for {
		if conn, err := sock.AcceptUnix(); err == nil {
			go serve(&context, conn)
		} else if errors.Is(err, net.ErrClosed) {
			log.Infoln("Shutting down agent.")
			break
		} else {
			// TODO should we just exit the loop on any kind of error?
			log.Infoln("Failed to accept socket connection:", err.Error())
		}
	}
}

func cmdGenerate(args []string) {
	log.Infoln("Generating new sealed wallet…")
	genFlags := flag.NewFlagSet("generate", flag.ExitOnError)
	outfile := genFlags.String("wallet", "wallet.sealed", "The destination file to store the generated sealed wallet.")
	genFlags.Parse(args)
	device, info, err := tkeywallet.ConnectWalleTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to establish secure session to TKey.")

	// FIXME need to confirm identity instead of assuming it is correct
	err = device.Session.Secure(info.Identity)
	hardened := tkeywallet.KDF(info.Identity)
	assert.Success(err, "Failed to establish secure connection with TKey.")
	defer io_.ClosePanicked(&device, "Failed to gracefully close connection with TKey.")

	assert.Success(device.Session.Send(0, tkeywallet.CmdGenerateSecret, hardened[:]), "Failed to connect to TKey.")
	data, err := device.Session.Receive(0, tkeywallet.RspGenerateSecret)
	assert.Success(err, "Failed to generate wallet secret.")
	// TODO only very basic file handling, want to lock/unlock, exclusive access to avoid failed writing after updates.
	// FIXME NEVER overwite an existing wallet. Instead, complain that it must be removed if so.
	// FIXME this index is probably off by one, i.e. index 5 (prev. index 1), instead of 6 (prev. index 2)
	log.Traceln("Fingerprint of generated wallet:", hex.EncodeToString(data[1:5]))
	err = os.WriteFile(*outfile, data[5:77], 0600)
	assert.Success(err, "Failed to write sealed wallet to file.")
}

func cmdLoad(args []string) {
	log.Infoln("Loading existing sealed wallet…")
	loadFlags := flag.NewFlagSet("load", flag.PanicOnError)
	infile := loadFlags.String("wallet", "wallet.sealed", "The destination file to store the generated sealed wallet.")
	loadFlags.Parse(args)
	var err error
	var data []byte
	data, err = os.ReadFile(*infile)
	if err != nil {
		log.Errorln("Failed to open wallet-file.", err)
		return
	}
	device, info, err := tkeywallet.ConnectWalleTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to connect to TKey.")
	// FIXME check if reported identity corresponds with wallet-data.
	// TODO mismatched identity may mean: wrong firmware version, wrong tkey, issues with connection, interference by some other process. Needs a bit of explanation.
	// FIXME need to match identity with sealed wallet, then pass on to establish authenticated secure session.
	err = device.Session.Secure(info.Identity)
	assert.Success(err, "Failed to establish secure session.")
	err = device.Session.Send(0, tkeywallet.CmdLoadWallet, data)
	assert.Success(err, "Failed to send command to wallet.")
	_, err = device.Session.Receive(0, tkeywallet.RspLoadWallet)
	assert.Success(err, "Failed to load wallet.")
}

func testwallet() wallet.Wallet {
	return wallet.LoadWallet([...]byte{219, 142, 164, 221, 228, 122, 145, 145, 169, 144, 77, 90, 74, 112, 184, 99, 70, 15, 138, 132, 70, 239, 76, 67, 251, 25, 168, 220, 115, 118, 225, 45})
}

// Bitcoin Core "external signer" support.
//
// Configure 'signer' in settings, or start with `-signer=<path-to-binary>`. (Another possibility: configuring
// path to HWI.py and have signer-binary implement HWI interface instead.)
//
// Note that examples in `external-signer.md` incorrectly leave out an 8th argument that should be 'true' to
// indicate that external signer should be called ('false' by default):
// `bitcoin-cli createwallet "walletkey" true true "" true true true true`
//
// References for the "external signer" API:
// - <https://developer.bitcoin.org/examples/testing.html>
// - <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md>
// - <https://github.com/bitcoin/bitcoin/blob/master/doc/descriptors.md>
// - <https://github.com/bitcoin/bitcoin/blob/master/doc/psbt.md>

const EXIT_CODE_GENERAL_FAILURE int = 1

const CHAIN_MAINNET string = "main"
const CHAIN_TESTNET string = "test"

type config struct {
	args        []string
	logfile     string
	fingerprint string
	chain       string
	descriptor  string
	stdin       bool
}

func (c *config) IsMainNet() bool {
	return c.chain == CHAIN_MAINNET
}

func parseSignerArguments(args []string) config {
	signerFlags := flag.NewFlagSet("signer", flag.ExitOnError)
	var cfg config
	signerFlags.StringVar(&cfg.logfile, "logfile", "walletsigner.log", "Log-file for interactions.")
	signerFlags.StringVar(&cfg.fingerprint, "fingerprint", "", "Fingerprint of the wallet.")
	testnet := signerFlags.Bool("testnet", false, "Indicate whether to operate on Bitcoin testnet.")
	// FIXME flag 'desc' never used because it is a subcommand-specific flag?
	signerFlags.StringVar(&cfg.descriptor, "desc", "", "Descriptor-path.")
	signerFlags.StringVar(&cfg.chain, "chain", "", "Targeted chain: main, test")
	// FIXME undocumented 'stdin' flag presumed to signal passing on raw binary data through Stdin.
	signerFlags.BoolVar(&cfg.stdin, "stdin", false, "Read signer operations from stdin. Allows Bitcoin Core to send instructions over stdin to process.")
	signerFlags.Parse(args)
	cfg.args = signerFlags.Args()
	if *testnet && cfg.chain == "" {
		cfg.chain = CHAIN_TESTNET
	} else if *testnet && cfg.chain != CHAIN_TESTNET {
		log.Errorln("'testnet'-flag set and 'chain'-flag set to non-testchain.")
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	} else if cfg.chain == "" {
		cfg.chain = CHAIN_MAINNET
	}
	log.Tracef("Config: %#v\n", cfg)
	return cfg
}

func configureSigner(cfg *config) error {
	var err error
	var logfile *os.File
	if logfile, err = os.OpenFile(cfg.logfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666); err != nil {
		return errors.Context(err, "failed to open log-file")
	}
	output := io.MultiWriter(os.Stderr, logfile)
	log.SetOutput(output)
	log.SetTraceOutput(output)
	return nil
}

// `enumerate` enumerates signers. (fingerprints)
//
// $ <cmd> enumerate
// [{"fingerprint": "00000000"}]
func cmdSignerEnumerate(_ *config) {
	conn := builtin.Expect(agent.Connect())
	wallets := builtin.Expect(conn.Wallets())
	var b bytes.Buffer
	b.WriteByte('[')
	for fpr, path := range wallets {
		if log.Tracing() {
			log.Traceln(hex.EncodeToString(fpr[:]), path)
		}
		b.WriteString("{\"fingerprint\":\"")
		b.WriteString(hex.EncodeToString(fpr[:]))
		b.WriteString("\",\"name\":\"")
		json.HTMLEscape(&b, []byte(path))
		b.WriteString("\"},")
	}
	b.Truncate(b.Len() - 1)
	b.WriteByte(']')
	_ = builtin.Expect(b.WriteTo(os.Stdout))
}

// `signtransaction` signs the provided PSBT-encoded transaction.
//
// $ <cmd> --fingerprint=<fingerprint> (--testnet) signtransaction <psbt>
// base64_encode_signed_psbt
//
// psbt should include BIP-32 derivations.
// Failure cases:
// - if user cancels
// - if none of the derivations match the keys managed by the device.
// FIXME fail on user cancel
// FIXME fail on zero matches for none of bip32 derivations match user's keys
// FIXME fail if '--testnet' is specified but derivations contain non-test-key-derivations (not `1h`)
func cmdSignerSignTransaction(cfg *config) {
	assert.Positive(len(cfg.fingerprint))
	assert.Positive(len(cfg.args[1]))
	log.Debugln("PSBT:", cfg.args[1])
	// FIXME command-arg 1: PSBT ('external-signer' spec is not explicit on the encoding)
	// FIXME implement handling "signtransaction" command, however Bitcoin Core seems to execute a different procedure that involves providing '--stdin' flag and passing command and PSBT on through stdin.
	panic("Unimplemented: Bitcoin Core v27 passes this data on through stdin with different parameters. Consequently, this part has not yet been implemented.")
}

// `getdescriptors` returns the descriptors supported by the device.
//
// $ <cmd> --fingerprint=00000000 --testnet getdescriptors
//
//	{
//	  "receive": [
//	    "pkh([00000000/44h/0h/0h]xpub6C.../0/*)#fn95jwmg",
//	    "sh(wpkh([00000000/49h/0h/0h]xpub6B..../0/*))#j4r9hntt",
//	    "wpkh([00000000/84h/0h/0h]xpub6C.../0/*)#qw72dxa9"
//	  ],
//	  "internal": [
//	    "pkh([00000000/44h/0h/0h]xpub6C.../1/*)#c8q40mts",
//	    "sh(wpkh([00000000/49h/0h/0h]xpub6B..../1/*))#85dn0v75",
//	    "wpkh([00000000/84h/0h/0h]xpub6C..../1/*)#36mtsnda"
//	  ]
//	}
//
// $ <cmd> --fingerprint=<fingerprint> (--testnet) getdescriptors <account> <xpub>
func cmdSignerGetDescriptors(cfg *config) {
	cmdflags := flag.NewFlagSet("getdescriptors", flag.ExitOnError)
	flagAccount := cmdflags.String("account", "0", "Account identifier")
	if err := cmdflags.Parse(cfg.args[1:]); err != nil {
		log.Errorln("Bad parameters for 'getdescriptors' command:", err)
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	if len(cfg.fingerprint) != 8 {
		log.Errorln("A fingerprint must be provided.")
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	log.Traceln("Account:", *flagAccount)
	var account = strconv.MustParseUintDecimal[uint32](*flagAccount)
	var fpr btc.FingerprintType
	if f, err := hex.DecodeString(cfg.fingerprint); err == nil {
		fpr = btc.FingerprintType(f)
	} else {
		log.Errorln("Bad fingerprint provided:", err.Error())
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	var conn = builtin.Expect(agent.Connect())
	id := builtin.Expect(conn.Identity())
	path := builtin.Expect(conn.Wallet(fpr))
	var device tkeywallet.TKeyWallet
	var err error
	if device, _, err = connectWallet(cfg, id, path); err != nil {
		log.Errorln("Cannot connect to wallet:", err.Error())
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	defer io_.CloseLogged(&device, "Failed to gracefully close connection with walleTKey")
	summary := device.Descriptors(account)
	// TODO not currently escaping the descriptors, because I don't expect them to include illegal characters (yet)
	os.Stdout.WriteString("{\"receive\":[\"" + strings.Join(summary.Receive, "\",\"") +
		"\"],\"internal\":[\"" + strings.Join(summary.Internal, "\",\"") + "\"]}")
}

// `displayaddress` displays the addresses for a specified descriptor.
func cmdSignerDisplayAddress(cfg *config) {
	var err error
	cmdflags := flag.NewFlagSet("displayaddress", flag.ExitOnError)
	flagDesc := cmdflags.String("desc", "", "Descriptor")
	if err = cmdflags.Parse(cfg.args[1:]); err != nil {
		panic("Bad parameters for 'displayaddress' command: " + err.Error())
	}
	assert.Positive(len(*flagDesc))
	var desc descriptor.Descriptor
	if desc, err = descriptor.Parse([]byte(*flagDesc)); err != nil {
		panic("Failed to parse descriptor: " + err.Error())
	}
	var fpr btc.FingerprintType
	if f, err := hex.DecodeString(cfg.fingerprint); err == nil {
		fpr = btc.FingerprintType(f)
	} else {
		log.Errorln("Bad fingerprint provided:", err.Error())
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	conn := builtin.Expect(agent.Connect())
	id := builtin.Expect(conn.Identity())
	path := builtin.Expect(conn.Wallet(fpr))
	var device tkeywallet.TKeyWallet
	if device, _, err = connectWallet(cfg, id, path); err != nil {
		log.Errorln("Cannot connect to wallet:", err.Error())
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
	defer io_.CloseLogged(&device, "Failed to gracefully close connection with walleTKey")
	masterfp, derivedxpub := device.DeriveAddress(desc.Origin.Path)
	assert.EqualSlices(masterfp[:], desc.Origin.Root[:])
	assert.EqualSlices(desc.Key.SerializeCompressed(), derivedxpub.Pubkey.SerializeCompressed())
	address := btc.EncodeP2PKH(cfg.IsMainNet(), [33]byte(derivedxpub.Pubkey.SerializeCompressed()))
	os.Stdout.WriteString("{\"address\":\"" + string(address[:]) + "\"}")
	// TODO does not yet receive signature for validation of origin of address (at current threat-level, program (and its results) are trusted to be non-malicious)
}

// TODO do proper error handling
func cmdSignerStdin(cfg *config) {
	conn := builtin.Expect(agent.Connect())
	id := builtin.Expect(conn.Identity())
	wallets := builtin.Expect(conn.Wallets())
	device, _, err := connectWallet(cfg, id, "")
	assert.Success(err, "Failed to connect to wallet")
	defer io_.CloseLogged(&device, "Failed to gracefully close connection with walleTKey")
	var command []byte
	command, err = io_.ReadUntil(os.Stdin, ' ')
	assert.Success(err, "Failed to read initial command from stdin.")
	log.Traceln("Stdin-command:", string(command))
	switch string(command) {
	case "signtx":
		log.Traceln("Start of signing-session…")
		var encodedpsbt []byte
		encodedpsbt, err = readInputPSBT()
		assert.Success(err, "Failed to read encoded PSBT from stdin.")
		var tx = builtin.Expect(psbt.NewFromRawBytes(bytes.NewReader(encodedpsbt), true))
		err = psbt.InputsReadyToSign(tx)
		assert.Success(err, "PSBT transaction inputs are not ready for signing.")
		// FIXME debugging code
		if log.Tracing() {
			os_.DumpToFileThroughFunc1("walletsigner-before-signing.output", tx.Serialize)
		}
		err = device.SignPSBT(wallets, tx)
		// FIXME debugging code
		if log.Tracing() {
			os_.DumpToFileThroughFunc1("walletsigner-after-signing.output", tx.Serialize)
		}
		assert.Success(err, "Failed to sign PSBT")
		var encoded string
		encoded, err = tx.B64Encode()
		assert.Success(err, "Transaction is bad. Serialization is not possible.")
		os.Stdout.WriteString("{\"psbt\":\"" + encoded + "\",\"error\":null}")
		log.Traceln("Finished signing-session.")
	default:
		log.Infoln("Unsupported command:", string(command))
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
}

func cmdSignerSubcommand(cfg *config) {
	log.Traceln("Command:", cfg.args[0], ", args:", cfg.args[1:])
	switch cfg.args[0] {
	case "enumerate":
		cmdSignerEnumerate(cfg)
	case "signtransaction":
		cmdSignerSignTransaction(cfg)
	case "getdescriptors":
		cmdSignerGetDescriptors(cfg)
	case "displayaddress":
		cmdSignerDisplayAddress(cfg)
	default:
		log.Errorln("unknown/unsupported command")
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
}

// walletsigner implements the "External signer" API as used by Bitcoin Core.
//
// Reference: <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md>
// - `enumerate` (required, <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md#enumerate-required>)
// - `signtransaction` (required, <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md#signtransaction-required>)
// - `getdescriptors` (optional, <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md#getdescriptors-optional>)
// - `displayaddress` (optional, <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md#displayaddress-optional>)
// - Bitcoin development, 'regtest' chain <https://developer.bitcoin.org/examples/testing.html>
// TODO fail if '(reg)test' but cointype is not '1h' (not strictly necessary, but useful to detect control anomalies)
// TODO how to load proper sealed-wallet? Do we need to maintain a local "registry" with fingerprints and sealed-wallet files to find automatically, or do we load manually then have `wallet-signer` interact, or ...?
func cmdSigner(args []string) {
	cfg := parseSignerArguments(args)
	var err error
	log.Traceln("Program arguments:", os.Args)
	if err = configureSigner(&cfg); err != nil {
		log.Errorln("Failed to configure walletsigner:", err)
		return
	}
	if cfg.stdin {
		assert.EmptySlice(cfg.args)
		cmdSignerStdin(&cfg)
	} else if len(cfg.args) > 0 {
		cmdSignerSubcommand(&cfg)
	} else {
		// FIXME walletkey is initialized before program arguments are checked, so takes long time to find out your command is wrong.
		log.Errorln("Either provide a subcommand or flag `--stdin` with instructions over stdin.")
		os.Exit(EXIT_CODE_GENERAL_FAILURE)
	}
}

// A short-cut when program is called as `wallet-signer`, immediately redirecting to `wallet signer` command.
const WALLET_SIGNER_CALL_SHORTCUT = "wallet-signer"

// TODO allow specifying a identity-key on the command-line for verifying the identity without working with a wallet. (Useful when creating wallet, such that one can ensure that identity is same as `verifyfw` results)
// TODO ensure that `identity` is hashed before storing on disk. Verify identity by querying, hashing then comparing with stored hash.
// TODO consider providing unix-pipe or other interop such that we can independently load the wallet and have Bitcoin Core wallet-signer communicate with it.
// FIXME update code to include 64-byte hardened-ID as part of generate/import/load.
// FIXME primarily use embedded firmware-binary, unless otherwise specified using flag.
func main() {
	defer builtin.RecoverLoggedStackTraceExit(1, "Unhandled panic: %+v")
	if path.Base(os.Args[0]) == WALLET_SIGNER_CALL_SHORTCUT {
		// Short-hand to Bitcoin external signer, in case `walletctl` is called as `wallet-signer`.
		cmdSigner(os.Args[1:])
		return
	}
	// The full `walletctl` command-line interface
	firmwarePath := flag.String("firmware", "", "Alternative firmware, specify path to firmware file.")
	flag.Parse()
	if flag.NArg() == 0 {
		// TODO make print-out for commandline flags and subcommands nicer (at some point...)
		flag.PrintDefaults()
		os.Stderr.WriteString("\nSubcommands: agent, export-firmware, generate, load, signer\n\nNote: a symbolic link named 'wallet-signer' will immediately interface with 'signer' subcommand.")
		return
	}
	if *firmwarePath != "" {
		// Override embedded firmware if path to firmware-binary is specified.
		if bin, err := os.ReadFile(*firmwarePath); err != nil {
			log.Errorln("Failed to load firmware from path", *firmwarePath, "(error:", err.Error(), ")")
			os.Exit(EXIT_CODE_GENERAL_FAILURE)
		} else {
			firmwarebin = bin
		}
	}
	args := flag.Args()
	switch args[0] {
	case "export-firmware":
		// note: this will export the specified firmware-binary if provided in program flags.
		cmdExportFirmware(args[1:])
	case "agent":
		cmdAgent(args[1:])
	case "generate":
		cmdGenerate(args[1:])
	case "load":
		cmdLoad(args[1:])
	case "signer":
		cmdSigner(args[1:])
	default:
		os.Stderr.WriteString("Unknown command:" + args[0] + "\n")
		return
	}
}
