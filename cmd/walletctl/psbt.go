package main

import (
	"io"
	"os"

	io_ "github.com/cobratbq/goutils/std/io"
)

func readInputPSBT() ([]byte, error) {
	var err error
	var encodedpsbt []byte
	if encodedpsbt, err = io_.ReadUntil(os.Stdin, ' '); err != nil && err != io.EOF {
		return nil, err
	}
	if encodedpsbt[0] == '"' {
		encodedpsbt = encodedpsbt[1:]
	}
	if encodedpsbt[len(encodedpsbt)-1] == '"' {
		encodedpsbt = encodedpsbt[:len(encodedpsbt)-1]
	}
	// We'll assume only 1 pair of double-quotes, i.e. at start and end of encoded-PSBT. Failing this
	// assumption, it likely means PSBT decoding fails. If this happens, there is likely a (significant)
	// change in format or IPC. (Not assuming more comprehensive use of double-quotes as these are not shell-
	// interactions.)
	return encodedpsbt, nil
}
