package main

import (
	"os"

	tkeywallet "codeberg.org/walletkey/walletkey/internal/wallet"
	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/errors"
	"github.com/cobratbq/goutils/std/log"
)

// connectWallet connects to TKey device, establishes and secure session and, if provided, loads a sealed
// wallet. If the path is empty, no wallet will be loaded.
//
// In case of errors while establishing a connection, no device-connection is returned. In case of errors
// while loading a sealed wallet, the device and app-info are still returned and ready for use.
// TODO allow specifying which firmware-binary should be loaded?
// TODO note that we should ensure that the AppInfo is trustworthy when we return it, given that we already establish a secure session, this should be a reasonable guarantee.
// TODO identity must be provided, but may be all-zero and if so, initially received AppInfo.identity will be used instead, to at least be sure that the promoted identity is owned.
// FIXME need to close connection to device for select error cases.
func connectWallet(cfg *config, id tkey.Identity, path string) (tkeywallet.TKeyWallet, tkey.AppInfo, error) {
	device, info, err := tkeywallet.ConnectWalleTKey(firmwarebin, []byte{})
	if err != nil {
		return tkeywallet.TKeyWallet{}, tkey.AppInfo{}, errors.Context(err, "Failed to connect to walleTKey")
	}
	if id == [32]byte{} {
		log.Traceln("Establishing secure session with queried identity. (No expected identity specified.)")
		err = device.Session.Secure(info.Identity)
	} else if id == info.Identity {
		log.Traceln("Establishing secure session with specified identity.")
		err = device.Session.Secure(id)
	} else {
		return tkeywallet.TKeyWallet{}, tkey.AppInfo{}, errors.Context(errors.ErrFailure, "Provided identity does not match identity provided in device's AppInfo. Aborting due to suspicious circumstances.")
	}
	if err != nil {
		return tkeywallet.TKeyWallet{}, tkey.AppInfo{}, errors.Context(err, "Failed to establish secure session with walleTKey")
	}
	device.SetTesting(!cfg.IsMainNet())
	if path == "" {
		// No wallet to load.
		log.Traceln("Connection with walleTKey established. No sealed wallet provided for loading.")
		return device, info, nil
	}
	// Note: actually superfluous, because restoring wallet also loads it.
	// TODO need to either return device (and info) or close device.
	if w, err := os.ReadFile(path); err != nil {
		return device, info, errors.Context(err, "Failed to read sealed wallet from file at: "+path)
	} else if len(w) != tkeywallet.SEALED_WALLET_LENGTH {
		return device, info, errors.Context(errors.ErrIllegal, "The sealed-wallet file should be exactly 72 bytes in size.")
	} else {
		wallet := tkeywallet.SealedWallet(w)
		// FIXME needs proper error handling
		err = device.LoadWallet(&wallet)
		assert.Success(err, "Failed to load sealed wallet")
	}
	log.Traceln("Connection with walleTKey established. Sealed wallet at", path, "loaded.")
	return device, info, nil
}
