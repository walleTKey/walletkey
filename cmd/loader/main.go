package main

import (
	"os"

	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/log"
)

func main() {
	if len(os.Args) <= 1 {
		log.Infoln("Provide app binary to load as program argument.")
		return
	}
	firmwarebin, err := os.ReadFile(os.Args[1])
	assert.Success(err, "Failed to read firmware-binary")
	tkey, err := tkey.ConnectTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to establish connection with TKey")
	tkey.Close()
}
