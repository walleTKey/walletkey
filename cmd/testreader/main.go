package main

import (
	"os"

	"codeberg.org/walletkey/walletkey/tkey"
	"github.com/cobratbq/goutils/assert"
	"github.com/cobratbq/goutils/std/log"
	"github.com/tillitis/tkeyclient"
)

var Command = tkey.NewCommand("Test", 2, tkeyclient.CmdLen128)

func main() {
	if len(os.Args) <= 1 {
		log.Infoln("Provide app binary to load as program argument.")
		return
	}
	firmwarebin, err := os.ReadFile(os.Args[1])
	assert.Success(err, "Failed to read firmware-binary")
	tkey, err := tkey.ConnectTKey(firmwarebin, []byte{})
	assert.Success(err, "Failed to establish connection with TKey")
	defer tkey.Close()
	data, header, err := tkey.ReadFrame(&Command, 0)
	log.Debugln(header)
	log.Debugln(data)
}
