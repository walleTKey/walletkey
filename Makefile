
.PHONY: all clean check firmware.bin
all: firmware.bin loader verifyfw testloader testreader walletctl wallet-signer

clean:
	make -C firmware clean
	rm -f firmware.bin verifyfw loader walletctl wallet-signer testloader testreader ./cmd/walletctl/firmware.bin

GOLIBSRC=tkey/*.go internal/curve/*.go internal/btc/*.go internal/btc/script/*.go internal/btc/txn/*.go internal/btc/psbt/*.go internal/btc/wallet/*.go internal/btc/wallet/hierarchical/*.go internal/wallet/*.go

loader: go.mod go.sum $(GOLIBSRC) ./cmd/loader/*.go
	go build -o loader ./cmd/loader/

verifyfw: go.mod go.sum $(GOLIBSRC) ./cmd/verifyfw/*.go
	go build -o verifyfw ./cmd/verifyfw/

walletctl: go.mod go.sum $(GOLIBSRC) ./cmd/walletctl/*.go firmware.bin
	cp firmware.bin ./cmd/walletctl/firmware.bin
	go build -tags tracelog -o walletctl ./cmd/walletctl/

wallet-signer: walletctl
	ln -s walletctl wallet-signer

testloader: go.mod go.sum $(GOLIBSRC) ./cmd/testloader/*.go
	go build -o testloader ./cmd/testloader/

testreader: go.mod go.sum $(GOLIBSRC) ./cmd/testreader/*.go
	go build -o testreader ./cmd/testreader/

firmware.bin:
	make -C firmware/ firmware.bin
	cp firmware/firmware.bin firmware.bin

verify:
	make -C firmware/ verify
	govulncheck -show verbose ./...
