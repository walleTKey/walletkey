// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

// TODO clean up unnecessary assert_nonzero_bytes calls, once we know the code is sound.
// TODO dependencies (c-files, implementations) not verified for correctness/security, not hardened.
#include <stdint.h>
#include <tkey/lib.h>
#include <monocypher.h>
#include <monocypher-ed25519.h>
#include "error.h"
// FIXME consider adopting proposed framing-protocol for more expressive error-responses
#include "tkey.h"
#include "session.h"
// TODO eventually, check out possibilities of faults and differential analysis, considering walleTKey a (partial) whitebox with reading, but not modification, possible.
#include "p256k-m.h"
#include "schnorr.h"
#include "sha-256.h"
// TODO eventually, check out possibilities of faults and differential analysis, considering walleTKey a (partial) whitebox with reading, but not modification, possible.
#include "bitcoin.h"

/**
 * OK represents execution/result without errors.
 */
const error_t OK = 0;
/**
 * ERR_BAD indicates bad input or bad use, i.e. the way the function is called does not make sense
 * or is problematic, therefore results in an error.
 */
const error_t ERR_BAD = -1;
/**
 * ERR_STATE indicates bad internal state.
 */
const error_t ERR_STATE = -2;
/**
 * ERR_FAILURE indicates bad outcome from processing. This may be used to indicate running into an
 * unsupported use-case, or a unexpected outcome that makes it impossible to proceed. (When not
 * attributable to bad input or bad state.)
 */
const error_t ERR_FAILURE = -3;
/**
 * ERR_BAD_CONTINUATION indicates that follow-up frames, that continue a comprehensive request,
 * were bad, i.e. contained an error.
 */
const error_t ERR_BAD_CONTINUATION = -4;
/**
 * ERR_BUG indicates an impossible outcome, likely pointing to a bug in the code.
 */
const error_t ERR_BUG = -99;

static const uint8_t app_name[8] = "tkbcwllt";
static const uint32_t app_version = 0x00000001;
static uint8_t app_hash[32];

#define WALLET_LED_COLOR LED_BLUE
#define ADMIN_LED_COLOR LED_RED
#define TOUCH_CONFIRMATION_TIMEOUT 60

// TODO ability to "finish" a session such that reinitialization/reconnect is no longer possible, therefore prevents some malicious process from querying the identity public key.
// TODO addition of admin- and wallet-PINs to introduce authorization of sensitive operations.
/**
 * `appcmd` is the enum containing the firmware-commands.
 *
 * - The pool of command IDs is not shared between requests and responses. Each request receives its
 *   own command-ID. Usually, a response carries the corresponding ID as a matter of predictability
 *   and convenience.
 * - If expected by logic, `FLAG_CONTINUATION` can be used to send follow-up frames for large
 *   payloads.
 */
enum appcmd {
	/**
	 * None/unknown/undefined command-code. Used internally to indicate undeclared or wildcard-
	 * match.
	 */
	CMD_NONE = 0x00,
	/**
	 * Query and response for name, version, checksum, identity.
	 * CMD: empty payload.
	 * RSP: 76-byte payload: 4 bytes for app_name0, 4 bytes for app_name1, 4 bytes LE unsigned
	 * integer app_version, 32-byte app checksum, 32-byte identity (public) key.
	 */
	CMD_APPINFO = 0x01,
	/**
	 * Establish secure connection with negotiated session key.
	 * In case of secure session: 32-byte non-zero public key to perform key-exchange, or  in case
	 * a secure session is active: 32-byte all-zero public key to clear session-key and
	 * re-initialize wallet-key.
	 * CMD: <32-byte their-DHE-public-key>
	 * RSP: <32-byte our-DHE-public-key> || <64-byte encrypted-signature> || <12-byte nonce>
	 */
	// TODO consider making CMD_SECURE_SESSION with all-0xff a command to finish session such that walletkey becomes unusable/unavailable. (similar to how all-zero is reinit)
	CMD_SECURE_SESSION = 0x02,
	/**
	 * Generate wallet secret internally.
	 * CMD: <64-byte hardened-identity>
	 * RSP: <fingerprint> || <sealed-wallet>
	 *
	 * <sealed-wallet> := <24-byte nonce> || <16-byte MAC> || <32-byte wallet-secret>
	 */
	// TODO allow providing randomness through request payload.
	// TODO allow specifying number of iterations to exercise randomness/hashing algorithm during generation.
	CMD_GENERATE_SECRET = 0x03,
	/**
	 * Restore wallet from secret imported from the command-payload.
	 * CMD: <32-byte wallet-secret> || <64-byte hardened-identity>
	 * RSP: <fingerprint> || <sealed-wallet>
	 *
	 * <sealed-wallet> := <24-byte nonce> || <16-byte MAC> || <32-byte wallet-secret>
	 */
	CMD_RESTORE_SECRET = 0x04,
	/**
	 * Load previously created sealed wallet seed/secret.
	 * CMD: <sealed-wallet> || <64-byte hardened-identity>
	 * RSP: <fingerprint>
	 */
	// TODO consider a new feature: a controlled migration that exchanges data between firmware versions such that a raw/unprotected wallet secret is never exposed.
	CMD_LOAD_WALLET = 0x05,
	/**
	 * Export wallet seed/secret.
	 * CMD: empty payload.
	 * RSP: <2-byte little-endian length `seedlength`> || <`seedlength`-bytes wallet secret>
	 */
	// TODO does it make sense to export only in encrypted form with user-supplied password? (i.e. raw wallet secret is not exposed in memory when transferred out of the device into wallet-cli memory?)
	CMD_EXPORT_WALLET_SECRET = 0x06,
	/**
	 * Query wallet information. (without sensitive material)
	 * CMD: empty payload.
	 * RSP: <33-byte compressed public key> || <32-byte chaincode> || <1-byte `numpay` number-of-payment-types> || <`numpay`-byte payment-types>
	 */
	// TODO need to consider if we want to expose xpub (public key information) as it can be used to derive all child wallet pubkeys (you don't want this data copied after leaving the TKey on your desk for an hour)
	CMD_WALLETINFO = 0x07,
	/**
	 * Unload wallet.
	 * CMD: empty payload.
	 * RSP: empty payload.
	 */
	CMD_UNLOAD_WALLET = 0x08,
	/**
	 * Check Bitcoin transaction.
	 * CMD: <32-byte Blake2s-checksum> || <2-byte L-E `length`> || <`length`-bytes transaction-data>
	 * RSP: empty payload.
	 *
	 * Note: in addition to successfully executing the command, success indicates that the device is
	 * now operating in a transaction-handling event-loop, opening up to commands w.r.t transaction
	 * processing, such as to provide additional information, perform additional verification and
	 * finally transaction signing.
	 */
	CMD_LOAD_TRANSACTION = 0x09,
	/**
	 * Sign Bitcoin transaction.
	 * CMD: <1-byte input-index> || <1-byte derivation-path-length> || <`derivation-path-length`*4-byte derivation-path> || <1-byte Bitcoin-hash-type>
	 * RSP: <64-byte P1363-formatted R||s signature>
	 *
	 * The signature is exported in `R||s` format (fixed, 64 bytes), and must be converted to DER
	 * before injection into the Bitcoin transaction.
	 */
	CMD_SIGN_TRANSACTION = 0x0A,
	/**
	 * Derive a hierarchical address from the master's wallet-seed.
	 * CMD: <1-byte `count`> || <`count` 4-byte little-endian path-element>
	 * RSP: <4-byte master-fingerprint> || <33-byte child-pubkey> || <32-byte child-chaincode> || <33-byte parent-pubkey>
	 *
	 * Note: 33-byte pubkey is compressed ECDSA public key.
	 */
	CMD_DERIVE_ADDRESS = 0x0B,
};

#define ROOTKEY_LENGTH 32
#define DHE_LENGTH 32
#define PUBLIC_KEY_LENGTH 32
#define PRIVATE_KEY_LENGTH 64
#define SIGNATURE_LENGTH 64
#define WALLET_SECRET_LENGTH 32

/**
 * The wallet (key) material of the loaded wallet.
 */
// TODO we could do some weird shit like masking/xor-ing the secret with some session secret, such that the memory isn't directly useable, but does it provide any actual benefit?
// TODO move all `wallet_t` operations into separate file?
// FIXME consider storing the `44h` derived legacy key-pair for use during operations, instead of the master seed, saving 2 steps in deriving accounts and keys.
struct wallet_t {
	uint8_t seed[WALLET_SECRET_LENGTH];
};

// TODO introduce admin-pin and check pin before allowing to continue.
// TODO introduce wallet-pin, interaction with smartphone to mitigate risks of PC as compromised device. (threat-model would be useful)
struct context_t {
	/**
	 * The identity. The `secret` should never be exported. Used to prove identity as the secure
	 * session is established. It allows us to reliably determine that we communicate with the same
	 * device as we did previously.
	 * The public key is exported together with a sealed wallet, therefore allowing the user to
	 * establish the device identity is same as the one that generated the sealed wallet.
	 */
	struct {
		uint8_t secret[PRIVATE_KEY_LENGTH];
		uint8_t public[PUBLIC_KEY_LENGTH];
	} identity;
	struct session session;
	struct wallet_t wallet;
};

// TODO what is a better name than `derive_purpose_secret` for deterministically deriving a secret value for a specific purpose/task?
static void derive_purpose_secret(uint8_t out[32], const void* const id, const size_t idlen) {
	if (out == 0 || id == 0 || idlen == 0) {
		abort();
	}
	uint8_t rootkey[ROOTKEY_LENGTH];
	static const uint8_t ID_MASTER[10] = "TKBCMASTER";
	// Calculating an (internal) rootkey first, also ensures that the purpose-secret itself does not
	// become a target for pre-image attack to acquire CDI.
	// TODO we may want to consider adding a dynamically derived component (from CDI?) for deriving the rootkey, such that there is an variable, unknown element as an obstacle when attacking `rootkey` to acquire CDI.
	blake2s256(rootkey, ID_MASTER, sizeof ID_MASTER, (const uint8_t*) TK1_MMIO_TK1_CDI_FIRST, CDI_LENGTH);
	// TODO consider additionally mixing in firmware-ROM data for further randomness. (Firmware is not very random, but it does provide significant additional bytes for processing. The firmware-ROM is tied to a device, which is fine because the secrets are too.)
	blake2s256(out, id, idlen, rootkey, ROOTKEY_LENGTH);
	crypto_wipe(rootkey, ROOTKEY_LENGTH);
}


static void init(struct context_t* const ctx) {
	// init(..) must be idempotent. Full re-initialization must be possible after finishing
	// (clearing) the secure session.
	led_set(LED_BLUE);
	crypto_wipe(ctx, sizeof *ctx);
	uint8_t seed[32];
	static const uint8_t ID_AUTHN[5] = "AUTHN";
	derive_purpose_secret(seed, ID_AUTHN, sizeof ID_AUTHN);
	// TODO consider calling `random_bytes(0, 0, seed, seedlen)` with some arbitrary data to mix some initial randomness into the random_bytes buffer.
	crypto_ed25519_key_pair(ctx->identity.secret, ctx->identity.public, seed);
	assert_nonzero_bytes(ctx->identity.secret, PRIVATE_KEY_LENGTH);
	assert_nonzero_bytes(ctx->identity.public, PUBLIC_KEY_LENGTH);
	led_set(LED_OFF);
}

#define APPINFO_IDX_NAME 0
#define APPINFO_IDX_VERSION (APPINFO_IDX_NAME+8)
#define APPINFO_IDX_IDENTITY (APPINFO_IDX_VERSION+4)
#define APPINFO_IDX_CHECKSUM (APPINFO_IDX_IDENTITY+PUBLIC_KEY_LENGTH)
// TODO consider adding firmware-ROM checksum for impression of secure computation environment. (standard/custom)
//#define APPINFO_IDX_FIRMWAREROM_CHECKSUM (APPINFO_IDX_CHECKSUM+32)

/**
 * app_info provides a response containing app-name, version and identity (public) key.
 * out: <4-byte app-name-0> || <4-byte app-name-1> || <4-byte app-version> || <32-byte public key> || <32-byte checksum hash>
 */
// TODO considering the purpose of the wallet, the identity becomes important as soon as a wallet is sealed (generated or imported seed). Consider only providing an identity with the sealed wallet, instead of on every request. This would be a clear TOFU approach, but one that makes sense, because there is risk only if there is an actual wallet secret in play.
static void app_info(const struct context_t* ctx, uint8_t out[8+4+32+PUBLIC_KEY_LENGTH]) {
	// TODO check command length, response length
	memcpy(&out[APPINFO_IDX_NAME], app_name, 8);
	memcpy(&out[APPINFO_IDX_VERSION], &app_version, 4);
	memcpy(&out[APPINFO_IDX_IDENTITY], ctx->identity.public, PUBLIC_KEY_LENGTH);
	memcpy(&out[APPINFO_IDX_CHECKSUM], app_hash, 32);
}

/**
 * ZERO_RESPONSE is useful to signal a bad frame s.t. not even the command-code is valid, therefore
 * a length-1 response with command-code `0` is sent.
 */
static const uint8_t ZERO_RESPONSE[] = {CMD_NONE};

/**
 * Read, decrypt, parse a received command.
 * In case a `cmdcode` is specified, i.e. not CMD_NONE, then return error if cmdcode does not match.
 */
static error_t read_command_encrypted(struct session* const session, struct cmd_t* const cmd) {
	error_t ret;
	if ((ret = protocol_read_command(cmd))) {
		// TODO temporary led-signal for reading unexpected frame
		led_signal(1, LED_RED, LED_OFF);
		return ret;
	}
	// TODO is this too expensive to execute for every received message?
	random(0, 0, cmd->data, cmd->hdr.len);
	session_crypt(session, cmd->data, cmd->hdr.len, RECEIVE);
	return OK;
}

/**
 * `reply_encrypted` replies according to the expected protocol, but encrypts everything except the
 * frame header. (the first byte; not considered part of the 1/4/32/128-byte payload)
 * The frame header defines only that a frame is intended for the loaded application, an id,
 * response-status, a command-length-class (1/4/32/128). The full command, including command-type,
 * are encrypted.
 *
 * Note: in case of bad requests, `reply_bad` is called/used, which sets the response-bad flag in
 * the header. These responses need not be encrypted, do not count toward nonce-rotation.
 */
static void reply_encrypted(struct session* const session, const struct frame_header* const hdr,
		enum cmdlen len, uint8_t resp[CMDLEN_MAXBYTES]) {
	const size_t num = protocol_framelength(len);
	session_crypt(session, resp, num, SEND);
	// TODO is this too expensive to execute for every received message?
	random(0, 0, resp, num);
	reply(STATUS_OK, hdr->id, hdr->endpoint, len, resp);
}

static error_t wallet_generate(struct context_t* const ctx, uint8_t fingerprint[const BITCOIN_FINGERPRINT_LENGTH]) {
	session_require_secure(&ctx->session);
	// TODO (if we're concerned about randomness), why don't we seed the ciphertext before decryption. The ciphertext is supposed to be hard to predict, based on shared secret in part from remote machine, unique for every session and message.
	// TODO decide on what type of secret data (master seed or seed for p256_gen_keypair), if master seed: decide on size: 256 or 512.
	random(ctx->wallet.seed, sizeof ctx->wallet.seed, 0, 0);
	uint8_t master[32];
	const error_t ret = bitcoin_generate_master_secret(master, 0, ctx->wallet.seed, sizeof ctx->wallet.seed);
	if (ret) {
		return ret;
	}
	uint8_t pubkey[33];
	p256_keypair_from_bytes_compressed(pubkey, master);
	crypto_wipe(master, sizeof master);
	bitcoin_fingerprint(fingerprint, pubkey);
	led_set(LED_GREEN);
	return OK;
}

static int wallet_is_loaded(const struct wallet_t* const wallet) {
	return !all_zero_bytes(wallet->seed, sizeof wallet->seed);
}

static error_t wallet_import_secret(struct context_t* const ctx,
		uint8_t fingerprint[const BITCOIN_FINGERPRINT_LENGTH],
		const uint8_t walletseed[const WALLET_SECRET_LENGTH]) {
	session_require_secure(&ctx->session);
	assert_nonzero_bytes(walletseed, WALLET_SECRET_LENGTH);
	// TODO ciphertext is still malleable. Assume that communication is error-free, and assume 'secure session' adapts for active/passive attacker as design/implementation progresses.
	uint8_t masterkey[32];
	// FIXME need to make this variable-length for wallet seeds.
	if (bitcoin_generate_master_secret(masterkey, 0, walletseed, WALLET_SECRET_LENGTH)) {
		// FIXME proper error handling
		return ERR_FAILURE;
	}
	uint8_t pubkey[33];
	if (p256_keypair_from_bytes_compressed(pubkey, masterkey)) {
		// FIXME proper error handling(?), wiping(?)
		return ERR_BAD;
	}
	crypto_wipe(masterkey, sizeof masterkey);
	bitcoin_fingerprint(fingerprint, pubkey);
	memcpy_s(ctx->wallet.seed, sizeof ctx->wallet.seed, walletseed, WALLET_SECRET_LENGTH);
	led_set(LED_GREEN);
	return OK;
}

#define NONCE_LENGTH 24
#define MAC_LENGTH 16
#define KEY_LENGTH 32
#define SEALED_WALLET_LENGTH (NONCE_LENGTH + MAC_LENGTH + WALLET_SECRET_LENGTH)

#define SEALED_WALLET_IDX_NONCE 0
#define SEALED_WALLET_IDX_MAC NONCE_LENGTH
#define SEALED_WALLET_IDX_WALLET_SECRET (NONCE_LENGTH+MAC_LENGTH)

// TODO decide on a final length for the hardened ID ... maybe 64 bytes is overkill ... as long as it is backed by an expensive computation
#define HARDENED_ID_LENGTH 64

static void wallet_clear(struct wallet_t* const wallet) {
	crypto_wipe(wallet, sizeof *wallet);
	led_set(LED_OFF);
}

static error_t wallet_seal(const struct wallet_t* const wallet, uint8_t out[const SEALED_WALLET_LENGTH],
		const uint8_t identity[const PUBLIC_KEY_LENGTH], const uint8_t hardened_id[const HARDENED_ID_LENGTH]) {
	if (!wallet_is_loaded(wallet)) {
		return ERR_STATE;
	}
	random(&out[SEALED_WALLET_IDX_NONCE], NONCE_LENGTH, 0, 0);
	assert_nonzero_bytes(&out[SEALED_WALLET_IDX_NONCE], NONCE_LENGTH);
	uint8_t key[KEY_LENGTH];
	derive_purpose_secret(key, "SEAL", 4);
	// Seal the wallet secret away. Use the (internally generated) identity public key as Additional-Data.
	uint8_t assoc_data[32];
	blake2s256(assoc_data, identity, PUBLIC_KEY_LENGTH, hardened_id, HARDENED_ID_LENGTH);
	crypto_aead_lock(&out[SEALED_WALLET_IDX_WALLET_SECRET], &out[SEALED_WALLET_IDX_MAC], key, &out[SEALED_WALLET_IDX_NONCE],
		assoc_data, 32, wallet->seed, sizeof wallet->seed);
	crypto_wipe(assoc_data, 32);
	crypto_wipe(key, KEY_LENGTH);
	return OK;
}

// FIXME consider speeding up operations by immediately generating master-key of hierarchical wallet, i.e. directly upon loading. (Especially if we need to derive fingerprint anyways.)
static error_t wallet_load(struct wallet_t* const wallet,
		uint8_t fingerprint[const BITCOIN_FINGERPRINT_LENGTH],
		const uint8_t identity[const PUBLIC_KEY_LENGTH],
		const uint8_t in[const SEALED_WALLET_LENGTH],
		const uint8_t hardened_id[const HARDENED_ID_LENGTH]) {
	uint8_t key[KEY_LENGTH];
	derive_purpose_secret(key, "SEAL", 4);
	uint8_t assoc_data[32];
	blake2s256(assoc_data, identity, PUBLIC_KEY_LENGTH, hardened_id, HARDENED_ID_LENGTH);
	int error = crypto_aead_unlock(wallet->seed, &in[SEALED_WALLET_IDX_MAC], key, &in[SEALED_WALLET_IDX_NONCE],
		assoc_data, sizeof assoc_data, &in[SEALED_WALLET_IDX_WALLET_SECRET], WALLET_SECRET_LENGTH);
	crypto_wipe(assoc_data, sizeof assoc_data);
	crypto_wipe(key, sizeof key);
	if (error) {
		wallet_clear(wallet);
		return ERR_FAILURE;
	}
	// FIXME we could reuse assoc_data-array here for efficiency
	uint8_t masterkey[32];
	error = bitcoin_generate_master_secret(masterkey, 0, wallet->seed, sizeof wallet->seed);
	if (error) {
		crypto_wipe(masterkey, sizeof masterkey);
		wallet_clear(wallet);
		return ERR_FAILURE;
	}
	uint8_t pubkey[33];
	if (p256_keypair_from_bytes_compressed(pubkey, masterkey)) {
		crypto_wipe(masterkey, sizeof masterkey);
		wallet_clear(wallet);
		return ERR_FAILURE;
	}
	bitcoin_fingerprint(fingerprint, pubkey);
	led_set(LED_GREEN);
	return OK;
}

static void wallet_unload(struct wallet_t* const wallet) {
	wallet_clear(wallet);
}

// PAYMENT_TYPE_P2PK should not be supported, is obsolete. It is a security risk: public key is
// (prematurely) exposed.
#define PAYMENT_TYPE_P2PK 0
// PAYMENT_TYPE_P2PKH indicates pay-to-public-key-hash standard-type.
#define PAYMENT_TYPE_P2PKH 1
// PAYMENT_TYPE_P2SH indicates pay-to-script-hash standard-type. (Allows free-form contracts.)
#define PAYMENT_TYPE_P2SH 2

// TODO ensure `supported_payment_types[0]+1 == sizeof supported_payment_types`
static const uint8_t supported_payment_types[3] = {1, PAYMENT_TYPE_P2PKH};

// FIXME replace chaincode length literals
static error_t wallet_info(const struct wallet_t* const wallet, uint8_t out[const 65+sizeof supported_payment_types]) {
	if (!wallet_is_loaded(wallet)) {
		return ERR_STATE;
	}
	uint8_t masterkey[32], chaincode[32];
	if (bitcoin_generate_master_secret(masterkey, chaincode, wallet->seed, sizeof wallet->seed)) {
		// FIXME proper error handling
		return ERR_FAILURE;
	}
	// TODO is there actual value in generating compressed-bytes pubkey here? Why not produce 64-byte pubkey? (or 65-byte if prefix is convenient)
	if (p256_keypair_from_bytes_compressed(out, masterkey)) {
		// FIXME proper error handling
		return ERR_FAILURE;
	}
	crypto_wipe(masterkey, sizeof masterkey);
	memcpy(&out[33], chaincode, 32);
	memcpy(&out[65], supported_payment_types, sizeof supported_payment_types);
	return OK;
}

#define DERIVATION_IDX_MASTER_FINGERPRINT 0
#define DERIVATION_IDX_CHILD_PUBKEY 4
#define DERIVATION_IDX_CHILD_CHAINCODE 37
#define DERIVATION_IDX_PARENT_PUBKEY 69
/** Enforce at least 3 levels of derivation to prevent extraction of master key-pair. */
#define DERIVATION_PATH_COUNT_MIN 3
/** Enforce at most 20 levels of derivation, allowing path to fit in single frame. */
#define DERIVATION_PATH_COUNT_MAX 20

// TODO replace literals with appropriate constants
// FIXME consider returning parent fingerprint, instead of full parent public key, as we do now compute the public key.
static error_t wallet_derive_address(struct wallet_t* const wallet, uint8_t out[const 4+33+32+33],
		const uint8_t* const derivpath, const uint8_t count) {
	// TODO do we need a minimal derivation path if we only ever return the public key?
	if (count < DERIVATION_PATH_COUNT_MIN || count > DERIVATION_PATH_COUNT_MAX) {
		return ERR_BAD;
	}
	error_t ret;
	uint8_t child_key[32], chaincode[32];
	if ((ret = bitcoin_generate_master_secret(child_key, chaincode, wallet->seed, sizeof wallet->seed))) {
		return ret;
	}
	if ((ret = p256_keypair_from_bytes_compressed(&out[DERIVATION_IDX_CHILD_PUBKEY], child_key))) {
		return ret;
	}
	bitcoin_fingerprint(&out[DERIVATION_IDX_MASTER_FINGERPRINT], &out[DERIVATION_IDX_CHILD_PUBKEY]);
	if (count == 0) {
		// if no derivation necessary, write master chaincode, zero parent pubkey and return.
		memcpy(&out[DERIVATION_IDX_CHILD_CHAINCODE], chaincode, 32);
		memset(&out[DERIVATION_IDX_PARENT_PUBKEY], 0, 33);
		return OK;
	}
	// Now take masterkey as parent and derive keys corresponding to specified derivation-path.
	uint8_t parent_key[32];
	for (uint8_t c = 0; c < count; c++) {
		memcpy(parent_key, child_key, 32);
		uint32_t index = decode_uint32(&derivpath[c*4]);
		bitcoin_derive_keys(child_key, chaincode, parent_key, chaincode, index);
	}
	if (p256_keypair_from_bytes_compressed(&out[DERIVATION_IDX_CHILD_PUBKEY], child_key)) {
		return ERR_FAILURE;
	}
	memcpy(&out[DERIVATION_IDX_CHILD_CHAINCODE], chaincode, 32);
	// FIXME we only need the parent's fingerprint (as opposed to full pubkey)
	if (p256_keypair_from_bytes_compressed(&out[DERIVATION_IDX_PARENT_PUBKEY], parent_key)) {
		return ERR_FAILURE;
	}
	// FIXME here probably needs some memory wiping (even if we overwrite on each iteration)
	return OK;
}

static error_t handle_load_wallet(struct context_t* const ctx, struct cmd_t* const command,
		uint8_t fingerprint[BITCOIN_FINGERPRINT_LENGTH]) {
	session_require_secure(&ctx->session);
	uint8_t sealed_wallet[SEALED_WALLET_LENGTH];
	memcpy_s(sealed_wallet, SEALED_WALLET_LENGTH, &command->data[1], SEALED_WALLET_LENGTH);
	uint8_t hardened_id[HARDENED_ID_LENGTH];
	memcpy_s(hardened_id, HARDENED_ID_LENGTH, &command->data[1+SEALED_WALLET_LENGTH], CMDLEN_MAXBYTES-1-SEALED_WALLET_LENGTH);
	error_t error = read_command_encrypted(&ctx->session, command);
	if (error || command->data[0] != (CMD_LOAD_WALLET|FLAG_CONTINUATION)) {
		return error;
	}
	memcpy_s(&hardened_id[CMDLEN_MAXBYTES-1-SEALED_WALLET_LENGTH], HARDENED_ID_LENGTH-(CMDLEN_MAXBYTES-1-SEALED_WALLET_LENGTH),
		&command->data[1], HARDENED_ID_LENGTH-(CMDLEN_MAXBYTES-1-SEALED_WALLET_LENGTH));
	if (all_zero_bytes(hardened_id, HARDENED_ID_LENGTH)) {
		// TODO temporary check, maybe we should drop this.
		return ERR_BAD;
	}
	return wallet_load(&ctx->wallet, fingerprint, ctx->identity.public, sealed_wallet, hardened_id);
}

// TODO prepare_transaction is relevant only when a verifying device is part of the signing process.
//static error_t prepare_transaction(const struct context* const ctx, uint8_t signature[64],
//		uint8_t checksum[32], const uint8_t* const transaction, const size_t len) {
//	if (!is_wallet_loaded(&ctx->wallet)) {
//		return ERR_STATE;
//	}
//	if (bitcoin_check_transaction(transaction, len)) {
//		return ERR_BAD;
//	}
//	// TODO perform rudimentary sanity checks of transaction, possibly find essential values to include in confirmation.
//	// FIXME need to figure out how much transaction validation we *can* perform in this limited capacity
//	// TODO consider what we want to serialize exactly, e.g. encode with a length-prefix to provide more certainty of prepared transaction?
//	blake2s256(checksum, "SigTransactionCheck", 19, transaction, len);
//	crypto_ed25519_sign(signature, ctx->identity.secret, checksum, 32);
//	// FIXME generate checksum, sign with identity for confirmation by verifier
//	return OK;
//}

static size_t min(size_t a, size_t b) {
	return a < b ? a : b;
}

#define TRANSACTION_HEADER_SIZE 34
#define TRANSACTION_BUFFER_MAX 10240
#define TRANSACTION_INOUT_MAX 150

// TODO the checksum would allow for quick verification that expected transaction is loaded, then perform multiple operations on transactions.
// TODO consider using a context-dependent checksum with internal secrets, s.t. it is not so easy to manipulate both transaction-buffer and checksum and expect to succeed. (E.g. take some derivative of CDI to mix in after initially verifying that raw provided checksum matches during transaction loading.)
// TODO using `uint8_t` data-type for txin-/txout-counts. Not expecting to support `> 255` entries with this limited capacity.
// FIXME separate transaction-buffer from transaction-context, have transaction-context contain metadata
struct transaction_ctx {
	const uint8_t* const checksum;
	const size_t size;
	const uint8_t* const buffer;
	struct txmetadata meta;
};

/**
 * transaction_read_header reads the first 34 bytes, which is the transaction header that provides
 * the necessary information to read the full transaction and verify that its contents are
 * correctly transferred.
 *
 * Note: the header is (currently) 34 bytes: `<32-byte checksum><2-byte L-E payload-size>`.
 * See TRANSACTION_HEADER_SIZE.
 */
static error_t transaction_read_header(uint8_t checksum[const 32], size_t* const size, const uint8_t buffer[const], const size_t len) {
	// requires that checksum and payload length are read in one go
	if (len < 34) {
		return ERR_BAD;
	}
	memcpy(checksum, buffer, 32);
	*size = decode_uint16(&buffer[32]);
	if (*size == 0 || *size > TRANSACTION_BUFFER_MAX) {
		return ERR_BAD;
	}
	return OK;
}

/**
 * FIXME describe transaction_read_full
 *
 * Transaction-content: `<32-byte blake2s-checksum> || <2-byte L-E unsigned content-length> ||
 *     <content-length-bytes content>`
 *
 * `data_offset` is the offset from the start of command->data (including command byte), allowing
 * other data to be present before start of the transaction-data.
 * `cmdcode` is the command-code byte that should be present as first byte in every received frame.
 *
 * Returns:
 * - ERR_TX_BAD_CONTINUATION_FRAME bad follow-up frame
 * - ERR_STATE a new request was received instead of the expected follow-up frame
 * - ERR_BAD/ERR_FAILURE issues while processing transaction
 */
static error_t transaction_read_full(struct context_t* const ctx, struct cmd_t* const command,
		struct transaction_ctx* const tx, uint8_t payload[const], const enum appcmd cmdcode) {
	size_t received = 0;
	// Read first frame to get checksum and transaction metadata.
	size_t length = min(tx->size, command->hdr.len-1-TRANSACTION_HEADER_SIZE);
	memcpy_s(payload, tx->size, &command->data[1+TRANSACTION_HEADER_SIZE], length);
	received += length;
	// Then continue reading until full transaction has been received.
	error_t ret;
	while (received < tx->size) {
		if ((ret = read_command_encrypted(&ctx->session, command))) {
			return ret;
		}
		if (command->data[0] != (cmdcode|FLAG_CONTINUATION)) {
			// FIXME custom code to signal for continuation interrupted?
			return ERR_BAD_CONTINUATION;
		}
		length = min(tx->size-received, command->hdr.len-1);
		memcpy_s(&payload[received], tx->size-received, &command->data[1], length);
		received += length;
	}
	if (received > tx->size) {
		abort(); return ERR_BUG;
	}
	return OK;
}

/**
 * Verify the transaction's integrity through calculating the checksum of the transaction buffer
 * and comparing against the known checksum.
 */
static error_t transaction_verify_integrity(const struct transaction_ctx* const tx) {
	uint8_t checksum[32];
	blake2s256(checksum, 0, 0, tx->buffer, tx->size);
	if (crypto_verify32(tx->checksum, checksum)) {
		return ERR_FAILURE;
	}
	return OK;
}

/**
 * Handling transaction-related commands.
 *
 * `handle_transaction` acts as an extension of the event-loop in main, that is specialized for
 * transactions, including a transaction-context.
 *
 * Refs:
 * - <https://en.bitcoin.it/wiki/Protocol_specification>
 */
// TODO need to handle some requests, such as address derivations, without leaving transaction-handling.
static void transaction_handle_requests(struct context_t* const ctx, struct cmd_t* const command,
	uint8_t resp[CMDLEN_MAXBYTES], const struct transaction_ctx* const tx) {
	// FIXME verify that combined output-amounts match with combined input-amounts, because we do not create or disappear value. (No data currently available on input txs. Do we care for purpose of signing, or let client or verifier handle transaction amount validity?)
	// FIXME can we verify scripts, or compare to supported default-scripts?
	// FIXME we cannot verify input scripts, because these are the (empty) sigscripts that will contain signatures once we start signing.
	// FIXME we can compare output-scripts to verify that they are standard scripts, but only to some extent. (i.e. script-execution, or comparing to predefined templates) Need to trust client and/or verifier.
	// FIXME we can verify balance of transaction input/output amounts only if we have received previous transactions, possibly because we also want to know which inputs and outputs are/remain under our control.
	// FIXME if we receive previous tx outputs, we can determine input-amounts
	// FIXME if we receive BIP32-derivations, then we can determine which inputs (and outputs) are under our own control, i.e. delta of amount that we actually lose.
	// FIXME if we receive BIP32-derivations, then we can determine inputs & outputs in control, but is annoying if we have to handle multiple wallets.
	// FIXME Tx-check: sum(inputs) >= sum(outputs), delta (sum(inputs)-sum(outputs)) is treated as transaction fee.
	// FIXME Tx-check: each input must have a scriptSig present, i.e. must unlock funds from previous transaction.
	// FIXME Tx-check: each output must have a scriptPubkey present, i.e. define recipient/unlock-criteria for fund recipients
	// FIXME if previous tx ouputs provided, we can verify that scriptPubkey is as expected for expected signing procedure
	// FIXME consider if we want to execute script as way to ensure unlocking, i.e. don't rely on client, or should that be considered out-of-scope, i.e. not a concern. (untested, valid, invalid)
	error_t ret;
	while (1) {
		if ((ret = read_command_encrypted(&ctx->session, command))) {
			reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_1, ZERO_RESPONSE);
			continue;
		}
		// TODO is verifying checksum on every command too expensive?
		if (transaction_verify_integrity(tx)) {
			// TODO proper way to respond. Do not take kindly to corruption of transaction-buffer.
			abort();
		}
		// FIXME (2.) Receive and process derivations to qualify inputs/outputs as our own. (remaining inputs/outputs are foreign, our inputs/outputs define our delta for this transaction)
		// FIXME (3.) Perform manual/automatic validation, send relevant information signed for authenticity. (Info based on qualified inputs/outputs in our control, and other inputs/outputs)
		// FIXME (4.) After successfully validated, sign relevant inputs.
		memset(resp, 0, CMDLEN_MAXBYTES);
		switch (command->data[0]) {
		// FIXME case CMD_... - a command for providing 2+ BIP-32 derivations s.t. we can validate inputs and UTXO are ours, in our control. (This simplifies transaction validation and reduces the amount of manual validation.)
		case CMD_LOAD_WALLET:
			// support switching wallets while processing a transaction, such that multiple sealed
			// wallets can be used for single transaction.
			resp[0] = CMD_LOAD_WALLET;
			if (handle_load_wallet(ctx, command, &resp[1])) {
				// FIXME should we be returning zero-response of expected length according to command? (consistency, indicating that issue is with processing rather than request)
				reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_1, resp);
				continue;
			}
			reply_encrypted(&ctx->session, &command->hdr, LEN_32, resp);
			break;
		case CMD_SIGN_TRANSACTION:
			resp[0] = CMD_SIGN_TRANSACTION;
			if (!wallet_is_loaded(&ctx->wallet)) {
				reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_1, resp);
				continue;
			}
			// TODO introduce verification + confirmation step (verifier) before (agreeing to) sign
			// TODO introduce touch and/or wallet pin check before signing
			if (!touch_request(TOUCH_CONFIRMATION_TIMEOUT, WALLET_LED_COLOR)) {
				// touch confirmation not received; cancelling
				reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
				continue;
			}
			// FIXME add (optional) redeemscript for P2SH
			// <1-byte input-index> || <1-byte path-length> || <`path-length*4`-byte L-E []uint32 derivation-path> || <1-byte hash-type>
			const uint8_t index = command->data[1];
			const uint8_t count = command->data[2];
			const uint8_t* const path = &command->data[3];
			if (index >= tx->meta.in_count || count < DERIVATION_PATH_COUNT_MIN ||
					count > DERIVATION_PATH_COUNT_MAX || (command->data[3+count*4]&0x7f) > 3) {
				reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
				continue;
			}
			const uint8_t hashtype = command->data[3+count*4];
			// TODO move generating master secret to wallet loading/initialization.
			uint8_t masterkey[32], chaincode[32];
			if ((ret = bitcoin_generate_master_secret(masterkey, chaincode, ctx->wallet.seed, sizeof ctx->wallet.seed))) {
				// FIXME debugging code
				bug(LED_RED, LED_BLUE);
			}
			uint8_t privkey[32];
			if ((ret = bitcoin_derive_path(privkey, chaincode, masterkey, chaincode, path, count))) {
				// FIXME debugging code
				bug(LED_RED, LED_BLUE);
			}
			crypto_wipe(masterkey, sizeof masterkey);
			crypto_wipe(chaincode, sizeof chaincode);
			// Strictly speaking, we should take the pubkeyscript from a UTXO. However, we expect a
			// predefined standard variant that simply encodes (the hash of) a public key, so we can write
			// this script ourselves. If it is incorrect, then we were never (supposed to be) able to release
			// the funds of the UTXO in the first place.
			uint8_t script[25];
			const uint32_t purpose = decode_uint32(&path[0]);
			const size_t scriptlen = bitcoin_write_pubkeyscript(script, purpose, privkey);
			assert(scriptlen <= sizeof script);
			if (scriptlen == 0) {
				crypto_wipe(privkey, sizeof privkey);
				reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
				continue;
			}
			// FIXME debugging code
			led_signal(5, LED_GREEN, LED_GREEN|LED_BLUE);
			// TODO determine how much of utxo/previous-transactions we need to verify on walleTKey to determine safe/correct/sound/etc.
			// TODO double-check if assumption of P2PKH is correct under all circumstances. Check with prev-out's pubkeyscript?
			// TODO need to be able to accept "redeem-script" from client for case of P2SH
			ret = bitcoin_sign_transaction(&resp[1], privkey, index, script, scriptlen, hashtype, &tx->meta);
			crypto_wipe(privkey, sizeof privkey);
			if (ret) {
				// FIXME debugging code
				bug(LED_RED, LED_BLUE);
				// FIXME needs a proper reply-bad response (Not done yet to have access to produced bad results)
			}
			reply_encrypted(&ctx->session, &command->hdr, LEN_128, resp);
			break;
		default:
			// TODO how to handle unknown command? Just drop back to primary event-loop and respond with bad request?
			command->reprocess = 1;
			return;
		}
	}
}

static error_t transaction_prepare_meta(struct context_t* const ctx, struct cmd_t* const command,
		uint8_t resp[CMDLEN_MAXBYTES], struct transaction_ctx* const tx,
		const uint8_t in_count, const uint8_t out_count) {
	if (in_count + out_count > TRANSACTION_INOUT_MAX) {
		abort(); return ERR_BUG;
	}
	struct txinput ins[in_count];
	struct txoutput outs[out_count];
	// Decode transaction-inputs for verification/navigation.
	size_t pos = tx->meta.in_idx;
	size_t n;
	for (uint8_t i = 0; i < in_count; i++) {
		if ((n = bitcoin_decode_txinput(&ins[i], &tx->buffer[pos], tx->size-pos)) == 0) {
			return ERR_FAILURE;
		}
		pos += n;
	}
	// TODO we should be able to check if `pos == tx->metadata.out_idx` here, as sanity-check
	// Decode transaction-outputs for verification/navigation.
	pos = tx->meta.out_idx;
	for (uint8_t i = 0; i < out_count; i++) {
		if ((n = bitcoin_decode_txoutput(&outs[i], &tx->buffer[pos], tx->size-pos)) == 0) {
			return ERR_FAILURE;
		}
		pos += n;
	}
	tx->meta.ins = ins;
	tx->meta.outs = outs;
	transaction_handle_requests(ctx, command, resp, tx);
	tx->meta.outs = 0;
	tx->meta.ins = 0;
	return OK;
}

// TODO <https://nullprogram.com/blog/2019/10/27/> is not positive at all about VLAs, advises against it. However, recommendations use dynamic allocation, which we try to avoid. Consider whether changing current design is necessary, e.g. to maintain control or reduce risks. (Note that VLAs are useful here, because we allocate multiple and this allows dynamic variation between sizes where we only have to be concerned with the combined total size.)
static void transaction_prepare(struct context_t* const ctx, struct cmd_t* const command, uint8_t resp[CMDLEN_MAXBYTES], const size_t size, const uint8_t checksum[const 32]) {
	if (size > TRANSACTION_BUFFER_MAX) {
		abort(); return;
	}
	session_require_secure(&ctx->session);
	uint8_t transaction_buffer[size];
	struct transaction_ctx tx = {
		.checksum = checksum,
		.size = size,
		.buffer = transaction_buffer,
	};
	// Acquire transaction in full, verify internal consistency, and collect metadata for navigation
	// and processing.
	resp[0] = CMD_LOAD_TRANSACTION;
	error_t ret;
	if ((ret = transaction_read_full(ctx, command, &tx, transaction_buffer, CMD_LOAD_TRANSACTION))) {
		// FIXME temporary debug
		led_signal(15, LED_RED, LED_BLUE);
		// Reply for bad transaction-command first, to maintain proper order.
		reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
		if (ret == ERR_BAD_CONTINUATION) {
			// The transaction-transfer is not properly continued, which is a failure of the client.
			// However, the frame itself may be a valid request. Mark the command for reprocessing in the
			// main event-loop.
			command->reprocess = 1;
		}
		// Other error-codes concern handling of the transaction payload, which at this point no longer
		// matters.
		return;
	}
	// With the raw transaction fully loaded, we can now process additional information and
	// eventually start signing inputs to make funds accessible to the transaction.
	if (transaction_verify_integrity(&tx)) {
		reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
		return;
	}
	if (bitcoin_inspect_transaction(&tx.meta, tx.buffer, tx.size) ||
			tx.meta.in_count + tx.meta.out_count > TRANSACTION_INOUT_MAX) {
		reply(STATUS_BAD, command->hdr.id, command->hdr.endpoint, LEN_128, resp);
		return;
	}
	reply_encrypted(&ctx->session, &command->hdr, LEN_1, resp);
	transaction_prepare_meta(ctx, command, resp, &tx, tx.meta.in_count, tx.meta.out_count);
}

/**
 * Stack-protector integration (`-fstack-protector-strong`)
 *
 * Integration with compiler stack-protector facilities. A default implementation of
 * _stack_chk_fail() is provided in tkey.c and can be overridden.
 *
 * ref: <https://embeddedartistry.com/blog/2020/05/18/implementing-stack-smashing-protection-for-microcontrollers-and-embedded-artistrys-libc/>
 */
// TODO stack_chk_guard needs more testing (does it acquire a TRNG value? does it work as expected? etc.)
uintptr_t __stack_chk_guard;

/**
 * Entry-point to the walleTKey firmware program.
 *
 * In case of unexplained aborts/cpu-halts, try:
 * - detect ending up in CPU-halt or `abort()/bug(..)`
 * - disable stack-protector
 * - disable CPU execution monitor (`monitor_application_memory` or `monitor_range` functions)
 */
// TODO we need to consider/evaluate the quality of randomness from random_bytes. We can seed, mix with existing buffer, add a few bytes of TRNG, but we need to be sure that it is sufficient. We can, for example, enlarge the buffer so we keep more historic entropy for mixing.
// TODO we currently trust `blake2s` provided in device native frozen firmware. Alternatively, we could switch to Blake2b from monocypher, but it would be nice to keep this in hardware.
// TODO consider secure communications, introducing key-exchange at start of communication to ensure communication with intended device. (derive keypair from master-key, sign client public key as challenge-response proving authentication, TOFU on generate, store public key with sealed wallet, etc.)
// FIXME choose whether to reply_bad or reply_ok for unknown commands.
// TODO consider using the CPU execution monitor to prevent execution of data (e.g. on stack) to avoid abuse and hacking attempts. (Use APP_ADDR and APP_SIZE to determine app bytes that must be executable.)
// TODO can we do something with keyed-hashing in the TKey firmware checksum with the CDI value before use, such that we add additional unpredictability that cannot later be retrieved due to program in-memory changing during operation? (probably not useful, if CDI is stolen, it can still be used in recreated circumstances)
// TODO must check/verify that destinations are for prod/test with no possibility for (malicious) confusion
// TODO consider that, using only stack, memory locations are very predictable. (Only within process execution, hardware applies randomization.)
__attribute__((noreturn)) int main(void) {
	// calculate firmware hash immediately, before any (memory) changes are accrued.
	tk1_hash_app(app_hash, 0, 0);
	__stack_chk_guard_init_tkey();
	monitor_application_memory();
	struct context_t ctx;
	struct cmd_t command;
	uint8_t resp[CMDLEN_MAXBYTES];
reinit:
	init(&ctx);
	// Restrict available commands to a small subset, as long as a session-key is not available.
	while (!session_is_secure(&ctx.session)) {
		if (protocol_read_command(&command)) {
			reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, ZERO_RESPONSE);
			continue;
		}
		if (command.data[0] & FLAG_CONTINUATION) {
			continue;
		}
		memset(resp, 0, CMDLEN_MAXBYTES);
		switch (command.data[0]) {
		case CMD_APPINFO:
			resp[0] = CMD_APPINFO;
			app_info(&ctx, &resp[1]);
			reply(STATUS_OK, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
			break;
		case CMD_SECURE_SESSION:
			resp[0] = CMD_SECURE_SESSION;
			if (all_zero_bytes(&command.data[1], DHE_LENGTH)) {
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			if (session_secure_session(&ctx.session, &resp[1], ctx.identity.secret, &command.data[1])) {
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			reply(STATUS_OK, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
			break ;
		default:
			resp[0] = command.data[0];
			reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
			break;
		}
	}
	// Session is established, enter loop offering all operations.
	led_signal(2, LED_GREEN, LED_OFF);
	uint8_t checksum[32];
	size_t size;
	while (session_is_secure(&ctx.session)) {
		if (command.reprocess == 0 && read_command_encrypted(&ctx.session, &command)) {
			reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, ZERO_RESPONSE);
			continue;
		}
		if (command.data[0] & FLAG_CONTINUATION) {
			continue;
		}
		// TODO consider following up with raw 32-byte frame containing MAC for previous frame/message. (determined essentially by type of attacker, iff assuming successful I/O)
		memset(resp, 0, CMDLEN_MAXBYTES);
		switch (command.data[0]) {
		case CMD_APPINFO:
			resp[0] = CMD_APPINFO;
			app_info(&ctx, &resp[1]);
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_SECURE_SESSION:
			// TODO consider a special case where we allow an unencrypted CMD_SECURE_SESSION with 32-byte all-zero key to force-reinitialize the wallet-key. This allows resetting the device when the session-key is not known. (But we have to screw around with the straight-forward encryption/decryption control flow.)
			resp[0] = CMD_SECURE_SESSION;
			if (!all_zero_bytes(&command.data[1], DHE_LENGTH)) {
				// Be very strict: do not allow "securing" an already secure session.
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
				continue;
			}
			// Clear the wallet, even if we subsequently loop and completely reinitialize the
			// application-context itself, clear the session and with it, leave the secure session
			// loop and perform a reinitialization of the application-context.
			wallet_clear(&ctx.wallet);
			reply_encrypted(&ctx.session, &command.hdr, LEN_1, resp);
			session_clear(&ctx.session);
			break;
		case CMD_GENERATE_SECRET:
			resp[0] = CMD_GENERATE_SECRET;
			if (all_zero_bytes(&command.data[1], HARDENED_ID_LENGTH)) {
				// TODO consider following outlined error response protocol for more granular response.
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
				continue;
			}
			// TODO it is possible to provide random data as seed entropy in call to random_bytes(0, 0, seed, length);
			if (wallet_generate(&ctx, &resp[1])) {
				abort();
			}
			// Send sealed wallet.
			// <nonce> || <mac> || <encrypted-wallet>
			if (wallet_seal(&ctx.wallet, &resp[5], ctx.identity.public, &command.data[1])) {
				bug(LED_RED, LED_BLUE);
			}
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_RESTORE_SECRET:
			resp[0] = CMD_RESTORE_SECRET;
			if (all_zero_bytes(&command.data[1+WALLET_SECRET_LENGTH], HARDENED_ID_LENGTH)) {
				// TODO consider following outlined error response protocol for more granular response.
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
				continue;
			}
			if (wallet_import_secret(&ctx, &resp[1], &command.data[1])) {
				// FIXME resp no longer completely empty
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			// Send sealed wallet.
			if (wallet_seal(&ctx.wallet, &resp[5], ctx.identity.public, &command.data[1+WALLET_SECRET_LENGTH])) {
				bug(LED_RED, LED_BLUE);
			}
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_LOAD_WALLET:
			// FIXME need to reply_encrypted inside handle_load_wallet, because continuation frames may be bad and responses should be in proper order.
			// FIXME prly better to move response-command into here, and have 'handle_load_wallet' take pointers for data-destinations.
			resp[0] = CMD_LOAD_WALLET;
			if (handle_load_wallet(&ctx, &command, &resp[1])) {
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
				continue;
			}
			reply_encrypted(&ctx.session, &command.hdr, LEN_32, resp);
			break;
		case CMD_UNLOAD_WALLET:
			wallet_unload(&ctx.wallet);
			resp[0] = CMD_UNLOAD_WALLET;
			reply_encrypted(&ctx.session, &command.hdr, LEN_1, resp);
			break;
		case CMD_EXPORT_WALLET_SECRET:
			resp[0] = CMD_EXPORT_WALLET_SECRET;
			if (!wallet_is_loaded(&ctx.wallet)) {
				// TODO crude check to see if wallet is loaded.
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			// FIXME introduce admin-password to verify authorization before continuing export-process.
			if (!touch_request(TOUCH_CONFIRMATION_TIMEOUT, ADMIN_LED_COLOR)) {
				// user did not touch within the time-limit, so we abort
				// TODO we need an 'Abort' signal for deliberate user choices
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			// TODO encode_length still needed? (does help to ensure all bytes were received, maybe need checksum?)
			*(uint16_t*)(&resp[1]) = WALLET_SECRET_LENGTH;
			memcpy(&resp[3], ctx.wallet.seed, sizeof ctx.wallet.seed);
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_WALLETINFO:
			resp[0] = CMD_WALLETINFO;
			if (wallet_info(&ctx.wallet, &resp[1])) {
				// TODO crude check to see if wallet is loaded.
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_DERIVE_ADDRESS:
			resp[0] = CMD_DERIVE_ADDRESS;
			uint8_t count = command.data[1];
			if (wallet_derive_address(&ctx.wallet, &resp[1], &command.data[2], count)) {
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			reply_encrypted(&ctx.session, &command.hdr, LEN_128, resp);
			break;
		case CMD_LOAD_TRANSACTION:
			resp[0] = CMD_LOAD_TRANSACTION;
			if (transaction_read_header(checksum, &size, &command.data[1], command.hdr.len-1)) {
				reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_128, resp);
				continue;
			}
			transaction_prepare(&ctx, &command, resp, size, checksum);
			break;
		default:
			resp[0] = command.data[0];
			reply(STATUS_BAD, command.hdr.id, command.hdr.endpoint, LEN_1, resp);
			break;
		}
	}
	led_set(LED_OFF);
	goto reinit;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Integration with p256-m, Schnorr-signatures
///////////////////////////////////////////////////////////////////////////////////////////////////

/** `p256_generate_random` integrates `random_bytes` with the p256-m library. */
void p256_generate_random(uint8_t* const out, unsigned outlen) {
	random(out, outlen, 0, 0);
}

/** `schnorr_sha256_init` integrates sha256 hashing with Schnorr signatures addition to p256-m. */
void schnorr_sha256_init(void* const ctx) {
	sha256_init((struct sha256_context*)ctx);
}

/** `schnorr_sha256_update` integrates sha256 hashing with Schnorr signatures addition to p256-m. */
void schnorr_sha256_update(void* const ctx, const uint8_t* const data, size_t len) {
	sha256_update((struct sha256_context*)ctx, data, len);
}

/** `schnorr_sha256_finalize` integrates sha256 hashing with Schnorr signatures addition to p256-m. */
void schnorr_sha256_finalize(void* const ctx, uint8_t out[32]) {
	sha256_finalize((struct sha256_context*)ctx, out);
}

