// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#ifndef SECURE_SESSION_UTILS_H
#define SECURE_SESSION_UTILS_H

#include <stddef.h>
#include <stdint.h>
#include "error.h"

#define SESSION_KEY_LENGTH 32
#define SESSION_NONCE_LENGTH 12

enum session_action_t {
  RECEIVE = 0,
  SEND = 1,
};

/**
 * The session data for the established secure session.
 *
 * NONCES:
 * Use two nonces, such that client and device cannot run out-of-sync during communication. A
 * client may send a second request before the first is fully processed, or in case of incorrectly
 * processing a message, we may end up reading fewer frames than sent before we reply. In these
 * cases, if we rely on a single nonce, device and client end up out-of-sync. Given that we can
 * assume communication is in-order and lossless, we know we can maintain synchronicity if we
 * maintain separate nonces for incoming (client-nonce) and outgoing (device-nonce) traffic.
 */
struct session {
	uint8_t key[SESSION_KEY_LENGTH];
	uint8_t clientnonce[SESSION_NONCE_LENGTH];
	uint8_t devicenonce[SESSION_NONCE_LENGTH];
};

/**
 * session_clear clears the current session-key, therefore clearing the session.
 */
void session_clear(struct session* const ctx);

/**
 * session_is_secure checks if a session-key is available, i.e. a secure session is established.
 */
int session_is_secure(const struct session* const ctx);

/**
 * session_require_secure checks if a session-key is available, i.e. a secure session is
 * established, and will abort if not in a secure session.
 */
void session_require_secure(const struct session* const ctx);

/**
 * session_secure_session establishes a secure session by performing a key-exchange using `theirs`
 * as the other party's public key. In addition to a ECDHE key-exchange, additional randomness is
 * added by performing a keyed hash with our public key (using shared secret as key). This 32-byte
 * digest of the keyed hash is established as session-key.
 * The other party's public key (`theirs`) is then signed with (our) identity private-key and the
 * signature encrypted with the established session-key.
 *
 * The response-message carries our ECDHE public key and the (session-)encrypted signature.
 *
 * TODO document the need for statelessness on the device-side, because this AKE does not positively authenticate the connecting client, therefore must not assume anything about the client and instead only work with data provided in the session by the client itself.
 */
error_t session_secure_session(struct session* const ctx, uint8_t response[const SESSION_KEY_LENGTH+64+SESSION_NONCE_LENGTH],
  const uint8_t identity_secret[const 64], const uint8_t theirs[const SESSION_KEY_LENGTH]);

/**
 * session_crypt encrypts provided content (`data` parameter) using the current session-key and
 * nonce in the session-context. (Data is encrypted in-place for efficiency.)
 */
void session_crypt(struct session* const ctx, uint8_t data[const], const size_t len, const enum session_action_t action);

#endif
