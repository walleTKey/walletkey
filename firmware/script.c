#include <stddef.h>
#include <stdint.h>
#include "error.h"
#include "script.h"

#define MAX_DEPTH 25

struct entry {
	size_t length;
	const uint8_t* value;
};

struct scriptctx {
	struct entry stack[MAX_DEPTH];
	size_t stack_idx;
	const uint8_t* const script;
	const size_t script_len;
	size_t script_idx;
};

const uint8_t ZERO[0];
const uint8_t ONE[1] = {1};

#define OP_0 0
#define DATA_01 0x01
#define DATA_75 0x4b
#define OP_1 0x51

#define OP_VERIFY 0x69

// TODO consider if it is possible (within constraints) to verify script consistency first, before any attempt to execute.
static error_t execute_script(struct scriptctx* const ctx) {
	// CHECK: write to stack? => update stack_idx.
	// CHECK: take bytes from script (as data)? => update script_idx
	// TODO check length before taking script-byte as length
	// TODO need to check stack_idx to ensure we're not underflowing
	for (; ctx->script_idx < ctx->script_len; ctx->script_idx++) {
		if (ctx->script[ctx->script_idx] >= DATA_01 && ctx->script[ctx->script_idx] <= DATA_75) {
			// Opcodes: 1 (0x01) -- 75 (0x4b): load next `x` (corresponding to op-code) bytes onto
			// the stack as data.
			ctx->stack_idx += 1;
			ctx->stack[ctx->stack_idx].value = &ctx->script[ctx->script_idx+1];
			ctx->stack[ctx->stack_idx].length = ctx->script[ctx->script_idx];
			// Increment script_idx by length of data pushed onto the stack, this is one off (for
			// the opcode itself) which will be incremented at the next loop iteration.
			ctx->script_idx += ctx->script[ctx->script_idx];
			continue;
		}
		switch (ctx->script[ctx->script_idx]) {
		case OP_0:
			ctx->stack_idx += 1;
			ctx->stack[ctx->stack_idx].length = 0;
			ctx->stack[ctx->stack_idx].value = ZERO;
			continue;
		case OP_1:
			ctx->stack_idx += 1;
			ctx->stack[ctx->stack_idx].length = 1;
			ctx->stack[ctx->stack_idx].value = ONE;
			continue;
		default:
			return ERR_BAD;
		}
	}
	return OK;
}

error_t bitcoin_script_execute(const uint8_t script[const], const size_t length) {
	struct scriptctx ctx = {
		.script = script,
		.script_len = length,
		.script_idx = 0,
		// As we increment the stack idx before use, this will point to 0 when stack first employed.
		// (according to C99 std, unsigned overflow)
		.stack_idx = SIZE_MAX,
	};
	return execute_script(&ctx);
}
