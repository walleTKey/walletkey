// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#ifndef BITCOIN_UTILS_H
#define BITCOIN_UTILS_H

#include <stddef.h>
#include <stdint.h>
#include "error.h"

#define BITCOIN_FINGERPRINT_LENGTH 4

error_t bitcoin_generate_master_secret(uint8_t master_secret[32], uint8_t chain_code[32], const uint8_t seed[const], const size_t len);

void bitcoin_fingerprint(uint8_t out[4], const uint8_t pubkey[33]);

error_t bitcoin_txid(uint8_t digest[32], const uint8_t transaction[const], const size_t len);

error_t bitcoin_derive_keys(uint8_t k_i[32], uint8_t c_i[32], const uint8_t k_parent[32], const uint8_t c_parent[32], const uint32_t i);

error_t bitcoin_derive_path(uint8_t k_i[32], uint8_t c_i[32], const uint8_t k_parent[32], const uint8_t c_parent[32], const uint8_t path[const], const uint8_t count);

size_t bitcoin_write_pubkeyscript(uint8_t script[const 25], const uint32_t purpose, const uint8_t privkey[const 32]);

/**
 * Bitcoin Developer documentation <https://developer.bitcoin.org/devguide/transactions.html>
 * - BIP-16 for P2SH
 * - BIP-68 for relative lock-time
 * - BIP-141 for segregated witnesses
 */
struct txmetadata {
	uint8_t txid[32];
	// TODO insert flag here
	uint32_t version;
	// TODO insert witness here
	uint8_t in_count;
	size_t in_idx;
	// note: `ins` may be NULL until array allocated in separate stage.
	const struct txinput* ins;
	uint8_t out_count;
	size_t out_idx;
	// note: `outs` may be NULL until array allocated in separate stage.
	const struct txoutput* outs;
	/**
	 * locktime
	 *
	 * Standard transactions require: either locktime is in the past (as unix timestamp) or less than
	 * or equal to the current block height, or all of its sequence numbers must be 0xffffffff, i.e.
	 * no "replacement" possible.
	 * Setting all input's sequence numbers to 0xffffffff, effectively disables locktime-capability.
	 */
	uint32_t locktime;
};

error_t bitcoin_inspect_transaction(struct txmetadata* const meta, const uint8_t buffer[const], const size_t len);

error_t bitcoin_sign_transaction(uint8_t signature[const 64], const uint8_t secret[const 32], const uint8_t index,
  const uint8_t* const subscript, const size_t subscriptlen, const uint8_t hashtype, const struct txmetadata* meta);

struct txinput {
	const uint8_t* outhash;
	uint32_t outindex;
	uint16_t script_len;
	const uint8_t* script;
	uint32_t sequence;
};

size_t bitcoin_decode_txinput(struct txinput* const txin, const uint8_t buffer[const], const size_t len);

struct txoutput {
	uint64_t amount;
	uint16_t script_len;
	const uint8_t* script;
};

size_t bitcoin_decode_txoutput(struct txoutput* const txout, const uint8_t buffer[const], const size_t len);

#endif
