#ifndef BITCOIN_SCRIPT_H
#define BITCOIN_SCRIPT_H

#include <stddef.h>
#include <stdint.h>
#include "error.h"

error_t bitcoin_script_execute(const uint8_t script[const], const size_t length);

#endif
