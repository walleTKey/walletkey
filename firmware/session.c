// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#include "session.h"
#include "tkey.h"
#include <monocypher-ed25519.h>

#define SIGNATURE_LENGTH 64

void session_clear(struct session* const ctx) {
	crypto_wipe(ctx, sizeof *ctx);
}

int session_is_secure(const struct session* const ctx) {
	return !all_zero_bytes(ctx->key, SESSION_KEY_LENGTH);
}

void session_require_secure(const struct session* const ctx) {
	if (!session_is_secure(ctx)) {
		abort();
	}
}

static void next_nonce(uint8_t nonce[const SESSION_NONCE_LENGTH]) {
	for (uint8_t i = 0; i < SESSION_NONCE_LENGTH && nonce[i]++ == 0xff; i++);
}

/**
 * `secure` secures communication between security key and client application. The use-case is
 * having confidential communication such that however we reach the security key, we know that
 * communication is confidential, and use a deterministically-derived identity to prove that we
 * continue to communicate with the same device that also generated our sealed wallet secret.
 *
 * After having completed this function, a session-key will be available in the context to use for
 * encrypted communication.
 *
 * RESPONSE: <our-dhe-public-key> || <encrypted-signature> || <encrypted-nonce>
 *
 * Note: crypto_ed25519_sign was chosen because it is most compatible with other libraries, as
 * opposed to uncommon combinations of cipher and hash function, e.g. EdDSA + blake2b.
 */
// TODO eventually, review choices for crypto-primitives after the foundational functionality has proven to work. (See also mindmap, refs; PQC Migration Handbook provides recommendations and most algorithms are listed as acceptable.)
// TODO optimization memory-use: `secret` and `signature` could share single 88-byte buffer. (`secret` byte-array would currently be too small)
// TODO current lack of authentication cannot distinguish manipulations from active attacker. (We could simply send a CmdLen32 MAC after each frame/command/request.)
// TODO currently making assumption that client access is exclusive, therefore no multiple communication streams. Thing is, that if a bad message is injected, then client and firmware will be out-of-sync w.r.t. session-nonce. This only holds for proper requests (bad-request does not have body) and with active interference.
// TODO we need to add context/prefix to ensure that 'identity signature' cannot be abused for other purposes.
error_t session_secure_session(struct session* const ctx,
		uint8_t response[const SESSION_KEY_LENGTH+SIGNATURE_LENGTH+SESSION_NONCE_LENGTH],
		const uint8_t identity_secret[const 64], const uint8_t theirs[const SESSION_KEY_LENGTH]) {
	if (all_zero_bytes(identity_secret, 64) || all_zero_bytes(theirs, SESSION_KEY_LENGTH)) {
		return ERR_BAD;
	}
	uint8_t secret[SESSION_KEY_LENGTH];
	random(secret, sizeof secret, 0, 0);
	assert_nonzero_bytes(secret, sizeof secret);
	// RESPONSE: first, add our public key: needed to produce the DHE secret.
	crypto_x25519_public_key(&response[0], secret);
	assert_nonzero_bytes(&response[0], SESSION_KEY_LENGTH);
	crypto_x25519(secret, secret, theirs);
	assert_nonzero_bytes(secret, sizeof secret);
	// Instead of using the shared secret directly, hash it with ours (responder) public keys
	// Note: we already use `theirs` for authn/identity-proof. Because the firmware's blake2s does
	// not support multiple updates, let's just use our ephemeral public key for now.
	blake2s256(ctx->key, secret, sizeof secret, &response[0], SESSION_KEY_LENGTH);
	crypto_wipe(secret, sizeof secret);
	// Generate proof of identity using their public key as the challenge-response nonce.
	uint8_t payload[SIGNATURE_LENGTH+SESSION_NONCE_LENGTH];
	crypto_ed25519_sign(&payload[0], identity_secret, theirs, SESSION_KEY_LENGTH);
	// Generate random nonce for using during this session.
	// TODO seed random with something such as `theirs` DH public key or the produced signature?
	random(&payload[SIGNATURE_LENGTH], SESSION_NONCE_LENGTH, 0, 0);
	// RESPONSE: next encrypt then concatenate the encrypted signature (and nonces) to the response
	// Use their ephemeral public key as a one-time challenge value for proof-of-identity. This
	// doubles as the proof that the proper value was received for the key-exchange and sessionkey.
	// Send first encrypted message with all-zero nonce. Payload does not contain critically
	// sensitive data, so if we happen upon the same shared secret twice, we do not expose critical
	// information. (We do not need to keep the nonces private, but we need to encrypt anyways to
	// provide key confirmation to the client.)
	memset(ctx->devicenonce, 0, sizeof ctx->devicenonce);
	crypto_chacha20_ietf(&response[SESSION_KEY_LENGTH], payload, sizeof payload, ctx->key, ctx->devicenonce, 0);
	memcpy(ctx->clientnonce, &payload[SIGNATURE_LENGTH], SESSION_NONCE_LENGTH);
	// Adapted client-nonce as device-nonce: flip most-significant bit; puts it in the other half of
	// the available space.
	memcpy(ctx->devicenonce, &payload[SIGNATURE_LENGTH], SESSION_NONCE_LENGTH);
	ctx->devicenonce[SESSION_NONCE_LENGTH-1] ^= 0x80;
	crypto_wipe(payload, sizeof payload);
	return OK;
}

/** session_crypt encrypts/decrypts raw data in-place. */
void session_crypt(struct session* const ctx, uint8_t data[const], const size_t len, const enum session_action_t action) {
	assert_nonzero_bytes(ctx->key, sizeof ctx->key);
	uint8_t* nonce;
	switch (action) {
	case RECEIVE:
		nonce = ctx->clientnonce;
		break;
	case SEND:
		nonce = ctx->devicenonce;
		break;
	default:
		// This should not happen if enum-type is used properly.
		abort();
		return;
	}
	crypto_chacha20_ietf(data, data, len, ctx->key, nonce, 0);
	next_nonce(nonce);
}
