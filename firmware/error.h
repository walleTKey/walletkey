// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#ifndef ERROR_UTILS_H
#define ERROR_UTILS_H

/**
 * A type-alias for a basic error-type.
 *
 * This basic error-type requires that the application declares the constants declared `extern`
 * here, among which: `OK`, `ERR_BAD`, `ERR_STATE`, etc.
 */
typedef int error_t;

// `OK` indicates no error occurred, i.e. successful execution.
extern const error_t OK;
// `ERR_BAD` indicates a problem caused by bad input or bad use by the caller.
extern const error_t ERR_BAD;
// `ERR_STATE` indicates a problem caused by bad internal state (management).
extern const error_t ERR_STATE;
// `ERR_FAILURE` indicates a processing failure, typically a failure because of specific situation
// in processing logic.
extern const error_t ERR_FAILURE;
// `ERR_BUG` indicates a problem caused by ending up in a situation that should not happen, i.e.
// something went wrong that shouldn't, likely indicating a bug in the software.
extern const error_t ERR_BUG;

#endif

