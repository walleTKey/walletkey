// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#include "tkey.h"
#include "tkey/led.h"
#include "tkey/tk1_mem.h"

/**
 * `reply_ok` replies to given frame header with a response of a specified length. The lengths are
 * one of 4 predefined options. In all cases, the `cmd` parameter will occupy the first byte of
 * the response.
 */
void reply(const enum status status, uint8_t id, const enum endpoints endp, const enum cmdlen len, const uint8_t resp[const]) {
	writebyte(genhdr(id, endp, (uint8_t) status, len));
	write(resp, protocol_framelength(len));
}

/**
 * `protocol_read_command` reads a single command and performs sanity-checks concerning commands
 * that do not below to the application. This may involve badly formatted commands or commands that
 * are intended for the FPGA firmware.
 */
error_t protocol_read_command(struct cmd_t* const cmd) {
	cmd->reprocess = 0;
	uint8_t hdrbyte = readbyte();
	if (parseframe(hdrbyte, &cmd->hdr)) {
		// return -1 because of unexpected result. This probably indicates a bad comment was sent.
		return ERR_BAD;
	}
	memset(cmd->data, 0, CMDLEN_MAXBYTES);
	if (read(cmd->data, CMDLEN_MAXBYTES, cmd->hdr.len)) {
		return ERR_FAILURE;
	}
	if (cmd->hdr.endpoint != DST_SW) {
		return ERR_BAD;
	}
	return OK;
}

size_t protocol_framelength(const enum cmdlen len) {
	switch (len) {
	case LEN_1:
		return 1;
	case LEN_4:
		return 4;
	case LEN_32:
		return 32;
	case LEN_128:
		return 128;
	default:
		abort();
	}
}

void encode_uint16(uint8_t out[const 2], const uint16_t v) {
	out[0] = (uint8_t)v;
	out[1] = (uint8_t)(v>>8);
}

void encode_uint32(uint8_t out[const 4], const uint32_t v) {
	out[0] = (uint8_t)v;
	out[1] = (uint8_t)(v>>8);
	out[2] = (uint8_t)(v>>16);
	out[3] = (uint8_t)(v>>24);
}

void encode_uint64(uint8_t out[const 8], const uint64_t v) {
	out[0] = (uint8_t)v;
	out[1] = (uint8_t)(v>>8);
	out[2] = (uint8_t)(v>>16);
	out[3] = (uint8_t)(v>>24);
	out[4] = (uint8_t)(v>>32);
	out[5] = (uint8_t)(v>>40);
	out[6] = (uint8_t)(v>>48);
	out[7] = (uint8_t)(v>>56);
}

uint16_t decode_uint16(const uint8_t in[const 2]) {
	return (uint16_t)((uint16_t)in[0] | (uint16_t)in[1]<<8);
}

uint32_t decode_uint32(const uint8_t in[const 4]) {
	return (uint32_t)((uint32_t)in[0] | (uint32_t)in[1]<<8 | (uint32_t)in[2]<<16 | (uint32_t)in[3]<<24);
}

uint64_t decode_uint64(const uint8_t in[const 8]) {
	return (uint64_t)((uint64_t)in[0] | (uint64_t)in[1]<<8 | (uint64_t)in[2]<<16 | (uint64_t)in[3]<<24 |
		(uint64_t)in[4]<<32 | (uint64_t)in[5]<<40 | (uint64_t)in[6]<<48 | (uint64_t)in[7]<<56);
}

/** `encode_data` encodes data (bytes) in `len(data)+2` bytes. */
void encode_data(uint8_t out[const], const size_t outsize, const size_t offset,
        const uint8_t in[const], const size_t inlen) {
	encode_uint16(&out[offset], (uint16_t)inlen);
	if (outsize-offset-2 < inlen) {
		abort();
	}
	memcpy_s(&out[offset+2], outsize-offset-2, in, inlen);
}

/**
 * `decode_data` decodes encoded data consisting of 2-byte length + `length`-bytes of data.
 *
 * Returns `0` on failure, or `n > 0` upon successful reading, with `n` indicating number of number
 * of bytes read.
 */
size_t decode_data(uint8_t out[const], const size_t outlen, const uint8_t in[const],
		const size_t inlen, const size_t idx) {
	if (idx > inlen) {
		abort();
	}
	if (inlen-idx < 2) {
		return 0;
	}
	const size_t length = decode_uint16(&in[idx]);
	if (inlen-idx-2 < length || outlen < length) {
		return 0;
	}
	memcpy_s(out, outlen, &in[idx+2], length);
	return idx+2+length;
}

// TODO future versions of TKey will use syscall to access Blake2s function; may require changes.
void blake2s256(uint8_t out[const 32], const uint8_t key[const], const size_t keylen,
		const uint8_t in[const], const size_t inlen) {
	if (keylen > 32) {
		// Blake2s produces at most 32 bytes of digest, and works with keys of at most 32 bytes.
		abort();
	}
	blake2s(out, 32, key, keylen, in, inlen);
}

// TODO future versions of TKey will use syscall to access Blake2s function; may require changes.
void blake2s128(uint8_t out[const 16], const uint8_t key[const], const size_t keylen,
		const uint8_t in[const], const size_t inlen) {
	if (keylen > 32) {
		// Blake2s produces at most 32 bytes of digest, and works with keys of at most 32 bytes.
		abort();
	}
	blake2s(out, 16, key, keylen, in, inlen);
}

static const uintptr_t* const APP_ADDR = (const uintptr_t*)TK1_MMIO_TK1_APP_ADDR;
static const uintptr_t* const APP_SIZE = (const uintptr_t*)TK1_MMIO_TK1_APP_SIZE;

void tk1_hash_app(uint8_t out[const 32], const uint8_t key[const], const size_t keylen) {
	// Note: remember that the memory containing the app may change as the app is executing.
	blake2s256(out, key, keylen, (uint8_t*) *APP_ADDR, *APP_SIZE);
}

/**
 * `TK1_ROM_SIZE` is not specified in tk1 memory model. Instead, this is the maximum allowed size that
 * is made available in EBR (Embedded Block RAM) in the verilog model. See
 * `hw/application_fpg/core/rom/rtl/rom.v` in the `tillitis-key1` repository containing the
 * hardware, fpga and firmware content.
 *
 * `rom.v` (verilog):
 *    "Max size for the ROM is 3072 [32-bit] words, and the address is 12 bits to support ROM
 *    with this number of words."
 */
#define TK1_ROM_SIZE (3072 * 4)

void tk1_hash_firmware_rom(uint8_t out[const 32], const uint8_t key[const], const size_t keylen) {
	blake2s256(out, key, keylen, (uint8_t*) TK1_ROM_BASE, TK1_ROM_SIZE);
}

static size_t min(const size_t a, const size_t b) {
	return a < b ? a : b;
}

static volatile uint32_t* const tk1_trng_status = (volatile uint32_t *)TK1_MMIO_TRNG_STATUS;
static volatile uint32_t* const tk1_trng_source = (volatile uint32_t *)TK1_MMIO_TRNG_ENTROPY;

int trng_ready(void) {
	return (*tk1_trng_status & 1) != 0;
}

void trng_wait(void) {
	while (!trng_ready());
}

uint32_t trng_next(void) {
	trng_wait();
	return *tk1_trng_source;
}

// TODO invariants to guard (any more?): (1.) internal buffer should remain unknown, (2.) buffer is mixed up with new unpredictability to frustrate prediction, (3.) prevent guessing between calls to random(), (4.) prevent guessing next (iteration) outputs of randomness
// TODO consider following: risks with using static memory? 4 bytes of true randomness okay? feeding previous buffer into blake? we could rehash the bytes before writing them to out? (that way we don't reuse same bytes we output) We could adopt a larger buffer. (Consider checking implementation of well-known CSRNG for elementary critical improvements within capability)
// TODO obvious weakness: large requests of randomness will have little remixing, i.e. within loop only about 32 bits of randomness and the hash-function.
void random(uint8_t out[const], const size_t outlen, const uint8_t seed[const], const size_t seedlen) {
	static uint8_t buffer[32];
	// Inject seed entropy (if any) into the buffer before acquiring new randomness.
	for (size_t i = 0; i < seedlen; i += 32) {
		// TODO consider feeding in true-randomness here, such that even fixed/predictable data gets mixed with randomness from the start. (e.g. XOR with seed-bytes)
		blake2s(buffer, 32, &seed[i], min(32, seedlen-i), buffer, sizeof buffer);
	}
	if (outlen == 0) {
		// Allow seeding the buffer without taking any output randomness.
		return;
	}
	// TODO without first seeding, it would be possible to have inspected the buffer-memory and know what the next `random` call is going to do (approximately). This is a bit excessive and maybe not a use-case we actually care about.
	uint8_t key[4];
	for (size_t i = 0; i < outlen; i += 32) {
		// TODO improve logic for sampling TRNG or remixing existing entropy until TRNG becomes ready
		if (i == 0 || trng_ready()) {
			encode_uint32(key, *tk1_trng_source);
		} else {
			// TRNG not ready, remix existing (unpredictable) TRNG bytes.
			key[0] ^= buffer[key[0] % 32];
			key[1] ^= buffer[key[1] % 32];
			key[2] ^= buffer[key[2] % 32];
			key[3] ^= buffer[key[3] % 32];
		}
		blake2s(buffer, 32, key, sizeof key, buffer, sizeof buffer);
		// FIXME instead of memcpy-ing, we should blake2s-derive output-randomness, such that internal buffer is not disclosed. (relying/assuming hash-function)
		memcpy_s(&out[i], outlen-i, buffer, min(sizeof buffer, outlen-i));
	}
	// Hash buffer-bytes to make output bytes remaining in buffer disappear.
	static const uint8_t FLUSHKEY[17] = "ByeByeOutputBytes";
	blake2s(buffer, 32, FLUSHKEY, sizeof FLUSHKEY, buffer, sizeof buffer);
}

static void sleeploop(const size_t cycles) {
	for (volatile size_t i = 0; i < cycles; i++);
}

static volatile uint32_t* const tk1_led = (volatile uint32_t *)TK1_MMIO_TK1_LED;

uint8_t led_read(void) {
	return *tk1_led & 0x7;
}

uint8_t led_change(const uint8_t color) {
	const uint8_t prev = led_read();
	*tk1_led = color & 0x7;
	return prev;
}

void led_signal(const size_t count, const uint8_t color1, const uint8_t color2) {
	const uint8_t restore = led_change(LED_OFF);
	for (size_t i = 0; i < count; i++) {
		led_set(color1);
		sleeploop(75000);
		led_set(color2);
		sleeploop(75000);
	}
	led_set(restore);
}

static volatile uint32_t* const tk1_touch = (volatile uint32_t *)TK1_MMIO_TOUCH_STATUS;

uint8_t touch_read(void) {
	return (uint8_t)(*tk1_touch & 1);
}

void touch_reset(void) {
	*tk1_touch = 0;
}

int touch_request(const uint16_t count, const uint8_t color) {
	uint8_t restore = led_read();
	// use quadruple count to account for led double-blinking.
	touch_reset();
	for (size_t i = 0; i < ((size_t)count)*4; i++) {
		led_set(i % 2 == 0 ? LED_OFF : color);
		if (touch_read()) {
			led_set(restore);
			return 1;
		}
		sleeploop(i % 4 == 0 ? 200000 : 70000);
	}
	led_set(restore);
	return 0;
}

/**
 * Request a composite touch-confirmation based on a (possibly random) pattern. The idea is that for
 * the most sensitive operations that may expose critical data, a composite touch request must be
 * successfully performed. That way, it starkly contrasts with the basic touch-requests that already
 * assist ordinary functions.
 *
 * A composite confirmation request will not fool the user due to
 * requesting touch-confirmation repeatedly but only sometimes. Therefore, the user must be actively
 * engaged in the confirmation-activity, as an automatic, mindless touch-confirmation does not
 * suffice.
 */
// TODO playing with the idea of a composite confirmation pattern, possibly random, such that the user must be present and aware of perform the confirmation, before a sensitive opeation is executed (such as communicating sensitive data).
error_t touch_confirmation(const uint32_t pattern, uint8_t num) {
	const uint8_t restore = led_read();
	led_set(LED_OFF);
	// TODO handle case where all bits (0-num) are zero, therefore waiting is the correct confirmation pattern.
	for (--num; num >= 0; --num) {
		const int touch = (pattern & (1 << num)) != 0;
		if (touch != touch_request(3, touch ? LED_BLUE : LED_RED)) {
			led_set(restore);
			return ERR_FAILURE;
		}
		led_signal(1, LED_GREEN, LED_OFF);
	}
	led_set(restore);
	return OK;
}

void monitor_range(const uintptr_t first, const uintptr_t last) {
	static volatile uintptr_t* const CPU_MON_FIRST = (volatile uintptr_t*)TK1_MMIO_TK1_CPU_MON_FIRST;
	static volatile uintptr_t* const CPU_MON_LAST = (volatile uintptr_t*)TK1_MMIO_TK1_CPU_MON_LAST;
	static volatile uint32_t* const CPU_MON_CTRL = (volatile uint32_t*)TK1_MMIO_TK1_CPU_MON_CTRL;
	static uint8_t available = 1;
	if (!available) {
		abort();
	}
	*CPU_MON_FIRST = first;
	*CPU_MON_LAST = last;
	*CPU_MON_CTRL |= 1;
	available = 0;
}

void monitor_application_memory(void) {
	monitor_range(*APP_ADDR + *APP_SIZE, TK1_RAM_BASE + TK1_RAM_SIZE - 1);
}

__attribute__((noreturn))
void bug(const uint8_t color, const uint8_t alter) {
	while(1) {
		led_set(color);
		sleeploop(100000);
		led_set(alter);
		sleeploop(100000);
	}
}

__attribute__((noreturn))
void bug3(const uint8_t color1, const uint8_t color2, const uint8_t color3) {
	while (1) {
		led_set(color1);
		sleeploop(100000);
		led_set(color2);
		sleeploop(100000);
		led_set(color3);
		sleeploop(100000);
	}
}

__attribute__((noreturn))
void abort(void) {
	// LED blinking with 400,000 cycles sleep is about twice as rapid as a CPU halt on illegal
	// instruction.
	while(1) {
		led_set(LED_RED);
		sleeploop(400000);
		led_set(LED_OFF);
		sleeploop(400000);
	}
}

// TODO is a macro better? Don't really care at this moment, but good to consider at some point. (Maybe should be able to disable for "release builds".)
void assert(int condition) {
	if (condition) {
		return;
	}
	abort();
}

int all_zero_bytes(const uint8_t data[const], const size_t len) {
	uint8_t c = 0;
	for (size_t i = 0; i < len; i++) {
		c |= data[i];
	}
	return c == 0;
}

int bytes_compare(const uint8_t a[const], const uint8_t b[const], const size_t len) {
	uint8_t delta = 0;
	for (size_t i = 0; i < len; i++) {
		delta |= a[i] ^ b[i];
	}
	return delta;
}

void assert_nonzero_bytes(const uint8_t data[const], const size_t len) {
	// TODO make NOOP for non-debug builds? (can we rely on compiler dropping/inlining empty function call?)
	if (all_zero_bytes(data, len)) {
		abort();
	}
}

// Stack-protector: basic facilities in case stack-protector is enabled.
// ref: <https://embeddedartistry.com/blog/2020/05/18/implementing-stack-smashing-protection-for-microcontrollers-and-embedded-artistrys-libc/>
extern uintptr_t __stack_chk_guard;

// `__stack_chk_guard_init_tkey` initializes `_stack_chk_guard` with a value from TRNG.
// Note: `__attribute__((no_stack_protector))` is needed to exempt `__stack_chk_guard_init_tkey`
// from `-fstack-protector-all` such that it is allowed to initialize `_stack_chk_guard`.
__attribute__((no_stack_protector))
void __stack_chk_guard_init_tkey(void) {
	while ((*tk1_trng_status & 1) == 0);
	__stack_chk_guard = *tk1_trng_source;
}

// default implementation for __stack_chk_fail that enters an abort-loop. (stack-protector)
// Note: function is marked "weak", so it may be overridden in user applications.
__attribute__((weak,noreturn))
void __stack_chk_fail(void) {
	abort();
}
