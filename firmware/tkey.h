// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

// NOTE: thread-(un)safety: the implementations may use static variables to efficiently (re)use local state, so parallel execution is unsafe/unsound.
// TODO consider/check if header-only implementation with static functions can further reduce size, given that unused functions are trivially discovered.
// TODO consider a function that performs an 'abort()' but with specific pattern to indicate security-violation, such as multiple wrong PIN submissions or hacking attempts, i.e. not a basic software bug/crash.
#ifndef TKEY_UTILS_H
#define TKEY_UTILS_H

#include <tkey/tk1_mem.h>
#include <tkey/lib.h>
#include <tkey/proto.h>
#include <tkey/blake2s.h>
#include <tkey/led.h>
#include "error.h"

#define LED_OFF 0
#define LED_RED (1 << TK1_MMIO_TK1_LED_R_BIT)
#define LED_GREEN (1 << TK1_MMIO_TK1_LED_G_BIT)
#define LED_BLUE (1 << TK1_MMIO_TK1_LED_B_BIT)
#define LED_YELLOW (LED_RED | LED_GREEN)
#define LED_PURPLE (LED_RED | LED_BLUE)
#define LED_CYAN (LED_GREEN | LED_BLUE)
#define LED_WHITE (LED_RED | LED_GREEN | LED_BLUE)

#define CDI_LENGTH 32

struct cmd_t {
	struct frame_header hdr;
	uint8_t data[CMDLEN_MAXBYTES];
	// `reprocess` can be used to indicate that a command-frame needs to be reprocessed, e.g. because
	// it was received with the expectation of being a follow-up frame ('continuation'-flag set) but
	// turned out to be a new request. Setting `reprocess` can signal main loop to handle current
	// command before reading next frame from rx-buffer.
	uint8_t reprocess;
};

#define MASK_COMMANDBYTE 0x7f
#define FLAG_CONTINUATION 0x80

/**
 * `read_protocol_command` reads a full frame and decodes it into the specified struct.
 */
error_t protocol_read_command(struct cmd_t* const out);

/**
 * `framelength` returns the size corresponding to enum `cmdlen`, or aborts on illegal input.
 */
size_t protocol_framelength(enum cmdlen len);

/**
 * `reply` sends a reply, either OK or BAD, with response.
 */
void reply(const enum status status, const uint8_t id, const enum endpoints endpoint, const enum cmdlen len, const uint8_t resp[const]);

void encode_uint16(uint8_t out[const 2], const uint16_t v);

void encode_uint32(uint8_t out[const 4], const uint32_t v);

void encode_uint64(uint8_t out[const 8], const uint64_t v);

uint16_t decode_uint16(const uint8_t in[const 2]);

uint32_t decode_uint32(const uint8_t in[const 4]);

uint64_t decode_uint64(const uint8_t in[const 8]);

/**
 * `encode_data` encodes data by first encoding a 2-byte length `len`, followed by `len` bytes of raw data.
 */
void encode_data(uint8_t out[const], size_t outlen, size_t offset, const uint8_t in[const], size_t inlen);

/**
 * `decode_data` decodes a 2-byte length `len`, followed by reading `len` bytes of raw data.
 */
size_t decode_data(uint8_t out[const], size_t outsize, const uint8_t in[const], size_t inlen, size_t idx);

/**
 * `blake2s256` calculates a 32-byte hash digest using in-firmware blake2s.
 */
void blake2s256(uint8_t out[32], const uint8_t key[const], const size_t keylen, const uint8_t in[const], const size_t inlen);

/**
 * `blake2s128` calculates a 16-byte hash digest using in-firmware blake2s.
 */
void blake2s128(uint8_t out[16], const uint8_t key[const], const size_t keylen, const uint8_t in[const], const size_t inlen);

/**
 * `tk1_hash_app` calculates the Blake2s hash of the in-memory application that is running.
 * Note: this produces the same hash, as is computed upon uploading the application, *only* if no in-memory changes were made yet.
 */
void tk1_hash_app(uint8_t out[32], const uint8_t key[const], const size_t keylen);

/**
 * `tk1_hash_firmware_rom` calculates the Blake2s hash of the TKey firmware ROM.
 * Note: the firmware-ROM is protected by the CPU execution monitor, but is still readable.
 */
void tk1_hash_firmware_rom(uint8_t out[32], const uint8_t key[const], const size_t keylen);

/**
 * `trng_ready` returns true iff TRNG entropy source is saturated.
 */
int trng_ready(void);

/**
 * `trng_wait` waits (blocking) until TRNG is ready.
 */
void trng_wait(void);

/**
 * `trng_next` returns next 4 bytes (32-bit) after waiting for TRNG to become ready.
 */
uint32_t trng_next(void);

/**
 * `random` takes entropy from the TRNG, mixes them with an existing buffer using Blake2s, then
 * takes the necessary amount of randomness to satisfy the caller. There is an optional `seed` and
 * `seedlen` that allow adding to existing randomness by mixing in the seed with the buffer using
 * Blake2s. If 0 length seed is provided, this action is skipped. Similarly, if outlen is 0, no
 * randomness is taken.
 */
void random(uint8_t out[const], const size_t len, const uint8_t seed[const], const size_t seedlen);

/**
 * `read_led` reads the current value for the TK1 led.
 */
uint8_t led_read(void);

/**
 * `signal_error` rapidly blinks the led to signal an error during processing, then resumes
 * control-flow.
 */
void led_signal(size_t count, uint8_t color1, uint8_t color2);

/**
 * `read_touch` reads the current value for the TK1 touch-sensor.
 */
uint8_t touch_read(void);

/**
 * `reset_touch` resets the TK1 touch-sensor.
 */
void touch_reset(void);

/**
 * `request_touch` requests touch-confirmation from the user.
 * `color` the color for the LED-signal
 */
int touch_request(uint16_t count, uint8_t color);

/**
 * `monitor_range` is a one-time function that can be called to set the CPU execution monitor to
 * monitor the specified range for attempts of executing memory that are not program code.
 *
 * `first` represents the first memory address of the range.
 * `last` represents the last memory address (inclusive) of the range.
 *
 * Note: only one CPU execution monitor range may be set. A second call will result in an abort.
 */
void monitor_range(uintptr_t first, uintptr_t last);

/**
 * Monitor all memory (remaining) that is available to the loaded application.
 *
 * See `monitor_range` function for more details.
 *
 * Note: only one CPU execution monitor range may be set. A second call will result in an abort.
 */
void monitor_application_memory(void);

/**
 * `bug` executes an infinite loop blinking the led with alternating led colors.
 */
__attribute__((noreturn))
void bug(uint8_t color, uint8_t alter);

/**
 * `bug3` executes an infinite loop blinking the led, alternating between 3 specified colors.
 */
__attribute__((noreturn))
void bug3(uint8_t color1, uint8_t color2, uint8_t color3);

/**
 *`abort` enters an infinite loop with a blinking red LED to signal that control-flow entered a
 * dead end, usually a bad situation. `abort` blinks about twice as rapid as the dead-state
 * initiated by a CPU halt on illegal instruction.
 */
__attribute__((noreturn))
void abort(void);

void assert(int condition);

/** all_zero_bytes checks if a byte-array contains all zeroes. */
int all_zero_bytes(const uint8_t data[const], size_t len);

/**
 * Compare two byte-arrays of equal length on all elements, return delta. Any difference is
 * indicated with a non-zero result. The comparison always compares all bytes in the arrays.
 * Byte-arrays must be of equal length, as indicated by the single `len` parameter.
 */
int bytes_compare(const uint8_t a[const], const uint8_t b[const], const size_t len);

/** assert_nonzero_bytes aborts if only zeroes are found in the byte-array. */
void assert_nonzero_bytes(const uint8_t data[const], size_t len);

/**
 * Initialize the stack-guard.
 *
 * tkey.{c,h} provide an `init` and `__stack_chk_fail` function to facilitate stack-protector
 * functionality. Both rely on `extern uintptr_t __stack_chk_guard`.
 */
void __stack_chk_guard_init_tkey(void);

#endif
