// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#include "bitcoin.h"

#include <monocypher-ed25519.h>
#include <tkey/lib.h>
#include "error.h"
#include "monocypher.h"
#include "p256k-m.h"
#include "schnorr.h"
#include "sha-256.h"
#include "rmd160.h"
#include "tkey.h"

extern const error_t OK;
extern const error_t ERR_BAD;
extern const error_t ERR_FAILURE;
extern const error_t ERR_BUG;

/**
 * bitcoin_compress_pub converts a public key into compressed format with prefix 02 (even) or 03 (odd).
 */
// TODO unused/needs testing.
static void bitcoin_compress_pub(uint8_t out[const 33], const uint8_t pub[const 64]) {
	out[0] =  pub[63] % 2 == 0 ? 0x02 : 0x03;
	memcpy(&out[1], pub, 32);
}

// bitcoin_generate_master_secret generates a master-keypair and chaincode given a seed.
// Note: `chain_code` is optional, i.e. if non-zero address provided.
error_t bitcoin_generate_master_secret(uint8_t master_secret[const 32], uint8_t chain_code[const 32], const uint8_t seed[const], const size_t len) {
	static const uint8_t BITCOIN_SEED_KEY[12] = "Bitcoin seed";
	crypto_sha512_hmac_ctx ctx;
	crypto_sha512_hmac_init(&ctx, BITCOIN_SEED_KEY, 12);
	crypto_sha512_hmac_update(&ctx, seed, len);
	uint8_t hmac[64];
	crypto_sha512_hmac_final(&ctx, hmac);
	// TODO how to handle failure; continue looping? (needs to be deterministic all wallets generate same master secret)
	if (p256_scalar_check(&hmac[0])) {
		return ERR_FAILURE;
	}
	memcpy(master_secret, &hmac[0], 32);
	if (chain_code != 0) {
		memcpy(chain_code, &hmac[32], 32);
	}
	crypto_wipe(hmac, 64);
	return OK;
}

// sha256d = SHA256(SHA256(in[0:len])) as 32-byte value
static void sha256d(uint8_t out[const 32], const uint8_t in[const], const size_t len) {
	struct sha256_context shactx;
	sha256_init(&shactx);
	sha256_update(&shactx, in, len);
	sha256_finalize(&shactx, out);
	sha256_init(&shactx);
	sha256_update(&shactx, out, 32);
	sha256_finalize(&shactx, out);
}

// hash160 = RIPEMD160(SHA256(in[0:len])) as 20-byte value
static void hash160(uint8_t out[const 20], const uint8_t in[const], const size_t len) {
	struct sha256_context shactx;
	sha256_init(&shactx);
	sha256_update(&shactx, in, len);
	uint8_t interim[32];
	sha256_finalize(&shactx, interim);
	unsigned long rmdctx[5];
	ripemd160_MDinit(rmdctx);
	// for messages larger than 64-bytes (`unsigned long X[16]`), process in chunks of 64 bytes, until
	// there are less than 64 bytes left. The remaining bytes will be processed during
	// `ripemd160_MDfinish`. Note that `ripemd160_MDfinish` expects the total length, i.e. not the
	// remainder.
	//ripemd160_compress(rmdctx, X) 
	ripemd160_MDfinish(rmdctx, interim, 32, 0);
	for (uint8_t i = 0; i < 20; i += 4) {
		encode_uint32(&out[i], rmdctx[i>>2]);
	}
}

void bitcoin_fingerprint(uint8_t out[const BITCOIN_FINGERPRINT_LENGTH], const uint8_t pubkey[const 33]) {
	uint8_t buffer[20];
	hash160(buffer, pubkey, 33);
	memcpy(out, buffer, BITCOIN_FINGERPRINT_LENGTH);
}

static const uint32_t HARDENED_CHILD = 0x80000000;

// Note: `c_i` and `c_parent` may be same. (`c_parent` is only used to init sha512, `c_i` only
// written to near the end)
static error_t CKD_priv(uint8_t k_i[const 32], uint8_t c_i[const 32], const uint8_t k_parent[const 32], const uint8_t c_parent[const 32], const uint32_t i) {
	if (p256_scalar_check(k_parent)) {
		return ERR_BAD;
	}
	crypto_sha512_hmac_ctx ctx;
	crypto_sha512_hmac_init(&ctx, c_parent, 32);
	if (i >= HARDENED_CHILD) {
		crypto_sha512_hmac_update(&ctx, (uint8_t[]){0}, 1);
		crypto_sha512_hmac_update(&ctx, k_parent, 32);
	} else {
		// `ser_P(point(k_parent))`
		uint8_t K_parent_comp[33];
		p256_keypair_from_bytes_compressed(K_parent_comp, k_parent);
		crypto_sha512_hmac_update(&ctx, K_parent_comp, sizeof K_parent_comp);
	}
	// `ser_32(i)`
	crypto_sha512_hmac_update(&ctx, (uint8_t[]){(uint8_t)(i >> 24), (uint8_t)(i >> 16), (uint8_t)(i >> 8), (uint8_t)i}, 4);
	uint8_t I[64];
	crypto_sha512_hmac_final(&ctx, I);
	memcpy(k_i, &I[0], 32);
	if (p256_scalar_add(k_i, k_parent)) {
		// FIXME handle errors
		return ERR_FAILURE;
	}
	memcpy(c_i, &I[32], 32);
	crypto_wipe(I, sizeof I);
	return OK;
}

// bitcoin_derive_keys derives a keypair from provided parent keypair and chaincode.
//
// Note: `c_i` and `c_parent` may be same memory-address, this will update the chaincode over the
// original.
error_t bitcoin_derive_keys(uint8_t k_i[const 32], uint8_t c_i[const 32], const uint8_t k_parent[const 32], const uint8_t c_parent[const 32], const uint32_t i) {
	return CKD_priv(k_i, c_i, k_parent, c_parent, i);
}

// bitcoin_derive_path derives a keypair from provided parent keypair and chaincode and a
// derivation-path.
//
// `path` is an 4-byte L-E encoded derivation-path, i.e. each 4 bytes represent an uint32_t, with
// `count` indicating the number of 4-byte entries available, i.e. number of derivation steps.
//
// Note: `c_i` and `c_parent` may be same memory-address, this will update the chaincode over the
// original.
error_t bitcoin_derive_path(uint8_t k_i[const 32], uint8_t c_i[const 32], const uint8_t k_parent[const 32], const uint8_t c_parent[const 32], const uint8_t* path, const uint8_t count) {
	memcpy(k_i, k_parent, 32);
	if (c_i != c_parent) {
		memcpy(c_i, c_parent, 32);
	}
	if (count == 0) {
		return OK;
	}
	uint8_t prevkey[32];
	uint32_t index;
	error_t ret;
	for (uint8_t i = 0; i < count; i++) {
		memcpy(prevkey, k_i, 32);
		index = decode_uint32(&path[i*4]);
		if ((ret = CKD_priv(k_i, c_i, prevkey, c_i, index))) {
			return ret;
		}
	}
	return OK;
}

/**
 * Read varint into uint8_t from buffer.
 * Return indicates number of bytes processed, with 0 for failure.
 * Failure if data-type is too small to contain value, or if buffer is too small.
 */
static size_t decode_varint_uint8(uint8_t* const dest, const uint8_t buffer[const], const size_t len) {
	if (len < 1 || (buffer[0] == 0xfd && (len < 3 || buffer[2] > 0)) || buffer[0] > 0xfd) {
		return 0;
	}
	if (buffer[0] < 0xfd) {
		*dest = buffer[0];
		return 1;
	}
	*dest = buffer[1];
	return 3;
}

/**
 * Read varint into uint16_t from buffer.
 * Return indicates number of bytes processed, with 0 for failure.
 * Failure if data-type is too small to contain value, or if buffer is too small.
 */
static size_t decode_varint_uint16(uint16_t* const dest, const uint8_t buffer[const], const size_t len) {
	if (len < 1 || (buffer[0] == 0xfd && len < 3) || buffer[0] > 0xfd) {
		return 0;
	}
	if (buffer[0] < 0xfd) {
		*dest = buffer[0];
		return 1;
	}
	*dest = (uint16_t)buffer[1];
	*dest |= (uint16_t)buffer[2] << 8;
	return 3;
}

/**
 * Read varint into uint32_t from buffer.
 * Return indicates number of bytes processed, with 0 for failure.
 * Failure if data-type is too small to contain value, or if buffer is too small.
 */
static size_t decode_varint_uint32(uint32_t* const dest, const uint8_t buffer[const], const size_t len) {
	if (len < 1 || (buffer[0] == 0xfd && len < 3) || (buffer[0] == 0xfe && len < 5) || buffer[0] > 0xfe) {
		return 0;
	}
	if (buffer[0] < 0xfd) {
		*dest = buffer[0];
		return 1;
	}
	*dest = (uint32_t)buffer[1];
	*dest |= (uint32_t)buffer[2] << 8;
	if (buffer[0] == 0xfd) {
		return 3;
	}
	*dest |= (uint32_t)buffer[3] << 16;
	*dest |= (uint32_t)buffer[4] << 24;
	return 5;
}

/**
 * Read varint into uint64_t from buffer.
 * Return indicates number of bytes processed, with 0 for failure.
 * Failure if data-type is too small to contain value, or if buffer is too small.
 */
static size_t decode_varint_uint64(uint64_t* const dest, const uint8_t buffer[const], const size_t len) {
	if (len < 1 || (buffer[0] == 0xfd && len < 3) || (buffer[0] == 0xfe && len < 5) || (buffer[0] == 0xff && len < 9)) {
		return 0;
	}
	if (buffer[0] < 0xfd) {
		*dest = buffer[0];
		return 1;
	}
	*dest = (uint64_t)buffer[1];
	*dest |= (uint64_t)buffer[2] << 8;
	if (buffer[0] == 0xfd) {
		return 3;
	}
	*dest |= (uint64_t)buffer[3] << 16;
	*dest |= (uint64_t)buffer[4] << 24;
	if (buffer[0] == 0xfe) {
		return 5;
	}
	*dest |= (uint64_t)buffer[5] << 32;
	*dest |= (uint64_t)buffer[6] << 40;
	*dest |= (uint64_t)buffer[7] << 48;
	*dest |= (uint64_t)buffer[8] << 56;
	return 9;
}

/**
 * Decode transaction input, returning the size of the entry.
 *
 * txin: if not `0`, destination for transaction input metadata.
 *
 * Returns number of bytes used for the entry, or `0` in case of bad input or other errors.
 */
size_t bitcoin_decode_txinput(struct txinput* const txin, const uint8_t buffer[const], const size_t len) {
	if (len < 37) {
		return 0;
	}
	// [32-byte] previous output hash (&buffer[0])
	// [4-byte] previous output index (&buffer[32])
	// [`scriptlen`-bytes] varint-length scriptSig
	uint16_t scriptlen;
	size_t n;
	if ((n = decode_varint_uint16(&scriptlen, &buffer[32+4], len-32-4)) == 0 ||
			len < 36+n+scriptlen+4) {
		return 0;
	}
	// [`scriptlen`-bytes] signature-script (&buffer[36+`n`])
	// [4-byte] sequence number (&buffer[36+`n`+`scriptlen`])
	if (txin != 0) {
		txin->outhash = &buffer[0];
		txin->outindex = decode_uint32(&buffer[32]);
		txin->script_len = scriptlen;
		txin->script = &buffer[32+4+n];
		txin->sequence = decode_uint32(&buffer[32+4+n+scriptlen]);
	}
	return 32+4+n+scriptlen+4;
}

size_t bitcoin_decode_txoutput(struct txoutput* const txout, const uint8_t buffer[const], const size_t len) {
	if (len < 9) {
		return 0;
	}
	// [8-byte] amount (in satoshis) (&buffer[0])
	uint16_t scriptlen;
	// [`scriptlen`-byte] varint-length of pubkeyscript (&buffer[8])
	size_t n;
	if ((n = decode_varint_uint16(&scriptlen, &buffer[8], len-8)) == 0 ||
			len < 8+n+scriptlen) {
		return 0;
	}
	if (txout != 0) {
		txout->amount = decode_uint64(&buffer[0]);
		txout->script_len = scriptlen;
		txout->script = &buffer[8+n];
	}
	return 8+n+scriptlen;
}

#define VERSION_MIN 1
#define VERSION_MAX 2

/**
 * Parse bitcoin transaction provided as raw transaction.
 *
 * Returns:
 * OK: if transaction is fully parseable with no surprises.
 * ERR_BAD: if transaction contains bad content or is incomplete or otherwise problematic.
 * ERR_FAILURE: if transaction is not successfully parsed, e.g. because of missing support. (SegWit)
 *
 * test-material:
 * - <https://bitcoinexplorer.org/tx/c0156f6835ddfc53f037361dece35c401ab4b585d4e413ab1591ba67b6fe3519#JSON>
 */
error_t bitcoin_inspect_transaction(struct txmetadata* const meta, const uint8_t buffer[const], const size_t len) {
	if (len < 5) {
		return ERR_BAD;
	}
	meta->version = decode_uint32(buffer);
	if (meta->version < VERSION_MIN || meta->version > VERSION_MAX) {
		// Consider unsupported version a failure, s.t. we can distinguish between unsupported and
		// supported-but-content-bad.
		return ERR_FAILURE;
	}
	if (buffer[4] == 0) {
		// TODO for now, do not allow segwit format, yet. And, in any case, not supported yet.
		return ERR_FAILURE;
	}
	size_t pos = 4;
	size_t n;
	if ((n = decode_varint_uint8(&meta->in_count, &buffer[pos], len-pos)) == 0 ||
			len <= pos+n) {
		return ERR_BAD;
	}
	pos += n;
	// Start processing tx-inputs ...
	meta->in_idx = pos;
	for (uint8_t i = 0; i < meta->in_count; i++) {
		if ((n = bitcoin_decode_txinput(0, &buffer[pos], len-pos)) == 0) {
			return ERR_BAD;
		}
		pos += n;
	}
	if (len < pos+1 || (n = decode_varint_uint8(&meta->out_count, &buffer[pos], len-pos)) == 0 ||
			len <= pos+n) {
		// TODO larger than uint8?
		return ERR_BAD;
	}
	pos += n;
	// Start processing tx-outputs ...
	meta->out_idx = pos;
	for (uint8_t i = 0; i < meta->out_count; i++) {
		if ((n = bitcoin_decode_txoutput(0, &buffer[pos], len-pos)) == 0) {
			return ERR_BAD;
		}
		pos += n;
	}
	if (len < pos+4) {
		return ERR_BAD;
	}
	meta->locktime = decode_uint32(&buffer[pos]);
	pos += 4;
	return pos == len ? OK : ERR_FAILURE;
}

/**
 * bitcoin_write_pubkeyscript writes the public-key script corresponding to a certain "purpose" in
 * the hierarchical deterministic wallet derivation-path.
 *
 * Supported:
 * - P2PKH (pay-to-public-key-hash, legacy): scriptPubKey: `OP_DUP OP_HASH160 <20-byte pubKeyHash>
 *   OP_EQUALVERIFY OP_CHECKSIG`, scriptSig: `<sig> <pubKey>`
 * Unsupported/obsolete:
 * - P2PK (pay-to-public-key): scriptPubKey: `<pubKey> OP_CHECKSIG`, scriptSig: `<sig>`
 * To-do:
 * - P2SH (pay-to-script-hash): scriptPubKey: `OP_HASH160 <scriptHash> OP_EQUAL`, scriptSig:
 *   `..sigs..<serialized-script>` (Note that this script is any arbitrary contract that matches
 *   with the hash in scriptPubKey.)
 */
// TODO add support for P2SH (BIP-13, BIP-16), meaning we need to acquire a "redeem-script" as part of the signing request
size_t bitcoin_write_pubkeyscript(uint8_t script[const 25], const uint32_t purpose, const uint8_t privkey[const 32]) {
	static const uint32_t PURPOSE_LEGACY_ID = 0x8000002c;
	static const size_t PURPOSE_LEGACY_SCRIPTLEN = 25;
	uint8_t cpubkey[33];
	switch (purpose) {
	case PURPOSE_LEGACY_ID:
		if (p256_keypair_from_bytes_compressed(cpubkey, privkey)) {
			return 0;
		}
		script[0] = 0x76;
		script[1] = 0xa9;
		script[2] = 0x14;
		hash160(&script[3], cpubkey, 33);
		script[23] = 0x88;
		script[24] = 0xac;
		return PURPOSE_LEGACY_SCRIPTLEN;
	}
	return 0;
}

error_t bitcoin_txid(uint8_t digest[const 32], const uint8_t transaction[const], const size_t len) {
	if (len == 0 || all_zero_bytes(transaction, len)) {
		// signal strange transaction content as serious issue
		return ERR_BAD;
	}
	sha256d(digest, transaction, len);
	return OK;
}

#define MASK_SIGHASH 0x7f
#define FLAG_ANYONECANPAY 0x80
#define SIGHASH_ALL 1
#define SIGHASH_NONE 2
#define SIGHASH_SINGLE 3

/**
 * Hash a transaction in preparation for signing.
 *
 * `script`: the (sub)script that is signed as part of the transaction.
 *
 * Errors:
 * - ERR_BAD indicates bad input value(s).
 * - ERR_FAILURE indicates an (internal) failure, usually reaching a (design/impl) limit.
 * - ERR_BUG indicates reaching a point that should not be possible to reach, likely indicating a bug.
 *
 * STATUS: tested for SIGHASH_ALL only right now.
 */
// TODO SIGHASH_SINGLE in particular, expects same number of inputs and outputs, though this is not always respected. See <https://en.bitcoin.it/wiki/OP_CHECKSIG#Procedure_for_Hashtype_SIGHASH_SINGLE> for details/comments on this.
static error_t hash_transaction(uint8_t hashout[const 32], const uint8_t index,
		const uint8_t script[const], const size_t scriptlen, const uint8_t hashtype,
		const struct txmetadata* const meta) {
	if (index >= 0xfd || meta->in_count >= 0xfd || meta->out_count >= 0xfd) {
		// running into limitations of the current implementation.
		return ERR_FAILURE;
	}
	const uint8_t sighash = hashtype & MASK_SIGHASH;
	const uint8_t flag_anyonecanpay = hashtype & FLAG_ANYONECANPAY;
	if (index >= meta->in_count || script == 0 || scriptlen == 0 || sighash == 0 || sighash > 3) {
		return ERR_BAD;
	}
	struct sha256_context shactx;
	sha256_init(&shactx);
	sha256_update(&shactx, (uint8_t*)&meta->version, 4);
	// Encode inputs
	if (flag_anyonecanpay) {
		sha256_update(&shactx, (uint8_t[]){1}, 1);
	} else {
		sha256_update(&shactx, (uint8_t[]){meta->in_count}, 1);
	}
	for (uint8_t i = 0; i < meta->in_count; i++) {
		if (flag_anyonecanpay && i != index) {
			continue;
		}
		sha256_update(&shactx, meta->ins[i].outhash, 32);
		sha256_update(&shactx, (uint8_t*)&meta->ins[i].outindex, 4);
		if (i == index) {
			if (scriptlen >= 0xfd) {
				// TODO assuming small scripts, not properly encoding var-int.
				return ERR_FAILURE;
			}
			sha256_update(&shactx, (uint8_t[]){(uint8_t)scriptlen}, 1);
			sha256_update(&shactx, script, scriptlen);
		} else {
			sha256_update(&shactx, (uint8_t[]){0}, 1);
		}
		if (i == index || sighash == SIGHASH_ALL) {
			sha256_update(&shactx, (uint8_t*)&meta->ins[i].sequence, 4);
		} else if (sighash == SIGHASH_NONE || sighash == SIGHASH_SINGLE) {
			sha256_update(&shactx, (uint8_t[]){0, 0, 0, 0}, 4);
		} else {
			return ERR_BUG;
		}
	}
	// Encode outputs
	switch (sighash) {
	case SIGHASH_ALL:
		sha256_update(&shactx, (uint8_t[]){meta->out_count}, 1);
		for (uint8_t o = 0; o < meta->out_count; o++) {
			if (meta->outs[o].script_len >= 0xfd) {
				// TODO incorrectly assume script-len is always < 0xfd. Needs improving.
				return ERR_FAILURE;
			}
			sha256_update(&shactx, (uint8_t*)&meta->outs[o].amount, 8);
			sha256_update(&shactx, (uint8_t[]){(uint8_t)meta->outs[o].script_len}, 1);
			sha256_update(&shactx, meta->outs[o].script, meta->outs[o].script_len);
		}
		break;
	case SIGHASH_SINGLE:
		sha256_update(&shactx, (uint8_t[]){index+1}, 1);
		// TODO investigate how this is supposed to work when there are more inputs than outputs .. does that mean below loop is faulty? (There is an informal expectation of equal inputs and outputs, which isn't strict, so need to check what consequences are and possibly limit allowed options for sake of sanity/safety)
		for (uint8_t o = 0; o <= index; o++) {
			if (o == index) {
				if (meta->outs[index].script_len >= 0xfd) {
					// TODO incorrectly assume script-len is always < 0xfd. Needs improving.
					return ERR_FAILURE;
				}
				sha256_update(&shactx, (uint8_t*)&meta->outs[index].amount, 8);
				sha256_update(&shactx, (uint8_t[]){(uint8_t)meta->outs[index].script_len}, 1);
				sha256_update(&shactx, meta->outs[index].script, meta->outs[index].script_len);
			} else {
				sha256_update(&shactx, (uint8_t[]){0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff}, 8);
				sha256_update(&shactx, (uint8_t[]){0}, 1);
			}
		}
		break;
	case SIGHASH_NONE:
		sha256_update(&shactx, (uint8_t[]){0}, 1);
		break;
	default:
		return ERR_BUG;
	}
	sha256_update(&shactx, (uint8_t*)&meta->locktime, 4);
	sha256_update(&shactx, (uint8_t[]){hashtype, 0, 0, 0}, 4);
	sha256_finalize(&shactx, hashout);
	sha256_init(&shactx);
	sha256_update(&shactx, hashout, 32);
	sha256_finalize(&shactx, hashout);
	return OK;
}

/**
 * bitcoin_sign_transaction signs a transaction (and first performs the necessary specialized
 * hashing).
 */
error_t bitcoin_sign_transaction(uint8_t signature[const 64], const uint8_t privkey[const 32],
		const uint8_t index, const uint8_t subscript[const], const size_t subscriptlen,
		const uint8_t hashtype, const struct txmetadata* const meta) {
	// TODO confirm one of predefined script-formats, such as P2PKH s.t. prly no need for full script-execution. (Script-execution would probably be a significant and large attack vector.)
	// TODO not performing script-execution yet, so we (1.) don't determine proper subscript, (2.) don't confirm successful execution.
	error_t ret;
	uint8_t hash[32];
	if ((ret = hash_transaction(hash, index, subscript, subscriptlen, hashtype, meta))) {
		return ret;
	}
	// only produce signature itself, let client reconstruct DER-encoding (ASN.1) and append hash-type
	// value (sighash | optional flag)
	if ((ret = p256_ecdsa_sign(signature, privkey, hash, 32)) == P256_SUCCESS) {
		return OK;
	}
	// failed to create signature: clean-up and return error.
	crypto_wipe(signature, 64);
	// case P256_RANDOM_FAILED: should not happen (see `extern p256_generate_random`)
	// other cases should not happen
	return ret == P256_INVALID_PRIVKEY ?  ERR_BAD : ERR_BUG;
}
