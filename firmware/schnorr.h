// SPDX-License-Identifier: GPL-2.0-only 
// SPDX-FileCopyrightText: 2024, Danny van Heumen <danny@dannyvanheumen.nl>

#ifndef P256_M_SCHNORR_ADDITION_H
#define P256_M_SCHNORR_ADDITION_H

#include <stdint.h>
#include <stddef.h>

#define SCHNORR_SECRET_LENGTH 32
#define SCHNORR_PUBLIC_LENGTH 32
#define SCHNORR_SIGNATURE_LENGTH 64

int p256_scalar_check(const uint8_t sk[32]);

int p256_scalar_add(uint8_t s[32], const uint8_t other[32]);

/**
 * Convert a point to compressed point encoding. (SEC1)
 */
void p256_point_to_bytes_compressed(uint8_t out[33], const uint32_t x[8], const uint32_t y[8]);

int p256_keypair_from_bytes_compressed(uint8_t pub[33], const uint8_t priv[32]);

int p256_pubkey_add(uint8_t pubkey[64], const uint8_t uncompressed[64]);

/**
 * Initialize/reset provided context for unused/new use of SHA-256 function.
 */
// TODO describe provisioning sha256 hash
extern void schnorr_sha256_init(void* const ctx);
/**
 * Add data to in-progress SHA-256 hashing process.
 */
extern void schnorr_sha256_update(void* const ctx, const uint8_t* const data, size_t len);
/**
 * Finalize current state of SHA-256 hashing process and receive digest.
 * out: out[32] to receive final hash-digest, 32 bytes/256 bits.
 */
extern void schnorr_sha256_finalize(void* const ctx, uint8_t out[32]);

/**
 * Generate a keypair for Schnorr signing/verification.
 */
int schnorr_generate_keypair(uint8_t sk[SCHNORR_SECRET_LENGTH], uint8_t pk[SCHNORR_PUBLIC_LENGTH]);

/**
 * Derive a Schnorr public-key from a provided secret-key bytes.
 *
 * Errors:
 * - P256_INVALID_PRIVKEY invalid private key provided. (Note: sk is not zeroized, in case of invalid key.)
 */
int schnorr_derive_public_key(uint8_t pk[SCHNORR_PUBLIC_LENGTH], const uint8_t sk[SCHNORR_SECRET_LENGTH]);

/**
 * Precompute some per-keypair parts to improve on efficiency computing Schnorr-signaures.
 * Note: `pubkey` may be `0` to skip serializing the public key.
 */
int schnorr_precompute(uint32_t d[8], uint32_t Px[8], uint8_t pubkey[32], const uint8_t sk[SCHNORR_SECRET_LENGTH]);

/**
 * Sign a message with Schnorr signature, using some precomputed components for efficiency.
 */
void schnorr_sign_precomp(void* const hashctx, uint8_t sig[SCHNORR_SIGNATURE_LENGTH], const uint32_t d[8], const uint32_t Px[8], const uint8_t m[const], const size_t len);

/**
 * Sign a message with Schnorr signature.
 */
int schnorr_sign(void* const hashctx, uint8_t sig[SCHNORR_SIGNATURE_LENGTH], const uint8_t sk[SCHNORR_SECRET_LENGTH], const uint8_t m[const], const size_t len);

/**
 * Verify a message using provided Schnorr signature.
 */
int schnorr_verify(void* const hashctx, const uint8_t pub[SCHNORR_PUBLIC_LENGTH], const uint8_t sig[SCHNORR_SIGNATURE_LENGTH], const uint8_t m[const], const size_t len);

#endif

