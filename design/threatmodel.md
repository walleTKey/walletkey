# Threat-model

Threat-model for _walleTKey_.

`TODO investigate the extent of transaction malleability that _we_ care about. There is some malleability which is mitigated by other measures, such as reduced use of script features. Investigate how much we need to take into account here. (<https://developer.bitcoin.org/devguide/transactions.html#transaction-malleability>)`

`TODO need to establish whether (1.) process memory observation is 'passive' or 'active' attack; and (2.) snooping on keyboard keypresses (e.g. PIN/passphrase) is 'passive' (<-- most likely) or 'active' attack; and (3.) listening in on (USB) serial port opened (exclusively) by wallet-cli to TKey with walletkey firmware is 'passive' or 'active' attack. (Note: this decision needs to be made because usual "Eve" attacker is based on network communication, where eavesdropping is a more accessible operation than within same operating system.)`

`FIXME at this point, the threat-model only includes parts from my own ideas/insights decided to classify as part of the threat-model. I still need to check with some existing cases to see if there are better ways of classifying/distributing information over sections. Current content already captures some notion of levels, classifications, strict separation, such that it helps to structure information, effort and quality.`

`FIXME Bitcoin delays exposure of public key until their final one-time use. (P2PKH, vs obsolete P2PK) This seems to be in-line with our not wanting to expose public key at rest, but do expose in secure sessions with walleTKey.`

## Terms

See terms definitions in the [main design document][walletkey-design].

## Trust-foundation: TKey + trust-on-first-use (TOFU)

`TODO how is this different from TCP (trusted computing base)?`

`TODO how is TKey different from a trusted enclave / TEE?`

_WalleTKey_ builds on a trust-based basic in the security proposition offered by _TKey_ and trust-on-first-use (TOFU). Given this foundation, it is important that there exists a high-quality implementation of the _walleTKey firmware_, i.e. correct, sound, reliable.

### tillitis TKey

The device used is the _tillitis TKey_. _walleTKey_ relies on certain security guarantees at the basis, provided by TKey. TKey itself is based on relatively new ideas, and current (hardware) versions have their own [documented limitations](<https://tillitis.se/products/threat-model/> "Threat Model - Tillitis"). TKey _is assumed_ secure.

_TKey_ runs a small firmware that hands over control to the application after loading. Consequently, the device makes it uniquely possible to create a bug-/error-free experience that makes it feasible to create solid, secure solutions. Even though the _TKey firmware_ is fixed/frozen, its size, limited scope and execution time, make such a high bar attainable.

The following assumptions are made about the foundation to make _walleTKey_ work as a solution. The basis is the _TKey_ device with its small firmware, and extended by its guarantee to generate a unique secret for each (byte-exact) application-binary. 

- _TKey_, as device, functions properly.
- _TKey_, as device, provides the trusted foundation necessary:
  - _TKey firmware_ internal device secret (_UDS_) is secure.
  - _TKey firmware_ deterministically, unpredictably derives unique¹ secret (_CDI_) for the loaded application, i.e. _walleTKey firmware_.
  - _TKey_ provides an isolated execution environment. (`FIXME is TEE?`, `FIXME note environment is not completely isolated, but execution is independent and memory is/can be protected to avoid manipulation. Is not completely isolated, but what do we call this, or am I too pedantic with this distinction?`)
- The wallet is created under acceptable circumstances, i.e. the _walleTKey firmware_ is trustworthy. `TODO see threat-model section for deviating req for generate/import/export/migrate`

¹: "_unique_" here means unique in a _practical sense_, to the extent possible given the restrictions of 32 bytes and the capabilities of the hash-function (i.e. _Blake2s_).

_TKey_ offers a unique proposition in that, although it cannot "_magically_" ensure that a loaded application will behave, it can promise that only the _byte-exact_ application-binary will have access to some unique secret value. Therefore, it is possible to claim that a secure (i.e. non-vulnerable) application-binary (i.e. _walleTKey firmware_) claims a secure _CDI_.

__key insights__:

- _TKey_ device (virtually) incorruptible.
- feasible, controlled secure foundation for program-binaries.

`TODO notice that we state the assumption for an incorruptible TKey (firmware). This is not fully established. For example, was TKey firmware fuzzed? It was audited IIRC, but I need to check how far that audit went with soundness, correctness, vulnerability analysis. The claim still holds true for the original goal.`

`TODO mention genuine-check as provided by tillitis for TKey devices programmed and sold by themselves.`

### _walleTKey firmware_

The firmware must be correct to achieve the desired result. This is likely an incremental process. A buggy (not necessarily malicious) firmware program can cause problems for the user. However, bad firmware of any kind cannot compromise the device, i.e. erode the foundation of trust.

Given this foundation, the program-binary itself now determines the quality of the _walleTKey firmware_. And that firmware-binary is _freely_ interchangeable.

_Note: the firmware-binary is freely interchangeable from the trust-foundation point-of-view. However, to migrate a wallet to another firmware-version still requires a methodical approach. This is, however, outside the scope of the trust-foundation._

__key insights__:

- No need to make firmware proprietary, because there is no inherent risk of crippling the device, such as by leaking secrets.
- No need to require (vendor) firmware control/signing, because:
  - there is no risk of firmware corrupting device.
  - there is no risk of firmware corrupting other firmware.
  - there is no risk of malicious firmware manipulations.
- A secret that is bound to a particular firmware-binary means that we need to design with consideration for this limitation/safety-measure.

This section discusses the firmware-binary as component in the trusted foundation. _Acquiring_ trustworthy firmware-binaries is discussed in a later section.

### Trust-on-first-use (TOFU)

Assuming the trusted foundation and correct _walleTKey firmware_, as described in the previous sections, each _walleTKey_ establishes a unique identity. The _identity_ is generated at initialization of the _walleTKey firmware_ and can only be determined when first connecting to the _walleTKey_, as it is based on the _CDI_.

To establish trust with the _walleTKey_ (_TKey device_ + _walleTKey firmware_ + _USS_), a user must be ensured of its identity. The identity becomes relevant from the moment a wallet is created, as that is the moment when user's secret data is being handled. The public key of an internal _identity key-pair_ is used for authentication. Trust with _walleTKey_ is essentially established at the moment a _wallet_ is created.

__key insights__:

- Design needs to incorporate authentication/validation.
- `FIXME more key insights from TOFU?`

_Note: strictly speaking, there is one exception where trust may be meaningfully established earlier in the process: `verifyfw` is designed to test essential operations of the _walleTKey_. Given that a _CDI_ is an essential part of _walleTKey firmware_, one can test a specific _walleTKey_, therefore verify that the identity is the same when subsequently creating the wallet._

## Acquiring _walleTKey firmware_

We discussed how the firmware-binary is a component in the trusted foundation. However, we must also acquire firmware-binaries that the user can trust to be authentic.

Note that we specifically target _authenticity_ of the file: it is an actual _walleTKey firmware_ binary, as opposed to some arbitrary (malicious) program. This does not in itself guarantee a correct, bug-free program.

Ways of acquiring a firmware-binary:

- Users/CI building from source.
- Downloading released/blessed firmware-binary.

Mechanisms in support of firmware-binary management:

- _[Reproducible builds][reproducible-builds]_: independently recreatable firmware-binaries; byte-exact comparable results.
- _Community-collaboration_: released/blessed firmware-binaries; signatures to prove authenticity and reproducibility.
- _Deterministic/platform-independent build-environment_: `TODO is it feasible to set up a deterministic and/or platform-independent build-environment, such as using Nix, Docker/Podman? Cross-compiling was always an issue before. Not sure if Nixpkgs is going to be cooperative given this needs cross-compiling.` Transparency of the build-environment allows scrutinizing, improving, recreating a build set-up.

## Risks

`FIXME freely querying for walleTKey identity is risk. Maybe we should consider querying only once (until reset) to ensure that successfully establishing a connection means that no background processes can interfere. And that when failing to establish a connection, it is caused by some background process beating us to the punch with querying the identity. Maybe then allow for providing hash of identity if people desperately want to validate identity before loading wallet.`

```FIXME
two-stage authentication:
1.  simple AKE to authenticate current walleTKey identity is as claimed and make identity known to client (however not yet ensured identity is same as expected for sealed-wallet),
2.  loading sealed wallet based on context information from TKey and client, with client's information being hardened to protect against brute-force,

We don't really know if we're talking to the same walleTKey until the wallet is loaded. Before that point, we know they own the identity, just not that the identity is the actual walleTKey that created the sealed wallet.

That means that the only true identity test is loading an existing wallet...

Hardening against brute-force attacks of passphrase makes sense in all cases where there has not been an encounter with the corresponding walleTKey while active, because then the corresponding identity is not known, therefore the only way to verify if a walleTKey matches with the sealed-wallet is to decrypt, requiring a hardened computation. (L3 adversary can query identity.)

Having sealed-wallet with `AD=blake2s(identity, KDF(identity))` with `KDF(x)` a hardened one-way function like `Argon2id`. This approach would disconnect easy generation from easy verification with a computationally-hard transformation. (This doesn't solve everything, as this makes access to the authenticator more difficult, but not the key to the ciphertext. So, given more/other knowledge, access to the authenticator may no longer be needed.)
```

An indexation of possible risks to _walleTKey_ design, implementation, use. The next section discusses threat-levels. These risks will be distributed and suitable mitigations discussed.

__Characteristic risks__
- __prerequisite__ A unique secret generated based on device, program-binary and USS. Basic functionality _must_ work in order to safe-guard access to the wallet in case of problems with firmware, such as bugs.
- __L3__ (Verifier) inherent risks of smartphone-app. The verifier is intended to complement missing capabilities of TKey. The risks can (hopefully) be acceptably reduced by e.g. ensuring no need for connectivity. (`TODO maybe also mention the general predisposition towards security of smartphones, even if not always towards privacy.`)

__User-caused accidents/mishandling/misuse__:
- __L1__ Corruption of "sealed wallet" file.
- __L1__ Incorrect use of wallet-client when updating "sealed wallet"-file.
- __L1__ Deletion/losing "sealed wallet" file.
- __L1__ Deletion/losing firmware-binary file.
- __L1__ Bug/bad/defective/edge-case in firmware-binary causes problems in handling wallet transactions.
- __L1__ Users need to understand that _walleTKey_ consists of: _sealed wallet_, _firmware-binary_, _device_, and _USS_. With any of them missing, wallet is gone. ('Migration' to change this combination. 'Identity' (hash) can be reconstructed.)
- __L3__ Mistaking critical operation for non-critical operation when performing touch-confirmation.
- __L3__ Use of privacy-invasive keyboard-apps on Android captures PIN-entry.
- __critical__ Exposing raw wallet secret, e.g. during migration or first import.

__TKey-device/-platform__:
- __L1__ Bad TKey-device
- __L1__ Loss/broken TKey-device
- __L1__ Buggy TKey-firmware
- __L3/L4__ TKey's _UDS_ (internal, inaccessible) secret is compromised (necessary assumption of trustworthiness of tillitis, part of the TKey device proposition or solved with _TKey Unlocked_ + _Programmer_)

__walleTKey__:
- __L1__ Bad (buggy/incomplete, non-malicious) firmware for _walleTKey_:
- __L3__ Malicious firmware for walleTKey (i.e. from first use deliberately chosen, as opposed to unknowingly swapped as in case above).
- __L3__ Device/firmware is (unknowingly) swapped/replaced.
- __L2__ Timing-attacks via messaging request-response.
- __L3__ Malicious (corrupted, specially-crafted) messages.
- __L4__ Brute-force attacks
- __L4__ Brute-forcing `USS` for known device+firmware-binary:
  - _for connected/active walleTKey (known identity)_: easy to verify; derive identity key-pair then compare identity public-key with known identity-key.
  - _for unknown identity_: expensive to verify; derive identity key-pair, compute (expensive) KDF-value, verify that wallet will load. `FIXME written with KDF-solution in mind, needs further detailing when solution known/checked`

__Compromised host system, used in passive attacks__:
- __L3__ Eavesdropping on communications with walleTKey.
- __L3__ Keyboard snooping
- __L2__ Duplication, reproduction, replaying of produced results from communication and/or stored on disk.
- __L2__ Exposing wallet secret.
- __L3__ Timing-attacks on walleTKey communication.
- __L2__ Timing-attacks on client application.
- __L2__ Exposure of wallet-related critical information. (`TODO Exposing main xpub?, Avoidable given Bitcoin Core?, ...?`)

__Compromised host system, used in active attacks__:
- __L3__ Inspection of process memory (invasive analysis/observation, is active attacker?)
- __L3__ Malicious interactions with walleTKey (e.g. process in background, malicious client)
  - request-confusion between malicious request and user's request
- __L3__ Malicious changes to walleTKey.
- __L3__ DoS (corrupting, malicious, random data)
- __L3__ Malicious handling of input/output from walleTKey.
- __L2__ Misuse of signatures outside of intended context.
- __L3__ Client-side resets of TKey. (`TODO see current status/conclusion in tillitis-key1 issue`)
- __L3__ Keyboard-snooping while entering admin-/wallet-PIN.

__Compromised verifier__:
- __L3__ False-positive authentication of walleTKey-messages.
- __L3__ False-negative authentication of walleTKey-messages.
- __L3__ Refusal to operate. (unusable, DoS, corrupted confirmation)
- `TODO further detailing out`

__Quantum-computation / vulnerable classical cryptography / post-quantum cryptography__:
- __L1__ Prevent (unnecessary) exposure of asymmetric-cryptographic components.
- __L2__ Harvest-now-decrypt-later
- __L3__ Vulnerability of asymmetric-key cryptography: AKE, identity, Secure Migration, `FIXME ...`. 
- __L2__ Less-vulnerable symmetric-key cryptography: sealed wallet, ephemeral confidential session, `FIXME ...`
- __L1__ Use of "Secure Migration", which would necessarily contain an asymmetric-key cryptographic component.

## Solutions

`FIXME need an entry for: changing PINs; automatically generating PINs upon first generation; ...`

`TODO need to consider value of admin-/wallet-pins considering L3 and higher .. need for comprehensive touch-confirmation to ensure physical presence.`

`FIXME category for local attacks/local presence/physical presence? (touch-confirmation, random touch-patterns, wallet-/admin-PINs, auth{n,z})`

`TODO storing hash of (identity) public key, reduces risk of threat from (1. wallet at rest) to (2. passive attacker) iff you grant passive attacker access to memory/USB exclusive USB-serial communication, or to (3. active attacker) if you consider process-memory/USB snooping as invasive operations.`

`TODO also consider the asymmetry of circumstances, given that Android-apps become increasingly more protected, isolated. A basic verifier-app as second-opinion becomes more reasonable, especially if corrupting the app opens up limited influence.`

`FIXME should this already be discussed/defined this early? Verifiers are only need to protect against compromised system by active attacker. Not exactly first priority. Verifier should be very minimal, no (unnecessary) interaction, use TPM or similar to securely store identity, no internet access, etc.`

`TODO compromised verifier is probably only handled in limited capacity: verifier should be simple s.t. internet/network/sensors are not a risk. Instead, working only with data input from QR-code(?) and registered public key(s)(?) to provide a confirmation of authenticity, possibly computing HOTP(?)-like(?) confirmation-codes`

`TODO initial selection of algorithms (Blake2s, Chacha20/Poly1305, etc.) qualify as acceptable according to the PQC Migration Handbook, furthermore exposure of public keys can be reduced to in-memory for the overwhelming part of operations.`

The over-all solution takes the method of minimalism-by-default for the walleTKey-firmware, have most of the necessary processing performed in the wallet-clients unless actions are security-sensitive.

_Minimalistic walleTKey-firmware_:
- __prerequisite__ results _must not_ be useful out-of-context.
- __prerequisite__ mechanical/methodical handling of data to cope with limited capacity of TKey.

_Mitigating circumstances_:
- __L2__ primarily/exclusively local use, i.e. limited exposure.
- __L2__ given use of ECC in Bitcoin, not useful being post-quantum safe if walleTKey wallet can be bypassed altogether. NIST SEC-P256K1 and ECDSA are foundational to Bitcoin, therefore not possible to mitigate/fix solely in _walleTKey_.
- __L3__ provide instructions alternative ways to look up transactions once part of blockchain. I.e. do not relying solely on local Bitcoin software to verify claimed submission.
- __L3__ corruption of external software/components is beyond our capability to prevent, but we can recognize suspicious signs, such as corrupt/illegal messages, and abort/fail operation.
- __L3__ symmetric-key cryptography less (or not at all) vulnerable, according to [PQC Migration Handbook][PQC-Migration-Handbook], paper [Too Much Crypto][JPAumasson-Too-Much-Crypto].

_Ideas/solutions_:
- __t.b.d.__ "classic" separate, independent receive-addresses (requires continuous resealing of wallet, thus hard to manage) (for now)
- __general__ documenting/warning for uncorrectable, unavoidable risks, risks rooted in human error `FIXME need to recheck what is implied: benefits, problems?`
- __general__ reproducible builds, for: trustworthiness, authenticity
- __security__ reproducible build-environment  
  _Can we define a (reasonably) reliable, trustworthy, reproducible (basis for a) build-environment._
- __security__ scripted, automated, independent builds using the _reproducible build-environment._
- __general__ repeated builds to check if both are byte-exact equal.
- __general__ blessed/released firmware-binaries
- __general__ authenticity of (firmware-)binaries through signatures.
- __general__ community-signatures (possible through _reproducible builds_)
- __general__ never overwrite wallet-files. Instead, create new at destination.
- __general__ display warning: that newly created sealed-wallet files must be backed-up, that old sealed-wallet files are not unusable/defunct/obsolete.
- __general__ sealed wallet opaque blob produced by firmware can be unstructured data, as it is only ever read/processed by exact same firmware
- __L1__ consider contents of sealed-wallet opaque blob, s.t. it is not possible to derive e.g. admin-PIN length from blob content length (and other such metadata). (as metadata side-channel)
- __L1__ AEAD with identity as associated-data ensures that we do not attempt to read data that is encrypted for a different walleTKey. (Therefore, unstructured blob is possible, viable.)
- __security__ strict-by-default (defensive) programming practices
- __general__ `verifyfw` as client-program to verify basic functionality, ensures exit-strategy exists.
- __security__ use of touch-confirmation to prove physical presence.
- __security__ use of comprehensive, random touch-patterns to prove awareness of critical action, on top of proving physical presence.
- TKey security features
- TKey hardware security mitigations
  - firmware-checksum ensures deterministically generated, random secret (CDI) is only available to binary-exact firmware
  - CPU execution monitor
  - RAM ASLR randomization
  - RAM scrambling
- software-/compiler-hardening capabilities/options
  - fortifying code
  - stack-protector (stack-check-guard)
- security-aware practices (e.g. programming)
  - stack-memory only (no dynamic allocation)
  - bug-prevention (static analysis)
  - undefined behavior prevention (static analysis)
  - do not store sensitive values, but compute on-the-fly
  - clear memory after use for sensitive data
  - simple, practical logic/code
- walleTKey-initiated verification/confirmations
  - prove physical presence
  - prove awareness (critical operations)
  - authorization-/confirmation-codes
  - input-verification/-validation
- __L1__ avoid exposing asymmetric-key cryptograpic components to protect against quantum-computing capabilities (at lower threat-levels)
- __L2__ client-side mitigations for timing-attacks.
- __L2__ identity-keypair ensures authentic walleTkey.
- __L2__ trust-on-first-use (TOFU) of walleTKey (wallet) when first created, based on walleTKey identity.
- __L2__ AKE ensures communication only with authentic walleTKey (including for communicating sensitive information like confirmation-codes (admin/wallet PIN))
- __L2__ secure session provides confidential communication
- __L2__ ensure results (e.g. signed transactions) are useless out-of-context. (necessary,implied: Bitcoin transaction signatures are useless out-of-context of intended transaction.)
- __L2__ use of (static) PINs (admin-/wallet-) to prevent issuing critical operations (extracting secrets, signing transactions, etc.) during (unauthorized) physical presence
- __L2__ limited authz-/confirm.-codes (re)tries, then block walleTKey
- __L3__ output-validation
- __L3__ authentication of confidential communication
- __L3__ use of (dynamic) PINs to prevent issuing critical operations (see static PINs) during (unauthorized) physical presence.
- __L3__ aim for non-networked verifier, s.t. unavailability of connectivity reduces the possibility for (coordinated) corruption.
- __L3__ Verifier functionality is reactive in nature, limited to interaction with clients
- __L3__ securely store public key in verifier
- __L3__ scan QR-code with Verifier for quick communication w/ wallet-software
- __L4__ use of KDF/hardening to prevent efficient brute-forcing of passphrase
- __L4__ allow backing up the firmware-binary (securely) in the verifier

`TODO secure migration protocol: must provide admin-PIN on transfer (both init and restore); maintain same PINs before/after migration, st PINs are known, proved known during procedure, then allow changing afterwards`

- `FIXME avoid quantum-computing risk by avoiding unnecessary (long-term, non-temporary) exposure of asymmetric-key cryptography elements.`

- Symmetric-key cryptography is deemed safe. `TODO according to Marc Stevens - PQC Migration Handbook: Blake2, ChaCha20, Poly1305, SHA-256 are fine -- as expected; Ed25519 keypair, X25519 Key Exchange are problematic. Keep same strategy of trying to minimize exposure of asymmetric cryptography in PoC. Important item to clear up: can we consider USB communication and process memory subject to active attacker? (or already at risk of passive attacker? Active attacker is problematic for multiple reasons anyways.)`, `TODO according to JP Aumasson - Too Much Crypto, there is doubt whether the O(n/2) bound for quantum-computing brute-forcing speed-up might misrepresent the constant-time factor of costs.`

## Levels

`FIXME need a concrete status of TKey hardware vulnerabilities, at end of proof-of-concept. Knowing and proving the concept is one thing, but we need to be clear on whether use can be recommended, possibly restricted to some extent?`

`FIXME levels of increasing complexity, with L4 being ultimate level, assuming maximum aggressiveness, loss of ownership, etc. Such that it would be possible to have a system brute-force attack a stolen device with a stolen sealed-wallet. (e.g. stolen device, stolen sealed-wallet, but unknown passphrase.)`

`FIXME consider a fourth level that involves attacker actively attacking walleTKey firmware through aggressive, brute-force communication with device.`

This section classifies the various features, mitigations and risks in one of several levels. Threat-levels are defined in the context of an existing wallet, with the first section covering the initial circumstances outlining the conditions in which the wallet is created, and the section covering the (critical) operations that _require_ a fully trustworthy environment.

As TKey does not offer persistence, there can be no lasting impact with whatever use of devices, firmware-binaries, interactions, as long as no wallet is created and adopted for use.

The following subsections discuss threat-levels, ideas that work for this level of threats, and possible mitigations that can be employed. Ideas and mitigations of higher threat-levels stack on top of those of lower threat-levels, unless specifically defined otherwise. Each threat-level is a profile, but also shows a general increase in effort and/or sophistication of the attacker.

Solutions can (roughly) be divided in the following classes:

- Programming best-practices and safety-practices.
- Tooling-based hardening (such as compiler hardening settings, stack-protector, etc.)
- Use/activation of hardware-provided security features.
- Design-considerations that avoid weaknesses (e.g. due to quantum-computing).

### Initial circumstances

The initial circumstances at time of wallet creation.

Note: _importing_ wallet secrets are a special case, discussed in the section on critical circumstances.

Wallets should always be generated on a known trustworthy system. Wallets may (in theory) be _generated_ even under _L2_, i.e. the presence of a passive adversary, given that the wallet secret is generated within the TKey and not exposed, as long as the selected firmware-binary is carefully selected. However, depending on the extent of the attacker's capabilities, the PINs may not be safe. It is a theoretically possible, but one should _NOT_ want to do this.

- Trust (TOFU) is not established (yet)  
  It is possible to manually specify an identity public-key, but given that there is no wallet yet, there has not been a need to establish the trusted identity.
- Applications and firmware must be carefully selected, verified.
  - Select from releases/blessed binaries, verify checksum, validate signature, run `verifyfw` to confirm that essentials are functioning.
  - Build from source, run `verifyfw` to test essentials. In case of blessed builds: verify checksum, validate signature.

Strictly speaking, at _L3_ and higher, there is no way to ensure that client and/or firmware-binary are not corrupt(ed) as the system itself cannot be trusted. All bets are off.

`TODO a lot can be accomplished when generating a wallet even on a system with an attacker present, but care should be taken when we start to enter PINs/passwords as these may then be compromised even if sealed wallet data is safe. It may work for short-term use and subsequent migration.`

`TODO given active attacker, situation gets tricky: DoS can be used as a signal to stop further activity. If there is active interference, communication may fail. If there is keyboard snooping, process memory inspection then at least your pin-codes aren't safe and whatever you type into your computer for sake of the wallet. Critical issue is fact that given first chosen "firmware + device + USS" you do not yet have an identity, therefore you cannot have absolute certainty that loaded firmware is indeed same firmware, therefore that generated wallet isn't compromised. Once a wallet is established, you can be certain of firmware.`

### Critical: import/export/migration

An extraordinary situation is that of creating a _walleTKey_ wallet by importing a wallet secret, and to a lesser extent exporting a wallet secret. This _requires_ a trusted, uncompromised environment because the raw wallet secret is processed. There is no way around this.

__Requires__:

- firmware-binary,
- client-application,
- authentic TKey device (TOFU is established as we import),
- computer/OS/environment need to be trusted/secured because of handling the raw wallet secret.
- If "_pigeons as surveillance drones_" is your thing, then make sure to close the curtains.

With the creation of the wallet, we also fix the identity for that wallet.

`FIXME emphasize ability to preserve/store the sealed-wallet after finishing the process.`

### L1: wallet-at-rest

Potential threats are based on off-line attacks and/or exfiltration of persisted data. There is no persistent attacker, active or passive. Instead, the focus is on one-off opportunities, such as Evil Maid or trojan or hacked web browser accessing file system to exfiltrate data.

__Security-goal__ _Persisted sealed-wallet data is secure. No unnecessary exposure of information._

`FIXME further sort risks/ideas out, align with Risks section`

__risks__:
- off-line key search by copying "sealed wallet" (for case where identity public key is present there) and brute-forcing keypair (possibly using quantum-computing).

`FIXME now using "trustworthy firmware" while not defined.`

`FIXME admin-/wallet-pin required here or in L2? (risk in L2 if keyboard snooping is possible, otherwise risk in L3)`

__ideas__:
- trustworthy firmware (transparency, reproducible builds, signatures)
- sealed wallet format (depends on trustworthy firmware)
- have walleTKey firmware decrypt wallet secret and feed back to wallet client
- store hash of identity public key (PQcrypto? i.e. don't expose public key directly)

__mitigations__:
- Compiler warnings and static analysis.
- Tooling-based hardening options, such as compiler hardening options.
- _TKey_ inherent hardware and software security features
- Design-considerations to prevent unnecessary exposure of asymmetric-cryptographic material:
  - Make use of hash-digest if original bytes are not strictly needed.
  - Prefer keeping public-keys in process memory only.
- `FIXME ...`


### L2: an observing/monitoring adversary

The observing or monitoring adversary does not have the ability to act, but does observe, read, monitor. They may have access to files stored on disk. They may observe (raw) communication data.

__Security-goal__ _A passive attacker is unable to acquire critical knowledge through plain observation or timing. (This excludes invasive actions like USB communication monitoring, process-memory monitoring, keyboard-snooping.)_

`FIXME Clarify that "passive attacker" here means an attacker that has at most the ability to passively observe. (I.e. does not have sufficient privileges to invade processes, install itself as rootkit, etc.)`

`FIXME consider whether we should make USB communication an ability only available to the active attacker, i.e. needs certain privileges to gain access to the USB serial communication. (This and access the process memory more or less determine whether quantum-computing is a risk at "L2 passive" or "L3 active". Especially given that we can never fully protect against L3 active attackers anyways.)`

`TODO content is classified under the assumption that access to raw process memory (wallet-cli process) is not possible. That is, it needs some invasive action not possible for a passive attacker. (Needs to be confirmed/established, as this shifts level of some mitigations.)`

`TODO making exception for observation of USB UART serial communication, as this levels communication with network communication, therefore we'll assume they're similar. I.e. let's not treat USB communication as special, even if a process can take exclusive lock on the serial port during use.`

__risks__:
- USB serial communication may or may not be observed. (exclusive lock on serial port may be sufficient to keep passive attacker out, but this would special-case serial port communication as opposed to network communication)
- identity public key is exposed (quatum-computing risk)
- walleTKey responses must be protected by context to avoid misuse/abuse/duplication/replay
- Firmware may assume that counter-part in confidential session is intended recipient, given security properties of secure session. (Authenticated but anonymous recipient.)

__ideas__:
- simple asymmetric AKE
- secure (ephemeral) key exchange, communication session
- transaction signing in walleTKey firmware
- secure migration protocol (essentially D-H key-exchange between two walleTKeys such that established symmetric key encrypts/decrypts wallet-secret without intervention of user, possibly with user admin-PIN input or something, such that migrations can be performed without passive attacker having access.)

__mitigations__:
- Software engineering best-practices and safety-practices.
  - Selective use of constant-time operations.
- Use of AKE to establish confidential session.
- `FIXME ...`

### L3: an invasive adversary

`TODO L3 makes an adversary very powerful, making it impossible/infeasible to fully protect in this environment. Focus is on making the walleTKey operate without verifiable boundaries or fail/abort/cause-problems.`

An invasive adversary that may perform actions to manipulate or block activity, independently interact with TKey, or perform invasive actions that allow them to acquire more (detailed) information.

__Security-goal__ _An active attacker is unable to manipulate the authentic process, USB communications, interaction, input or output such that the intended corruption of walleTKey (inter)action is achieved._ (I'm aiming for the idea of "tweaking" transaction values, message-values, etc. to manipulate walleTKey, user into signing a transaction resulting in undetected, unplanned loss of money, ... within reason. `FIXME this needs very careful consideration. How can we express "within reason" in concrete, strict/binary terms?`)

`FIXME discuss limited capability to prevent issues, operating under L3 means working if possible, detection if possible, failing signals corruption/DoS/etc. Problems under L3 are primarily a signal that something is going on and therefore a signal to be careful and most likely switch to another system altogether.`

`FIXME explain that this "level" "active attacker" has sufficient privileges to perform active attacks, also e.g. ability to invade and inspect other process' memory and USB UART communication streams.`

`FIXME discuss (relatively) limited impact of quantum-computing risks as long as wallet-secrets are not communicated. See also section "Critical: Importing/Exporting/Migrating". Whenever wallet secrets are handled, there must be a strictly trustworthy environment.`

`FIXME not much time spent in distinguishing "coercive manipulation to influence outcomes to benefit of the attacker" from "destructive manipulation to deny access, deny functionality, corrupt/interfere with operations to deny user use of walleTKey". (Mostly, coercive manipulation must be protected against, while destructive can probably only function as a signal to go to an uncompromised system.)`

__risks__:
- invasive action to observe raw process memory
- interference with TKey
- malicious communication with _walleTKey_ 
  - attempts to exfiltrate _CDI_, _identity_ keypair, `FIXME ..`
  - attempts to exfiltrate internal symmetric key used to seal wallet
- malicious client application

__ideas__:
- signed responses allow passing information through untrusted relays/applications
- authorization/confirmation with cryptographic component ensures critical information is received and validated
- authorization/confirmation codes must be deterministically, unpredictably random.
- authorization/confirmation codes cannot be reusable.
- verifier to double-check on critical information, e.g. signing bitcoin transactions
- PINs to protect wallet- and admin-operations
- touch-confirmation (requiring physical presence) to protect from initiating operations in the background, i.e. without physical presence,
- touch-patterns (requiring physical presence and attention) may protect critical operations exposing wallet secret,
- send authentication frame with each command to/from _walleTKey_

__mitigations__:
- Use of stack-only development model to avoid some classes of memory vulnerabilities.
- Securely wipe sensitive data in (stack) memory after use.
- Compute (instead of memorize) sensitive cryptographic values on-the-fly.
- Enable compiler-based _stack-protector_ support.
- Activate CPU execution monitor at firmware initialization.
- Process input data with security in mind: do not trust data.
- `FIXME ...`

`TODO consider that with a sufficient mechanism to protect against unintentionally exposing the wallet secret, even without PINs/passwords, for short-term use this is safe, as long as the sealed wallet is uncorrupted. If the wallet is generated, the secret will not leave the walleTKey unless requested.`

`TODO assumption on quantum-computing risks: when it is said that "asymmetric cryptography" is weakest class, I'm assuming every aspect, such that we cannot unnecessarily expose public keys without risk of quantum-computing from performing key-search for corresponding private key.`

`TODO amortize (?) risk by having a second device, a verifier, to assist in validating the data that the walleTKey and client claim to process. (I.e. we can't avoid the wallet process being hijacked, but with a verifier present we can at least detect suspicious activity. If we can't protect against passive attacks, we can at least detect manipulation.)`

`FIXME boundary-conditions for wallet migrations: it is hardly possible to migrate a wallet on a fully compromised machine and expect it to be safe.`

`FIXME do not keep rootkey in memory, in case of malicious attempt of reading memory, instead compute on-the-fly when needed for deriving secret for specific purpose.`

`FIXME to a lesser extent, migration is also critical. It's nice'n'all that the migration does not expose the raw secret, but if the attacker migrates to their own wallet then we failed equally.`

### L4: aggressive adversary (full capabilities)

A persistent, aggressive attacker, that targets computer, client, firmware or device, that has a broad range of capabilities to its disposal and full availability of time. Attacks may be performed using their own hardware on stolen devices and information, possibly without knowledge of the owner therefore without ability to interfere with or stop the attack.

The goal is not to prevent this kind of attacker, but instead to make such attacks difficult, e.g. time-consuming, complicated, etc.

`FIXME not sure if L4 or L1, but mechanism to prevent efficiently brute-forcing sealed-wallet if identity is yet unknown. (E.g. passphrase is unknown, so brute-forcing passphrase is hampered by fact that a hardened computation must be performed to determine if right identity is found to use the wallet. Also helps deniability, because you don't know whether something is a wallet until the wallet is opened, meaning you're in already.)`

`FIXME (L4) needs adding, see mindmap`

``FIXME define security-goal for L4 __Security-goal__ _An active attacker is unable to manipulate the authentic process, USB communications, interaction, input or output such that the intended corruption of walleTKey (inter)action is achieved._ (I'm aiming for the idea of "tweaking" transaction values, message-values, etc. to manipulate walleTKey, user into signing a transaction resulting in undetected, unplanned loss of money, ... within reason. `FIXME this needs very careful consideration. How can we express "within reason" in concrete, strict/binary terms?`)``

## Extending capabilities: verifier

`FIXME need to be synchronized with work/knowledge in mindmap`

It is known from the start that TKey is too limited to, by itself, be a _wallet_. There is no way to show the user the information that needs approval. By using an external _verifier_, it is possible to validate the information _as it is known_ by _walleTKey_. _walleTKey_ is considered a trustworthy, self-sufficient component. (See section on _trust-foundation_.) We do not want to extend trust for how walleTKey operates. However, we can "extend" capabilities, by processing (authentic) _walleTKey_ data to display.

The threat-level strongly influences how complicated this process must be. At __L2__ with only the possibility for a passive attacker, there is no need for verification. At __L3__ we cannot trust that shown information is authentic: both input and output can be corrupted. The verifier, as a small, unconnected application, provides a way to process and validate critical information. Furthermore, input can be expected to be monitored/logged, so authorization-/confirmation-codes should be dynamic (deterministically, unpredictably random), to prevent reuse.

`FIXME possibility: provide extended capabilities for checking transaction-data. This is very much undesirable, because it is essentially extending trust.`

## Known attack targets

`FIXME specify target properties with information being targeted, such as identity => deniability.`

- _Assumed security of TKey_:
  - `TODO given prototype/experiment centers around TKey device, assumption of trust taken as TKey, i.e. there are some issues involved with FPGA. (<https://hackaday.com/2018/09/27/three-part-deep-dive-explains-lattice-ice40-fpga-details/>, <https://github.com/sylefeb/Silice/blob/draft/projects/ice40-warmboot/README.md>)`
  - `TODO assumption of trust in firmware: incorruptibility is critical for protection from malicious programs.`
  - Target: _UDS_, to simulate any walleTKey based on specific device (hardware).
  - Target: _CDI_, to derive symmetric keys and identity keypair for a specific walleTKey.
- `CDI` is read-only, meaning that the program's unpredictable, unique secret from which secret material is derived, is available. Attacks with malicious input that are able to access `CDI` will be able to use/extract this value. (<https://github.com/tillitis/tillitis-key1/issues/186>)
- Target _identity_ (_deniability_): given some sealed wallet, it should not be easier to determine that a file is a sealed wallet than it is to load a sealed wallet.


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"
[reproducible-builds]: <https://reproducible-builds.org/> "Reproducible builds — a set of software development practices that create an independently-verifiable path from source to binary code."
[PQC-Migration-Handbook]: <https://www.tno.nl/en/newsroom/2023/04-0/pqc-migration-handbook/> "The PQC Migration Handbook (2023-04-04)"
[JPAumasson-Too-Much-Crypto]: <https://eprint.iacr.org/2019/1492.pdf> "J.P. Aumasson, Taurus SA, Switzerland: Too Much Crypto"
