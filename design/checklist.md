# Checklist

- __verify__: strict-by-default implementation (deviations from expected result in error/abort/failure)
- __verify__: _firmware_ sensitive data contained in variables zeroed after use.
- __verify__: verify constant-time operations for `walletctl`, `firmware`.
- __verify__: sealed wallet (completely) looks like uniformly random bytes, given insufficient information to interpret data. 
- __verify__: new version of sealed-wallet written to disk cannot corrupt old version, cannot result in bad data in case of failures/bugs.
- __verify__: outputs cannot be (ab)used out-of-context.
- __verify__: outputs can be authenticated for cases where there is room to doubt authenticity.
- __verify__: build is reproducible?
- __verify__: local/isolated build-signature matches with dev-machine, matches with CI/CD env, etc.?
- __verify__: build(-environment) still minimal?
- __verify__: dependencies up-to-date?
- __verify__: dev-environment/build-environment (Docker-)images/-scripts up-to-date, independently executable/reproducible?
- __verify__: ...
