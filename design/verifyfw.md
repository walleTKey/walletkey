# `verifyfw` (walleTKey essentials verification)

__threat-level__ L1 (and higher)

A verification tool that ensures the wallet secret is accessible, despite possible bugs or other issues in the _walleTKey_ firmware.

## Terms

See terms definitions in the [main design document][walletkey-design].

## Requirements

Load a (selected) firmware-binary and ensure that the wallet secret is extractable.

## Notes

`FIXME allow specifying an identity public key via the commandline to allow checking without working with a wallet: verifyfw, walletctl`

`FIXME verifyfw to report firmware-hash, identity to establish exactly which walleTKey was verified. Can be used to manually confirm that you're working with the same walleTKey that was verified. (simple manual comparison of results)`


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"

