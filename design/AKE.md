# Simple, asymmetric AKE

`TODO consider applicability of [Noise Protocol framework](<http://noiseprotocol.org/>)`

`FIXME needs reference to eCK for security of AKE, and cryptography-2023 article "Authenticated Key Exchange Protocol in the Standard Model under Weaker Assumptions"`

`FIXME HASH(pk_user^sk_device, pk_device) was originally intended to provide more randomness, uniformly random bytes, given that c = a^b for many pairs of a, b`

__threat-level__ `FIXME depends on whether observing USB serial communication is L2 (passive) or L3 (active)` (and higher)

An simple, asymmetric authenticated key exchange (AKE).

This is an authenticated key exchange, specifically useful for situations where one of two participants only responds to incoming messages. In this case, _walleTKey_ (`device`) only responds to messages from the client (`user`). Because of this respond-only position and the fact that _walleTKey_ does not carry data, all initiative and all risk sides with the client (`user`). There is no need to have `user` authenticate to `device`.

__note__ at this point I would expect this protocol to exist already, but I have not yet found/identified it (yet). `FIXME identified protocol or found paper?`

## Terms

See terms definitions in the [main design document][walletkey-design].

## Characteristics

This simple two-party AKE provides asymmetric security guarantees. One party `user` represents a user with full control and risks, relying on authentication, and the other party `device` which is a participant that only responds and relies solely on a confidential session.

- 1 Round-trip
- Authenticated Key Exchange
  - D-H key exchange
  - Asymmetric authentication: `user` authenticates `device`
  - `FIXME other side relies on the implicit authentication of a 2-party confidential session such that the other party must be the one that started the AKE`
- Confidential session
  - Ephemeral session-key

``FIXME below is not quite accurate: device can be sure that other party it communicates with is the party that performed the key exchange. If this is a malicious actor, then `device` will work and act on data provided by malicious actor. However, no misidentification can take place, because device does not maintain data for/from clients.``

- `device` is guaranteed a confidential session with the initiator (user)
- `user` is guaranteed a confidential session with `device` and proven identity.
- (Short-lived) session:
  - no session identifier (although should be trivial to add)
  - no elaborate features, such as session resumption or session key rotation
- _analysis_: satisfies the properties of the extended Canetti-Krawczyk (eCK) model. (`FIXME is analysis concluded? is fully symmetric or partially one-sided? I'm not fully convinced we can count Key Confirmation for device, as it has to await first message and see if it makes sense, but then initiative is not at device.`)

- `FIXME note unique suitability for TKey device, given its capability of deterministically generating a unique identity key-pair`

- `FIXME note unique suitability for TKey device: light-weight protocol for one-sided asymmetry given there is no initiative on part of device: in communication nor data-storage.`

_Note_ that session IDs are not supported by default, because the most basic use-case, one client interacting with a device, does not require a session distinguisher.

## The protocol

`TODO note that this design currently handles a single session at a time. (As per the nature of TKey device.) A session ID can (most likely) be trivially added in cases where needed.`

with:

- `sk_` prefix for secret/private keys
- `pk_` prefix for public keys
- `digest = HASH(key, content)` for keyed hash-function
- `ciphertext = ENCRYPT(key, plaintext)`
- `plaintext = DECRYPT(key, ciphertext)`
- `signature = SIGN(secret_key, content)`
- `VALIDATE(public_key, signature, content)`

__precondition__ given an identity `pk_identity` acquired at an earlier time.  
__requirement__ `device` must not rely on `user`'s identity.

`FIXME needs to include prefix if we want to prevent signature or content to be abused outside of context.`

``FIXME currently, the justification for `HASH(pk_user^sk_device, pk_device)` is not well-established. Hashed DH is recommended by monocypher. The usual recommendation is to hash both public keys. We currently only hash the device's public key, because the firmware-provided `blake2s` implementation only allows one-shot keyed hashing, and at this point we are only sure about the device's public key. Given that the key is the shared secret, we are already sure that the session-key contains an unpredictable result. Yet, are there hidden risks? Furthermore, _walleTKey firmware_ is considered trustworthy, so gives the benefit that hashing is performed with trustworthy data. (Claim a bit dubious, given that shared-secret assumes authentic pk_user without being able to confirm.) (Note that pk_user will already be ensured when verifying proof-of-identity signature, i.e. later step in the protocol.)``


```
User                                                             Device
=======================================================================
knows `pk_identity`                  knows `sk_identity`, `pk_identity`
generates `sk_user`, `pk_user`

User ----------------------------[pk_user]---------------------> Device

                                     generates `sk_device`, `pk_device`
                        sessionkey = HASH(pk_user^sk_device, pk_device)
                                 signature = SIGN(sk_identity, pk_user)
                               enc_sig = ENCRYPT(sessionkey, signature)

User <----------------------[pk_device,enc_sig]----------------- Device

sessionkey = HASH(pk_device^sk_user, pk_device)
proof = DECRYPT(sessionkey, enc_sig)
VALIDATE(pk_identity, proof, pk_user)
```

`FIXME lack of prefix (context for signing)`

Execution concludes successfully when the proof validates successfully. Any corruption results in failure to validate the signature. Given that all risk lies with the user, anything other than (complete) success is reason for termination.

After successful execution:

- a confidential session is established with ephemeral session key
- we are assured that `pk_device` is authentic
- we are assured that `pk_user` was received unchanged
- we are assured that device has ownership of identity-keypair

Failed execution:

- `pk_user` is invalid/corrupted
- `pk_device` is invalid/corrupted
- `sessionkey` cannot be produced due to `pk_device`  
  _shared-secret depends on some pair `a`, `b` that result in said shared-secret. The keyed-hash derivation of `sessionkey` ensures that `pk_device` is authentic._
- validation of `pk_user` fails due to:
  - `proof` is not authentic `signature`:
    - due to corruption of `enc_sig`
    - incorrect `sessionkey`
  - device having no ownership over `sk_identity`

## Insights

Some key insights to the AKE.

- Use of ephemeral D-H public key (which already requires freshness, is already publicly exposed) as challenge nonce to be signed for proof of identity (ownership).
- The D-H public key cannot be arbitrarily swapped as the corresponding private key means any deviation is detected, therefore doubling as nonce for signing means any opportunity to replace or manipulate said nonce is negated/detected.
- Fixing one of two D-H public keys and resulting shared secret of the key exchange, ensures other key is fixed, as we would otherwise compute a different resulting session-key. (`FIXME not sure if this is a real issue. Also, produces uniformly random bytes .. Hashed D-H`)
- Given the asymmetry, a failing signature validation against `user`'s public key for _any_ reason results in a failed key exchange and thus untrustworthy session, without needing to distinguish failure cases.
- Given that `device` party is reactive, there is no need for guarantees apart from the secure (confidential) session that is established. (`FIXME am I forgetting any trivial properties that already hold?`)
- `FIXME remove for unnecessary?` Requires that `device` does not rely on authenticating `user`, e.g. because `device` is stateless, session-scoped, does not persist data, and/or expects `user` to provide all data.

## Analysis

`TODO eCK, eCK-PFS, ...`

`FIXME are the security guarantees, in particular authentication, still valid if we cannot validate the identity with sealed-wallet until wallet is loaded? It seems so, mostly, because we have still ensured that walleTKey owns identity and we established secure session. However, matching identity with that of sealed-wallet is now delayed until wallet is loaded. (given that any identity-hint would make it easier to identify a sealed-wallet by identity than by loading the wallet. See sealed-wallet.md)`

### Requirements

- `device` must not require identity/authenticity of the connected client, i.e. working with provided data only, ensures that no data of other clients is unintentionally disclosed/exposed.
- `device` assumes that confidential session ensures only intended counterpart can communicate within the established session. (`FIXME relying on AKE, confidential communication`)

### Characteristics

A two-party, asymmetric authenticated key exchange satisfying the requirements of the extended Canetti-Krawczyk (eCK) model.

`TODO at protocol finish, only 'user' is fully informed on some of the eCK properties. 'device' needs to derive from 'user' that if further communication is desirable and user responds with appropriate session-key that therefore other security properties hold. Need to investigate the exact definition of extended Canetti-Krawczyk to be sure if claim holds that this asymmetric AKE satisfies eCK (in intention rather than in strict symmetric expectations)`

__(Implicit) Key Authentication__: a Diffie-Hellman key exchange performed between two parties. Correct execution implies that both parties conclude on the same shared secret.

__Key Confirmation__: `FIXME should hold, needs to be proven` (`FIXME this may be one-sided, because at end of protocol, user knows session key is correct, but device will only know once first encrypted message is received. User knows communication is possible.`)

> TODO given DH key exchange with subsequent key-hashing of device public key, we fix 2 out of 3 components of the mathematical computation, therefore IIUC it should follow that the 3rd component must also be equal, since any other value results in a different outcome. Therefore we are assured that both parties work with (byte-)exact same values.

__Known Key Security__: every key exchange is performed with fully random data with no state/awareness of prior sessions.

__Security against Unknown Key Share (UKS) attacks__: there are two parties involved. Diffie-Hellman key exchange ensures no man-in-the-middle interference. Device is agnostic of client identity, i.e. will respond to whoever establishes the session. User receives a proof-of-identity signed with device identity.

__Security against Key Compromise Impersonation (KCI) attacks__: `FIXME to be proven`

`TODO 1. given long-term identity of device, signature based on value with prefix/context should prevent abuse outside of the intended use-case.`
`TODO 2. identity is not used in AKE outside of generating proof intended for the user.`

__Forward Secrecy__: Any session is established with only prior knowledge of device identity. Other variables are generated from random data.

`TODO note: not completely fair in that it doesn't "heal" during a single session after keys are disclosed. As long as a session is active, the same key will be used. However, the assumption is that sessions are short-lived, and there is no history to derive keys from, so if ever a session's key is exposed/leaked, the next session will be unrelated. This may need to be documented better or more explicitly.`

`FIXME weak is proved with passive attacker using verifpal, active still requires proving as we run into bug.`

`TODO note required honesty of device? -> guaranteed by reproducible builds, open-source, firmware checksums, firmware+USS-specific secrets (incl. identity) in TKey.`

`TODO note expected honesty of user? -> if dishonest, impacted by its own actions.`

- Key exchange for ephemeral session keys.

`FIXME justify one-sidedness by explaining how initiative always comes from one party, with the other party being reactive. The one party controls the initiative and controls access to the sensitive data. The other party (only) controls its own internal secrets, to be applied in a process rather than exposed. Therefore, other party cannot be misinformed or make incorrect judgement calls for unknowns in session-security circumstances.`
`FIXME lack of authentication of user to device is mitigated by fact that device only responds, i.e. relies on judgement of the user it interacts with, for secure setting/execution.`
`FIXME the one-sidedness only holds up if we can guarantee that subsequent interactions (session) is confidential and authenticated (authenticated is still an issue).`

- Asymmetric authentication: authenticate device to user.

## Proofs

Proofs using [Verifpal] proof-system.

- ✓ [passive](<ake-passive.vp> "Proof of AKE under passive attacker")
- ⋯ [active](<ake-active.vp> "Proof of AKE under active attacker")  
  _Verification delayed due to what seems like a bug in or limitation of Verifpal._

## Open issues

- [Verifpal produces erroneous output](<https://lists.symbolic.software/pipermail/verifpal/2023/000454.html> "[Verifpal] Checked SIGNVERIF passed, but checked ASSERT fails (was: Mistake or missed case? SIGNVERIF with original of sent value)") (counter-example) for AKE with active attacker. Suggested failure-trace modifies variables that are original and fixed, therefore suggesting a case need never occur.


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"
[Verifpal]: <https://verifpal.com/> "Verifpal: Cryptographic Protocol Analysis for Students and Engineers"

