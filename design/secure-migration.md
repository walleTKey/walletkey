# Secure Migration protocol

## Requirements

```FIXME
ZERO: optional feature, because it is always possible to extract the raw wallet secret and import it elsewhere.
FIRST: is there a common protocol/procedure for such use-cases already? (Not looked for existing solutions yet, notes are based on intuitive "obvious" idea, not further investigated/verified/validated)

__requirements/design/notes__:
- wallet secret cannot be exposed unprotected, e.g. in raw/encoded format
- admin-PIN must be verified at migration (original walleTKey) and restoration (destination walleTKey)
- migrate/restore wallet as-is, further modifications such as changing PINs must be separate operations (keep things as simple, straight-forward as possible)
- use of ephemeral key, with some form of key-agreement between src and dst walleTKeys
- provide some sort of "knowledge" component (one-time password, nonce, ...) that need not be written down if secure migration happens all-at-once. That way, even long-term knowledge --such as admin-PIN-- is not enough to duplicate the migration-payload for a second restoration later-on, possibly by attacker. (Allow user to forget immediately after successful restoration, but adds an unknown factor.)
- aim for something like (deterministically generate key at destination, load payload into source -> source exports encrypted payload based on key-agreement, load export-payload into destination)
- possibly use Verifier for deterministically generating the random component, such that we can help with true randomness, expecting only needed one time.
- allow both source and destination walleTKeys to be powered down, i.e. don't rely on persistent memory for temporary keys/nonces/etc. (Needed in case of migration to different passphrases using same device/hardware for source and destination.)
- sealed wallets are never made "inoperable", because are essentially read-only. "Old wallet" is always still accessible, unless deleted/hardware destroyed/passphrase forgotten.

1. [at destination] generate migration(-description)-payload.
2. [at source] perform secure migration using descriptor-payload. (also enter admin-PIN, one-time key)
3. [at destination] perform secure restoration using secure-migration-payload from (2.). (also enter admin-PIN, one-time key)

Components: one-time key, admin-PIN, key-agreement key, touch/confirm/authz pattern/code, ...
```
