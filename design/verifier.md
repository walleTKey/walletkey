# Verifier (L3)

__threat-level__ L3 (and higher)

Verifier provides verification capabilities of feedback from the walleTKey and a way of confirming/providing authorization for walleTKey operations.

## Terms

See terms definitions in the [main design document][walletkey-design].

## Requirements

`FIXME determine whether read/write is necessary. (Although we may need to write new sealed-wallet if changes in verifier/PINs.)`

## Design

`FIXME have verifier work with a 2-byte format indicator, such that any number of messages can be defined and no trickery or format-id recycling needs to happen. If the format changes, change the ID. Keeps verifiers maximally compatible without sacrificing complexity and keeps things straight-forward.`

Verifier is an _extension_ of the capabilities of the _TKey_, complementing TKey in function but not trust. _walleTKey_ is (supposed to be) fully self-sufficient for computation and verification, but delegates to _Verifier_ where display of critical values and confirmation are needed. _walleTKey_ can determine, based on user input, if information is in sync, i.e. user is correctly informed.

`FIXME ...`

## Notes

`FIXME ...`

```
- no network/communication required,
  - hence no manipulation, attack-vector from internet + no ability for "hacked" app to query instructions from internet server.
  - hence not possible to check with bitcoin full-node. Instead, knowing information is valid, should reassure that the desktop-client operates without issues/corruption. Possibly provide enough information to check with full-node or on-line for current status of funds. It is important to keep app disconnected such this doesn't become a communication channel for malicious interactions.
- assume basic app isolation by Android OS
- assume (somewhat) secure OS
- limited importance -> limited influence
  - limit influence to actions predefined by _walleTKey_, such that walleTKey is in control of whatever it allows _verifier_ to do/be involved in. That way, corruption must occur (1.) for the right action, (2.) at the right time, (3.) with the right information to be credible, effective.
- use secure key-store for identity pubkey to reduce exposure of asymm crypto.
- use secure/custom keyboard for PIN-entry
- shield external observation of screen (limited, privacy settings of Android?)
- relies on secure OS for privacy
```


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"
