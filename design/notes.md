# Notes (development)

``FIXME is p256-m 'Apache-2.0' a problem, incompatible with GPL-2.0-only?``

``TODO set LICENSE on walleTKey firmware (probably GPL-2.0-only), note that `tkey-libs` uses GPL-2.0-only, `p256-m` uses Apache-2.0, `sha-256` uses Unlicense/Zero-BSD, `ripemd160` uses MIT.``

`FIXME need to investigate potential license incompatibility between GPL and Apache 2.0, but then would also be a problem for another project.`

`TODO build CI/CD environment, independently build images, (Docker-)image/nix-script/... for independent set-up of build environment, recreation of minimal build-environment for reproducible builds?`

- _consideration_: a separate firmware-file can always be back-upped, verified, used, without there ever being a question which firmware was loaded. It is possible to embed a full binary into a wallet client, which is convenient because the client and firmware are bundled, but it prevents: verification, transparency, manual rebuilding, etc.

`TODO discuss whether wallet-at-rest format needs to support read-only or read/write is necessary`

```FIXME
Move somewhere else? (migration protocol, design, implementation)
Migration:
- migrate to different device, firmware, passphrase, or any combination thereof.
> secure migration protocol: multi-step secure migration process that migrates a wallet without exposing the wallet secret or other sensitive information during the migration process.
  - needs to be authenticated (admin-pin? wallet-pin + admin-pin?)
  - needs to negotiate a temporary (one-time) symmetric key
```
```FIXME
> Products:
  - TKey basic utility-functions
  - Basic one-sided AKE with implementation
  - p256k1 library (for TKey)
  - Schnorr signatures (for TKey) (???maybe???)
  - binary: walleTKey firmware
  - application: walleTKey firmware essentials verifier
  - application: walleTKey cli

> PoC, but also simple solutions:
  - want to know if you can trust a device? Test its identity on multiple PCs, check if it is the same identity everywhere.
  - TOFU: the device (identity) that created the wallet becomes part of the sealed wallet. Test walleTKey identity as many times as needed. The sealed wallet is tied to a single walleTKey (TKey + firmware + USS = identity) and will rely on this identity.
  - walleTKey: not going to be a holy grail, but may be go-to wallet with more benefits than software-only, more stability given locked-in firmware, simpler, relying on existing software wallets?

define active attacks:
  - DoS?
  - malicious interference with communication?
  - misrepresentation of critical information in attempt to fool walleTKey in signing away  possessions
```

`FIXME limitations of walleTKey?`

`FIXME reliance on (full node) or software wallet for "mundane" tasks.`

```FIXME
walleTKey:
- managing access to wallet seed
- managing access to signing capability
- provide trustworthy information/actions where external interactions are concerned
- provide a mechanism with a hardware "token" to manage a bitcoin wallet
```
