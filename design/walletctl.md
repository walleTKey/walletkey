# Wallet

__threat-level__ L1 (and higher)

## Terms

See terms definitions in the [main design document][walletkey-design].

## Requirements

`FIXME reconsider how to work with the agent .. it currently exposes the identity, making it easily accessible. This defeats the purpose. Better to query identity from walleTKey every time? That way, we only have fingerprints and paths to wallets.`

`FIXME determine whether read/write is necessary. (Although we may need to write new sealed-wallet if changes in verifier/PINs.)`

`FIXME must be fool-proof (no overwriting)`

`FIXME allow user to manually provide fingerprint of identity key that can be matched, regardless of other checks?`

## Design

`FIXME ...`

### Agent

Given that _Bitcoin Core_ relies on multiple calls to the signer to operate, we require some intelligence `FIXME continue here ...`

## Notes

``FIXME we may need a process that caches/provides information to `wallet-signer` such that wallet-signer knows what sealed-wallet to load for a particular fingerprint and is able to operate independently without expecting constant/repeated/duplicate user-input.``

`FIXME allow specifying an identity public key via the commandline to allow checking without working with a wallet: verifyfw, walletctl`

`TODO can we take steps to further isolate/harden the process at start-up such that access to memory and other invasive techniques is impossible/restricted to highest privileges? Goal is to restrict actions to an active attacker such that we're in a high threat-level, such that mitigations are in the class that tackle even more serious/critical classes of intervention.`

`TODO consider having walletcli application check firmware-hashes against internal database before working with firmware, to avoid users screwing around with random dev builds? (unless -development command-line flag specified)`

`TODO better approach is to generate a wallet within the walleTKey such that they do not need to trust the client application with their wallet seed/secret.`


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"

