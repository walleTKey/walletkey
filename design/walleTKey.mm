<map version="freeplane 1.11.5">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="walleTKey" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_843161942"><hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_143434279" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#000000" STYLE="fork" NUMBERED="false" FORMAT="STANDARD_FORMAT" TEXT_ALIGN="DEFAULT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT" CHILD_NODES_LAYOUT="AUTO" VGAP_QUANTITY="2 pt" COMMON_HGAP_QUANTITY="14 pt" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#808080" BORDER_DASH_LIKE_EDGE="false" BORDER_DASH="SOLID">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_143434279" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="Noto Sans" SIZE="11" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge COLOR="#808080"/>
<richcontent TYPE="DETAILS" CONTENT-TYPE="plain/html"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/html"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#cc0000">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#aabdff" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#4e85f8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#c04600" STYLE="fork">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font SIZE="11" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" COLOR="#ff0000">
<icon BUILTIN="yes"/>
<font SIZE="12" BOLD="true"/>
</stylenode>
<stylenode TEXT="Question" COLOR="#2b2b2b" BACKGROUND_COLOR="#eaeaea">
<icon BUILTIN="help"/>
<font ITALIC="true"/>
</stylenode>
<stylenode TEXT="Details" COLOR="#cc0000">
<font SIZE="10"/>
</stylenode>
<stylenode TEXT="TODO" COLOR="#ffffff" BACKGROUND_COLOR="#aaaa00">
<font BOLD="true"/>
<edge COLOR="#aaaa00"/>
</stylenode>
<stylenode TEXT="FIXME" COLOR="#ffffff" BACKGROUND_COLOR="#cc0000">
<font BOLD="true"/>
<edge COLOR="#ff0000"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="15"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="32" RULE="ON_BRANCH_CREATION"/>
<node TEXT="threatmodel" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="bottom_or_right" ID="ID_989806744">
<edge COLOR="#ff0000"/>
<node TEXT="out-of-scope" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1130252158"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Risks/problems that are out-of-scope.
    </p>
  </body>
</html></richcontent>
<node FOLDED="true" ID="ID_1135836033"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>TKey</b>&#xa0;vulnerabilities of TKey device/hardware
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_603751829" MAX_WIDTH="314.73913 pt" MIN_WIDTH="314.73913 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      software<span style="font-weight: normal;">-based: low risk, due to minimal firmware, minimal surface area for attack.</span>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node ID="ID_1018290653" MAX_WIDTH="321.3913 pt" MIN_WIDTH="321.3913 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      hardware<span style="font-weight: normal;">-based: some risks, see references for more details on strengths and weaknesses of FPGA design/-workings. Some risks avoided due to limited featureset of RISC-V processor.</span>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
</node>
<node ID="ID_966833912"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Bitcoin </b>vulnerabilities in elliptic curve cryptography concerned in Bitcoin (protocol) foundation
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="corruption of external components…" FOLDED="true" ID="ID_786991822"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      e.g. Bitcoin full-node
    </p>
  </body>
</html></richcontent>
<node TEXT="Actually, this statement is too superficial. Corruption of the Bitcoin Core application or full-node data can interfere with operations, such that the signed transaction is incorrect, therefore the signed transaction is not a valid transaction for the real/true Bitcoin blockchain. Such manipulations lead to DoS. However, as with corrupted clients, the walleTKey must perform sufficient internal verification.&#xa;It is simply that we have no influence over the integrity and operations of other applications, such as Bitcoin Core." STYLE_REF="Details" ID="ID_375984395" MAX_WIDTH="358.56521 pt" MIN_WIDTH="358.56521 pt"/>
</node>
</node>
<node TEXT="trust-foundation" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1921657576"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The foundation of trust, decomposed in its discrete, individual components.
    </p>
  </body>
</html></richcontent>
<node ID="ID_153072401" MAX_WIDTH="281.50277 pt" MIN_WIDTH="281.50277 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>assumption</b>&nbsp;TKey security guarantees
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The TKey provides guarantees such that the platform is incorruptible and guarantees a safe environment for the program-binary without risking exposure of the secret in case of a corrupted binary.
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_1906242870"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>assumption</b>&nbsp;trust-on-first-use (TOFU), repeated checking possible
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Repeated querying of identity possible. When wallet is created (generated/imported), identity is established.
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_628847848"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>requirement</b>&nbsp;firmware program-binary correctness
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Given the guarantees of the platform, correctness and soundness of the firmware-binary is sufficient.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="separate challenge for acquiring authentic binary" STYLE_REF="Question" ID="ID_937409810"/>
</node>
<node TEXT="risks" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" ID="ID_345147961" MAX_WIDTH="156.65217 pt" MIN_WIDTH="156.65217 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Index of (envisioned) possible risks (upon established trust foundation).
    </p>
  </body>
</html></richcontent>
<node TEXT="TODO Need to handle risk of brute-forcing passphrase, by hardening decryption, such that -- when corresponding identity is unknown -- testing walleTKey (identity) for sealed wallet is hard." LOCALIZED_STYLE_REF="styles.important" ID="ID_1314437794" MAX_WIDTH="415.30434 pt" MIN_WIDTH="415.30434 pt"/>
<node TEXT="It is easier to copy/extract the identity than it is to steal the device." LOCALIZED_STYLE_REF="styles.important" ID="ID_1227092614" MAX_WIDTH="421.17391 pt" MIN_WIDTH="421.17391 pt">
<node TEXT="So storing a separate identity-hint with the sealed-wallet is most likely problematic in all cases?" STYLE_REF="Question" ID="ID_578448189"/>
</node>
<node TEXT="Verifier (Android): needs protection against malicious apps primarily. (Hence keystore for public-key, etc. are necessary as it protects data at rest. But will not protect against OS-rooted manipulation by itself. This can essentially only be mitigated, e.g. by limitation power/influence of the Android-app itself.)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1169504296" MAX_WIDTH="486.13043 pt" MIN_WIDTH="486.13043 pt"/>
<node TEXT="Did we already include a risk for brute-forcing the walleTKey identity when in possession of the sealed-wallet-file? (New category &apos;compromised sealed-wallet&apos;?)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1425046855" MAX_WIDTH="486.13043 pt" MIN_WIDTH="486.13043 pt"/>
<node TEXT="characteristic risks" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_483551236"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Risks inherent to the project
    </p>
  </body>
</html></richcontent>
<node ID="ID_295633384" MAX_WIDTH="349.95652 pt" MIN_WIDTH="349.95652 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">prerequisite</span>&#xa0;A unique secret is generated based on device, program-binary and USS. Basic functionality <b>must</b>&#xa0;work in order to safe-guard access to the wallet in case of problems with firmware, such as bugs.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_262735203" MAX_WIDTH="353.21739 pt" MIN_WIDTH="353.21739 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;(Verifier) inherent risks of smartphone app. The verifier is intended to complement missing capabilities of TKey. The risks can (hopefully) be acceptably reduced by e.g. ensuring no need for connectivity.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="user-caused issues" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_979531934" MAX_WIDTH="127.82609 pt" MIN_WIDTH="127.82609 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Accidents, mishandling, misuse, mistakes
    </p>
  </body>
</html></richcontent>
<node ID="ID_841728760"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;corruption of &quot;sealed wallet&quot;-file
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1472954049"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;incorrect use of wallet-client when updating &quot;sealed wallet&quot;-file
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1760766084"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;deleting/losing &quot;sealed wallet&quot;-file
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_753186786"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;deleting/losing firmware-binary file
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_381014969"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;bug/bad/defective/edge-case in firmware-binary causes problems in handling wallet transactions
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_740642377"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;using `wallet` commandline client with random development firmware-binaries, consequently running into issues
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_442138389"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;users need to understand that walleTKey consists of: sealed wallet, firmware-binary, device, USS. With any of them missing, wallet is gone.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_280822629"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;mistaking critical operation for non-critical operation when performing touch-confirmation
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1964681616"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;use of privacy-invasive keyboard apps on Android captures PIN entry
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_902107532"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">critical</span>&#xa0;exposing raw wallet secret, e.g. during migration or first import
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="TKey-device" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1658981295"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      tillitis TKey hardware
    </p>
  </body>
</html></richcontent>
<node ID="ID_4769258"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;bad TKey device
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_502663164"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;loss/broken TKey device
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_860877246"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;buggy firmware in TKey
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_862446564"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3/L4</span>&#xa0;TKey's UDS (internal secret) is compromised
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="walleTKey" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1281552889"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      TKey with walleTKey firmware loaded.
    </p>
  </body>
</html></richcontent>
<node ID="ID_1764833077"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;bad (buggy/incomplete, non-malicious) firmware
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_678885384"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;malicious firmware, from first-use non-accidental (as opposed to mistakenly/unknowingly)
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_888919834"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;device/firmware is (unknowingly) swapped/replaced
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Should this be L3/L4 or L1? (depends on how we draw boundary, if we allow Evil Maid to steal TKey at L1, seems reasonable.)" STYLE_REF="Question" ID="ID_192904169"/>
</node>
<node ID="ID_85901138"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;timing-attacks via messaging request-response
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_779642826"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;malicious (corrupted, specially-crafted) messages.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_13919688"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L4</span>&#xa0;brute-force attacks
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_896222129"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L4&#xa0;</span>infinite attempts possible
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="compromised host system (passive)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_575431177"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Passive monitoring/observation of host.
    </p>
  </body>
</html></richcontent>
<node FOLDED="true" ID="ID_461903712"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;eavesdropping on communications with walleTKey
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Is this L2 or L3?" STYLE_REF="Question" ID="ID_888096237"/>
</node>
<node FOLDED="true" ID="ID_529753219"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;keyboard-snooping
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Is this L2 or L3?" STYLE_REF="Question" ID="ID_816523738"/>
</node>
<node ID="ID_769297560"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;duplication, reproduction, replaying of produced information (communicated or persisted)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_904555930"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;exposing wallet secret
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_1529502710"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;timing-attacks on walleTKey communication
    </p>
  </body>
</html>
</richcontent>
<node TEXT="assuming this requires multiple, methodically-chosen requests, would be L3" LOCALIZED_STYLE_REF="styles.important" ID="ID_820709036"/>
</node>
<node FOLDED="true" ID="ID_823098517"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;timing-attacks on client application
    </p>
  </body>
</html>
</richcontent>
<node TEXT="assuming multiple requests, might be L3, but is also within same system so can be quite precise" LOCALIZED_STYLE_REF="styles.important" ID="ID_1954306620" MAX_WIDTH="303.52174 pt" MIN_WIDTH="303.52174 pt"/>
</node>
<node ID="ID_193430697"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;exposure of wallet-related critical information.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="compromised host system (active)" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_1460359989"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Active interference from host.
    </p>
  </body>
</html></richcontent>
<node ID="ID_1147638661"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;inspection of process memory (invasive analysis/observation)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1444539754"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;malicious interactions with walleTKey (e.g. background process, malicious client)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1558879991"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;malicious changes to walleTKey
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_199570550"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;DoS (corrupting, malicious, random data)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_534105639"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;malicious handling of input/output from walleTKey
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      e.g. (ab)use outside of intended context
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_898287216"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;misuse of signatures outside of intended context
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_732513551"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;client-side resets of TKey
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1021579390"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;keyboard-snooping while entering admin-/wallet-PIN
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="compromised verifier" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_150015977" MAX_WIDTH="145.04348 pt" MIN_WIDTH="145.04348 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Corruption of the verifier
    </p>
  </body>
</html></richcontent>
<node TEXT="Consider the asymmetry of circumstances, given that plain Android apps become increasingly more protected and isolated. A basic verifier-app as second-opinion becomes more reasonable, especially considering that corrupting the app gives limited influence." STYLE_REF="Details" ID="ID_555327369" MAX_WIDTH="354.14423 pt" MIN_WIDTH="354.14423 pt"/>
<node LOCALIZED_STYLE_REF="default" FOLDED="true" ID="ID_1208356081" MAX_WIDTH="207.91304 pt" MIN_WIDTH="207.91304 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">assumption</span>&#xa0;Why we would/could trust the verifier-app in the first place
    </p>
  </body>
</html>
</richcontent>
<node TEXT="separation of concerns (few specific, dedicated functions)" ID="ID_928761134" MAX_WIDTH="295.95652 pt" MIN_WIDTH="295.95652 pt"/>
<node TEXT="different system" ID="ID_1416255829"/>
<node TEXT="Android increasingly (improves) isolates apps" ID="ID_1844324199"/>
<node TEXT="phones usually have a viable keystore to prevent exposure of public key to other apps" ID="ID_1135349972" MAX_WIDTH="303.52174 pt" MIN_WIDTH="303.52174 pt"/>
<node TEXT="no internet connection required, i.e. working with inputted data only, so less risk of abuse/leaking" ID="ID_1386229161" MAX_WIDTH="304.30434 pt" MIN_WIDTH="304.30434 pt"/>
<node TEXT="no internet connection, i.e. no outside influence to verifier-app directly (cannot guarantee for malicious other apps, bad device security)" ID="ID_488490371" MAX_WIDTH="304.30434 pt" MIN_WIDTH="304.30434 pt"/>
<node TEXT="even if verifier is compromised in some way, must still be coordinated with client for immediate impact" ID="ID_1852788602" MAX_WIDTH="306.13043 pt" MIN_WIDTH="306.13043 pt"/>
<node TEXT="purpose of verifier is reactive, so is only given influence at specific moments, with specific operations" ID="ID_1362906276" MAX_WIDTH="304.17391 pt" MIN_WIDTH="304.17391 pt"/>
</node>
<node ID="ID_344770383"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;false-positive authentication of walleTKey-messages
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_943590689" MAX_WIDTH="290.21739 pt" MIN_WIDTH="290.21739 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;false-negative authentication of walleTKey-messages
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1710061965"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;refusal to operate (unusable, DoS? depends on role)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TO BE DETAILED OUT" LOCALIZED_STYLE_REF="styles.important" ID="ID_1077944584"/>
</node>
<node TEXT="quantum-computing" LOCALIZED_STYLE_REF="AutomaticLayout.level,3" FOLDED="true" ID="ID_397780840" MAX_WIDTH="168.3913 pt" MIN_WIDTH="168.3913 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Vulnerable classic cryptography, quantum-computing, PQC
    </p>
  </body>
</html></richcontent>
<node ID="ID_830848294"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;Prevent (unnecessary) exposure of asymmetric-key cryptographic components
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_917037351"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;Harvest-now-decrypt-later
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_812389912"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;Asymmetric-key cryptography (vulnerable): AKE, identity.
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_732164314"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;Asymmetric-key cryptography (vulnerable): Secure Migration.
    </p>
  </body>
</html>
</richcontent>
<node TEXT="Separate Secure Migration to L2, because the secure migration protects from observers too? But then it is unique to L2, because other asymm-crypto is L3." STYLE_REF="Question" ID="ID_692050273"/>
</node>
<node ID="ID_788638932" MAX_WIDTH="288.65217 pt" MIN_WIDTH="288.65217 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;Symmetric-key cryptography (less/not vulnerable): sealed wallet, ephemeral confidential session.
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Note: extent of vulnerability not clear due to unclear how large the constant factor is in the O(n/2) perf speed-up claimed by Grover's Algorithm.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node TEXT="solutions" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_909690416" MAX_WIDTH="178.95652 pt" MIN_WIDTH="178.95652 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A listing of solutions for a variety of problems/risks.
    </p>
  </body>
</html></richcontent>
<node ID="ID_1863464119" MAX_WIDTH="164.60869 pt" MIN_WIDTH="164.60869 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">default</span>&#xa0;minimalism-by-default
    </p>
    <p>
      for walleTKey-firmware
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_893598921" MAX_WIDTH="289.69565 pt" MIN_WIDTH="289.69565 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">prerequisite</span>&#xa0;results must not be useful out-of-context (unusable/verifiable)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1960974558"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">prerequisite</span>&#xa0;methodical/structured handling of data to cope with limited capacity of TKey.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="mitigating circumstances" ID="ID_547031781">
<node ID="ID_1911175596" MAX_WIDTH="310.95652 pt" MIN_WIDTH="310.95652 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;primarily/exclusively local use (limited exposure)
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      prevent unnecessary exposure of asymmetric cryptography, reduce opportunities for harvest-now-decrypt-later
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_273998218" MAX_WIDTH="355.17391 pt" MIN_WIDTH="355.17391 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;given use of ECC in Bitcoin, not useful being post-quantum safe if walleTKey can be bypassed altogether.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1486895458" MAX_WIDTH="366.7826 pt" MIN_WIDTH="366.7826 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;provide instructions how to look up transaction on-line once part of blockchain(????) I.e. not relying on local Bitcoin software to verify claimed submission.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_689029751" MAX_WIDTH="366.7826 pt" MIN_WIDTH="366.7826 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;corruption of external software/components is beyond our capability to prevent, but we can recognize suspicious signs (such as corrupt/illegal messages) and abort/fail operation.
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="where to put (L3) constant-time verification, resistance to timing-attacks at walleTKey-firmware?" LOCALIZED_STYLE_REF="styles.important" ID="ID_328428190" MAX_WIDTH="466.17391 pt" MIN_WIDTH="466.17391 pt"/>
<node ID="ID_38089214" MAX_WIDTH="357.78261 pt" MIN_WIDTH="357.78261 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">t.b.d.</span>&#xa0;&quot;classic&quot; separate, independent receive-addresses (requires continuous resealing of wallet, thus hard to manage) (for now)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_293545166" MAX_WIDTH="357.78261 pt" MIN_WIDTH="357.78261 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;documenting/warning for uncorrectable, unavoidable risks, risks rooted in human error
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1033630441" MAX_WIDTH="350.86956 pt" MIN_WIDTH="350.86956 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;limited capabilities (effectiveness, impact) given that device does not connect to internet directly)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="need to recheck, what is implied? benefits, problems?" LOCALIZED_STYLE_REF="styles.important" ID="ID_1693342341"/>
</node>
<node ID="ID_405034015"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;reproducible builds
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_359673430"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;reproducible build-environment
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Can we define a (reasonably) reliable, trustworthy, reproducible (basis for a) build-environment
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_837900405"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;scripted, automated, independent builds using the <span style="font-style: italic;">reproducible build-environment</span>
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1330780561" MAX_WIDTH="315.26087 pt" MIN_WIDTH="315.26087 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;repeated builds to check if both are byte-exact equal
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_891631852"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;blessed/released firmware-binaries
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1000651189" MAX_WIDTH="327.26087 pt" MIN_WIDTH="327.26087 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;authenticity of (firmware-)binaries through signatures
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_215473225" MAX_WIDTH="353.60869 pt" MIN_WIDTH="353.60869 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;community-signatures (possible through <i>reproducible builds</i>)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1115404233" MAX_WIDTH="360.52174 pt" MIN_WIDTH="360.52174 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;never overwite wallet-files, instead create new at destination
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_910530137" MAX_WIDTH="360.52174 pt" MIN_WIDTH="360.52174 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;display warning that newly created sealed-wallet files must be backed-up, that old sealed-wallet files are not unusable/defunct/obsolete
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1441867243" MAX_WIDTH="315.13043 pt" MIN_WIDTH="315.13043 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span><span style="font-weight: normal;">&#xa0;strict-by-default (defensive) programming practices</span>
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_325601330" MAX_WIDTH="421.17391 pt" MIN_WIDTH="421.17391 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;`<i>verifyfw</i>` as client-program for verification of basic functionality
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_55468028" MAX_WIDTH="374.34782 pt" MIN_WIDTH="374.34782 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;use of touch-confirmation to prove physical presence
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1044831514" MAX_WIDTH="346.17391 pt" MIN_WIDTH="346.17391 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;use of comprehensive, random touch-patterns to prove awareness of critical action, on top of proving physical presence
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="TKey security features" ID="ID_65763217">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="TKey hardware security mitigations" ID="ID_1754269909">
<icon BUILTIN="button_ok"/>
<node TEXT="firmware-checksum ensures secret (CDI) is only available to binary-exact firmware" ID="ID_1003976612">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="CPU execution monitor" ID="ID_251322527">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="RAM ASLR randomization" ID="ID_1582108014">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="RAM scrambling" ID="ID_431950126">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="software-/compiler-hardening capabilities/options" ID="ID_1480479622">
<icon BUILTIN="button_ok"/>
<node TEXT="fortifying code" ID="ID_1451720868">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="stack-protector (stack-check-guard)" ID="ID_513798236">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="security-aware practices (e.g. programming)" ID="ID_50744601">
<icon BUILTIN="emoji-1F6E0"/>
<node TEXT="stack-only memory-model" ID="ID_100161213">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="bug prevention (static analysis)" ID="ID_1148253112">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="undefined behavior prevention (static analysis)" ID="ID_1606342114">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="do not store sensitive values, but compute on-the-fly" ID="ID_1071335532">
<icon BUILTIN="emoji-1F6E0"/>
</node>
<node TEXT="clear memory after use for sensitive data" ID="ID_1069425557">
<icon BUILTIN="emoji-1F6E0"/>
</node>
<node TEXT="simple, practical logic/code" ID="ID_1168538726">
<icon BUILTIN="emoji-1F6E0"/>
</node>
</node>
<node TEXT="walleTKey-initiated verification/confirmations" ID="ID_1745538035">
<icon BUILTIN="emoji-1F6E0"/>
<node TEXT="prove physical presence" ID="ID_1968457527"/>
<node TEXT="prove awareness (critical operations)" ID="ID_1615434725"/>
<node TEXT="confirmation-codes" ID="ID_309267793"/>
<node TEXT="input-verification/-validation" ID="ID_1657058300"/>
</node>
<node ID="ID_446786826" MAX_WIDTH="394.69565 pt" MIN_WIDTH="394.69565 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;avoid exposing asymmetric cryptographic components to protect against quantum-computers (at lower threat-levels)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_805635510"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;client-side mitigations for timing-attacks
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_345126305">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;identity-keypair ensures authentic walleTKey
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_7711246" MAX_WIDTH="427.95652 pt" MIN_WIDTH="427.95652 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;AKE ensures communication only with authentic walleTKey (including for communicating sensitive information like confirmation-codes (admin/wallet PIN)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1844852240" MAX_WIDTH="410.60869 pt" MIN_WIDTH="410.60869 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;secure session provides confidential communication
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1592622940" MAX_WIDTH="410.60869 pt" MIN_WIDTH="410.60869 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;ensure results (e.g. signed transactions) are useless out-of-context.
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_665574067" MAX_WIDTH="411.13043 pt" MIN_WIDTH="411.13043 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;use of (static) PINs (admin-/wallet-) to prevent issuing critical operations (extracting secrets, signing transactions, etc.) during (unauthorized) physical presence
    </p>
  </body>
</html>
</richcontent>
<node TEXT="PINs have value in preventing critical operations from being executed without consequence (even if not confirmed)" STYLE_REF="Question" ID="ID_96270859" MAX_WIDTH="396.13043 pt" MIN_WIDTH="396.13043 pt"/>
<node TEXT="Limiting number of PIN attempts? (KDF/hardening?)" STYLE_REF="Question" ID="ID_798322419" MAX_WIDTH="267.91304 pt" MIN_WIDTH="267.91304 pt"/>
<node TEXT="Consider how to respond to failure to provide correct PIN? E.g. block firmware to prevent further attempts." STYLE_REF="Question" ID="ID_427561625"/>
<node TEXT="what threat-level?" STYLE_REF="Question" ID="ID_956010113"/>
<node TEXT="Entering (static) PIN from client may be risky, in case of unknown state of reliability of the client. Consider if it is reasonable to enter confirmation code using PIN through verifier." STYLE_REF="Question" ID="ID_190872485"/>
<node TEXT="note: PINs are only relevant if ownership over device lost, or no physical confirmation needed. (although confusion between operations when confirming is also a risk)" LOCALIZED_STYLE_REF="styles.important" ID="ID_882126068" MAX_WIDTH="377.34782 pt" MIN_WIDTH="377.34782 pt"/>
<node TEXT="note: different concerns for L2 (no keyboard snooping, no communication snooping, no corruption) and L3 (keyboard snooping, communication snooping, corruption of communication)" LOCALIZED_STYLE_REF="styles.important" ID="ID_217209512"/>
</node>
<node ID="ID_32135661"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2</span>&#xa0;limited authz/confirm codes tries, then block
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_38557579"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;output-validation
    </p>
  </body>
</html>
</richcontent>
<node TEXT="verifier" ID="ID_693655816"/>
<node TEXT="feedback to forward to verifier" LOCALIZED_STYLE_REF="default" ID="ID_321193105"/>
<node STYLE_REF="Question" ID="ID_1744434034" MAX_WIDTH="293.34782 pt" MIN_WIDTH="293.34782 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      does PSBT signing result need protecting? (signature <span style="font-weight: bold;">should</span>&#xa0;&#xa0;only be suitable for intended bitcoin transaction)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node FOLDED="true" ID="ID_663684899" MAX_WIDTH="302.60869 pt" MIN_WIDTH="302.60869 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;authentication of confidential communication
    </p>
  </body>
</html>
</richcontent>
<node TEXT="authentication-frame following command-frames to TKey for L3 (i.e. risk of corruption)" ID="ID_488907447" MAX_WIDTH="286.69565 pt" MIN_WIDTH="286.69565 pt"/>
<node TEXT="ensure authentic communication data" ID="ID_1308690964"/>
</node>
<node FOLDED="true" ID="ID_256180" MAX_WIDTH="350.47826 pt" MIN_WIDTH="350.47826 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;use of (dynamic) PINs to prevent issuing critical operations (see static PINs) during (unauthorized) physical presence.
    </p>
  </body>
</html>
</richcontent>
<node ID="ID_1532146869"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;authz/confirm codes not reusable
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_315288385"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;authz/confirm codes incorporate admin-/wallet-PIN
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_542529677"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;&#xa0;authz/confirm codes include verifier-registrated secret
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node FOLDED="true" ID="ID_1653599752" MAX_WIDTH="394.17391 pt" MIN_WIDTH="394.17391 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;aim for non-<span style="font-style: italic;">NETWORK</span>ed verifier, such that unavailability of connectivity reduces the possibility for corruption.
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      By isolating the app as much as possible, we reduce the chance of coordinated, in-sync corruption of information. (E.g. won't stop DoS and other blunt attacks, but reduces chance of misrepresenting information for targeted attacks, etc.)
    </p>
  </body>
</html></richcontent>
<node ID="ID_1334998767"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">assumption</span>&#xa0;to a reasonable extent, this assumes and depends on trustworthy Android OS
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Separating concerns and posing strict restrictions may help to reduce opportunities and chance of harmful corruption, such that a solution, such as an app, although inherently risky is reduced to least necessary." ID="ID_984456974" MAX_WIDTH="366.13043 pt" MIN_WIDTH="366.13043 pt"/>
</node>
<node FOLDED="true" ID="ID_39821926" MAX_WIDTH="299.86956 pt" MIN_WIDTH="299.86956 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;securely store public key in verifier
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Validation must be possible, with least exposure of public key
    </p>
  </body>
</html></richcontent>
<node TEXT="use secure key-store in OS to store public key with minimal/no exposure to others" ID="ID_25813652"/>
</node>
<node ID="ID_824529661" MAX_WIDTH="381.26087 pt" MIN_WIDTH="381.26087 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L3</span>&#xa0;scan QR-code with Verifier for quick communication w/ wallet-software
    </p>
  </body>
</html>
</richcontent>
</node>
<node FOLDED="true" ID="ID_1483204305" MAX_WIDTH="375.7826 pt" MIN_WIDTH="375.7826 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L4</span>&#xa0;use of KDF/hardening to prevent efficient brute-forcing of passphrase
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Case of stolen device and sealed-wallet file, such that passphrase must be guessed and identity-checksum is used for verification.
    </p>
  </body>
</html></richcontent>
<node TEXT="note that this involves the client primarily, so we have the capacity (computational, memory) to perform hardness mitigations." LOCALIZED_STYLE_REF="styles.important" ID="ID_839523765" MAX_WIDTH="377.34782 pt" MIN_WIDTH="377.34782 pt"/>
<node TEXT="E.g. Argon2id recommended for general use." ID="ID_1204889830"/>
<node TEXT="Note that this is originally intended for securely storing passwords. However, we store a public key, such that brute-force computing passphrases is delayed when derived identity public-key needs to be validated. (Some indirection.)" ID="ID_501237172" MAX_WIDTH="403.17391 pt" MIN_WIDTH="403.17391 pt"/>
<node TEXT="note that hardening the identity alone cannot work, because the generated identity can be tested against the auth-enc sealed-wallet blob. Maybe it is possible to compute on-the-fly and transmit over secure session. That way, we provide a hardened value (one hardened for a normal desktop computer) then generate symmetric key from that." LOCALIZED_STYLE_REF="styles.important" ID="ID_1233835361" MAX_WIDTH="377.34782 pt" MIN_WIDTH="377.34782 pt"/>
<node TEXT="assumption that CDI and firmware-binary are known,&#xa;brute-forcing passphrase," ID="ID_1160378032"/>
</node>
<node ID="ID_873248836" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L4</span>&#xa0;allow backing up the firmware-binary (securely) in the verifier
    </p>
  </body>
</html>
</richcontent>
</node>
<node LOCALIZED_STYLE_REF="styles.important" ID="ID_1297533368"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">TODO</span>&#xa0;secure migration protocol
    </p>
  </body>
</html>
</richcontent>
<node TEXT="must provide admin-PIN on transfer (both for initiation and restoration)" ID="ID_642784980"/>
<node TEXT="maintain same PINs before/after migration, s.t. PINs are known, proved known during procedure, then allow changing afterwards" ID="ID_1845339447"/>
</node>
<node ID="ID_30073897"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">general</span>&#xa0;it is okay for &quot;sealed wallet&quot; opaque blob to be unstructured, as it only ever has to be processed by the same firmware
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_721458971" MAX_WIDTH="385.56521 pt" MIN_WIDTH="385.56521 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;consider the contents of the sealed-wallet opaque blob such that it is not possible to easily derive e.g. admin-PIN length from blob content length or other such metadata.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1487432908" MAX_WIDTH="385.56521 pt" MIN_WIDTH="385.56521 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L1</span>&#xa0;AEAD with identity as associated-data ensures that we do not attempt to read data that is encrypted for a different walleTKey. (Therefore, unstructured blob is possible, viable.)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Do we need to have a small process started that caches/provides information on wallets by fingerprint, s.t. repeated executions of `wallet-signer` can acquire proper information and locate wallets, if no path to sealed-wallet is provided? Also, we need to (pre)load the firmware with specific password if that is expected." STYLE_REF="Question" ID="ID_1030687600" MAX_WIDTH="520.76893 pt" MIN_WIDTH="520.76893 pt"/>
</node>
<node TEXT="levels" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_789625977" MAX_WIDTH="206.47826 pt" MIN_WIDTH="206.47826 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Levels are additive in terms of risks and necessary mitigations. Up to L3, assumes ownership is retained.
    </p>
  </body>
</html></richcontent>
<node TEXT="document/add requirement to prevent, if possible, continued operation in whatever way, in case of corrupted/suspicious external components" LOCALIZED_STYLE_REF="styles.important" ID="ID_270293408" MAX_WIDTH="408.65217 pt" MIN_WIDTH="408.65217 pt"/>
<node TEXT="note: goal of the threat-levels is not necessarily to operate under each one of them, but at least to provide mitigations, signaling unfavorable circumstances and/or unexpected behavior." LOCALIZED_STYLE_REF="styles.important" ID="ID_919580155" MAX_WIDTH="408.65217 pt" MIN_WIDTH="408.65217 pt"/>
<node FOLDED="true" ID="ID_109464535"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>initial</b>&nbsp;generate
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Initial circumstances for generating a wallet. Assuming TOFU or a known identity, an environment up to L2 is sufficiently safe, given that sensitive operations are performed within walleTKey.
    </p>
  </body>
</html></richcontent>
<node TEXT="initial conditions are only special because we have to establish TOFU. No data other than wallet is exposed at rest, so satisfies L2 mostly(???)" STYLE_REF="Question" ID="ID_7718056" MAX_WIDTH="361.46114 pt" MIN_WIDTH="361.46114 pt"/>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1771375586">
<node TEXT="TODO" LOCALIZED_STYLE_REF="styles.important" ID="ID_898580171"/>
</node>
</node>
<node FOLDED="true" ID="ID_612518147"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>critical</b>&nbsp;import/export/migration
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A special circumstance, in which we need to introduce the wallet secret, therefore the secret is exposed by default. A critical situation that can only be done safely in a trusted environment.
    </p>
  </body>
</html></richcontent>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_529288314">
<node TEXT="wallet secret exposed" ID="ID_1824497316">
<icon BUILTIN="messagebox_warning"/>
</node>
<node TEXT="admin-/wallet-pin exposed" ID="ID_623155008">
<icon BUILTIN="messagebox_warning"/>
</node>
</node>
</node>
<node FOLDED="true" ID="ID_1424140472"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>L1</b>&nbsp;wallet-at-rest
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      An opaque binary blob that contains the wallet contents as created by walleTKey-firmware. When data is at rest, it is stored with the sufficient information to authenticate walleTKey on subsequent interactions.
    </p>
  </body>
</html></richcontent>
<node TEXT="Security-goal: Persisted sealed-wallet data is secure. No unnecessary exposure of information" LOCALIZED_STYLE_REF="styles.important" ID="ID_750382658" MAX_WIDTH="333.26087 pt" MIN_WIDTH="333.26087 pt"/>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1266653262">
<node ID="ID_346128005"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;Exfiltration attacks, such as Evil Maid and malware data-stealers.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_152593644"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;Harvest now, decrypt later
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1148989615"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;Quantum-computing
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node LOCALIZED_STYLE_REF="default" FOLDED="true" ID="ID_197877467"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>L2</b>&nbsp;observing/monitoring adversary
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      An adversary that can monitor/observe (desktop) client interactions. The client will still be assumed to work correctly, however we cannot expose ... FIXME continue ...
    </p>
  </body>
</html></richcontent>
<node TEXT="Security-goal: A passive attacker is unable to acquire critical knowledge through plain observation or timing. (This excludes invasive actions like USB communication monitoring, process-memory monitoring, keyboard-snooping.)" LOCALIZED_STYLE_REF="styles.important" ID="ID_379645261" MAX_WIDTH="521.34782 pt" MIN_WIDTH="521.34782 pt"/>
<node TEXT="assumptions" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1006334795">
<node TEXT="identity public key is still safe, as we only process identity in-memory" ID="ID_1610743746"/>
</node>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_217652713">
<node LOCALIZED_STYLE_REF="styles.important" ID="ID_1429777292"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>assumption</b>&nbsp;that process memory cannot be read by adversary
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="assumption that UART communication cannot be read by adversary" LOCALIZED_STYLE_REF="styles.important" ID="ID_174840394"/>
<node TEXT="how did we plan to handle keyboard snooping?" STYLE_REF="Question" ID="ID_1219017043"/>
</node>
</node>
<node FOLDED="true" ID="ID_90677675"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>L3</b>&nbsp;invasive adversary
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      An adversary that perform invasive actions, such as invading the process or corrupting communications.
    </p>
  </body>
</html></richcontent>
<node TEXT="Security-goal: An active attacker is unable to manipulate the authentic process, USB communications, interaction, input or output such that the intended corruption of walleTKey (inter)action is achieved._ (I&apos;m aiming for the idea of &quot;tweaking&quot; transaction values, message-values, etc. to manipulate walleTKey, user into signing a transaction resulting in undetected, unplanned loss of money, ... within reason." LOCALIZED_STYLE_REF="styles.important" ID="ID_963147348" MAX_WIDTH="527.60869 pt" MIN_WIDTH="527.60869 pt"/>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1782618887">
<node ID="ID_1117764929"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;invasion in client process memory
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_835270305"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;invasion in UART communication
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_976940131"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b><span style="font-weight: normal;">&#xa0;corruption</span>&#xa0;of client
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1887298988"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>firmware</b>&nbsp;independent interaction with walleTKey device/firmware
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1639119506"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;corruption of sent/received information
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_194288939"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>client</b>&nbsp;manipulation of displayed information
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="where to put querying identity public key for use in quantum-computing vector?" STYLE_REF="Question" ID="ID_1687430912"/>
<node TEXT="if we are to distinguish a next threat-level, consider actively corrupting activities of the authentic client, as different from full-on attacks of own initiative with corrupted client, possibly in the background on the system." STYLE_REF="Question" ID="ID_1002250617"/>
<node TEXT="did we take into account interrupting active session and continuing on from active session? (as long as session key not known, this cannot be done without losing access)" STYLE_REF="Question" ID="ID_25997405"/>
</node>
<node FOLDED="true" ID="ID_189528658"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L4</span>&#xa0;aggressive adversary (full capabilities)
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A persistent, aggressive attacker that targets computer, client, firmware or device. (Full capabilities on original hardware, or by taking components from owner.)
    </p>
  </body>
</html></richcontent>
<node TEXT="risks" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1748772682">
<node TEXT="brute-force, malicious, persistent communication" ID="ID_18929677"/>
<node TEXT="corrupt(ed) communication" ID="ID_755667953"/>
<node TEXT="TKey device is stolen, firmware is copied, attacker can employ a persistent, brute-force attacks to hack the device/firmware." ID="ID_1492787958" MAX_WIDTH="327.78261 pt" MIN_WIDTH="327.78261 pt"/>
</node>
<node TEXT="this level would not be so much functional, as it may be an unknown risk if walleTKey is connected to an unknown, adversarial environment. (E.g. previously compromised computer) Attack would be made with idea that snooping on communications isn&apos;t sufficient, therefore better to immediately resort to &quot;one-shot&quot; aggressive attack on the device." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_345741434" MAX_WIDTH="364.82608 pt" MIN_WIDTH="364.82608 pt"/>
<node TEXT="this level would be aimed to avoid, so project must be resistant when subject to/under these circumstances" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1995211012" MAX_WIDTH="364.82608 pt" MIN_WIDTH="364.82608 pt"/>
<node TEXT="assumes the effort of actively attacking with corrupt communication, corrupted values, etc. is &quot;next-level&quot; effort compared to corrupting activities originally initiated by the (authentic) client." LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_1376567229" MAX_WIDTH="364.82608 pt" MIN_WIDTH="364.82608 pt"/>
<node TEXT="abuse of encrypted and decrypted communication for manipulation of firmware" LOCALIZED_STYLE_REF="defaultstyle.details" ID="ID_187044227" MAX_WIDTH="364.82608 pt" MIN_WIDTH="364.82608 pt"/>
<node TEXT="not necessarily attacking on the PC. Maybe the device was stolen and they&apos;re performing brute-force attacks via a dedicated set-up." STYLE_REF="Details" ID="ID_135083031" MAX_WIDTH="364.82608 pt" MIN_WIDTH="364.82608 pt"/>
<node TEXT="FIXME continue" LOCALIZED_STYLE_REF="styles.important" ID="ID_413075182"/>
<node TEXT="L4 does not strictly build upon risks of L3, as it may be easier to steal the device and sealed-wallet than to compromise someone&apos;s system. It is an increment in case of freedom available to attacker of device/sealed-wallet." LOCALIZED_STYLE_REF="styles.important" ID="ID_657468818" MAX_WIDTH="434.21739 pt" MIN_WIDTH="434.21739 pt"/>
<node TEXT="set strict distinction where, up to L3, there is no loss of ownership(??? ... is a bit idealistic to try to make the definition strict ... ???)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1455983980" MAX_WIDTH="434.21739 pt" MIN_WIDTH="434.21739 pt"/>
</node>
</node>
<node TEXT="[L4-]classes" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1035985500" MAX_WIDTH="82.82609 pt" MIN_WIDTH="82.82609 pt">
<node ID="ID_1754321497" MAX_WIDTH="359.45455 pt" MIN_WIDTH="359.45455 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">assumption</span>&#xa0;UDS is insecure, i.e. TKey-hardware/tillitis compromised
    </p>
  </body>
</html>
</richcontent>
<node TEXT="brute-force CDI" ID="ID_861236079"/>
<node TEXT="brute-force passphrase" ID="ID_1675071040"/>
</node>
<node ID="ID_1943599476" MAX_WIDTH="315.54546 pt" MIN_WIDTH="315.54546 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">assumption</span>&#xa0;UDS is secure, i.e. TKey-hardware/tillitis secure
    </p>
  </body>
</html>
</richcontent>
<node TEXT="brute-force passphrase" ID="ID_916018048"/>
<node TEXT="brute-force CDI" ID="ID_308519739"/>
<node TEXT="abuse logical, control-flow, ... errors" ID="ID_2687897"/>
<node TEXT="corrupt input" ID="ID_1153349606"/>
</node>
<node ID="ID_1930736428"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">assumption</span>&#xa0;CDI is secure, i.e. firmware is secure
    </p>
  </body>
</html>
</richcontent>
<node TEXT="brute-force passphrase" ID="ID_1734342274"/>
</node>
</node>
</node>
<node TEXT="work" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="bottom_or_right" ID="ID_1586000733">
<edge COLOR="#ff0000"/>
<node TEXT="Where to put verification of Bitcoin transaction/PSBT data? (not expecting corruption until L3)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1436778626" MAX_WIDTH="340.95652 pt" MIN_WIDTH="340.95652 pt"/>
<node TEXT="Verification/validation, what to put in L2 and L3?" LOCALIZED_STYLE_REF="styles.important" ID="ID_647365840" MAX_WIDTH="308.47826 pt" MIN_WIDTH="308.47826 pt"/>
<node TEXT="Check distribution of items between L1, L2 and L3." LOCALIZED_STYLE_REF="styles.important" ID="ID_454091944" MAX_WIDTH="315.26087 pt" MIN_WIDTH="315.26087 pt"/>
<node TEXT="Possibility for side-project that can validate proper functioning of Schnorr-signatures and other necessary implementations." STYLE_REF="Question" FOLDED="true" ID="ID_1839576956" MAX_WIDTH="353.18182 pt" MIN_WIDTH="353.18182 pt">
<icon BUILTIN="button_ok"/>
<node TEXT="nostrkey (started out) as testvehicle for p256k1-conversion, Schnorr-signatures, code-structure, protocol-extensions, etc." STYLE_REF="Details" ID="ID_1327115455" LINK="https://codeberg.org/walletkey/nostrkey"/>
</node>
<node TEXT="[dev] minimal-functional" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_497327886" MAX_WIDTH="201.65217 pt" MIN_WIDTH="201.65217 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Minimal functionality to have walleTKey perform wallet functions.
    </p>
  </body>
</html></richcontent>
<node TEXT="implement &quot;HWI&quot; / or &quot;Bitcoin Core external signer&quot;" FOLDED="true" ID="ID_1217857567">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Bitcoin Core &quot;external signer&quot; support sufficient. (HWI not yet necessary as fallback.)
    </p>
  </body>
</html></richcontent>
<node TEXT="implement Signer-role" ID="ID_1116377382">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="implement additional roles as necessary&#xa;(Updater-role, ...)" ID="ID_1914803729">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="FIXME risk of complex updates?" LOCALIZED_STYLE_REF="styles.important" ID="ID_1897600710"/>
<node TEXT="enumerate devices" ID="ID_301464021">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="prefer Bitcoin Core &quot;external Signer&quot; API" ID="ID_1285265165">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="fallback to HWI" STYLE_REF="Question" ID="ID_1362012238"/>
<node TEXT="optional interface for hierarchical wallets" ID="ID_662209998">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="legacy (before segwit, taproot) transactions/Schnorr-signatures support" ID="ID_64451465" MAX_WIDTH="295.90909 pt" MIN_WIDTH="295.90909 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Progress of implementation is not a concern for this item.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="walleTKey-PSBT-custom" FOLDED="true" ID="ID_359904296">
<icon BUILTIN="button_ok"/>
<node TEXT="Interfacing client with walleTKey, transferring the PSBT data to walleTKey as necessary, in distinct requests or for streamed processing." STYLE_REF="Details" ID="ID_1207287358" MAX_WIDTH="359.73913 pt" MIN_WIDTH="359.73913 pt"/>
<node TEXT="PSBT parsing/reading" ID="ID_271694629">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Use of &lt;github.com/btcsuite/btcd/btcutil/psbt&gt; primarily, pass on raw transaction to walleTKey.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="interpret/process (limited selection of) op-codes" ID="ID_999561215">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Not needed directly. Primarily handling predefined standard transactions, therefore preferring byte-exact matching of script content.
    </p>
  </body>
</html></richcontent>
<node TEXT="Limited implementation to confirm proper understanding of scripting. Need to determine necessity for actual script processing on walleTKey. There are predefined formats and most Bitcoin clients are extremely strict or reject non-standard formats. It may be better, simpler and less error-prone to detect standard transaction scripts only." STYLE_REF="Details" ID="ID_1596743733" MAX_WIDTH="421.69565 pt" MIN_WIDTH="421.69565 pt"/>
</node>
<node TEXT="reasonable integrity-checking" ID="ID_1280956453">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Read and parse full raw transaction. Ensure we read everything. Ensure no unexpected content. Ensure sane length, var-int values don't violate length-bound. Able to inspect transaction and gather metadata. Ensure to properly process content and finally sign transaction.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="convert PSBT to custom format (multiple messages, responses)" ID="ID_1702533615" MAX_WIDTH="346.04347 pt" MIN_WIDTH="346.04347 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Limited to necessary for performing signing: load transaction, sign transaction
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="split PSBT in parts for limited capacity, consumable on walleTKey" ID="ID_234929649">
<icon BUILTIN="button_ok"/>
<node TEXT="Limited necessity. Usually, transactions are small enough. There will be per-purpose processing in that BIP32-derivations for marking outputs as &quot;ours&quot;, i.e. (hierarchical) keypair within our control. There may be other such independent, optional &quot;contributions&quot; of metadata for the transaction loaded in walleTKey." STYLE_REF="Details" ID="ID_497152574"/>
</node>
<node TEXT="expect handling multiple messages in predefined order to facilitate efficient processing" ID="ID_140164632">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="parts:" ID="ID_1548395176">
<node TEXT="previous transactions" ID="ID_1750976192"/>
<node TEXT="utxo" ID="ID_218550454"/>
<node TEXT="BIP-32 derived addresses" ID="ID_1701333502"/>
</node>
</node>
<node TEXT="perform (cryptographic) operations for signing" FOLDED="true" ID="ID_1806129343">
<icon BUILTIN="button_ok"/>
<node TEXT="transaction parsing/reading" ID="ID_1462384081">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="implement cryptographic operations" ID="ID_1258776636">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Note: PKH (legacy BIP-44 <span style="font-weight: bold;">44h purpose</span>) keys only.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="implement signing logic" ID="ID_471113252">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="use SIGHASH indicator" ID="ID_1891395458" MAX_WIDTH="358.56521 pt" MIN_WIDTH="358.56521 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Incomplete. Working for SIGHASH_ALL.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="use ANYONECANPAY flag indicator" ID="ID_1692074279" MAX_WIDTH="340.69565 pt" MIN_WIDTH="340.69565 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Untested. Present in logic.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Bitcoin transaction signing" FOLDED="true" ID="ID_579056797">
<icon BUILTIN="button_ok"/>
<node TEXT="implement HWI / or implement directly and avoid HWI" ID="ID_42042295">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Implemented Bitcoin Core &quot;external signer&quot; API only.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="splitting out information necessary for walleTKey" ID="ID_523134233">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Ability to produce raw transaction proved sufficient for basic case. Gathered BIP-32 derivation paths are used as input to the signing process, for deriving the correct hierarchical key-pair.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="transformation/constructing messages for walleTKey" ID="ID_1117270275">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      (1.) Send raw transaction with Blake2s checksum.
    </p>
    <p>
      (2.) Send signing-requests with signing-index, derivation-path for correct keypair.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="handling messages in walleTKey" ID="ID_249691426">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="(basic) verification of messages in walleTKey" ID="ID_1525779110">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="validation of (transaction) information in walleTKey" ID="ID_923104064">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Verified that raw transaction can be fully read, buffer completely read, sizes are manageable, values and lengths make sense, supported format.
    </p>
    <p>
      /!\ Except for scripts, in-out-amount-balance, due to missing information.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="perform cryptographic/bitcoin-operations" ID="ID_150872272">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="multiple message interactions during transaction loading/verification/signing/response" ID="ID_1685849722">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      This is possible but hardly needed. Loading wallets, therefore switching between wallets, is possible during transaction handling.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="return integrated results in PSBT via HWI" ID="ID_30848959">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="return results in reply" ID="ID_139433281">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="set up testnet" FOLDED="true" ID="ID_1390254839" MAX_WIDTH="88.09091 pt" MIN_WIDTH="88.09091 pt">
<icon BUILTIN="button_ok"/>
<node TEXT="how it works" ID="ID_1093102836" MAX_WIDTH="83.18182 pt" MIN_WIDTH="83.18182 pt">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="what/how to change in walleTKey to adapt to other (test)nets" ID="ID_1189034653" MAX_WIDTH="236.21739 pt" MIN_WIDTH="236.21739 pt">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="support testnet in walleTKey-firmware" ID="ID_1047519844">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Only through derivation-paths that contain <span style="font-weight: bold;">BIP-44 1h</span><span style="font-weight: normal;">&#xa0;&#xa0;coin-types, so implicit support through proper input.</span>
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="NOT expected to have complete supporting/understanding/parser for all transactions" ID="ID_1285834506" MAX_WIDTH="452.47826 pt" MIN_WIDTH="452.47826 pt">
<icon BUILTIN="emoji-1F6E0"/>
<font ITALIC="true"/>
</node>
</node>
<node TEXT="[dev] L1-complete" LOCALIZED_STYLE_REF="styles.topic" ID="ID_847195825" MAX_WIDTH="153.52174 pt" MIN_WIDTH="153.52174 pt">
<icon BUILTIN="hourglass"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Possibility of physical/remote data-exfiltration. (data-at-rest)
    </p>
  </body>
</html></richcontent>
<node TEXT="Strictly speaking, Evil Maid is not completely covered, because that scenario also allows installing trojans, corrupt bootloader, install rootkits, etc." LOCALIZED_STYLE_REF="styles.important" ID="ID_190030090" MAX_WIDTH="497.21739 pt" MIN_WIDTH="497.21739 pt"/>
<node TEXT="Check firmware-ROM-checksums for known/unknown indicator (i.e. known tillitis firmware or unknown blob?)" STYLE_REF="Question" ID="ID_383373851" MAX_WIDTH="454.17391 pt" MIN_WIDTH="454.17391 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Would allow for limited (backwards-incompatible) variation in API if necessary. Support would be in Go-client, so we could even allow manually mapping checksum to API-identifier.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Do we need to verify any part of previous transactions? Or can we rely on the fact that bad input data would only result in an unusable signature anyways?" STYLE_REF="Question" ID="ID_1676046462" MAX_WIDTH="453.3913 pt" MIN_WIDTH="453.3913 pt"/>
<node TEXT="proper procedures for deriving hierarchical keys" ID="ID_1618582100">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="BIP-39 deterministic security words for wallet seed" ID="ID_835021128" MAX_WIDTH="302.21739 pt" MIN_WIDTH="302.21739 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Restoring from seed words. (Not yet generating seed words.)
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="&quot;detect&quot; support for wallet-types on import" FOLDED="true" ID="ID_219297840" MAX_WIDTH="251.47826 pt" MIN_WIDTH="251.47826 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Already happens through descriptors produced by walleTKey firmware.
    </p>
  </body>
</html></richcontent>
<node TEXT="BIP-32 hierarchical accounts" ID="ID_1390708998">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="BIP-43 purpose field for wallet" ID="ID_307074579">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="BIP-44 predefined account types support (only some types implemented)" ID="ID_1676632921">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Generate descriptors based on derived account and coin-types + internal/public path identifiers" ID="ID_518043613">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="CMD_WALLETINFO returning types of supported payment mechanisms (e.g. P2PKH, P2SH, etc.)" ID="ID_365278524">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="Generate descriptors based on wallet-info." ID="ID_1280369388">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node TEXT="use of KDF for identity-derivative stored with opaque sealed-wallet blob" FOLDED="true" ID="ID_1194470815" MAX_WIDTH="292.04348 pt" MIN_WIDTH="292.04348 pt">
<icon BUILTIN="button_ok"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hardened value derived from walleTKey identity.
    </p>
  </body>
</html></richcontent>
<node LOCALIZED_STYLE_REF="default" ID="ID_1525389564" MAX_WIDTH="396.65217 pt" MIN_WIDTH="396.65217 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">Goal</span>: the &quot;sealed wallet&quot; file must NOT be useful to acquire/derive the identity public-key, or make it trivial to brute-force the identity public-key, or provide hints as to when the right identity public-key is discovered.
    </p>
    <p>
      Conversely, need not offer any protection if TKey loaded with correct walleTKey-firmware and passphrase is available.
    </p>
  </body>
</html>
</richcontent>
</node>
<node LOCALIZED_STYLE_REF="default" ID="ID_1479659288" MAX_WIDTH="396.13043 pt" MIN_WIDTH="396.13043 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">Note</span>: this assumes possession of the &quot;sealed wallet&quot; file, hence must provide protection for all cases when the corresponding TKey is not in possession or the passphrase for walleTKey-firmware program is not known.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Note: check current design. Identity-derivative might be problematic as it functions as a inexpensive test for identifying the right walleTKey-instance. (Counteracts the &quot;hardening ID&quot; already employed.)" STYLE_REF="Details" ID="ID_614607326" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
</node>
<node TEXT="derive hierarchical (hardened) wallets as function of wallet" ID="ID_172429826" MAX_WIDTH="315.26087 pt" MIN_WIDTH="315.26087 pt">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="loading different wallets while handling a transaction" ID="ID_1804200344" MAX_WIDTH="302.21739 pt" MIN_WIDTH="302.21739 pt">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="implement other HWI-roles as necessary" ID="ID_281338874">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="extensive support for PSBT, i.e. error on known unsupported/irrelevant/illegal information" ID="ID_570340501" MAX_WIDTH="290.08695 pt" MIN_WIDTH="290.08695 pt">
<icon BUILTIN="hourglass"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      On client-software. WalleTKey only checks raw transaction reading/processing.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="validation of previous transactions (input txs)" ID="ID_870107532">
<icon BUILTIN="hourglass"/>
<node TEXT="Does it make sense to fully process the previous-out-transactions? (Not actually in scope when concerning trustworthiness of client.)" STYLE_REF="Question" FOLDED="true" ID="ID_929642370" MAX_WIDTH="335.21739 pt" MIN_WIDTH="335.21739 pt">
<node TEXT=" If we conclude they&apos;re false, the signature would be invalid anyways." ID="ID_1530495554" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
<node TEXT="We cannot verify whether the utxo was already spent. (That would take the whole blockchain, therefore we rely on honest input from the client regardless.)" ID="ID_1377476958" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
<node TEXT="The signature we currently produce is based on a predefined standard format that is expected anyways." ID="ID_194354228" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
<node ID="ID_518490749" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold; font-style: italic;">TODO</span>&#xa0;We can already do qualification of ownership of claimed utxo keys, through BIP32-derivation.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_629182573" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold; font-style: italic;">TODO</span>&#xa0;We could feed utxo in for verification in basic sense, and confirm standard script.
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Transaction could potentially take up significant memory during processing, would need to process txid, however only pubkeyscript is really necessary." ID="ID_1829859269" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
<node TEXT="Even if fake/false/manipulated data is provided for pubkeyscript, it will only result in a incorrect signature therefore no clearance of the utxo-bitcoins." ID="ID_396273747" MAX_WIDTH="365.73913 pt" MIN_WIDTH="365.73913 pt"/>
</node>
</node>
<node TEXT="consider if current walletctl approach is compatible with interop with Bitcoin Core (maybe load firmware first, then allow Bitcoin Core interop?)" ID="ID_823577823" MAX_WIDTH="390.7826 pt" MIN_WIDTH="390.7826 pt">
<icon BUILTIN="hourglass"/>
<node TEXT="Previous approach performed all actions in call from Bitcoin Core, but multiple calls are required, therefore multiple full initializations which is rather slow." STYLE_REF="Details" ID="ID_922244672" MAX_WIDTH="327.65217 pt" MIN_WIDTH="327.65217 pt"/>
<node TEXT="Currently, changed approach to include an &quot;agent&quot; program to run to provide some basic working information. This is now used for enumerating wallets, i.e. no longer requires loading wallet itself." STYLE_REF="Details" ID="ID_516163058" MAX_WIDTH="327.65217 pt" MIN_WIDTH="327.65217 pt"/>
</node>
<node TEXT="complete legacy (before segwit, taproot) Bitcoin transaction-types support" ID="ID_1056625060" MAX_WIDTH="396.91304 pt" MIN_WIDTH="396.91304 pt">
<icon BUILTIN="hourglass"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Within reason, in case of weird unexpected exotic edge cases, e.g. don't focus on multi-sig yet.
    </p>
  </body>
</html></richcontent>
<node TEXT="P2PK" ID="ID_1938619589">
<icon BUILTIN="button_cancel"/>
</node>
<node TEXT="P2PKH" ID="ID_316352753">
<icon BUILTIN="button_ok"/>
</node>
<node TEXT="P2SH (redeemscript)" ID="ID_1313189812">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="P2SH-P2WKH (witness-script via redeemscript)" ID="ID_347338430">
<icon BUILTIN="help"/>
</node>
</node>
<node TEXT="error-handling for (detected) corruption" ID="ID_88504568">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="support/implement necessary bitcoin scripting" FOLDED="true" ID="ID_516189398">
<icon BUILTIN="unchecked"/>
<node TEXT="Accept predefined formats only?" STYLE_REF="Question" ID="ID_1887782825"/>
</node>
<node TEXT="extensive testing/verification to ensure proper Bitcoin transactions" ID="ID_288054371" MAX_WIDTH="359.08695 pt" MIN_WIDTH="359.08695 pt">
<icon BUILTIN="unchecked"/>
</node>
<node TEXT="support for multiple wallet types(?)" STYLE_REF="Question" ID="ID_650735095"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Partially determined by BIP-32, BIP-44 support.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Open issue: should we include a few predefined uniformly-random bytes in the hardened context to ensure uniqueness per sealed-wallet? (low urgency, easy to implement)" STYLE_REF="Question" ID="ID_1863747777" MAX_WIDTH="446.73913 pt" MIN_WIDTH="446.73913 pt"/>
<node TEXT="Scoping: still assuming reliable, trustworthy client, so we can accept redeemscript (or lack thereof) when performing signing operations." STYLE_REF="Question" ID="ID_459076395" MAX_WIDTH="358.95652 pt" MIN_WIDTH="358.95652 pt">
<icon BUILTIN="emoji-1F6E0"/>
</node>
<node TEXT="Generate private key in walleTKey would require generating seed-words or sending seed to desktop? (Trust is always required, so might as well send seed itself back and let client generate seed-words.)" STYLE_REF="Question" ID="ID_113262890" MAX_WIDTH="358.95652 pt" MIN_WIDTH="358.95652 pt"/>
<node TEXT="verification of HWI messages" ID="ID_1184104614">
<icon BUILTIN="button_cancel"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Bitcoin Core &quot;external signer&quot; interface is sufficient to cover primary support for integration. HWI not necessary, will be desirable for broader adoption.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="[dev] finalized design" LOCALIZED_STYLE_REF="styles.topic" ID="ID_248582786" MAX_WIDTH="179.73913 pt" MIN_WIDTH="179.73913 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Use insight from early, minimalistic work for finalizing design input
    </p>
  </body>
</html></richcontent>
<node TEXT="consider moving design to later, because threat-levels not yet established at this point. Is important for some key decisions in secure-migration, L3/L4 mitigations, ..." LOCALIZED_STYLE_REF="styles.important" ID="ID_989004332"/>
<node TEXT="NOT secure-migration" ID="ID_36825858"/>
<node TEXT="complete verifier (features, expectations)" ID="ID_1791787909"/>
<node TEXT="define authz/confirm. codes" ID="ID_234017375"/>
<node TEXT="identify verifier use-cases" ID="ID_1802124337"/>
<node TEXT="finalize sealed-wallet" ID="ID_487374300">
<node TEXT="final design for hardening component" ID="ID_719450858"/>
</node>
<node TEXT="finalize admin-/wallet-PINs" ID="ID_949248113"/>
<node TEXT="finalize verifyfw" ID="ID_972886589"/>
<node TEXT="finalize messages/interop best-practices" ID="ID_1779319364"/>
<node TEXT="finalize messages/actions (firmware)" ID="ID_1694142633"/>
<node TEXT="authn-frames added to secure session" ID="ID_1066976371">
<node TEXT="Slightly tricky because it&apos;s easier to process frame-by-frame given low expectations on memory." ID="ID_712324788"/>
</node>
</node>
<node TEXT="[dev] L2-complete" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1340981096"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Presence of a passive attacker
    </p>
  </body>
</html></richcontent>
<node TEXT="What risks are there in exposing the chain code to the client application in presence of passive attacker?" STYLE_REF="Question" ID="ID_265815032"/>
<node TEXT="simple AKE" ID="ID_708335480">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="secure session" ID="ID_564308748">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="(basic) security of RNG" ID="ID_1039771547" MAX_WIDTH="194.21739 pt" MIN_WIDTH="194.21739 pt">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="strict-by-default checking/processing" ID="ID_99863823" MAX_WIDTH="194.21739 pt" MIN_WIDTH="194.21739 pt">
<icon BUILTIN="hourglass"/>
</node>
<node TEXT="static confirm/authz codes (up to L2)" ID="ID_243270545" MAX_WIDTH="191.47826 pt" MIN_WIDTH="191.47826 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Validate authz for wallet and admin operations.
    </p>
  </body>
</html></richcontent>
<node TEXT="use of static confirmation codes (wallet-/admin-operations)" ID="ID_1102722536" MAX_WIDTH="302.21739 pt" MIN_WIDTH="302.21739 pt"/>
<node TEXT="add/adapt walleTKey messages to incorporate confirmation codes" ID="ID_1881966716" MAX_WIDTH="256.95652 pt" MIN_WIDTH="256.95652 pt"/>
<node TEXT="adapt `verifyfw` to work with (random) PIN" ID="ID_932563596" MAX_WIDTH="256.95652 pt" MIN_WIDTH="256.95652 pt"/>
<node TEXT="adapt walletctl to include PINs in sealed-wallet management" ID="ID_297056239" MAX_WIDTH="256.95652 pt" MIN_WIDTH="256.95652 pt"/>
<node TEXT="admin-PIN" ID="ID_1938804705"/>
<node TEXT="wallet-PIN" ID="ID_869584816"/>
<node TEXT="restrict number of attempts" ID="ID_1596417404">
<node TEXT="A hard bound cannot be enforced, but we can block the firmware after a predefined number of attempts, such that the process has to be restarted and manual, physical intervention is required." STYLE_REF="Details" ID="ID_631043636"/>
</node>
</node>
<node TEXT="validation of walleTKey transaction-data" ID="ID_371153309" MAX_WIDTH="208.82608 pt" MIN_WIDTH="208.82608 pt">
<node TEXT="verify derived keys" ID="ID_1645656309"/>
<node TEXT="validate previous transactions" ID="ID_1782571212"/>
<node TEXT="extract all relevant (uncheckable) values for verification/confirmation by user" ID="ID_1807815783"/>
<node TEXT="receive confirmation-key (based on some PIN and significant data)" ID="ID_1383711662"/>
</node>
<node TEXT="complete support for legacy (pre-taproot)&#xa;transaction-types/signature-types?" LOCALIZED_STYLE_REF="default" ID="ID_931771281" MAX_WIDTH="218.47826 pt" MIN_WIDTH="218.47826 pt"/>
<node TEXT="add context to identity-signature in AKE" ID="ID_321033338">
<node TEXT="How do nicely if only 1 param in blake2s?" STYLE_REF="Question" ID="ID_1264808853"/>
</node>
<node TEXT="mitigations for client-side timing-attacks" ID="ID_1330486374"/>
<node TEXT="ensure results are useless out-of-context" ID="ID_532841373"/>
<node TEXT="remove `identity` from `walletctl agent` mode because it exposes the identity unnecessarily?" STYLE_REF="Question" ID="ID_183384334"/>
</node>
<node TEXT="[alpha] finalized threat-model" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1271915338"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Finish threat-model.
    </p>
  </body>
</html></richcontent>
<node TEXT="establish levels for active attackers (L3+L4)" ID="ID_1241879805"/>
<node TEXT="refine threat-levels (content)" ID="ID_1173098097"/>
<node TEXT="combine risks, solutions, levels" ID="ID_1433770478"/>
<node TEXT="create list of problematic combinations of risks" ID="ID_1844024106" MAX_WIDTH="243.13043 pt" MIN_WIDTH="243.13043 pt"/>
<node TEXT="update design as necessary to tackle (critical) issues" ID="ID_530881748" MAX_WIDTH="269.86956 pt" MIN_WIDTH="269.86956 pt"/>
<node TEXT="finalize threat-model" ID="ID_1054310467"/>
</node>
<node TEXT="[alpha] verifier" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1294185291" MAX_WIDTH="198.26087 pt" MIN_WIDTH="198.26087 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Verifier receives/authenticates information originating from walleTKey, authorizes approved actions
    </p>
  </body>
</html></richcontent>
<node TEXT="Minimal, text-based Android app. The core function is to validate actions taken by walleTKey and clients to detect suspicious/malicious." STYLE_REF="Details" ID="ID_360258165"/>
<node TEXT="Can we query the origin if shared information such that we can be sure that the origin is the QR-scanner?" STYLE_REF="Question" ID="ID_131423244"/>
<node TEXT="Can we set a default origin and warn if the origin changed?" STYLE_REF="Question" ID="ID_1659594573"/>
<node TEXT="Which mechanism for authz/confirm codes?" STYLE_REF="Question" ID="ID_216363729">
<node TEXT="HTOP?" ID="ID_1492705667"/>
<node TEXT="truncated hash/HMAC?" ID="ID_1150173206"/>
<node TEXT="other, specialized mechanisms?" ID="ID_718867999"/>
</node>
<node TEXT="What actions can be performed within TEE?keystore/strongbox? (otherwise load sensitive material in memory only)" STYLE_REF="Question" ID="ID_626378745"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      signature-validation, hashing/HMAC, ...
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_1179701148"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;verifier: no connectivity
    </p>
  </body>
</html>
</richcontent>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      reduce opportunities for corruption/manipulation, no privacy-concerns
    </p>
  </body>
</html></richcontent>
</node>
<node ID="ID_1192885029" MAX_WIDTH="309.13637 pt" MIN_WIDTH="309.13637 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;verifier: avoid (unnecessarily) exposing public key
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1197291141"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;toggle for debug-output (transparency)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1046237054"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;assumptions for reduced risk
    </p>
  </body>
</html>
</richcontent>
<node TEXT="select, minimal features" ID="ID_1109053811"/>
<node TEXT="reactive by nature" ID="ID_571936939" MAX_WIDTH="244.30435 pt" MIN_WIDTH="244.30435 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Less risk of corruption if only reacting to events/requests, reduce capability to coordinate.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="verifiers/wallets" ID="ID_63077740">
<node TEXT="load a wallet: public key" ID="ID_1274788813"/>
<node TEXT="select wallet / autodetect wallet" ID="ID_986932368"/>
<node TEXT="securely store (seal) public key" ID="ID_1909899911"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      TEE/keystore/strongbox
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="generate (dyn.) authz/confirm codes" ID="ID_402048063">
<node TEXT="initialize for authz/confirm" ID="ID_186841020"/>
<node TEXT="securely store necessary secret" ID="ID_932197617"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      TEE/keystore/strongbox
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="receive (QR-scan) authz-/confirm-request" ID="ID_1647895054"/>
<node TEXT="accept input values" ID="ID_521528762"/>
<node TEXT="accept PIN-input" ID="ID_884376536"/>
<node TEXT="restricted attempts" ID="ID_1715605220"/>
</node>
<node TEXT="walleTKey-firmware&#xa;authz-/confirm-codes" ID="ID_24989315">
<node TEXT="register verifier in sealed-wallet" ID="ID_608807351"/>
<node TEXT="support dynamic authz-/confirm-codes" ID="ID_46373578"/>
<node ID="ID_1555775850" MAX_WIDTH="298.30434 pt" MIN_WIDTH="298.30434 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;authz/confirm codes not reusable (see solutions)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_845845969" MAX_WIDTH="220.17391 pt" MIN_WIDTH="220.17391 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;authz/confirm codes incorporate admin-/wallet-PIN (see solutions)
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_384734877" MAX_WIDTH="213.3913 pt" MIN_WIDTH="213.3913 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">security</span>&#xa0;authz/confirm codes include verifier-registrated secret (see solutions)
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="report validated content" ID="ID_1801861071">
<node TEXT="receive (QR-scan) content for validation" ID="ID_366527024"/>
<node TEXT="status-indicator for validation status" ID="ID_200192282"/>
<node TEXT="highlight/emphasize significant values" ID="ID_983774412"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      e.g. bright-red for invalid information
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="highlight/emphasize failures" ID="ID_1341009017"/>
<node TEXT="signature validation" ID="ID_676399359"/>
<node TEXT="display validated content from scan" ID="ID_618127384"/>
</node>
<node TEXT="simple 2-byte message-ID for all interactions" ID="ID_1389434820" MAX_WIDTH="237.13637 pt" MIN_WIDTH="237.13637 pt">
<node TEXT="Possibly use a bit from 2nd byte for some other indicator, e.g. expecting follow-up message." ID="ID_512926865"/>
<node TEXT="Enough to make message-IDs throw-away, i.e. easy to adapt to firmware- and client-advancements." ID="ID_1204476029"/>
</node>
<node TEXT="optional" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_1520468010">
<node TEXT="(conveniently) support multiple wallets" ID="ID_1215489567"/>
<node TEXT="signature validation within TEE" ID="ID_110657412"/>
</node>
</node>
<node TEXT="[alpha] L3-complete" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1724763570"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Presence of an active attacker
    </p>
  </body>
</html></richcontent>
<node TEXT="First look at possibilities of differential attacks using constructed (faulty) inputs?" STYLE_REF="Question" ID="ID_849335250"/>
<node TEXT="What risks are there in exposing the chain code to client in presence of an active attacker?" STYLE_REF="Question" ID="ID_1490656382"/>
<node TEXT="authentication-frames for secure session" ID="ID_1019851509"/>
<node TEXT="dynamic confirm/authz codes" ID="ID_1157638237"/>
<node TEXT="use verifier for authz-/confirm-codes" ID="ID_417672420"/>
<node TEXT="return feedback for verifier" ID="ID_219026426">
<node TEXT="produce critical transaction data in report" ID="ID_1804128738"/>
<node TEXT="leave out data that can be verified internally" ID="ID_778140528"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      e.g. UTXO that can be verified by walleTKey (derived address, so known to be in walleTKey's control)
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="sign critical transaction" ID="ID_1592944773"/>
</node>
<node TEXT="confirmation by verifier" ID="ID_1005769549"/>
<node TEXT="random touch-patterns for physical confirmation for critical (admin) procedures" ID="ID_962911301"/>
<node TEXT="verification of walleTKey transaction data" ID="ID_525572055">
<node TEXT="verify inputs, outputs, previous txs" ID="ID_142163923"/>
<node TEXT="verify internal consistency of data, references, input-structure" ID="ID_547200462"/>
</node>
<node TEXT="user to verify bitcoin transaction data" ID="ID_1938584289"/>
<node TEXT="verifying PSBT data" ID="ID_1181606518">
<node TEXT="How can we be sure that we can trust the PSBT data?" STYLE_REF="Question" ID="ID_1931811458"/>
<node TEXT="Need to correlate the PSBT data to the raw transaction to sign?" STYLE_REF="Question" ID="ID_1473506849"/>
</node>
<node TEXT="modify P2SH sign-transaction request handling to send full redeem-script for verification and have firmware calculate its hash" ID="ID_1942687667">
<node TEXT="How exactly are P2SH payments requested, do we have the script? Do we have to trust the sender? Can we do anything more robust than comparing 20-byte hash value?" STYLE_REF="Question" ID="ID_710079357"/>
</node>
<node TEXT="optional" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_1451927209">
<node TEXT="include Bitcoin Taproot support in L3, because newer, more robust implementation? Is this feasible?" STYLE_REF="Question" ID="ID_1321274744"/>
<node TEXT="advanced static analysis tooling (PVS Studio?, etc.) (otherwise L4-complete)" ID="ID_944270226"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Use more/advanced tooling for static analysis to gain further insight into quality of code-base.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Bitcoin Taproot support?" ID="ID_287962282"/>
</node>
</node>
<node TEXT="[beta] release-process" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_516003508" MAX_WIDTH="153.13043 pt" MIN_WIDTH="153.13043 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Release of firmware to public, possibly as beta/RC.
    </p>
  </body>
</html></richcontent>
<node TEXT="NEW: experiment with Guix to create a reproducible, deterministic build-environment, store as container-image, use container of Guix build-environment for &quot;portable&quot;, isolated, scriptable building process." LOCALIZED_STYLE_REF="styles.important" ID="ID_442403512" MAX_WIDTH="446.60869 pt" MIN_WIDTH="446.60869 pt"/>
<node TEXT="complete release-procedure" ID="ID_393928067"/>
<node TEXT="complete checklist" ID="ID_847712457"/>
<node TEXT="methodical review of security-requirements and others concerns" LOCALIZED_STYLE_REF="default" ID="ID_825928773" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="scripted/automated build-environment" LOCALIZED_STYLE_REF="default" ID="ID_190370083" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Docker-image, guix, nix, ...
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="select forge suitable for independent build-environment, as second environment next to local (dev-)machine" LOCALIZED_STYLE_REF="default" ID="ID_1691278191" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="independent builds in forge&apos;s build environment" LOCALIZED_STYLE_REF="default" ID="ID_570152416" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="repeat builds reproducible, part of build-process" LOCALIZED_STYLE_REF="default" ID="ID_767228379" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="validate available signatures as part of (release-)build" LOCALIZED_STYLE_REF="default" ID="ID_1673880718" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="only allow release-build successful if at least one signature validates" LOCALIZED_STYLE_REF="default" ID="ID_1411153701" MAX_WIDTH="352.30434 pt" MIN_WIDTH="352.30434 pt"/>
<node TEXT="choose/define target architectures for client-applications" LOCALIZED_STYLE_REF="default" ID="ID_1773607073" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="packaging for firmware-binaries" LOCALIZED_STYLE_REF="default" ID="ID_756064213" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="packaging for wallet-clients + verifyfw" LOCALIZED_STYLE_REF="default" ID="ID_342566497" MAX_WIDTH="333.52174 pt" MIN_WIDTH="333.52174 pt"/>
<node TEXT="build-environment" ID="ID_353115036">
<node TEXT="How to ensure predictable, deterministic, reproducible build-environment itself?" STYLE_REF="Question" ID="ID_751050128"/>
<node TEXT="How to make build-environment (conveniently) available for reproducible builds of walleTKey?" STYLE_REF="Question" ID="ID_1671454761"/>
</node>
<node TEXT="Can we use Guix to build a deterministic, reproducible build-environment and containers for making it easily accessible, scriptable for CI/CD (build) operations?" STYLE_REF="Question" ID="ID_468623132"/>
</node>
<node TEXT="[beta] protocol proofs" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1802243561"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Prove sealed-wallet format.
    </p>
  </body>
</html></richcontent>
<node TEXT="learn another proof-system" ID="ID_122639381">
<node TEXT="learn the system (Tamarin/Proverif)" ID="ID_1243359669"/>
<node TEXT="find suitable reference material" ID="ID_422972134"/>
<node TEXT="set up the basic symbolic types necessary" ID="ID_1460123650"/>
<node TEXT="set up various attackers" ID="ID_1031531272"/>
<node TEXT="set up basic queries/verification rules" ID="ID_1691666873"/>
</node>
<node TEXT="sealed-wallet exchange/interaction proof" ID="ID_947558445" MAX_WIDTH="221.73913 pt" MIN_WIDTH="221.73913 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Proof of security of communication and use of sealed-wallet (opaque blob).
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="AKE, recreate passive-attacker proof" ID="ID_1010629616"/>
<node TEXT="AKE, recreate active-attacker proof" ID="ID_568657818"/>
</node>
<node TEXT="[beta] L4-complete" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_333189527" MAX_WIDTH="208.43478 pt" MIN_WIDTH="208.43478 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Security, when everything goes.
    </p>
    <p>
      (brute-forcing, corruption, etc.)
    </p>
  </body>
</html></richcontent>
<node TEXT="TODO: the goal is to aim for mitigations where reasonable: detection if possible, early abort/failure upon suspicious circumstances/messages/behaviors" LOCALIZED_STYLE_REF="styles.important" ID="ID_1868608194" MAX_WIDTH="335.21739 pt" MIN_WIDTH="335.21739 pt"/>
<node TEXT="Complete (missing) critical features?" STYLE_REF="Question" ID="ID_1923376686"/>
<node ID="ID_1376522139" MAX_WIDTH="321 pt" MIN_WIDTH="321 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      complete advanced static-analysis (<span style="font-style: italic;">assuming tooling available</span>)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="(recheck, complete) hardening against timing-attacks (complete earlier effort)" ID="ID_72686718" MAX_WIDTH="295.95652 pt" MIN_WIDTH="295.95652 pt"/>
<node TEXT="(recheck, complete) hardening against brute-force attacks" ID="ID_1505026615" MAX_WIDTH="302.21739 pt" MIN_WIDTH="302.21739 pt"/>
<node TEXT="(recheck, complete) hardening using message(-processing) corruption (fuzzing)" ID="ID_672245764" MAX_WIDTH="219.78261 pt" MIN_WIDTH="219.78261 pt"/>
<node TEXT="(recheck, complete) ensure context for signing operations" ID="ID_507366875" MAX_WIDTH="302.21739 pt" MIN_WIDTH="302.21739 pt"/>
<node TEXT="(recheck, complete) ensure results useless out-of-context" ID="ID_842962763" MAX_WIDTH="296.47826 pt" MIN_WIDTH="296.47826 pt"/>
<node TEXT="recheck proper variable clearing" ID="ID_915265625"/>
<node TEXT="(CDI-clearing if supported)" ID="ID_1356343765"/>
<node TEXT="strict transitions (fail on suspicious behavior)" ID="ID_186658522"/>
<node TEXT="restrict attempts (abort on violations)" ID="ID_1133743458"/>
<node TEXT="optional" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_175063923">
<node TEXT="complete/harden Bitcoin Taproot support?" ID="ID_1523954128"/>
</node>
</node>
<node TEXT="[RC] secure-migration protocol/procedure" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1330098426" MAX_WIDTH="208.43478 pt" MIN_WIDTH="208.43478 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Protocol and procedure for secure migration between walleTKeys.
    </p>
  </body>
</html></richcontent>
<node TEXT="secure migration-protocol proof" STYLE_REF="Question" ID="ID_1861311467">
<node TEXT="learn alternative proof-system" ID="ID_1051892917"/>
<node TEXT="prove migration-protocol (DH-escque key-establishment of symmetric key for encryption)" ID="ID_1513962810"/>
</node>
<node TEXT="define the protocol" ID="ID_1732760533">
<node STYLE_REF="Details" ID="ID_1714566868"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">essentially</span>: key-exchange, symm encryption, data-transfer, symm decryption, seal wallet, preserve existing PINs, mix PINs with symmetric key to ensure restoring is only possible if PINs are known, one-time key, key-agreement
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="secure migration-protocol" ID="ID_1887224914">
<node TEXT="implement the protocol in firmware" ID="ID_1925367281"/>
<node TEXT="implement the &quot;support-role&quot; in the wallet-cli" ID="ID_1111496623"/>
<node TEXT="TODO" LOCALIZED_STYLE_REF="styles.important" ID="ID_1819960096"/>
</node>
<node TEXT="data-format containing (key-)values of migration" ID="ID_1221723206"/>
</node>
<node TEXT="[RC] extensive testing, verification, audit, completion" LOCALIZED_STYLE_REF="styles.topic" FOLDED="true" ID="ID_1037728950" MAX_WIDTH="208.43478 pt" MIN_WIDTH="208.43478 pt">
<node TEXT="checking, verification, hardening of Verifier-app (assuming edge-cases and peculiarities that make app vulnerable by default)" ID="ID_196393055"/>
<node TEXT="T.B.D." LOCALIZED_STYLE_REF="styles.important" ID="ID_850130214"/>
</node>
<node TEXT="See `design` for notes regarding design and implementation." LOCALIZED_STYLE_REF="default" ID="ID_1651618418" MAX_WIDTH="331.63637 pt" MIN_WIDTH="331.63637 pt">
<font BOLD="false" ITALIC="true"/>
</node>
</node>
<node TEXT="support" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" POSITION="bottom_or_right" ID="ID_1532905092">
<edge COLOR="#ff0000"/>
<node TEXT="[beta] support/bootstrapping protocol proofs (optional)" ID="ID_528868146" MAX_WIDTH="290.60869 pt" MIN_WIDTH="290.60869 pt"/>
<node TEXT="[RC] (security) audit" ID="ID_88649118"/>
<node TEXT="[RC] community-managed trust (reproducibility, community-signed blessed builds)" ID="ID_870068784" MAX_WIDTH="425.08695 pt" MIN_WIDTH="425.08695 pt"/>
<node TEXT="[RC] forge for build CI/CD environment for (independent) builds" ID="ID_1802445338" MAX_WIDTH="328.56521 pt" MIN_WIDTH="328.56521 pt"/>
<node TEXT="[RC] Insight into more peculiar edge-cases of Android app development, as we want the app to be secure, but there might be obscure situations that must be tackled. (Lack of knowledge/experience means this is a possibility.)" ID="ID_95337765" MAX_WIDTH="396.13043 pt" MIN_WIDTH="396.13043 pt"/>
<node TEXT="??? If possible, PVS Studio test-run (open-source license?) to perform static analysis on the code-base." ID="ID_1705342178" MAX_WIDTH="356.21739 pt" MIN_WIDTH="356.21739 pt"/>
</node>
<node TEXT="use-cases" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_1748824027" TEXT_ALIGN="RIGHT">
<edge COLOR="#7c0000"/>
<node TEXT="manage wallet seed" ID="ID_1618647446" TEXT_ALIGN="RIGHT"/>
<node TEXT="send bitcoin transaction" ID="ID_1310866500" TEXT_ALIGN="RIGHT"/>
<node TEXT="generate/derive bitcoin receive-addresses" ID="ID_565410427" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="goals" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_1257431467" VGAP_QUANTITY="2 pt" COMMON_HGAP_QUANTITY="14 pt" TEXT_ALIGN="RIGHT">
<edge COLOR="#7c0000"/>
<node TEXT="primarily a proof-of-concept" ID="ID_1527986103" MAX_WIDTH="147.3913 pt" MIN_WIDTH="147.3913 pt" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT"/>
<node TEXT="improvements over software-only wallets" ID="ID_1879093036" MAX_WIDTH="215.86956 pt" MIN_WIDTH="215.86956 pt" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT"/>
<node TEXT="exploration of alternative approach to hardware wallet" ID="ID_1214906804" MAX_WIDTH="287.08695 pt" MIN_WIDTH="287.08695 pt" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT"/>
<node TEXT="exploration of capabilities of TKey with &apos;wallet&apos; as ambitious goal" ID="ID_228004026" MAX_WIDTH="332.34782 pt" MIN_WIDTH="332.34782 pt" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT"/>
<node TEXT="general wallet-concept foundation" ID="ID_1967591134" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT" MAX_WIDTH="181.04348 pt" MIN_WIDTH="181.04348 pt"/>
<node TEXT="a secure, open wallet concept" ID="ID_780276657" TEXT_ALIGN="RIGHT" TEXT_WRITING_DIRECTION="LEFT_TO_RIGHT" MAX_WIDTH="206.47826 pt" MIN_WIDTH="206.47826 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      A concept with realistic, high expectations
    </p>
  </body>
</html></richcontent>
<node TEXT="transparency" ID="ID_1799684013" MAX_WIDTH="156 pt" MIN_WIDTH="156 pt" TEXT_ALIGN="RIGHT">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      open hardware, open software
    </p>
  </body>
</html></richcontent>
<node TEXT="issue is preprogrammed firmware by tillitis" LOCALIZED_STYLE_REF="styles.important" ID="ID_1743403621"/>
</node>
<node TEXT="anonymity" ID="ID_1767179995" MAX_WIDTH="66.26087 pt" MIN_WIDTH="66.26087 pt" TEXT_ALIGN="RIGHT">
<font BOLD="true"/>
<node TEXT="Connectivity through (Bitcoin Core or other) full-node/wallet software." ID="ID_1715505031" MAX_WIDTH="196.17391 pt" MIN_WIDTH="196.17391 pt" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="deniability" ID="ID_168373041" MAX_WIDTH="140.60869 pt" MIN_WIDTH="140.60869 pt" TEXT_ALIGN="RIGHT">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      &quot;sealed wallet&quot;-format contains content that is possibly uniformly-random
    </p>
  </body>
</html></richcontent>
<node TEXT="sealed-wallet" ID="ID_1099649170" TEXT_ALIGN="RIGHT" MAX_WIDTH="162.13043 pt" MIN_WIDTH="162.13043 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      it must not be easier to identify a sealed-wallet file than it is to load that sealed-wallet
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="confidentiality" ID="ID_958665656" MAX_WIDTH="81.91304 pt" MIN_WIDTH="81.91304 pt" TEXT_ALIGN="RIGHT">
<font BOLD="true"/>
<node TEXT="communication with walleTKey" ID="ID_1912003055"/>
<node TEXT="sealed-wallet contents" ID="ID_859995903"/>
</node>
<node TEXT="integrity" ID="ID_262658960" TEXT_ALIGN="RIGHT" MAX_WIDTH="81.91304 pt" MIN_WIDTH="81.91304 pt">
<font BOLD="true"/>
<node TEXT="TODO!" LOCALIZED_STYLE_REF="styles.important" ID="ID_1334135671"/>
<node TEXT="sealed-wallet file" ID="ID_70923105"/>
<node TEXT="device" ID="ID_1275729832"/>
<node TEXT="firmware-binary" ID="ID_1554854502"/>
</node>
<node TEXT="indistinguishability" ID="ID_1478045911" TEXT_ALIGN="RIGHT" MAX_WIDTH="114.52174 pt" MIN_WIDTH="114.52174 pt">
<font BOLD="true"/>
<node TEXT="Must not be able to distinguish &quot;sealed wallet&quot; from arbitrary bytes (except for size)." ID="ID_1559383713" TEXT_ALIGN="RIGHT" MAX_WIDTH="268.30434 pt" MIN_WIDTH="268.30434 pt"/>
<node TEXT="Must not be able to distinguish identity corresponding to sealed wallet from any other public key, given &quot;sealed wallet&quot; alone." ID="ID_368948710" MAX_WIDTH="335.47826 pt" MIN_WIDTH="335.47826 pt" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="standardization, interoperability" ID="ID_1380829727" MAX_WIDTH="216.52174 pt" MIN_WIDTH="216.52174 pt" TEXT_ALIGN="RIGHT">
<font BOLD="true"/>
<richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Bitcoin Core &quot;external signer&quot;, PSBT, HWI, ...
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="flexibility w/o security-tradeoff (safe)" ID="ID_1586995122" MAX_WIDTH="216.52174 pt" MIN_WIDTH="216.52174 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Arbitrary programs/firmware without risk of corruption.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="&quot;reasonable expectation&quot; that operating walleTKey on compromised system is not immediately problematic" ID="ID_530075038" MAX_WIDTH="218.86956 pt" MIN_WIDTH="218.86956 pt" TEXT_ALIGN="RIGHT">
<node TEXT="needs rewording, intention is to have walleTKey malfunction before it leaks/exposes secrets, such that there is reasonable expectation of discovering a corrupted system before it is critically broken" LOCALIZED_STYLE_REF="styles.important" ID="ID_1750754820" MAX_WIDTH="428.21739 pt" MIN_WIDTH="428.21739 pt"/>
</node>
<node TEXT="(limited) resistance to local attacks(?)" LOCALIZED_STYLE_REF="styles.important" ID="ID_1024468386" MAX_WIDTH="243.13043 pt" MIN_WIDTH="243.13043 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="define secure, e.g. rather have software crash to be abused ... define in a few of such goals to substantiate in which way it is secure" LOCALIZED_STYLE_REF="styles.important" ID="ID_1357252136" MAX_WIDTH="352.82608 pt" MIN_WIDTH="352.82608 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="solid, correct, sound, simple implementation" STYLE_REF="Question" ID="ID_790213497" MAX_WIDTH="352.82608 pt" MIN_WIDTH="352.82608 pt" TEXT_ALIGN="RIGHT"/>
</node>
</node>
<node TEXT="aspects" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_1516303293" TEXT_ALIGN="RIGHT">
<edge COLOR="#00007c"/>
<node TEXT="requirements" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_975479260">
<node ID="ID_1783970493" MAX_WIDTH="359.95828 pt" MIN_WIDTH="359.95828 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>strict,security</b>&nbsp;exit-strategy must be available, i.e. the wallet-secret must be extractable even from possibly buggy firmware to ensure funds are never lost.
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1367455440" MAX_WIDTH="359.95828 pt" MIN_WIDTH="359.95828 pt"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>ambition,security</b>&nbsp;post-quantum mitigations:&nbsp;reduce exposure of asymmetric cryptography where possible
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_619645671"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>strict,security</b>&nbsp;wallet (seed/secret) is protected
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_277194839"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>strict,security</b>&nbsp;secure handling of sensitive data
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_703252362"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      strict,security<span style="font-weight: normal;">&#xa0;ensure context to avoid misuse of signatures</span>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node ID="ID_523005677"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>ambition,security</b>&#xa0;can we offer <span style="font-style: italic;">deniability</span>&#xa0;&#xa0;for the sealed-wallet format? (I.e. all parts are uniformly random?)
    </p>
  </body>
</html>
</richcontent>
<node TEXT="deniability for sealed-wallet format" ID="ID_125186944"/>
<node TEXT="undeterminable purpose of TKey devices" ID="ID_223050622"/>
<node TEXT="identity can only be confirmed for correct combination: (device, firmware-binary, USS)" ID="ID_1822531808" MAX_WIDTH="230.73913 pt" MIN_WIDTH="230.73913 pt"/>
</node>
<node FOLDED="true" ID="ID_51103283"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>strict,security</b>&nbsp;secure environment for wallet
    </p>
  </body>
</html>
</richcontent>
<node TEXT="transaction signing" ID="ID_616995114"/>
<node TEXT="transaction verification (limited)" ID="ID_696014708"><richcontent TYPE="DETAILS">
<html>
  

  <head>

  </head>
  <body>
  </body>
</html></richcontent>
<node TEXT="The aim is to do basic sanity checking and ensure that information is not misrepresented. Full validation is impossible, given the very limited information present within walleTKey and available to walleTKey. This is not a problem, as long as we can either verify critical information or present it to user for confirmation." STYLE_REF="Details" ID="ID_1269515649"/>
</node>
</node>
<node ID="ID_714545287"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">strict,security</span>&#xa0;output must not be open to misuse
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="design" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_760449517">
<node TEXT="implement interface to software-wallet/(full)node" ID="ID_1441023088"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      interface with Bitcoin full node or software wallet
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="complicated touch patterns for confirmation" STYLE_REF="Question" ID="ID_1057149629"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      In case a pin is insecure/undesirable, ask for a complicated touch pattern for confirmation as physical presence.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="wallet-/admin-pin for confirmation" STYLE_REF="Question" ID="ID_1395822168"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      In case of extra security by requesting pin confirmation for sensitive operations.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="minimalistic firmware" ID="ID_586204954"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Minimal firmware that is still able to provide the security guarantees. (The rest is implemented in client or connected software.)
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="implementation" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_235934550">
<node ID="ID_1481002151"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>security</b>&nbsp;clear sensitive data after use
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1979978922"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>security</b>&nbsp;compute sensitive values on-the-fly
    </p>
  </body>
</html>
</richcontent>
</node>
<node ID="ID_1922142547"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>development</b>&nbsp;stack-only memory-model
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="operation" LOCALIZED_STYLE_REF="AutomaticLayout.level,2" FOLDED="true" ID="ID_1953229252">
<node TEXT="community-based trust (signatures)" ID="ID_1225072342"/>
<node TEXT="reproducible builds" ID="ID_1430222720"/>
<node TEXT="assist/motivate migration to newest/secured version?" STYLE_REF="Question" ID="ID_1353553725"/>
</node>
<node TEXT="(very) incomplete, see design documents" LOCALIZED_STYLE_REF="styles.important" ID="ID_1729447011"/>
</node>
<node TEXT="open issues" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_1670013795" TEXT_ALIGN="RIGHT">
<edge COLOR="#00007c"/>
<node TEXT="keyboard-snooping, communication-snooping considered L3?" STYLE_REF="Question" ID="ID_660036957" TEXT_ALIGN="RIGHT" MAX_WIDTH="314.73913 pt" MIN_WIDTH="314.73913 pt"/>
<node TEXT="input-/output-corruption of wallet-client for L3?" STYLE_REF="Question" ID="ID_173575914" TEXT_ALIGN="RIGHT"/>
<node TEXT="Harvest-now-decrypt-later attacks not yet included?" STYLE_REF="Question" ID="ID_197109321" TEXT_ALIGN="RIGHT"/>
<node TEXT="There is a remaining issue that admin- and wallet-pin entered on client, may be compromised at L2 and higher, which is quite soon. Is there a way to use admin- and wallet-pins in verifier only, such that these are at less risk?" STYLE_REF="Question" ID="ID_1934065216" MAX_WIDTH="520.42614 pt" MIN_WIDTH="520.42614 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="If pins used only at verifier, can we still adequately protect the wallet given attempts at stealing sealed-wallet or TKey? (HWI interface discourages entering PINs on client computers.)" STYLE_REF="Question" ID="ID_1037900597" MAX_WIDTH="521.17855 pt" MIN_WIDTH="521.17855 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="verifier: does the verifier need to actively block next execution step, or simply provide advice after validating input? A corrupt verifier risks blocking critical operations, but at best can give bad advice for non-blocking operations. Even if giving bad advice, with sufficient information displayed, discrepancies and other suspicious information may already alert user, such that even a corrupted non-blocking verifier is only problematic." STYLE_REF="Question" ID="ID_150201660" MAX_WIDTH="522.82016 pt" MIN_WIDTH="522.82016 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="verifier: consider how much reduced risk there is in a corrupt verifier? The verifier has less reach, less influence. There need not be an internet connection. [FIXME ...]" STYLE_REF="Question" ID="ID_296175287" MAX_WIDTH="522.82016 pt" MIN_WIDTH="522.82016 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="what threat-level for need for PINs?" STYLE_REF="Question" ID="ID_1956003430" TEXT_ALIGN="RIGHT"/>
<node TEXT="Is there value in encrypting (without authentication) the wallet whenever a passphrase is used, such that only right passphrase coalesces walleTKey-identity and sealed-wallet-data?" STYLE_REF="Question" ID="ID_1237026319" MAX_WIDTH="454.17391 pt" MIN_WIDTH="454.17391 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="How can we determine type of wallet for just any imported seed? Do we need to ask? Do we always also offer access to raw private-public keypair of wallet seed?" STYLE_REF="Question" ID="ID_1442186880" MAX_WIDTH="403.17391 pt" MIN_WIDTH="403.17391 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Maybe first restrict to working with seeds initially generated by walleTKey? (or freshly generated by Bitcoin Core or smth?)" STYLE_REF="Question" ID="ID_583517027" MAX_WIDTH="403.17391 pt" MIN_WIDTH="403.17391 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="How robust can a firmware-version be for future developments in wallet/addressing/transaction format extensions? E.g. to what extent will transactions become unsignable because the format changes or unknown sections are introduced." STYLE_REF="Question" ID="ID_1229869397" MAX_WIDTH="435.26087 pt" MIN_WIDTH="435.26087 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Would it be possible to use GNU Guix to compose a minimal, trustworthy base-image, then offer container-image for accessible, reproducible builds?" STYLE_REF="Question" ID="ID_1535113131" MAX_WIDTH="384.13043 pt" MIN_WIDTH="384.13043 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Support indiscriminate reset, e.g. with special message that is interpreted before decrypting?" STYLE_REF="Question" ID="ID_961870520" MAX_WIDTH="373.17391 pt" MIN_WIDTH="373.17391 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Use Guix to deterministically create a reliable, trustworthy environment (container-image) for build-environment?" STYLE_REF="Question" ID="ID_1164806133" MAX_WIDTH="300.65217 pt" MIN_WIDTH="300.65217 pt" TEXT_ALIGN="RIGHT"/>
<node STYLE_REF="Question" ID="ID_1764748919" MAX_WIDTH="379.30434 pt" MIN_WIDTH="379.30434 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <span style="font-weight: bold;">L2???</span>&#xa0;solution: migration-protocol for confidentally migrating between two walleTKeys (without exposing raw secrets)
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="where to place &quot;L4,security a stolen device and sealed-wallet&quot;? (two independent issues simultaneously)" STYLE_REF="Question" ID="ID_1521289256" MAX_WIDTH="404.08695 pt" MIN_WIDTH="404.08695 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="There may be some critical issues that emerge from certain combinations of risks. Need to explore. Explore (semi-)exhaustively?" STYLE_REF="Question" ID="ID_1520727793" MAX_WIDTH="410.21739 pt" MIN_WIDTH="410.21739 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Consider whether we need `walletsigner` executable to communicate over IPC/unix-socket to avoid giving Bitcoin Core, as parent process, access to `walletsigner` process memory." STYLE_REF="Question" ID="ID_1074601567" MAX_WIDTH="375.7826 pt" MIN_WIDTH="375.7826 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Is R-value grinding still a thing/concern that I need to take into account?" STYLE_REF="Question" ID="ID_1919431591" LINK="https://github.com/bitcoin/bitcoin/issues/26030" MAX_WIDTH="384.3913 pt" MIN_WIDTH="384.3913 pt"/>
<node TEXT="We (probably) should derive a dynamic value from CDI when computing the rootkey." STYLE_REF="Question" ID="ID_1539992247" MAX_WIDTH="384.3913 pt" MIN_WIDTH="384.3913 pt" TEXT_SHORTENED="true"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Right now, we derive purpose-secrets using static information. However, this also means that for a pre-image attack we're providing a static target. We already have the intermediate `rootkey` thus protecting CDI from being the target of pre-image attack, but we might want to include a (derived) unknown variable value from CDI such that the input isn't a static value, therefore making pre-image attack harder(?)
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Will differential cryptanalysis become an issue, given that firmware is open?" STYLE_REF="Question" ID="ID_1682313227" MAX_WIDTH="384.3913 pt" MIN_WIDTH="384.3913 pt" TEXT_SHORTENED="true"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      The &quot;Session on whitebox Cryptography&quot;, IACR, 2022-09-22 showed mathematical tricks and introduction of faults to extract information on the private key (for ECDSA, signatures) so we might want to check or unpredictably/unreversibly modify user input before performing cryptographic operations to prevent manipulation, if feasible.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="Quantum-computing resistance" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1579963412" TEXT_ALIGN="RIGHT" MAX_WIDTH="220.43478 pt" MIN_WIDTH="220.43478 pt">
<node TEXT="What is included in &quot;risk of exposing critical wallet-information?&quot; (e.g. wallet seed, main xpub?, etc.)" STYLE_REF="Question" ID="ID_1831463562" MAX_WIDTH="342.26087 pt" MIN_WIDTH="342.26087 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Bitcoin" LOCALIZED_STYLE_REF="styles.subtopic" ID="ID_819136893" MAX_WIDTH="44.73913 pt" MIN_WIDTH="44.73913 pt" TEXT_ALIGN="RIGHT">
<node ID="ID_157051456" MAX_WIDTH="333.91304 pt" MIN_WIDTH="333.91304 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      (Obsolete) P2PK payments make the public key available, therefore at risk of breaking. (Current) P2PKH only publishes the public key's hash. The public key becomes known only when a payment is executed and the funds leave the (one-time) address.
    </p>
    <p>
      <span style="font-weight: bold;">Script</span>: <span style="font-style: italic;">scriptPubKey `&lt;pubKey&gt; OP_CHECKSIG`, scriptSig `&lt;sig&gt;`</span>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Can we use something like IPFS to make binary-exact binaries available unconditionally, in perpetuity?" STYLE_REF="Question" ID="ID_1541713213" TEXT_ALIGN="RIGHT">
<node TEXT=" Some publicly accessible, p2p or publicly guaranteed, maintained, supported content-addressable storage (CAS) would be ideal, if it would guarantee that the firmware-binaries couldn&apos;t just disappear and the checksum always gives you that exact binary. That way, you can always acquire the firmware binary if you lose it yourself." STYLE_REF="Details" ID="ID_1869394989" MAX_WIDTH="329.34782 pt" MIN_WIDTH="329.34782 pt" TEXT_ALIGN="RIGHT"/>
</node>
</node>
<node TEXT="extensions" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_161888220" TEXT_ALIGN="RIGHT">
<edge COLOR="#00007c"/>
<node TEXT="Bitcoin Taproot support (or in L3?)" ID="ID_1420075057" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Schnorr signatures, witness, new transaction-types
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="rewrite of walleTKey firmware in rust" ID="ID_1893898118" TEXT_ALIGN="RIGHT"/>
<node TEXT="rewrite TKey device-firmware in rust" ID="ID_784355973" TEXT_ALIGN="RIGHT"/>
<node TEXT="implementation of commandline clients in rust" ID="ID_1111889976" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      (Note: requires tkeyclient-lib in rust)
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="advanced (non-minimalistic) verifier" ID="ID_618919176" TEXT_ALIGN="RIGHT"/>
<node TEXT="support multiple transaction-types" ID="ID_1977540687" TEXT_ALIGN="RIGHT"/>
<node TEXT="elaborate wallet management" ID="ID_615090207" TEXT_ALIGN="RIGHT"/>
<node TEXT="further firmware hardening" ID="ID_1423903627" TEXT_ALIGN="RIGHT"/>
<node TEXT="investigate further minimalization&#xa;(while retaining security properties)" ID="ID_1903858987" TEXT_ALIGN="RIGHT"/>
<node TEXT="support for other crypto-currencies" ID="ID_561101618" TEXT_ALIGN="RIGHT"/>
<node TEXT="improved protection against quantum-computing" ID="ID_579615435" TEXT_SHORTENED="true" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      design, crypto-primitives, PQC
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="clients (other platforms)" ID="ID_839867702" TEXT_ALIGN="RIGHT"/>
<node TEXT="verifiers (other platforms)" ID="ID_714421848" TEXT_ALIGN="RIGHT"/>
<node TEXT="Android-wallet (tricky, given risks, no verifier?)" FOLDED="true" ID="ID_354356240" TEXT_ALIGN="RIGHT">
<node TEXT="how/what to do verifier?" STYLE_REF="Question" ID="ID_192490649"/>
</node>
<node TEXT="audit of walleTKey" ID="ID_4634175" TEXT_ALIGN="RIGHT"/>
<node TEXT="include select TKey device vulnerabilities" ID="ID_1170641706" MAX_WIDTH="216.68182 pt" MIN_WIDTH="216.68182 pt" TEXT_SHORTENED="true" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Spend time to improve TKey firmware/FPGA recipe if/where possible. (no experience with FPGA)
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="provide ability to (conveniently) backup firmware-binary securely in verifier" ID="ID_1303665301" MAX_WIDTH="209.21739 pt" MIN_WIDTH="209.21739 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="BIP-352: Silent Payments (not finalized yet)" ID="ID_1125713" TEXT_ALIGN="RIGHT"/>
<node TEXT="elegant, pretty, fancy Android app" ID="ID_1442318665" TEXT_ALIGN="RIGHT"/>
<node TEXT="embedded QR-scanning (library)" ID="ID_1234762142" TEXT_ALIGN="RIGHT"/>
<node TEXT="Bitcoin multi-sig support" ID="ID_915580586" MAX_WIDTH="227.86956 pt" MIN_WIDTH="227.86956 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      I.e. properly implemented/tested/supported, not just incidentally supporting some of the underlying mechanisms.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="(Extensive) Graphical (UI) wallet-client" ID="ID_1352288479" MAX_WIDTH="269.73913 pt" MIN_WIDTH="269.73913 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      For administrative purposes rather than replacement for Bitcoin Core or another wallet-software.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="&quot;Simplified wallet&quot;-design with relying on TKey-device" ID="ID_1481492860" TEXT_ALIGN="RIGHT" MAX_WIDTH="361.90909 pt" MIN_WIDTH="361.90909 pt"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Given the same idea, it is possible to derive a &quot;simplified wallet&quot; concept that builds on different assumptions, among which &quot;ownership of the device is secure&quot;, such that it can directly derive the wallet from the CDI.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="references" LOCALIZED_STYLE_REF="AutomaticLayout.level,1" FOLDED="true" POSITION="top_or_left" ID="ID_887631894" TEXT_ALIGN="RIGHT">
<edge COLOR="#00007c"/>
<node TEXT="cryptography" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1280060273">
<node TEXT="PQC Migration Handbook (TNO, CWI, AIVD)" ID="ID_684798339" LINK="https://www.tno.nl/en/newsroom/2023/04-0/pqc-migration-handbook/" TEXT_ALIGN="RIGHT"/>
<node TEXT="J.P. Aumasson, Taurus SA, Switzerland:&#xa;Too Much Crypto" LOCALIZED_STYLE_REF="default" ID="ID_1046582905" LINK="https://eprint.iacr.org/2019/1492.pdf" TEXT_ALIGN="RIGHT"/>
<node TEXT="WalleTKey project concerns whitebox (+grey, +black) cryptography, limited to reading." ID="ID_29604865" MAX_WIDTH="275.73913 pt" MIN_WIDTH="275.73913 pt" TEXT_SHORTENED="true" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Binary-dependent secret negates the ability to modify software.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node TEXT="Bitcoin mechanics" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1677803205">
<node TEXT="Bitcoin-wiki: Protocol specification" ID="ID_1422236416" LINK="https://en.bitcoin.it/wiki/Protocol_specification"/>
<node TEXT="Bitcoin-wiki: Script" ID="ID_1608552502" LINK="https://en.bitcoin.it/wiki/Script" TEXT_ALIGN="RIGHT" MAX_WIDTH="120.78261 pt" MIN_WIDTH="120.78261 pt"/>
<node TEXT="Bitcoin-wiki: OP_CHECKSIG (script)" ID="ID_350204718" LINK="https://en.bitcoin.it/wiki/OP_CHECKSIG" TEXT_ALIGN="RIGHT" MAX_WIDTH="196.69565 pt" MIN_WIDTH="196.69565 pt"/>
<node TEXT="Bitcoin docs: Partially Signed Bitcoin Transaction (PSBT)" ID="ID_571023135" LINK="https://github.com/bitcoin/bitcoin/blob/master/doc/psbt.md" TEXT_ALIGN="RIGHT" MAX_WIDTH="308.47826 pt" MIN_WIDTH="308.47826 pt"/>
<node TEXT="Bitcoin docs: external signer" ID="ID_1506008307" LINK="https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md" TEXT_ALIGN="RIGHT"/>
<node TEXT="HWI" ID="ID_777403112" LINK="https://github.com/bitcoin-core/HWI" TEXT_ALIGN="RIGHT"/>
<node TEXT="BIP-11 [Final]: M-of-N Standard Transactions" ID="ID_1863394260" LINK="https://bips.dev/11/" TEXT_ALIGN="RIGHT" MAX_WIDTH="251.08695 pt" MIN_WIDTH="251.08695 pt"/>
<node TEXT="BIP 16 [Final]: Pay to Script Hash" ID="ID_1896537783" LINK="https://bips.dev/16/" TEXT_ALIGN="RIGHT" MAX_WIDTH="186.52174 pt" MIN_WIDTH="186.52174 pt"/>
<node TEXT="BIP-32 [Final]: Hierarchical Deterministic Wallets" ID="ID_438288969" LINK="https://bips.dev/32/" TEXT_SHORTENED="true" MAX_WIDTH="268.69565 pt" MIN_WIDTH="268.69565 pt" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Hierarchical, derived (hardened) wallet addresses from root wallet seed.
    </p>
  </body>
</html></richcontent>
</node>
<node TEXT="BIP-39 [Final]: Mnemonic code for generating deterministic keys" ID="ID_1795270425" LINK="https://bips.dev/39/" MAX_WIDTH="353.86956 pt" MIN_WIDTH="353.86956 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="BIP-174 [Final]: Partially Signed Bitcoin Transaction-format" FOLDED="true" ID="ID_1912793331" LINK="https://bips.dev/174/" MAX_WIDTH="322.17391 pt" MIN_WIDTH="322.17391 pt" TEXT_SHORTENED="true" TEXT_ALIGN="RIGHT"><richcontent TYPE="DETAILS">
<html>
  <head>
    
  </head>
  <body>
    <p>
      Version 0, informally version 1. PSBT, standardized transaction handling
    </p>
  </body>
</html></richcontent>
<node TEXT="PSBT contains previous transactions, useful for verifying input funds" ID="ID_351735345" MAX_WIDTH="345.79711 pt" MIN_WIDTH="345.79711 pt"/>
<node TEXT="PSBT is created with the intention of facilitating hardware (signers) with a common mechanism that provides necessary additional information, transforms information in better suitable/consumable form, suited for situations of limited capacity." STYLE_REF="Details" ID="ID_1922678218" MAX_WIDTH="345.79711 pt" MIN_WIDTH="345.79711 pt"/>
<node TEXT="Need to index which addresses are under our management, then select the previous transactions for those and send both the (partial-)transaction-to-sign as well as all corresponding previous txs to walleTKey." STYLE_REF="Details" ID="ID_1622676781" MAX_WIDTH="345.79711 pt" MIN_WIDTH="345.79711 pt"/>
</node>
<node TEXT="BIP-340 [Final]: Schnorr-signatures for secp256k1" ID="ID_510452515" LINK="https://bips.dev/340/" MAX_WIDTH="278.73913 pt" MIN_WIDTH="278.73913 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="BIP-352 [Proposed]: Silent Payments" ID="ID_1553703919" LINK="https://bips.dev/352/" MAX_WIDTH="209.60869 pt" MIN_WIDTH="209.60869 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="SilentPayments.xyz" ID="ID_1243217330" LINK="https://silentpayments.xyz" MAX_WIDTH="120 pt" MIN_WIDTH="120 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="BIP-370 [Draft]: PSBT version 2" ID="ID_1336371572" LINK="https://bips.dev/370/" MAX_WIDTH="178.69565 pt" MIN_WIDTH="178.69565 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="RFC-4231: HMAC-SHA-{256,512}" ID="ID_553376861" LINK="https://datatracker.ietf.org/doc/html/rfc4231" TEXT_ALIGN="RIGHT"/>
<node TEXT="BitcoinSV: SIGHASH-flags" ID="ID_1884940327" LINK="https://wiki.bitcoinsv.io/index.php/SIGHASH_flags" TEXT_ALIGN="RIGHT"/>
<node TEXT="Wallet Import/Export Format (WIF)" ID="ID_1129420882" LINK="https://en.bitcoin.it/wiki/Wallet_import_format" TEXT_ALIGN="RIGHT"/>
<node TEXT="List of address version prefixes" ID="ID_102572320" LINK="https://en.bitcoin.it/wiki/List_of_address_prefixes" TEXT_ALIGN="RIGHT"/>
<node TEXT="SLIP-0032: Registered HD version bytes for BIP-0032" ID="ID_89096424" LINK="https://github.com/satoshilabs/slips/blob/master/slip-0132.md" MAX_WIDTH="290.60869 pt" MIN_WIDTH="290.60869 pt" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="development" LOCALIZED_STYLE_REF="styles.topic" ID="ID_415399464">
<node TEXT="Android keystore system" ID="ID_643953840" LINK="https://developer.android.com/privacy-and-security/keystore" TEXT_ALIGN="RIGHT"/>
<node TEXT="Base58Check-encoding" FOLDED="true" ID="ID_138937662" LINK="https://en.bitcoin.it/wiki/Base58Check_encoding" TEXT_ALIGN="RIGHT">
<node TEXT="1 (main) / 111 (testnet)" ID="ID_1452048101" TEXT_ALIGN="RIGHT"/>
<node TEXT="5 (main) / 196 (testnet)" ID="ID_714418230" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="Base64-coding" ID="ID_1433975016" LINK="https://datatracker.ietf.org/doc/html/rfc4648#section-4" MAX_WIDTH="96.52174 pt" MIN_WIDTH="96.52174 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="CompactSize-coding" ID="ID_1228123557" LINK="https://en.bitcoin.it/wiki/Protocol_documentation#Variable_length_integer" MAX_WIDTH="125.86956 pt" MIN_WIDTH="125.86956 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="Go-pkg: &lt;github.com/btcsuite/btcd/btcutil&gt;" ID="ID_1681960424" LINK="https://pkg.go.dev/github.com/btcsuite/btcd/btcutil" TEXT_ALIGN="RIGHT"/>
<node TEXT="libbase58 &lt;github.com/luke-jr/libbase58&gt;" ID="ID_725723551" LINK="https://github.com/luke-jr/libbase58"/>
</node>
<node TEXT="licensing" LOCALIZED_STYLE_REF="styles.topic" ID="ID_444096620">
<node TEXT="Stack-overflow &quot;Is there a chart of which OSS License is compatible with which? [closed]&quot;" ID="ID_1909800623" LINK="https://stackoverflow.com/a/1978524" MAX_WIDTH="305.47826 pt" MIN_WIDTH="305.47826 pt" TEXT_ALIGN="RIGHT"/>
<node TEXT="The Free-Libre / Open Source Software (FLOSS) License Slide" ID="ID_1811962243" LINK="https://web.archive.org/web/20210101030518/https://dwheeler.com/essays/floss-license-slide.html" MAX_WIDTH="333.91304 pt" MIN_WIDTH="333.91304 pt" TEXT_ALIGN="RIGHT"/>
</node>
<node TEXT="(temp) tools" LOCALIZED_STYLE_REF="styles.topic" ID="ID_11244333">
<node TEXT="BIP32 Deterministic Key Generator" ID="ID_419993327" LINK="http://bip32.org/"/>
<node TEXT="XPUB converter" ID="ID_412809979" LINK="https://jlopp.github.io/xpub-converter/"/>
<node TEXT="Bitcoin extended public &amp; private key converter" ID="ID_989388663" LINK="https://3rditeration.github.io/btc-extended-key-converter/"/>
</node>
<node TEXT="TKey" LOCALIZED_STYLE_REF="styles.topic" ID="ID_1716818195">
<node TEXT="client-side resets" LOCALIZED_STYLE_REF="default" ID="ID_1911459607" LINK="https://github.com/tillitis/tillitis-key1/issues/199">
<node TEXT="API provided for use by TKey-programs. No forced reset capabilities on the USB interface. See ref." ID="ID_1298733075" LINK="https://github.com/tillitis/tillitis-key1/pull/242"/>
</node>
</node>
</node>
</node>
</map>
