# "Sealed wallet"-format

__threat-level__ (L1) wallet-at-rest (and higher)

`FIXME document to fail/abort in whatever way suitable, in case of corruption/suspicious activity in external components. (I.e. on L3 compromised system, operation may not be reliable, if so prevent operation under suspicious circumstances.)`

`FIXME design: there needs to be no format indicator. The sealed wallet is always opened by the exact same program. Any walleTKey can generate its own format. The sealed wallet is guarded by the identity as associated-data, so only the exact same walleTKey will open the sealed-wallet file. Content can be treated as a binary blob and constructed however is applicable for the code-base. (As opposed to secure-migration which would need some sort of convention on fields to be readable among different versions.)`

An on-disk wallet-format for storing a generated or imported _walleTKey_ wallet. The format is intended to be encrypted/decrypted within walleTKey only. The idea reflects the mechanism of a _cookie¹_. The user may store a "sealed wallet" in whichever way is appropriate, comfortable, extra secure or conveniently accessible. Given that the symmetric key is never exposed, the cookie is unusable by anything but the corresponding _walleTKey_.

¹: the _cookie_ here, represents the mechanism of storing server-side data on the client, intended for the client. Given that TKey does not offer persistence, this allows persistence of data without access, due to the encryption/decryption key being known within the walleTKey-firmware only.

## Terms

See terms definitions in the [main design document][walletkey-design].

## Requirements

`TODO consider explicit exclusion for sealed-wallet data-file size, given that we can accomplish only so much by arbitrarily padding a small binary file. The main goal is to have no identifying characteristics in the sealed-wallet content, such that there is no part that strictly identifies a sealed-wallet file as such.`

- (L1) Must contain wallet data as acquired, unchanged from the _walleTKey (firmware)_.
- (L1) Must not expose data of asymmetric cryptography (directly). (Threat-model: quantum-computing risks)
- (L2) Must be valid, sensible only for specific _walleTKey_.
- (L2) Must contain sufficient information to confirm _walleTKey_ identity.
- (L4) Must be resistant to passphrase brute-forcing attempts.  
  _Sealed wallet contains derivative value of the_ identity public-key. _This value must be hardened, such that the case of an unknown passphrase cannot be brute-forced by verifying the resulting identity._
- (L2) It _must not_ be easier to prove a file is a sealed wallet than it is to load a sealed wallet. (_deniability_, _distinguishability_)
- `FIXME ideally, must be all content that resembles uniformly random bytes without knowledge on how to interpret the data (_deniability_)`
- (L4) Must provide contextual information as _Associated Data_ to ensure decryption succeeds only within the intended context.
- (L4) Must include computationally-expensive value as part of _Associated Data_ to ensure hardness when brute-forcing decryption of sealed wallet.
- (L2) `FIXME or L3?` Must be necessary to perform computationally-expensive calculation on every use, without (unintentionally) offering a short-cut.
- (L4) _Must not_ provide hints as to identity (public-key) of walleTKey if TKey (device) is not in possession or walleTKey-firmware binary or passphrase is not known, or allow short-cuts for identifying the corresponding walleTKey instance. That is, no hints as to facilitate in brute-forcing attempts or recreating/mocking corresponding walleTKey.

`FIXME need to check/outline reasons why it is sufficient to produce a hardened value from only the walleTKey identity (public key). Using walleTKey identity as observed/established by client means that we confirm that the expected identity is used every time, and also that the (non-hardened) value need not be stored anywhere, and is trivially accessible only if corresponding walleTKey is available.`

`FIXME ... Given that identity is easier to steal than the device or UDS or CDI, for most situations, it is problematic to have an identity-hint as part of sealed-wallet. As soon as the identity is obtained, even if walleTKey no longer loaded/active, it could be used to prove sealed-wallet with no other components necessary. So it doesn't seem feasible to have a separate identity-hint. (As long as identity/authn guarantees of AKE and over-all are still maintained, sound.)`

Given the risks of quantum-computing, require specifically the ability to _confirm that an identity matches original _walleTKey_ identity_. Therefore, we need not necessarily store the identity public-key, which is a part of asymmetric cryptography and therefore vulnerable. Instead, a client may query the _walleTKey_ for its identity and wallet-data can be used to verify that identities match.

`FIXME rewrite below to proper requirements, because they're not part of proper design`

_TKey_ does not provide persistence, therefore wallet data is stored at a client, for use by _walleTKey_. This requires stored data to have the following security guarantees:

- __deniability__
  - Data consists of information in uniformly-random bytes. Therefore, without specific knowledge, it is impossible to interpret the data in a meaningful way.
- __confidentiality__ data contains secrets, therefore must not be readable outside of _walleTKey firmware_.
  - Do not expose the wallet secret, and in the future possible other sensitive information.
- __integrity__ (malicious) modification must be detected.
  - Ensure that only an unmodified wallet is handled, i.e. no silent errors or undetected processing failures, no manipulated input.
- __authenticity__:
  - Data must be authentic, i.e. originate from the (same) _walleTKey_. Also,
  - In follow-up sessions (after creation of wallet) with _walleTKey_, we must ensure same identity: this is the basis for the trust-relationship with _walleTKey_. This ensures that a malicious firmware cannot impersonate and/or otherwise acquire sensitive information.
- __indistinguishability__:
  - Sealed-wallet data is indistinguishable from random bytes, therefore only confirmed (accessible) within _walleTKey_, and is handled as an opaque blob during storage and handling.
  - Given (only) sealed-wallet data, corresponding walleTKey identity indistinguishable from any other identity, including (implicitly) any passphrase for same device, any other device, and any (other) arbitrary identity. (As long as walleTKey isn't loaded.)

`FIXME extra item, for which category? sealed wallet does not offer an oracle for off-line attacks, i.e. is this exactly indistinguishability?`

`FIXME have we expressed the targets that are covered by hardening the client context that is part of the AD? Frustrate (L1) brute-forcing UDS, CDI, internal symmetric key, or identity; (L4) brute-forcing passphrase discovery attacks, in case of owned TKey device`

`FIXME symmetric key as well?, strictly speaking it protects the authentication header only, IIUC`

Furthermore, given vulnerability of asymmetric cryptography to __quantum-computing__:

- _Must not_ store identity (public key) directly. Exposing walleTKey's identity directly, opens up possibility to recreating key-pair. Consequently, it would become possible to mock the original walleTKey and perform man-in-the-middle (MitM) attacks.

## Design

Assumptions:

- Device is trustworthy at time of sealed wallet creation.
- Sealed wallet is created with trustworthy firmware-binary.
- Any form of corruption of device or firmware-binary is detected: by virtue of TKey functionality, would result in different CDI.

Following solutions `FIXME make elegant sentence ...`:

- _symmetric encryption/decryption with AEAD_ for wallet content.
- with _identity_ as _associated data_ to AEAD.
- `FIXME use of KDF to harden against brute-forcing, but requires providing computed value when loading wallet.`

``FIXME need to document use of `KDF(identity)` as hardened Associated-Data to frustrate brute-forcing, and also to make it dependent on known, significant context, and also to not have it persisted. (Cannot persist, because if it becomes part of the sealed-wallet-file, brute-forcing can be done off-line with only the sealed-wallet-file, as opposed to with proper device. If hardened value is from device identity, one has trivial access to the AD *only if* the right device is accessible. Hence, no way to derive/determine the identity without the device.) KDF-computation performed on client, passed to walleTKey for use, s.t. it can be computationally-hard by workstation-hardware standards as opposed to embedded-hardware standards.``

`FIXME need to establish, decide on Argon2id parameters for desktop clients to use; maybe offer configurable values.`

Note: the symmetric key used to encrypted the "sealed wallet" is not and never will be exposed to the user. This is by design. Other measures are taken to help reduce risk of losing access to the wallet secret.

__Associated data__:

`TODO need to check how to refer to the 'identity' (encoded public key) in a consistent way.`

- Firmware: internally known `identity`
  _Pin the sealed-wallet to the specific identity as known in the firmware._
- Client: hardened context (`KDF(identity)`)  
  _Pin the sealed-wallet to a hardened identity as known and computed by (desktop) client._ `FIXME focuses on (L1) as identity can be discovered when device is queried. Use-case is opportunistic data exfiltration without having had access to corresponding walleTKey.`
  1.  Computationally expensive (hardened) value, i.e. hardened against attacks using a general computing platform as opposed to an embedded system.
  1.  Includes context from client's perspective, to ensure expected context for established session.
  1.  No need to persist additional data.
  1.  Provides no shortcut to `identity` value (without walleTKey present).
  1.  Provides no shortcut to hardened value.
  1.  Requires presence of walleTKey and sealed wallet to confirm context, i.e. offers no oracle for off-line attacks.

Note: hardening focuses on (L1) stored sealed-wallet case for symmetric key and/or identity, as _identity_ must not be known already; and (L4) use-case where _passphrase_ is not known and therefore _identity_ unknown, so brute-forcing passphrase requires recomputing a hardened value for each attempt. (`TODO described somewhere else, in different, in another section?`)

### Associated data

`FIXME this is now duplicate`

The "sealed wallet" is encrypted with associated data to validate the context along with decrypting the wallet. Both firmware and client contribute a part of the associated data. Consequently, we can perform some validation of both contexts as perceived.

__Firmware__

(True) identity, as known by the firmware, incorporated as part of _associated data_ to pin "sealed wallet" to walleTKey instance.

__Client__

Identity, as known by client, hardened to introduce a computationally-expensive component and passed into firmware with the generate-/import-/load-request. The identity, as known by client, ensures that the "sealed wallet" is only loaded when the client preceives that it communicates with the authentic walleTKey, as opposed to some mocked device/connection.

`FIXME [L4] consider using KDF (argon2 or something) to prevent efficient brute-forcing passphrases given known device/CDI and firmware-binary. Note that this won't have the planned effect, because the opaque sealed-wallet blob is not "hardened", so any identity can just be tested with the sealed-wallet blob (auth-enc) instead of the kdf-hardened-identity "prefix".`

### Risks

Known risks in use of the "sealed wallet" format.

- Wallet created with corrupted client may seal wallet to hardened value based on wrong identity. (That is, assuming a successful MitM is performed during AKE.) This cannot be easily prevented. The _TKey_ cannot perform the same hardened computation due to severely limited capacity and capabilities. However, the identity itself can be checked (repeatedly) on any number of computers, as this does not require loading the "sealed wallet" or establishing a secure session.

### Previous considerations

A short outline and summary of previous considerations. These were previous ideas that cannot work without sacrificing some desired properties. Useful to provide insight or to (re)check at a later time in case changes need to be made or circumstances change.

- Cannot append `identity` public key to sealed wallet to allow identifying right walleTKey early, because that would expose public key directly, i.e. vulnerable to quantum cryptography and contributes towards identifying the correct walleTKey or identity key-pair. (indistinguishability)
- Cannot append `HASH(identity)` (unhardened) to sealed wallet to identify right walleTKey early, because it would contribute towards identifying the correct walleTKey or identity key-pair (off-line). (indistinguishability)
- Cannot append `KDF(identity)` to sealed wallet, although it would help to identify the correct identity early, it would also provide a shortcut to the hardened value to send to walleTKey firmware.
- Use of `HASH(KDF(identity))` to identify wrong/bad/corrupt walleTKey early, without providing shortcut to the hardened value? This would allow us to decide early whether or not to trust the walleTKey while not exposing the hardened value itself, as long as the cryptographic assumption of the hash-function isn't broken. However, this would also enable an attacker to distinguish/identify key-pairs without actual hardware being present.
- In my current line of reasoning, storing any value or derivative of the identity will result in it being a weakness or at least a target, possibly function as an oracle. Current approach assumes storing any such a value is a no-go. Therefore, must take data available at time of use or derivative thereof.

## Format

`FIXME correct descriptions/spec below, once established what to do with Blake2s, KDF, identity-hint, deniability`

The format for a wallet at-rest such that it contains sufficient information to authenticate the corresponding _walleTKey_ and provide the data for processing.

The sealed wallet as generated/provided by firmware:

```
sealed-wallet := NONCE || MAC || AEAD_ENCRYPT(key, MAC, NONCE, wallet-secret, AD=blake2s(identity, KDF(identity)))
```

- where `KDF(identity)` being provided by the client as part of sealed wallet generate/import/loading request. The _associated data_ contained context of which _walleTKey_ provides its own identity and the client provides the computationally-expensive derivation.

with:

- `identity` being the (internally derived) _walleTKey_ identity's public-key.
- `digest := HASH(x)` being a hash-function producing a digest over `x`, with
  - `blake2s` as hash-function, as provided by _TKey firmware_.
- `ciphertext := AEAD_ENC(key, mac, nonce, plaintext, additional_data)`, with proof-of-concept choice preliminarily defaulting to monocypher: `chacha20`+`poly1305` (`FIXME not very exactly described`)
- `KDF(identity)` is _Argon2id_ with parameters: `FIXME prescribe parameters`
- Note: `key` is only known within _walleTKey_ and is different for each.
- Note: `AD=identity` ensures that _walleTKey_ can establish context using its own (internally generated) identity.

`FIXME should we provide a wallet-specific salt during hardening, apart from the identity? Otherwise, hardened value is same for all sealed-wallets created under same walleTKey.`

`TODO check with monocypher for exact parameters, construction, sizes; such as chacha20 IETF flavor having different distribution of nonce-bytes vs CTR-bytes (IIRC)`

## Open issues

Current open issues, need follow up at a later moment.

- Verifpal (it seems) cannot express the idea of a "cookie" for storing `device`-side data locally on `client`, such that client passes it back whenever `device` is needed. This and other questions have been asked. Issue is that Verifpal will not accept data it already knows according to the model, and Verifpal cannot express a notion of "forgetting" data, or changing phases such that some data is forgotten.

`TODO for PoC went with predefined composition, parameters of monocypher, as opposed to carefully selecting every parameter from top-to-bottom myself (focus on proving the concept first).`

## Notes

- There is considerable freedom is changing the contents of the (opaque) `sealed-wallet` blob, given that _only_ the same firmware is expected to read it. For purpose of upgrades, one must always perform a migration-procedure.

`TODO not yet documented with specific contents, types, byte-lengths, etc. Is not that critical given PoC and fact that most are byte-values or opaque, pass-through blobs anyways.`


[walletkey-design]: <walletkey.md> "Main design document. (walletkey.md)"
