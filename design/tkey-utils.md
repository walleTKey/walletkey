# TKey utils

`tkey.c` and `tkey.h` provide a number of utilities that build upon the `tkey-libs` dev-kit provided by tillitis. Some considerations and design choices are described here.

## Randomness

`FIXME consider having 2 * 32 bytes buffer such that one half can feed into the other half for more randomness/less risk of running into collision`

``FIXME update description to reflect use of previous `key` with buffer to produce new unpredictability such that we can continue without waiting for TRNG to be ready. (relies on `key` for unpredictability, and `buffer` for randomness, produces new `key` value, then continues iterating with `key` that is unpredictable even if TRNG isn't ready yet.)``

- static buffer such that we have a previous state to continue from, i.e. a changing arbitrary starting position.
- if `seed` is provided:
  - hash `seed` in with current buffer.
loop, in batches of 32 bytes:
  - use 4 bytes of randomness as key and hash current buffer
  - copy buffer to output at current index (index is multiple of 32, sized either 32 bytes or remaining output capacity)
- rehash buffer to purge bytes already used in output.

## Touch

- do not use uniformly timed/spaced blinking:
  - too similar to (uniformly blinking) error state.
  - too similar to short blinks that may be used to communicate acknowledgement or state change to the user.
- Two rapid LED blinks with longer pause:
  - grabs attention,
  - easier to distinguish as requiring action.

