# walleTKey

__NOTE__ design is still in progress.

`TODO given the same idea, it is possible to derive a "simplified wallet" concept that builds on different assumptions, among which "ownership of the device is secure", such that it can directly derive the wallet from the CDI. This idea is simpler but positions the device as a single-point-of-failure. This concept is less attractive as long-term solution, but *is* attractive as short-term solution in higher-risk environments because the device is able to protect better from untrustworthy environments/systems. (See below for notes.)`

`TODO mindmap may contain updates not yet properly documented in the design`

An open-source wallet design based on tillitis TKey (open-source hardware) security key.

The _walleTKey_ is a (proof-of-concept) Bitcoin wallet built on the TKey hardware platform. It is an experiment. An attempt to be an improvement over software-only wallets with an alternative selection of security guarantees compared to well-known hardware wallets. TKey as hardware platform provides unique benefits compared to other security keys and harware wallets, but has limitations that must be complemented.

`FIXME rephrase, explain in motivation terms`

_walleTKey_ is a solution specifically designed to leverage the unique properties of _TKey_. Two of the leading problems it tries to solve with this proof-of-concept (with potential for more) is the reliance on an often closed-sourced vendor and the uncertainties with vendor-controlled or vendor-signed firmware.

## `TODO: Feasibility (temporary)`

Possibly a temporary section, discussing the feasibility of the project given the reliance on a novel hardware device TKey with different security properties and general applicability.

- Computational capacity: sufficient
  - ephemeral key exchange, secure session establishment
  - signing proof-of-identity
  - p256k1 can be processed
- TKey memory capacity:
  - program size + available memory seem to be sufficient
  - no further (sizeable) dependencies expected
- Explore and experiment with TKey capabilities:
  - explore the C programming language when restricted to static allocation (stack-only)
  - CDI for unique deterministic secret derivation
  - software and hardware security hardening, e.g. CPU execution monitor
- Limitations w.r.t. device features:
  - device capabilities are not ideal:
    - limited communication capabilities mean that a systematic, straightfoward approach is necessary.
    - lack of display means we cannot present critical information of a transaction directly to the user.
    - `FIXME ...`
  - .., but workable.
    - LED can be used to indicate when significant state transitions are made, or critical actions in flight.
    - solutions, depending on threat-level, range from unnecessary to facilitating a secondary device, such as a smartphone, to function as _verifier_.
    - `FIXME ...`

__research__:

- simple, asymmetric AKE, light-weight and suitable for use with TKey projects
- a different approach to trusted foundation
- (engineering) experiment in C (firmware) program with restrictions to control/contain risks:
  - static allocation only
  - software and hardware security mitigations
  - static analysis
- `FIXME ...`

## Terms

This section describes a number of terms that are used in the design documents. Some terms are specific to _walleTKey_ or the _TKey_ device, others are relatively unknown or misunderstood.

__walleTKey__ this project, the idea of a Bitcoin wallet built on top of the TKey device and other systems. Furthermore, when a specific _walleTKey_ is referenced, it refers to a _TKey device_ with _firmware-binary_ loaded; a specific triple of _TKey device_ + _firmware-binary_ + _USS_, as each triple gets assigned its own unique secret (_CDI_).

__wallet__ is the stored data in the _sealed wallet_ format as created by a _walleTKey_. It refers to the wallet-secret in its sealed (encrypted, protected) form for use with the corresponding _walleTKey_.

__TKey__ [tillitis TKey][tillitis-tkey], the (hardware) device used as a platform for _walleTKey_.

__UDS__ the TKey carries a "device secret", a secret value injected into the device during construction. It is read once upon initialization then cleared, such that an application-specific secret can be derived, but the device secret is never accessible.

__USS__ the user-specified secret that may be accompanied with the application being loaded onto the _TKey_. The _USS_ is a 32-byte value, usually a hash digest of a passphrase. The _USS_ contributes to the unique assigned secret, see _CDI_.

__CDI__ an internal secret provided to a TKey application, such as the _walleTKey_ _firmware_. A deterministically generated, unique secret is provided for a triple (_device_, _application-binary_, _USS_), derived from an internal device secret not exposed to anything but the _TKey firmware_ once at start-up.

__firmware__ this term will always refer to the _walleTKey_ application loaded onto the TKey device, as opposed to the walleTKey (client) applications (for desktops). The only exception is when _TKey firmware_ is explicitly mentioned as such, to refer to the firmware embedded into the TKey device itself. Although this might seem confusing at first, using the term _firmware_ allows it to correspond to the notion of firmware as known for hardware wallets and most other devices, while the _TKey firmware_ itself is fixed/frozen (unchangeable) and exists only for bootstrapping the device and accepting the application to be loaded.

__sealed wallet__ a storage format for storing the wallet that is loaded into the _walleTKey firmware_. Given that _TKey_ does not offer persistence, the data is passed back to the client for storage (a.k.a. _cookie_), in a format only readable by _walleTKey firmware_.

__cookie__ the technical mechanism of storing server-side information of a specific client at that client, requiring it to be passed back as needed for providing services.

__"[external signer][Bitcoin-external-signer]"__ the term used by Bitcoin Core to refer to an external application that provides the bridge/interface to hardware-signers or other signer-software. The _walleTKey_-client aims to implement the "external signer" (command-line) API.

__HWI__ the "[Hardware Wallet Interface][HWI-repository]" that provides a glue-layer facilitating the interaction between Bitcoin Core "external signer" support and hardware-wallets with a python-based interface. _walleTKey_ does not primarily aim to support this interface.

## High-level overview

- Exploring a largely unknown hardware platform, sporting a novel selection of (security) features. Only few applications available for TKey.
- Hardware-platform offers open-source approach/design.
- "Incorruptible" foundation of trust.
  - _challenge_: solid, correct implementation of _walleTKey firmware_.
- Hardware-platform offers a limited feature-set.
  - _challenge_: extend from a solid but limited hardware-platform.
- Advancing/demonstrating security capabilities of open hardware platform.
- An alternative approach to existing hardware wallets (special-purpose).
- An improvement over (open-source) software wallets.
- Trust-on-first-use (TOFU) at wallet creation with possibility to verify.

`TODO consider if "deniability" is a valid claim for this solution, i.e. the TKey is generally applicable, given securely stored wallet, nobody can claim that owning a TKey means you own a Bitcoin (or any other cryptocurrencies) wallet.`

### Features

`FIXME benefits over hardware wallets: due to hardware being generic security device, freedom to start using at any moment, fully in user's control, hardware shipped to home address is not necessarily used for bitcoin/crypto-currency.`

`TODO clean up the features list to only high-level stuff. Let the rest move to design specifics or implementation specifics.`

`TODO add subselection of (most important) security features`

`TODO describe features`

- generating a new wallet seed
- importing existing wallet seed
- a sealed wallet format for storage and repeat use
- export the (raw) wallet seed from a sealed wallet
- providing receipt addresses (public keys) for receiving bitcoin
- `FIXME in progress` verifying/signing a raw bitcoin transaction for sending coins
- `FIXME no risk of corruption of hardware platform, firmware`

### Security properties

> ☐ to-do, ⋯ in-progress, ⚡(external) issues, ☑ done, ☒ cancelled (✔✘)

`TODO move this section below and discuss in more detail`

`FIXME indicate scope for various properties, e.g. deniability only for sealed-wallet data`

`FIXME document "strict-by-default" attitude to implementing`

- Separation (not quite isolation) of sensitive processing within walleTKey
- Anonymity of addresses `FIXME needs further elaboration, connect to full-node`
  - ✔ Bitcoin full-node will be downloading all recent blockchain data. walleTKey wallet works with (local) full-node.
  - ✘ address(es) known in (local) Bitcoin Core,
- Deniability `FIXME needs further elaboration`
  - ✔ TKey-device is general-purpose (security) device, therefore purpose is not implied. (ownership/acquiring/ordering)
  - ✔ sealed-wallet file looks like uniformly random bytes (lacking knowledge to interpret the data, unless the matching _walleTKey_ is loaded),
  - ✘ (local) presence of firmware-binary, wallet-client, Bitcoin Core software,
- Physical confirmation for sensitive operations
- Secure communication with walleTKey
- _walleTKey_ authentication, identity validation
- Protection from firmware "surprises", both uncontrolled upgrades and corruption
- Firmware: open-source, transparent, auditable

`TODO switch proof-system (symbolic proofs): Tamarin, Proverif? (Verifpal lacks reliability, features.)`

### Risks in other wallets

__hardware__:

- risk of cross-contamination when support for multiple crypto-wallets
- risk of increased attack surface with support for multiple crypto
- forced/unexpected firmware upgrades
- risk of backdoored/customized firmware being served
- signed firmware image by vendor:
  - no custom firmware, necessary to protect device against malicious firmware
  - control taken away from customers wanting to install custom firmware
  - (`FIXME probably`) risk of exposing sensitive internals
- vendor software dependent on vendor servers
- vendor software, not always fully in best interest of the user (e.g. privacy)
- lack of anonymity, given (primarily) connectivity with their servers/cloud

__software__:

- wallet and software on computer, so greater risk of data copying/exfiltration
- wallet and software on computer, so greater risk of manipulation of wallet-software
- `FIXME continue here ...`

### Benefits

- _TKey_ is open hardware.
- _TKey_ is essentially incorruptable:
  - every power-on is full initialization,
  - every loaded application starts from blank initial state,
  - there is no persistence,
  - it is not possible for loaded application to corrupt/influence other applications, i.e. no consequences to running buggy/malicious applications,
- As many _walleTKey_ as needed, based on (_device_, _firmware_, _USS_).
- No unattended/surprise firmware-upgrades. (Not possible without performing migration, due to each firmware-binary receiving unique secret data.)
- Once good, reliable _walleTKey_ firmware is available, everything should run very predictably.

### Weaknesses

- (Safe) use requires understanding that _wallet_, _firmware_, _USS_, _device_ all are important, not just the storage-file.
- Explicit migration process, requires manual assistance.
- _TKey_ hardware is limited, i.e. designed for simpler goals. _WalleTKey_ design incorporates mitigations to extend the bounds of these limitations. (For example, lack of a display means higher threat-levels need extra work/steps to secure.)
- Secrets are unique to each firmware, therefore bad or buggy firmware-binaries can cause real problems.
- Occasional manual migrations may be necessary to include developments in Bitcoin address/content/transaction formats and other such developments.

`FIXME preliminary selection of cryptographic primitives in part by availability in monocypher. Quantum-computing risks are recognized and partially mitigated. However, given attacker with sufficient privileges, (limited) risk is unavoidable. However, depending on which actions are intercepted, risk may be relatively small.`

`FIXME weakness: community-managed trust necessary --> reproducible builds & collect signatures for firmware-binaries, leave it to users to decide how many signatures they want to see before trusting/adopting a firmware. (relies on reproducible builds)`

## Design

The proposition to start with is:
- given a _TKey_ device to provide a secure computing environment,
- design and implement a (Bitcoin) wallet,
- knowing that _TKey_ has limitations:
  - _capacity_: take the approach of minimalism: perform only those actions on TKey that are necessary to ensure/preserve the security of your wallet,
  - _features_: mitigate and/or complement the missing features of _TKey_ with other solutions.

The [threat-model for _walleTKey_][threat-model] guides the design.

__components__:

- [Authenticated key exchange (AKE)][AKE]  
  _A light-weight, one-sided AKE suitable and sufficient for use with _TKey__.
- ["Sealed wallet" storage format][sealed-wallet]  
  _A storage-format that makes it possible to store a wallet with minimal risk._
- _walleTKey firmware_ (program) for TKey  
  _The firmware program to run on the _TKey__.
- client-applications for desktop  
  _The _walleTKey_ applications that interact with the firmware._
  - [`verifyfw`][verifyfw]  
    _A verification tool that performs the essential operations, ensuring that a wallet secret can be extracted if all else fails._
  - [`walletctl`][walletctl]  
    _A command-line client that interacts with the firmware._
- [Verifier][verifier] the smartphone-app to (independently) validate walleTKey output and generate authorization-/confirmation-codes.
- Secure Migration-protocol: `TODO t.b.d.`

__Note__ given (need for) minimalism of the firmware-binary, the full design is not implemented/enforced completely within the firmware. For example, the "sealed wallet" format, must be composed by the client. While firmware provides the necessary information, for example, combining the hashed identity with the sealed wallet is an action for the wallet client.

## Implementation

Now, let's discuss the noteworthy implementation details for _walleTKey_.

__tools__:

- `verifyfw` a verification tool that loads specified firmware-binary (file) and performs a set of basic checks to ensure a minimal functioning subset. This ensures that the wallet secret is retrievable in case bugs in firmware interfere with more advanced operations.
- `walletctl` the main wallet command-line front-end:
  - When `walletctl` is executed as `wallet-signer` (for example, aliased through symlink), execution redirects towards the `signer` subcommand (i.e. `walletctl signer`), for convenient interfacing with Bitcoin Core.

__code-base__:

- `main.c` firmware entry-point, control-flow
- `error.h` a definition of an error-type
- `tkey.{c,h}` additional utilities for TKey
- `session.{c,h}` secure-session establishment ([AKE])
- `p256k-m.{c,h}`, `schnorr.h` implementation of NIST SEC p256k1 (adapted from original which provided p256r1) and Schnorr-signatures  
  Note: the implementation currently contains extra functions needed for bitcoin logic `bitcoin.{c,h}` 
- `bitcoin.{c,h}` logic specific to Bitcoin
- `script.{c,h}` `TODO` initial work on Bitcoin transaction-script execution

__others__:

- `sha-256.{c,h}` SHA-256 hash function implementation
- `rmd160.{c,h}` RIPEMD-160 hash function implementation ([homepage](<https://homes.esat.kuleuven.be/~bosselae/ripemd160/ps/AB-9601/>))

### Signaling-mechanisms

`TODO move to section on TKey and available mechanisms/capabilities`

The only manner of communication is via the USB UART serial interface. However, other features of the _TKey_ provide ways of interacting on a limited level, such as LED and touch sensor.

The mechanisms for signaling and/or interacting with the user.

- CPU halt: uniform, slow blinking LED; alternating red-off; execution halts.
- abort: uniform, medium blinking LED; alternating red-off; execution halts.
- bug: uniform, fast blinking LED; alternating user-specified colors; execution halts.
- signal: uniform, faster blinking LED; alternating user-specified colors.
- fixed color: set LED to a fixed color.
- touch-request: (non-uniform, to catch attention) blinking led with two rapid blinks and longer delay; user-specified number of blinks, while waiting for touch-confirmation.

### Security-mitigations

`FIXME simple, asymmetric AKE: no use for session identifier: the established session is all there is. (There is an id in the frame header, but is currently not used.)`

__TKey__:

- CPU execution monitor to protect TKey firmware memory
- CPU execution monitor available for use by (loaded) application
- address randomization (i.e. addresses as observed from outside)
- address-dependent data scrambling
- restricted memory access for (sensitive) device-regions
- UDS cleared at TKey initialization

__Compiler__:

- stack-protector
- hardened build configuration
- strict with compiler warnings
- static analysis
- `FIXME continue here...`

__Practices__:

- compute security-sensitive values on-the-fly
- securely wipe sensitive memory locations after use
- `abort()` at incorrect use of core logic
- no (heap-based) dynamic memory allocation:
  - reduced chances of use-after-free (only stack-based pointer-mismanagement)
  - no double-free
  - no memory-leaks
  - known lifetimes for memory on stack, also pointers for memory regions
  - predictable allocation of memory

`TODO currently, allowing stack-allocation of VLA to accurately allocate memory for transaction processing with varying numbers of inputs and outputs.`

Note: can be abused, but you have to work harder to abuse this for stack memory

__Community__:

- reproducible builds
- signing released/blessed firmware-binaries

See <https://dev.tillitis.se/hw/> for details on hardware capabilities.

### LED signals

`TODO move to section on TKey and available mechanisms/capabilities`

Given the limited ability for TKey device to communicate with user, i.e. there is no display, some essential state changes are indicated by LED signals:

- Blue led: initialization.
- 2 Green flashes: AKE finishes; session-key established.
- Green led: wallet loaded into _walleTKey_ (generated wallet secret, imported wallet secret, or loaded wallet).

## `TODO: simplified wallet concept`

Rough notes on a simplified wallet concept with different assumptions/guarantees, less suitable for long-term use. Device becomes SPOF but with benefits of less variability and more hardware-enforced, therefore more trustworthy under insecure/untrustworthy circumstances (systems).

```
- Assumption: ownership of device is guaranteed.
- Assumption: system with client-software is insecure.
- Wallet-seed not extractable/exportable.
- Possibility: derive (hardened) hierarchical (child-)keys
- Single-point-of-failure: device broken/stolen => bad luck (not meant for long-term use, meant for short-term use in high-risk/-complexity/precarious circumstances)
- corrupted client should cause crashes but not loss/manipulation, i.e. important factors enforced by firmware/device.
- assumption: some information known in advance, s.t. use of device in untrustworthy/unreliable environment does not require trusting critical information for proper use. (For example, look up wallet addresses/identifiers beforehand.)
- assumption: consider how to apply PINs/touch-confirmation, given that you probably want to refrain from entering codes in untrustworthy environment. (So maybe use the touch confirmation-pattern to prove physical presence and "intelligent awareness" to avoid manipulating outcomes.)
- depends almost entirely on correct, sound, "unhackable" firmware/code-base
```


[tillitis-tkey]: <https://tillitis.se/products/tkey/> "tillitis TKey: a new type of flexible USB security token"
[AKE]: <AKE.md> "A simple, one-sided authenticated key exchange."
[threat-model]: <threatmodel.md> "Threat-model for walleTKey"
[sealed-wallet]: <sealed-wallet.md> "\"Sealed wallet\"-format"
[verifyfw]: <verifyfw.md> "`verifyfw`: the command-line tool to verify essentials of a walleTKey firmware-binary"
[walletctl]: <walletctl.md> "`walletctl`: the command-line tool for managing the walleTKey wallet"
[verifier]: <verifier.md> "Verifier: the Android verifier-app that validates information and generates authorization/confirmation codes"
[Bitcoin-external-signer]: <https://github.com/bitcoin/bitcoin/blob/master/doc/external-signer.md> "Doc: Support for signing transactions outside of Bitcoin Core"
[HWI-repository]: <https://github.com/bitcoin-core/HWI> "Bitcoin Hardware Wallet Interface"
