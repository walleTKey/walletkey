# Firmware

`FIXME write this ...`

The firmware is loaded into the TKey upon every use.

__Features__:

- Open-source firmware.
- Full control over firmware(-upgrades): a sealed wallet only opens on the firmware it was created on.
- A transfer is needed to migrate a sealed wallet to a new firmware version.
- No surprise firmware changes or addition of unwanted features.

## Design

Design-notes on various aspects of the firmware-implementation.

### Transaction-handling

- Select commands are allowed within the transaction-handling event-loop, such as `CMD_LOAD_WALLET`. This enables working with multiple wallets when handling (signing) transactions.

### Randomness

A utility-function `random(..)`.

- Maintains a static 32-byte buffer that is updated as randomness is taken out.
- Allow seeding without taking out randomness.
- Uses `blake2s` to produce random bytes.
- Takes an optional `seed` to be processed as `key` input (max 32 bytes) to `blake2s` for mixing in with existing buffer.
- Takes 32 bits (4 bytes) of entropy from TRNG (benefits unpredictability) to mix in with persistent buffer as randomness is taken out.
- Ciphertexts (after encrypting/before decrypting) are fed into `random(..)` for entropy-seeding.
- To benefit performance, the same 32 bits (4 bytes) of entropy may be reused in conjunction with the internal buffer, to remix and reuse this known unpredictability, to avoid waiting for TRNG.

### Framing-protocol (modifications)

The firmware uses a modified version of the [framing-protocol](<https://dev.tillitis.se/protocol/#framing-protocol>) proposed by Tillitis.

1.  Do not share pool of IDs
1.  Command-bit to indicate continuation-frame
1.  "Namespaced" follow-up frames
1.  Distinct uses for `Response-not-OK` header-bit

Resulting in general support for arbitrary-length payloads, generalized dismissal of stray continuation-frames, 128 IDs available for distinct commands, follow-up frames "namespaced" within the context of the request.

#### Distinct pools for request and response IDs

_walleTKey_ and the host-system are not peers. The requests for the _walleTKey_ do not make sense as requests for the host. Therefore, there is no point in sharing the available IDs between requests and responses. Instead, use full byte for available IDs and make recipient an identifying component, resulting in 256 IDs for requests (to _walleTKey_) and 256 IDs for responses (_host_).

#### Most-significant command-bit for continuation

Most-significant command-bit (`0x80` or `0b10000000`) of the first byte. If unset, indicates first frame, i.e. a frame issuing a new command. If set, indicates a follow-up as part of a previous command. These frames may carry data that starts at an arbitrary position, or may contain only partial/incomplete data. These frames should be dismissed if encountered independently, as the previous command has likely failed.

- Reduces the space to 128 distinct commands.
- Reduce/obviate need for other command definitions and follow-up commands.
- Allows handling large payloads, payloads can be of arbitrary length.
- Allows handling arbitrary-sized payloads in standardized manner, independent of request/command.
- Generalized mechanism for dropping disconnected follow-up frames in case errors prematurely ended a previous command execution.
- Allows handling of non-follow-up frames by marking them for "reprocessing" then leaving the specialized event-loop in order to drop back to the main/primary event-loop.  
  _This is (usually) the obvious solution anyways, because continuation-frames were interrupted/missing and the next command was sent._

#### "Namespaced" follow-up frames

Given that the MSB of the command indicates a "continuation", i.e. a follow-up for a previous request(-frame), this semantically "namespaces" any subsequent message to the initial frame carrying the request. Generic message handling is instructed to ignore these follow-up frames. This places any command-byte of a follow-up frame, within the context of the (original) request.

- Possibility to define another 128 distinct values (for each distinct initial request).
- Definition is "namespaced" within the context of the initial request-frame.
- Follow-up frames are ignored upon falling back/returning to the general control-flow.

Note that this is a side-effect of how general control-flow will skip/ignore frames that have the continuation-bit set.

#### Distinct uses for `Response-not-OK` header-bit

It is possible to express distinct classes of errors using only fields in the header-byte.

- `Response-not-OK`+`Command-0`+`Length-1`: error in header-byte: bad destination, forbidden bits set.
- `Response-not-OK`+`<command>`+`Length-1`: error in command: unknown command, bad command, incorrect state for issuing this command.
- `Response-not-OK`+`<command>`+`<length>`: error in command-payload.

`FIXME maintain practice of returning all-zero payload in case of error? (also documented in framing protocol)`

`FIXME no plans to encrypted error responses at-this-moment, given that they carry "no" (very little) information. This may prove to be a mistake, but then we cannot encrypt the frame-header, so we would need to invent an error mechanism.`

## Notes

Notes concerning design choices and/or implementation choices concerning firmware. Any of these may later be reconsidered.

- Given limited ability to communicate and mitigate errors and anomalies in input, be strict and rely on client to detect genuine errors/mistakes early. WalleTKey must check errors to guard against malicious input, but does not need to be the primary source of information for troubleshooting.
- Initial implementation constructs, rather than receives, the pubkeyscript for _P2PKH_ payment, because it is standardized, we know exactly how it is supposed to look for our public key, consequently if we construct a "bad" (non-matching) signature, we have been acting on bad input.
- Initial implementation for transaction hashing/signing imposes limits to simplify initial implementation. These are marked with `TODO`. These usually entail limited lengths and capacities, such as script-length of `< 0xfd`. (These are most likely temporary.)
- Firmware will support limited number of inputs/outputs. Currently, supports combined `150` inputs/outputs in arbitrary permutations.
- Firmware will not support "R-value grinding", i.e. brute-forcing signatures until a signature is produced with the R-value in the lower half, therefore saving space in the transaction. This is already (not) assumed by Bitcoin Core, but will also not be supported, due to limited computational capacity of TKey. (<https://github.com/bitcoin/bitcoin/issues/26030>)
